<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller {
    public function Index() {
        if (!Auth::check()) return redirect('/login');

        // switch (Auth::user()->role) {
        //     case 0: return redirect('/admin');
        //     case 1: return redirect('/admin');
        //     case 2: return redirect('/agent');
        //     default: return redirect('/account/signout');
        // }

        // switch (Auth::user()->role) {
        //     case 'admin': return redirect('/admin');
        //     case 'admin': return redirect('/admin');
        //     case 'staff': return redirect('/agent');
        //     default: return redirect('/account/signout');
        // }

        $user = Auth::user();

        if ($user->hasRole('admin')) {
            return redirect('/admin');
        }  elseif ($user->hasRole('superadmin')) {
            return redirect('/admin');
        } elseif ($user->hasRole('staff')) {
            return redirect('/agent');
        } else {
            return redirect('/logout');
        }
    }

}
