<?php

namespace App\Http\Controllers;

use App\Export\ReportExportBalanceSheetCustomer;
use App\Export\ReportExportBalanceSheetSupplier;
use App\Export\ReportExportBillingCompany;
use App\Export\ReportExportBookingOrder;
use App\Export\ReportExportCargoRecord;
// use PDF;
use App\Export\ReportExportContainerLoading;
use App\Export\ReportExportDailyPayment;
use App\Export\ReportExportDailyPaymentTransfer;
use App\Export\ReportExportMonthlyStatement;
use App\Export\ReportExportStaffCommission;
use App\Export\ReportExportYearlyStatement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Image;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Hash;


class AdminController extends Controller
{
    private $limits = 15;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        switch (Auth::user()->role) {
            // case 0: return redirect('/admin');
            // case 1: return redirect('/admin');
            case 2: return redirect('/agent');
                // default: return redirect('/account/signout');
        }

        return view('dashboard/admin');
    }

    // Setting
    public function Setting()
    {
        $dto = \App\Lib\Queries\Admin\GetSettings::Result();
        $vm = new \App\Http\ViewModels\Admin\SettingViewModel($dto);

        return view('setting/index', ['vm' => $vm]);
    }

    public function SettingPost(Request $request)
    {
        $data = $request->all();
        foreach ($data['setting'] as $setting) {
            $dto = \App\Lib\Queries\Admin\GetSetting::Key($setting['setting_key']);
            if (!empty($dto)) {
                dispatch(new \App\Lib\Commands\Admin\UpdateSetting($setting));
            } else {
                dispatch(new \App\Lib\Commands\Admin\CreateSetting($setting));
            }
        }

        return $this->JsonOk();
    }

    // User
    public function Users(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'name';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
        ];

        $dto = \App\Lib\Queries\Admin\GetUsers::Result($params);
        $paging = \App\Lib\Queries\Admin\GetUsers::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\UsersViewModel($dto, $paging);

        return view('user/list', ['vm' => $vm]);
    }

    public function AddUser()
    {
        $vm = new \App\Http\ViewModels\Admin\UserViewModel();

        return view('user/add', ['vm' => $vm]);
    }

    public function AddUserPost(\App\Http\Requests\Admin\UserRequest $request)
    {
        $data = $request->validated();
         // dispatch(new \App\Lib\Commands\Admin\CreateUser($data));

        $user = \App\User::create([
            'username' => $data['username'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'status' => $data['status'],
            'role' => $data['role'], // 可选
        ]);

        // 根据角色 ID 赋予角色
        if ($data['role'] == 1) {
            $user->assignRole('admin'); 
        } elseif ($data['role'] == 0) {
            $user->assignRole('superadmin'); 
        }

        return $this->JsonOk();
    }

    public function EditUser($id)
    {
        $dto = \App\Lib\Queries\Admin\GetUser::Result($id);
        $vm = new \App\Http\ViewModels\Admin\UserEditViewModel($dto);

        return view('user/edit', ['vm' => $vm]);
    }

    public function EditUserPost(\App\Http\Requests\Admin\UserInfoRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\UpdateUser($data));

        return $this->JsonOk();
    }

    public function DeleteUserPost(Request $request)
    {
        $data['id'] = $request->input('id');
        $userId  = $request->input('id');



        // 验证是否可以删除
        if (!$this->ValidateDeleteUser($userId)) {
            return $this->JsonError('无法删除此员工。');
        }

        // 查找员工
        $user = \App\User::find($userId);
        if (!$user) {
            return $this->JsonError('用户不存在。');
        }

        // 移除角色和权限
        $user->syncRoles([]);
        $user->syncPermissions([]);

        // 删除员工记录
        $user->delete();

        // 调用命令以处理其他关联逻辑
        dispatch(new \App\Lib\Commands\Admin\DeleteUser($data));

        // if ($this->ValidateDeleteUser($request->input('id'))) {
        //     dispatch(new \App\Lib\Commands\Admin\DeleteUser($data));

        //     return $this->JsonOk();
        // } else {
        //     return $this->JsonError('无法删除此用户。');
        // }
    }

    private function ValidateDeleteUser($id)
    {
        $result = \App\Lib\Queries\Admin\GetUser::ValidateDelete($id);

        return $result;
    }

    public function ResetUserPasswordPost(\App\Http\Requests\Admin\ResetUserPasswordRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\ResetUserPassword($data));

        return $this->JsonOk();
    }

    // Employee
    public function Employees(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'name';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
        ];

        $dto = \App\Lib\Queries\Admin\GetEmployees::Result($params);
        $paging = \App\Lib\Queries\Admin\GetEmployees::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\EmployeesViewModel($dto, $paging);

        return view('employee/list', ['vm' => $vm]);
    }

    public function AddEmployee()
    {
        $vm = new \App\Http\ViewModels\Admin\EmployeeViewModel();

        return view('employee/add', ['vm' => $vm]);
    }

    public function AddEmployeePost(\App\Http\Requests\Admin\EmployeeRequest $request)
    {
        $data = $request->validated();
        if (!$request->has('agent')) {
            $data['agent'] = 0;
        }
        // dispatch(new \App\Lib\Commands\Admin\CreateEmployee($data));

        // 创建员工（假设 `users` 表存储员工信息）
        $employee = \App\User::create([
            'username' => $data['username'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'status' => $data['status'],
            'role' => 2, // 可选
        ]);
        // 赋予员工角色
        $employee->assignRole('staff'); // 假设角色名称为 'employee'

        DB::table('employee')->insert([
            'user_id' => $employee->id,
            'agent' => $data['agent'],
        ]);

        return $this->JsonOk();
    }

    // public function EditEmployee($id)
    // {
    //     $dto = \App\Lib\Queries\Admin\GetEmployee::Result($id);
    //     $vm = new \App\Http\ViewModels\Admin\EmployeeEditViewModel($dto);

    //     return view('employee/edit', ['vm' => $vm]);
    // }

    // public function EditEmployeePost(\App\Http\Requests\Admin\EmployeeInfoRequest $request)
    // {
    //     $data = $request->validated();
    //     dispatch(new \App\Lib\Commands\Admin\UpdateEmployee($data));

    //     return $this->JsonOk();
    // }


    public function EditEmployee($id)
    {
        $dto = \App\Lib\Queries\Admin\GetEmployee::Result($id);

        $user = \App\User::with('permissions')->find($dto->user_id);
    
        $vm = new \App\Http\ViewModels\Admin\EmployeeEditViewModel($dto, $user);
    
        return view('employee/edit', ['vm' => $vm]);
    }

    public function EditEmployeePost(\App\Http\Requests\Admin\EmployeeInfoRequest $request)
    {
        $data = $request->validated();

        // 查找指定员工
        $user = \App\User::find($data['id']);
        if (!$user) {
            return $this->JsonError('员工不存在。');
        }

        // 更新员工基本信息
        dispatch(new \App\Lib\Commands\Admin\UpdateEmployee($data));

        // 获取表单提交的权限数组
        $permissions = $request->input('permissions', []);

        // 同步员工权限
        $user->syncPermissions($permissions);

        return $this->JsonOk();
    }


    public function DeleteEmployeePost(Request $request)
    {
        $employeeId = $request->input('id');
        $userId  = $request->input('user_id');
        $data['user_id'] = $request->input('user_id');

        // 验证是否可以删除
        if (!$this->ValidateDeleteEmployee($employeeId)) {
            return $this->JsonError('无法删除此员工。');
        }

        // 查找员工
        $user = \App\User::find($userId);
        if (!$user) {
            return $this->JsonError('员工不存在。');
        }

        // 移除角色和权限
        $user->syncRoles([]);
        $user->syncPermissions([]);

        // 删除员工记录
        $user->delete();

        // 调用命令以处理其他关联逻辑
        dispatch(new \App\Lib\Commands\Admin\DeleteEmployee($data));


        // if ($this->ValidateDeleteEmployee($request->input('id'))) {
        //     dispatch(new \App\Lib\Commands\Admin\DeleteEmployee($data));
        //     return $this->JsonOk();
        // } else {
        //     return $this->JsonError('无法删除此员工。');
        // }
    }

    private function ValidateDeleteEmployee($id)
    {
        $result = \App\Lib\Queries\Admin\GetEmployee::ValidateDelete($id);

        return $result;
    }

    // Agent
    public function Agents(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'name';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
        ];

        $dto = \App\Lib\Queries\Admin\GetAgents::Result($params);
        $paging = \App\Lib\Queries\Admin\GetAgents::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\AgentsViewModel($dto, $paging);

        return view('admin/agent/list', ['vm' => $vm]);
    }

    public function AddAgent()
    {
        $vm = new \App\Http\ViewModels\Admin\AgentViewModel();

        return view('admin/agent/add', ['vm' => $vm]);
    }

    public function AddAgentPost(\App\Http\Requests\Admin\AgentRequest $request)
    {
        $data = $request->validated();
        if (!$request->has('agent')) {
            $data['agent'] = 0;
        }
        dispatch(new \App\Lib\Commands\Admin\CreateAgent($data));

        return $this->JsonOk();
    }

    public function EditAgent($id)
    {
        $dto = \App\Lib\Queries\Admin\GetAgent::Result($id);
        $vm = new \App\Http\ViewModels\Admin\AgentEditViewModel($dto);

        return view('admin/agent/edit', ['vm' => $vm]);
    }

    public function EditAgentPost(\App\Http\Requests\Admin\AgentInfoRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\UpdateAgent($data));

        return $this->JsonOk();
    }

    public function DeleteAgentPost(Request $request)
    {
        $data['id'] = $request->input('id');
        if ($this->ValidateDeleteAgent($request->input('id'))) {
            dispatch(new \App\Lib\Commands\Admin\DeleteAgent($data));

            return $this->JsonOk();
        } else {
            return $this->JsonError('无法删除此代理。');
        }
    }

    private function ValidateDeleteAgent($id)
    {
        $result = \App\Lib\Queries\Admin\GetAgent::ValidateDelete($id);

        return $result;
    }

    // Country
    public function Countries(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'name';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
        ];

        $dto = \App\Lib\Queries\Admin\GetCountries::Result($params);
        $paging = \App\Lib\Queries\Admin\GetCountries::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\CountriesViewModel($dto, $paging);

        return view('country/list', ['vm' => $vm]);
    }

    public function AddCountry()
    {
        $vm = new \App\Http\ViewModels\Admin\CountryViewModel();

        return view('country/add', ['vm' => $vm]);
    }

    public function AddCountryPost(\App\Http\Requests\Admin\CountryRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\CreateCountry($data));

        return $this->JsonOk();
    }

    public function EditCountry($id)
    {
        $dto = \App\Lib\Queries\Admin\GetCountry::Result($id);
        $vm = new \App\Http\ViewModels\Admin\CountryEditViewModel($dto);

        return view('country/edit', ['vm' => $vm]);
    }

    public function EditCountryPost(\App\Http\Requests\Admin\CountryInfoRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\UpdateCountry($data));

        return $this->JsonOk();
    }

    public function DeleteCountryPost(Request $request)
    {
        $data['id'] = $request->input('id');
        if ($this->ValidateDeleteCountry($request->input('id'))) {
            dispatch(new \App\Lib\Commands\Admin\DeleteCountry($data));

            return $this->JsonOk();
        } else {
            return $this->JsonError('无法删除此国家。');
        }
    }

    private function ValidateDeleteCountry($id)
    {
        $result = \App\Lib\Queries\Admin\GetCountry::ValidateDelete($id);

        return $result;
    }

    // State
    public function States(Request $request)
    {
        if (null !== $request->input('country_id')) {
            $country_id = $request->input('country_id');
        } else {
            $country_id = '';
        }

        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'name';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'country_id' => $country_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetStates::Result($params);
        $paging = \App\Lib\Queries\Admin\GetStates::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\StatesViewModel($dto, $paging, $country_id);

        return view('state/list', ['vm' => $vm]);
    }

    public function AddState(Request $request)
    {
        if (null !== $request->input('country_id')) {
            $country_id = $request->input('country_id');
        } else {
            $country_id = '';
        }

        $vm = new \App\Http\ViewModels\Admin\StateViewModel($country_id);

        return view('state/add', ['vm' => $vm]);
    }

    public function AddStatePost(\App\Http\Requests\Admin\StateRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\CreateState($data));

        return $this->JsonOk();
    }

    public function EditState($id)
    {
        $dto = \App\Lib\Queries\Admin\GetState::Result($id);
        $vm = new \App\Http\ViewModels\Admin\StateEditViewModel($dto);

        return view('state/edit', ['vm' => $vm]);
    }

    public function EditStatePost(\App\Http\Requests\Admin\StateInfoRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\UpdateState($data));

        return $this->JsonOk();
    }

    public function DeleteStatePost(Request $request)
    {
        $data['id'] = $request->input('id');
        if ($this->ValidateDeleteState($request->input('id'))) {
            dispatch(new \App\Lib\Commands\Admin\DeleteState($data));

            return $this->JsonOk();
        } else {
            return $this->JsonError('无法删除此州属。');
        }
    }

    private function ValidateDeleteState($id)
    {
        $result = \App\Lib\Queries\Admin\GetState::ValidateDelete($id);

        return $result;
    }

    // Port
    public function Ports(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'country';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
        ];

        $dto = \App\Lib\Queries\Admin\GetPorts::Result($params);
        $paging = \App\Lib\Queries\Admin\GetPorts::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\PortsViewModel($dto, $paging);

        return view('port/list', ['vm' => $vm]);
    }

    public function AddPort()
    {
        $vm = new \App\Http\ViewModels\Admin\PortViewModel();

        return view('port/add', ['vm' => $vm]);
    }

    public function AddPortPost(\App\Http\Requests\Admin\PortRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\CreatePort($data));

        return $this->JsonOk();
    }

    public function EditPort($id)
    {
        $dto = \App\Lib\Queries\Admin\GetPort::Result($id);
        $vm = new \App\Http\ViewModels\Admin\PortEditViewModel($dto);

        return view('port/edit', ['vm' => $vm]);
    }

    public function EditPortPost(\App\Http\Requests\Admin\PortInfoRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\UpdatePort($data));

        return $this->JsonOk();
    }

    public function DeletePortPost(Request $request)
    {
        $data['id'] = $request->input('id');
        if ($this->ValidateDeletePort($request->input('id'))) {
            dispatch(new \App\Lib\Commands\Admin\DeletePort($data));

            return $this->JsonOk();
        } else {
            return $this->JsonError('无法删除此码头。');
        }
    }

    private function ValidateDeletePort($id)
    {
        $result = \App\Lib\Queries\Admin\GetPort::ValidateDelete($id);

        return $result;
    }

    // Company
    public function Companies(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'name';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
        ];

        $dto = \App\Lib\Queries\Admin\GetCompanies::Result($params);
        $paging = \App\Lib\Queries\Admin\GetCompanies::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\CompaniesViewModel($dto, $paging);

        return view('company/list', ['vm' => $vm]);
    }

    public function AddCompany()
    {
        $vm = new \App\Http\ViewModels\Admin\CompanyViewModel();

        return view('company/add', ['vm' => $vm]);
    }

    public function AddCompanyPost(\App\Http\Requests\Admin\CompanyRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\CreateCompany($data));

        return $this->JsonOk();
    }

    public function EditCompany($id)
    {
        $dto = \App\Lib\Queries\Admin\GetCompany::Result($id);
        $vm = new \App\Http\ViewModels\Admin\CompanyEditViewModel($dto);

        return view('company/edit', ['vm' => $vm]);
    }

    public function EditCompanyPost(\App\Http\Requests\Admin\CompanyInfoRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\UpdateCompany($data));

        return $this->JsonOk();
    }

    public function DeleteCompanyPost(Request $request)
    {
        $data['id'] = $request->input('id');
        if ($this->ValidateDeleteCompany($request->input('id'))) {
            dispatch(new \App\Lib\Commands\Admin\DeleteCompany($data));

            return $this->JsonOk();
        } else {
            return $this->JsonError('无法删除此公司。');
        }
    }

    private function ValidateDeleteCompany($id)
    {
        $result = \App\Lib\Queries\Admin\GetCompany::ValidateDelete($id);

        return $result;
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\CreateShippingCompany($data));

        return $this->JsonOk();
    }

    // Shipping Company
    public function ShippingCompanies(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'name';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
        ];

        $dto = \App\Lib\Queries\Admin\GetShippingCompanies::Result($params);
        $paging = \App\Lib\Queries\Admin\GetShippingCompanies::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\ShippingCompaniesViewModel($dto, $paging);

        return view('shipping_company/list', ['vm' => $vm]);
    }

    public function AddShippingCompany()
    {
        $vm = new \App\Http\ViewModels\Admin\ShippingCompanyViewModel();

        return view('shipping_company/add', ['vm' => $vm]);
    }

    public function AddShippingCompanyPost(\App\Http\Requests\Admin\ShippingCompanyRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\CreateShippingCompany($data));

        return $this->JsonOk();
    }

    public function EditShippingCompany($id)
    {
        $dto = \App\Lib\Queries\Admin\GetShippingCompany::Result($id);
        $vm = new \App\Http\ViewModels\Admin\ShippingCompanyEditViewModel($dto);

        return view('shipping_company/edit', ['vm' => $vm]);
    }

    public function EditShippingCompanyPost(\App\Http\Requests\Admin\ShippingCompanyInfoRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\UpdateShippingCompany($data));

        return $this->JsonOk();
    }

    public function DeleteShippingCompanyPost(Request $request)
    {
        $data['id'] = $request->input('id');
        if ($this->ValidateDeleteShippingCompany($request->input('id'))) {
            dispatch(new \App\Lib\Commands\Admin\DeleteShippingCompany($data));

            return $this->JsonOk();
        } else {
            return $this->JsonError('无法删除此船公司。');
        }
    }

    private function ValidateDeleteShippingCompany($id)
    {
        $result = \App\Lib\Queries\Admin\GetShippingCompany::ValidateDelete($id);

        return $result;
    }

    // Customs Broker
    public function CustomsBrokers(Request $request)
    {
        if (null !== $request->input('country_id')) {
            $country_id = $request->input('country_id');
        } else {
            $country_id = '';
        }

        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'name';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'country_id' => $country_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetCustomsBrokers::Result($params);
        $paging = \App\Lib\Queries\Admin\GetCustomsBrokers::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\CustomsBrokersViewModel($dto, $paging, $country_id);

        return view('customsbroker/list', ['vm' => $vm]);
    }

    public function AddCustomsBroker(Request $request)
    {
        if (null !== $request->input('country_id')) {
            $country_id = $request->input('country_id');
        } else {
            $country_id = '';
        }

        $vm = new \App\Http\ViewModels\Admin\CustomsBrokerViewModel($country_id);

        return view('customsbroker/add', ['vm' => $vm]);
    }

    public function AddCustomsBrokerPost(\App\Http\Requests\Admin\CustomsBrokerRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\CreateCustomsBroker($data));

        return $this->JsonOk();
    }

    public function EditCustomsBroker($id)
    {
        $dto = \App\Lib\Queries\Admin\GetCustomsBroker::Result($id);

        $vm = new \App\Http\ViewModels\Admin\CustomsBrokerEditViewModel($dto);

        return view('customsbroker/edit', ['vm' => $vm]);
    }

    public function EditCustomsBrokerPost(\App\Http\Requests\Admin\CustomsBrokerInfoRequest $request)
    {
        $data = $request->validated();

        // if(!$request->has('balance')){
        //   $data['balance'] =  '';
        // }
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";

        dispatch(new \App\Lib\Commands\Admin\UpdateCustomsBroker($data));

        return $this->JsonOk();
    }

    public function DeleteCustomsBrokerPost(Request $request)
    {
        $data['id'] = $request->input('id');
        if ($this->ValidateDeleteCustomsBroker($request->input('id'))) {
            dispatch(new \App\Lib\Commands\Admin\DeleteCustomsBroker($data));

            return $this->JsonOk();
        } else {
            return $this->JsonError('无法删除此清关行。');
        }
    }

    private function ValidateDeleteCustomsBroker($id)
    {
        $result = \App\Lib\Queries\Admin\GetCustomsBroker::ValidateDelete($id);

        return $result;
    }

    // Job
    public function Jobs(Request $request)
    {
        if (null !== $request->input('country_id')) {
            $country_id = $request->input('country_id');
        } else {
            $country_id = '';
        }

        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'name';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'country_id' => $country_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetJobs::Result($params);
        $paging = \App\Lib\Queries\Admin\GetJobs::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\JobsViewModel($dto, $paging, $country_id);

        return view('job/list', ['vm' => $vm]);
    }

    public function AddJob(Request $request)
    {
        if (null !== $request->input('country_id')) {
            $country_id = $request->input('country_id');
        } else {
            $country_id = '';
        }

        $vm = new \App\Http\ViewModels\Admin\JobViewModel($country_id);

        return view('job/add', ['vm' => $vm]);
    }

    public function AddJobPost(\App\Http\Requests\Admin\JobRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\CreateJob($data));

        return $this->JsonOk();
    }

    public function EditJob($id)
    {
        $dto = \App\Lib\Queries\Admin\GetJob::Result($id);
        $vm = new \App\Http\ViewModels\Admin\JobEditViewModel($dto);

        return view('job/edit', ['vm' => $vm]);
    }

    public function EditJobPost(\App\Http\Requests\Admin\JobInfoRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\UpdateJob($data));

        return $this->JsonOk();
    }

    public function DeleteJobPost(Request $request)
    {
        $data['id'] = $request->input('id');
        if ($this->ValidateDeleteJob($request->input('id'))) {
            dispatch(new \App\Lib\Commands\Admin\DeleteJob($data));

            return $this->JsonOk();
        } else {
            return $this->JsonError('无法删除此清关行。');
        }
    }

    private function ValidateDeleteJob($id)
    {
        $result = \App\Lib\Queries\Admin\GetJob::ValidateDelete($id);

        return $result;
    }

    // Warehouse
    public function Warehouses(Request $request)
    {
        if (null !== $request->input('country_id')) {
            $country_id = $request->input('country_id');
        } else {
            $country_id = '';
        }

        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'name';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'country_id' => $country_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetWarehouses::Result($params);
        $paging = \App\Lib\Queries\Admin\GetWarehouses::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\WarehousesViewModel($dto, $paging, $country_id);

        return view('warehouse/list', ['vm' => $vm]);
    }

    public function AddWarehouse(Request $request)
    {
        if (null !== $request->input('country_id')) {
            $country_id = $request->input('country_id');
        } else {
            $country_id = '';
        }

        $vm = new \App\Http\ViewModels\Admin\WarehouseViewModel($country_id);

        return view('warehouse/add', ['vm' => $vm]);
    }

    public function AddWarehousePost(\App\Http\Requests\Admin\WarehouseRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\CreateWarehouse($data));

        return $this->JsonOk();
    }

    public function EditWarehouse($id)
    {
        $dto = \App\Lib\Queries\Admin\GetWarehouse::Result($id);
        $vm = new \App\Http\ViewModels\Admin\WarehouseEditViewModel($dto);

        return view('warehouse/edit', ['vm' => $vm]);
    }

    public function EditWarehousePost(\App\Http\Requests\Admin\WarehouseInfoRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\UpdateWarehouse($data));

        return $this->JsonOk();
    }

    public function DeleteWarehousePost(Request $request)
    {
        $data['id'] = $request->input('id');
        if ($this->ValidateDeleteWarehouse($request->input('id'))) {
            dispatch(new \App\Lib\Commands\Admin\DeleteWarehouse($data));

            return $this->JsonOk();
        } else {
            return $this->JsonError('无法删除此仓库。');
        }
    }

    private function ValidateDeleteWarehouse($id)
    {
        $result = \App\Lib\Queries\Admin\GetWarehouse::ValidateDelete($id);

        return $result;
    }

    // Customer
    public function Customers(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'name';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('keyword')) {
            // $keyword = '%'.strtolower($request->input('keyword')).'%';
            $keyword = '%'.strtolower(urldecode($request->input('keyword'))).'%';

            $inputs['keyword'] = $request->input('keyword');
        } else {
            $keyword = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'keyword' => $keyword,
        ];

        $dto = \App\Lib\Queries\Admin\GetCustomers::Result($params);
        $paging = \App\Lib\Queries\Admin\GetCustomers::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\CustomersViewModel($dto, $paging);

        return view('customer/list', ['vm' => $vm]);
    }

    public function AddCustomer()
    {
        $vm = new \App\Http\ViewModels\Admin\CustomerViewModel();

        return view('customer/add', ['vm' => $vm]);
    }

    public function AddCustomerPost(\App\Http\Requests\Admin\CustomerRequest $request)
    {
        $data = $request->validated();
        // Biiling
        if ($data['billing_state_id'] != '' || $data['billing_state_id'] != 0) {
            $billing_state = \App\Lib\Queries\Admin\GetState::Result($data['billing_state_id']);
            if (!empty($billing_state)) {
                $data['billing_state'] = $billing_state->name;
            } else {
                $data['billing_state'] = '';
            }
        } else {
            $data['billing_state'] = '';
        }

        if ($data['billing_country_id'] != '' || $data['billing_country_id'] != 0) {
            $billing_country = \App\Lib\Queries\Admin\GetCountry::Result($data['billing_country_id']);
            $data['billing_country'] = $billing_country->name;
        } else {
            $data['billing_country'] = '';
        }

        // Shipping
        if ($data['shipping_state_id'] != 0) {
            $shipping_state = \App\Lib\Queries\Admin\GetState::Result($data['shipping_state_id']);
            if (!empty($billing_state)) {
                $data['shipping_state'] = $billing_state->name;
            } else {
                $data['shipping_state'] = '';
            }
        } else {
            $data['shipping_state'] = '';
        }

        if ($data['shipping_country_id'] != '' || $data['shipping_country_id'] != 0) {
            $shipping_country = \App\Lib\Queries\Admin\GetCountry::Result($data['shipping_country_id']);
            $data['shipping_country'] = $shipping_country->name;
        } else {
            $data['shipping_country'] = '';
        }

        $data['created_by'] = Auth::user()->name;
        $data['updated_by'] = Auth::user()->name;
        dispatch(new \App\Lib\Commands\Admin\CreateCustomer($data));

        return $this->JsonOk();
    }

    public function EditCustomer($id)
    {
        $dto = \App\Lib\Queries\Admin\GetCustomer::Result($id);
        $vm = new \App\Http\ViewModels\Admin\CustomerEditViewModel($dto);

        return view('customer/edit', ['vm' => $vm]);
    }

    public function EditCustomerPost(\App\Http\Requests\Admin\CustomerInfoRequest $request)
    {
        $data = $request->validated();

        // Biiling
        if ($data['billing_state_id'] != '' || $data['billing_state_id'] != 0) {
            $billing_state = \App\Lib\Queries\Admin\GetState::Result($data['billing_state_id']);
            if (!empty($billing_state)) {
                $data['billing_state'] = $billing_state->name;
            } else {
                $data['billing_state'] = '';
            }
        } else {
            $data['billing_state'] = '';
        }

        if ($data['billing_country_id'] != '' || $data['billing_country_id'] != 0) {
            $billing_country = \App\Lib\Queries\Admin\GetCountry::Result($data['billing_country_id']);
            $data['billing_country'] = $billing_country->name;
        } else {
            $data['billing_country'] = '';
        }

        // Shipping
        if ($data['shipping_state_id'] != 0) {
            $shipping_state = \App\Lib\Queries\Admin\GetState::Result($data['shipping_state_id']);
            if (!empty($billing_state)) {
                $data['shipping_state'] = $billing_state->name;
            } else {
                $data['shipping_state'] = '';
            }
        } else {
            $data['shipping_state'] = '';
        }

        if ($data['shipping_country_id'] != '' || $data['shipping_country_id'] != 0) {
            $shipping_country = \App\Lib\Queries\Admin\GetCountry::Result($data['shipping_country_id']);
            $data['shipping_country'] = $shipping_country->name;
        } else {
            $data['shipping_country'] = '';
        }

        $data['updated_by'] = Auth::user()->name;

        dispatch(new \App\Lib\Commands\Admin\UpdateCustomer($data));

        return $this->JsonOk();
    }

    public function CopyCustomer($id)
    {
        $dto = \App\Lib\Queries\Admin\GetCustomer::Result($id);
        $vm = new \App\Http\ViewModels\Admin\CustomerEditViewModel($dto);

        return view('customer/copy', ['vm' => $vm]);
    }

    public function DeleteCustomerPost(Request $request)
    {
        $data['id'] = $request->input('id');
        if ($this->ValidateDeleteCustomer($request->input('id'))) {
            dispatch(new \App\Lib\Commands\Admin\DeleteCustomer($data));

            return $this->JsonOk();
        } else {
            return $this->JsonError('无法删除此客户。');
        }
    }

    private function ValidateDeleteCustomer($id)
    {
        $result = \App\Lib\Queries\Admin\GetCustomer::ValidateDelete($id);

        return $result;
    }

    // Booking
    public function Bookings(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'updated_at';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'desc';
        }

        if (null !== $request->input('keyword')) {
            $keyword = '%'.strtolower($request->input('keyword')).'%';
            $inputs['keyword'] = $request->input('keyword');
        } else {
            $keyword = '';
            $inputs['keyword'] = '';
        }

        if (null !== $request->input('status_id')) {
            $status_id = $request->input('status_id');
            $inputs['status_id'] = $request->input('status_id');
        } else {
            $status_id = '';
            $inputs['status_id'] = '';
        }

        if (null !== $request->input('user_id')) {
            $user_id = $request->input('user_id');
            $inputs['user_id'] = $request->input('user_id');
        } else {
            $user_id = '';
            $inputs['user_id'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'keyword' => $keyword,
            'status_id' => $status_id,
            'user_id' => $user_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetBookings::Result($params);
        $paging = \App\Lib\Queries\Admin\GetBookings::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\BookingsViewModel($dto, $paging, $inputs);

        return view('booking/list', ['vm' => $vm]);
    }

    public function AddBooking()
    {
        $vm = new \App\Http\ViewModels\Admin\BookingViewModel();

        return view('booking/add', ['vm' => $vm]);
    }

    public function AddBookingPost(\App\Http\Requests\Admin\BookingRequest $request)
    {
        $data = $request->validated();

        if ($data['booking_type'] == 'booking') {
            $location = url('/booking/add/booking');
        } elseif ($data['booking_type'] == 'declaration') {
            $location = url('/booking/add/declaration');
        } elseif ($data['booking_type'] == 'cargo') {
            $location = url('/cargo/add');
        }

        return $this->JsonOk(['location' => $location]);
    }

    public function AddBookingBooking(Request $request)
    {
        $vm = new \App\Http\ViewModels\Admin\BookingBookingViewModel();

        return view('booking/add/booking', ['vm' => $vm]);
    }

    public function AddBookingBookingPost(\App\Http\Requests\Admin\BookingBookingRequest $request)
    {
        $data = $request->validated();

        if (isset($data['booking_date'])) {
            $data['booking_date'] = str_replace('/', '-', $data['booking_date']);
            $data['booking_date'] = date('Y-m-d', strtotime($data['booking_date']));
        }

        if (isset($data['closing_date'])) {
            $data['closing_date'] = str_replace('/', '-', $data['closing_date']);
            $data['closing_date'] = date('Y-m-d', strtotime($data['closing_date']));
        }

        if (isset($data['sailing_date'])) {
            $data['sailing_date'] = str_replace('/', '-', $data['sailing_date']);
            $data['sailing_date'] = date('Y-m-d', strtotime($data['sailing_date']));
        }

        $data['rate'] = $this->CurrentCurrencyRate();
        $data['order_no'] = 'OR'.date('Y').str_pad($this->currentOrderNo(), 4, '0', STR_PAD_LEFT);
        $data['order_num'] = $this->currentOrderNo();
        $data['booking_no'] = $data['customer_code'].'-'.$data['booking_no_1'].'-'.$data['booking_no_2'];
        $data['status'] = 1;
        $data['owner_id'] = Auth::user()->id;
        $data['created_by'] = Auth::user()->name;
        $data['updated_by'] = Auth::user()->name;

        // prevent empty country
        if ($data['delivery_state_id'] != '' || $data['delivery_state_id'] != 0) {
            $state = \App\Lib\Queries\Admin\GetState::Result($data['delivery_state_id']);
            if (!empty($state)) {
                $data['delivery_state'] = $state->name;
            }
        }

        // prevent empty state
        if ($data['delivery_country_id'] != '' || $data['delivery_country_id'] != 0) {
            $country = \App\Lib\Queries\Admin\GetCountry::Result($data['delivery_country_id']);
            $data['delivery_country'] = $country->name;
        }

        dispatch(new \App\Lib\Commands\Admin\CreateBooking($data));

        return $this->JsonOk();
    }

    public function AddBookingLoadingPost(Request $request)
    {
        $data['id'] = $request->input('id');
        $data['updated_by'] = Auth::user()->username;

        $dto = \App\Lib\Queries\Admin\GetBooking::Result($request->input('id'));

        if (!empty($dto)) {
            if (!empty($dto->booking_date)) {
                $booking_date = str_replace('/', '-', $dto->booking_date);
                $data['loading_date'] = date('Y-m-d', strtotime($booking_date));
            } else {
                $data['loading_date'] = date('Y-m-d');
            }
        }
        dispatch(new \App\Lib\Commands\Admin\CreateLoading($data));

        return $this->JsonOk();
    }

    public function AddBookingDeclaration()
    {
        $vm = new \App\Http\ViewModels\Admin\BookingDeclarationViewModel();

        return view('booking/add/declaration', ['vm' => $vm]);
    }

    public function AddBookingDeclarationPost(\App\Http\Requests\Admin\BookingDeclarationRequest $request)
    {
        $data = $request->validated();
        $data['rate'] = $this->CurrentCurrencyRate();
        $data['order_no'] = 'OR'.date('Y').str_pad($this->currentOrderNo(), 4, '0', STR_PAD_LEFT);
        $data['order_num'] = $this->currentOrderNo();
        $data['booking_no'] = $data['customer_code'].'-'.$data['booking_no_1'].'-'.$data['booking_no_2'];
        $data['status'] = 4;
        $data['owner_id'] = Auth::user()->id;
        $data['created_by'] = Auth::user()->name;
        $data['updated_by'] = Auth::user()->name;
        $data['agent_id'] = 0; // no agent need

        if (isset($data['loading_date'])) {
            $data['loading_date'] = str_replace('/', '-', $data['loading_date']);
            $data['loading_date'] = date('Y-m-d', strtotime($data['loading_date']));
        }

        if (isset($data['sst'])) {
            $data['sst'] = 1;
        } else {
            $data['sst'] = 0;
        }

        if (isset($data['eta_date'])) {
            $data['eta_date'] = str_replace('/', '-', $data['eta_date']);
            $data['eta_date'] = date('Y-m-d', strtotime($data['eta_date']));
        }

        if (isset($data['custom_goods_inv_date'])) {
            $data['custom_goods_inv_date'] = str_replace('/', '-', $data['custom_goods_inv_date']);
            $data['custom_goods_inv_date'] = date('Y-m-d', strtotime($data['custom_goods_inv_date']));
        }

        if (isset($data['custom_handling_inv_date'])) {
            $data['custom_handling_inv_date'] = str_replace('/', '-', $data['custom_handling_inv_date']);
            $data['custom_handling_inv_date'] = date('Y-m-d', strtotime($data['custom_handling_inv_date']));
        }

        if (isset($data['billing_goods_inv_date'])) {
            $data['billing_goods_inv_date'] = str_replace('/', '-', $data['billing_goods_inv_date']);
            $data['billing_goods_inv_date'] = date('Y-m-d', strtotime($data['billing_goods_inv_date']));
        }

        if (isset($data['billing_handling_inv_date'])) {
            $data['billing_handling_inv_date'] = str_replace('/', '-', $data['billing_handling_inv_date']);
            $data['billing_handling_inv_date'] = date('Y-m-d', strtotime($data['billing_handling_inv_date']));
        }

        dispatch(new \App\Lib\Commands\Admin\CreateDeclaration($data));

        return $this->JsonOk();
    }

    public function UpdateDeclarationPost(Request $request)
    {
        $data['id'] = $request->input('id');
        $data['updated_by'] = Auth::user()->username;
        dispatch(new \App\Lib\Commands\Admin\UpdateDeclartionStatus($data));

        return $this->JsonOk();
    }

    public function UpdateStatusPost(Request $request)
    {
        $data['id'] = $request->input('id');
        $data['status_id'] = $request->input('status_id');
        $data['updated_by'] = Auth::user()->username;
        dispatch(new \App\Lib\Commands\Admin\UpdateBookingStatus($data));

        return $this->JsonOk();
    }

    // edit booking
    public function Booking($id)
    {
        $dto = \App\Lib\Queries\Admin\GetBooking::Result($id);
        if (!empty($dto)) {
            // check status return to right page
            if ($dto->status_id == 1) {
                return redirect('/booking/edit/booking/'.$id);
            } elseif ($dto->status_id == 2 || $dto->status_id == 3) {
                return redirect('/booking/edit/loading/'.$id);
            // return view('booking/edit/loading', ['vm' => $vm]);
            } elseif ($dto->status_id == 4 || $dto->status_id == 5) {
                return redirect('/booking/edit/declaration/'.$id);
                // return view('booking/edit/declaration', ['vm' => $vm]);
            }
        } else {
            return view('404');
        }
    }

    public function EditBooking($id)
    {
        $dto = \App\Lib\Queries\Admin\GetBooking::Result($id);
        if (!empty($dto)) {
            // check status return to right page
            $vm = new \App\Http\ViewModels\Admin\BookingEditViewModel($dto);

            return view('booking/edit/booking', ['vm' => $vm]);
        } else {
            return view('404');
        }
    }

    public function EditBookingPost(\App\Http\Requests\Admin\BookingEditRequest $request)
    {
        $data = $request->validated();
        if (isset($data['booking_date'])) {
            $data['booking_date'] = str_replace('/', '-', $data['booking_date']);
            $data['booking_date'] = date('Y-m-d', strtotime($data['booking_date']));
        }

        if (isset($data['closing_date'])) {
            $data['closing_date'] = str_replace('/', '-', $data['closing_date']);
            $data['closing_date'] = date('Y-m-d', strtotime($data['closing_date']));
        }

        if (isset($data['sailing_date'])) {
            $data['sailing_date'] = str_replace('/', '-', $data['sailing_date']);
            $data['sailing_date'] = date('Y-m-d', strtotime($data['sailing_date']));
        }

        // prevent empty country
        if ($data['delivery_state_id'] != '' || $data['delivery_state_id'] != 0) {
            $state = \App\Lib\Queries\Admin\GetState::Result($data['delivery_state_id']);
            if (!empty($state)) {
                $data['delivery_state'] = $state->name;
            }
        }

        // prevent empty state
        if ($data['delivery_country_id'] != '' || $data['delivery_country_id'] != 0) {
            $country = \App\Lib\Queries\Admin\GetCountry::Result($data['delivery_country_id']);
            $data['delivery_country'] = $country->name;
        }

        $data['updated_by'] = Auth::user()->username;

        dispatch(new \App\Lib\Commands\Admin\UpdateBooking($data));

        return $this->JsonOk();
    }

    // edit loading
    public function EditLoading($id)
    {
        $dto = \App\Lib\Queries\Admin\GetBooking::Result($id);
        if (!empty($dto)) {
            $vm = new \App\Http\ViewModels\Admin\BookingLoadingViewModel($dto);

            return view('booking/edit/loading', ['vm' => $vm]);
        } else {
            return view('404');
        }
    }

    public function EditLoadingPost(\App\Http\Requests\Admin\BookingLoadingRequest $request)
    {
        $data = $request->validated();

        if (isset($data['forme_apply'])) {
            $data['forme_apply'] = 1;
        } else {
            $data['forme_apply'] = 0;
        }

        if (isset($data['loading_date'])) {
            $data['loading_date'] = str_replace('/', '-', $data['loading_date']);
            $data['loading_date'] = date('Y-m-d', strtotime($data['loading_date']));
        }

        if (isset($data['etd_date'])) {
            $data['etd_date'] = str_replace('/', '-', $data['etd_date']);
            $data['etd_date'] = date('Y-m-d', strtotime($data['etd_date']));
        }

        if (isset($data['eta_date'])) {
            $data['eta_date'] = str_replace('/', '-', $data['eta_date']);
            $data['eta_date'] = date('Y-m-d', strtotime($data['eta_date']));
        }

        // if ($request->hasFile('loading_photo')) {
        //     $data['loading_photo'] = $request->file('loading_photo')->store('download');
        //     unset($data['loading_photo']);
        // }else{
        //    $data['loading_photo'] ="";
        // }
        $data['updated_by'] = Auth::user()->username;

        dispatch(new \App\Lib\Commands\Admin\UpdateLoading($data));

        return $this->JsonOk();
    }

    // edit declaration
    public function EditDeclaration($id)
    {
        $dto = \App\Lib\Queries\Admin\GetBooking::Result($id);
        if (!empty($dto)) {
            $vm = new \App\Http\ViewModels\Admin\BookingDeclarationEditViewModel($dto);

            return view('booking/edit/declaration', ['vm' => $vm]);
        } else {
            return view('404');
        }
    }

    public function EditDeclarationPost(\App\Http\Requests\Admin\BookingDeclarationEditRequest $request)
    {
        $data = $request->validated();
        if (isset($data['sst'])) {
            $data['sst'] = 1;
        } else {
            $data['sst'] = 0;
        }

        // if(isset($data['declaration_date'])){
        //   $data['declaration_date'] = str_replace('/', '-', $data['declaration_date']);
        //   $data['declaration_date'] = date('Y-m-d', strtotime($data['declaration_date']));
        // }

        if (isset($data['loading_date'])) {
            $data['loading_date'] = str_replace('/', '-', $data['loading_date']);
            $data['loading_date'] = date('Y-m-d', strtotime($data['loading_date']));
        }

        if (isset($data['eta_date'])) {
            $data['eta_date'] = str_replace('/', '-', $data['eta_date']);
            $data['eta_date'] = date('Y-m-d', strtotime($data['eta_date']));
        }

        if (isset($data['custom_goods_inv_date'])) {
            $data['custom_goods_inv_date'] = str_replace('/', '-', $data['custom_goods_inv_date']);
            $data['custom_goods_inv_date'] = date('Y-m-d', strtotime($data['custom_goods_inv_date']));
        }

        if (isset($data['custom_handling_inv_date'])) {
            $data['custom_handling_inv_date'] = str_replace('/', '-', $data['custom_handling_inv_date']);
            $data['custom_handling_inv_date'] = date('Y-m-d', strtotime($data['custom_handling_inv_date']));
        }

        if (isset($data['billing_goods_inv_date'])) {
            $data['billing_goods_inv_date'] = str_replace('/', '-', $data['billing_goods_inv_date']);
            $data['billing_goods_inv_date'] = date('Y-m-d', strtotime($data['billing_goods_inv_date']));
        }

        if (isset($data['billing_handling_inv_date'])) {
            $data['billing_handling_inv_date'] = str_replace('/', '-', $data['billing_handling_inv_date']);
            $data['billing_handling_inv_date'] = date('Y-m-d', strtotime($data['billing_handling_inv_date']));
        }

        $data['updated_by'] = Auth::user()->username;
        dispatch(new \App\Lib\Commands\Admin\UpdateDeclaration($data));

        return $this->JsonOk();
    }

    public function AddBookingPersonPost(\App\Http\Requests\Admin\BookingPersonRequest $request)
    {
        $data = $request->validated();

        dispatch(new \App\Lib\Commands\Admin\CreateBookingPerson($data));

        return $this->JsonOk();
    }

    public function AddBookingFeePost(\App\Http\Requests\Admin\BookingFeeRequest $request)
    {
        $data = $request->validated();

        dispatch(new \App\Lib\Commands\Admin\CreateBookingFee($data));

        return $this->JsonOk();
    }

    public function DeleteBookingPost(Request $request)
    {
        $data['id'] = $request->input('id');
        $data['deleted_by'] = Auth::user()->username;
        dispatch(new \App\Lib\Commands\Admin\DeleteBooking($data));

        return $this->JsonOk();
    }

    public function DeleteBookingPersonPost(Request $request)
    {
        $data['booking_person_id'] = $request->input('booking_person_id');
        dispatch(new \App\Lib\Commands\Admin\DeleteBookingPerson($data));

        return $this->JsonOk();
    }

    public function DeleteBookingAttachmentPost(Request $request)
    {
        $data['booking_attachment_id'] = $request->input('booking_attachment_id');
        dispatch(new \App\Lib\Commands\Admin\DeleteBookingAttachmentPost($data));

        return $this->JsonOk();
    }

    public function DeleteBookingFeePost(Request $request)
    {
        $data['booking_fee_id'] = $request->input('booking_fee_id');
        dispatch(new \App\Lib\Commands\Admin\DeleteBookingFee($data));

        return $this->JsonOk();
    }

    public function GetPersons($id)
    {
        $dto = \App\Lib\Queries\Admin\GetBooking::GetBookingPersons($id, 1);

        return view('booking/edit/persons', ['data' => $dto]);
    }

    public function GetInchargePersons($id)
    {
        $dto = \App\Lib\Queries\Admin\GetBooking::GetBookingPersons($id, 1);

        return view('booking/edit/incharges', ['data' => $dto]);
    }

    public function GetInspectPersons($id)
    {
        $dto = \App\Lib\Queries\Admin\GetBooking::GetBookingPersons($id, 2);

        return view('booking/edit/inspects', ['data' => $dto]);
    }

    public function GetPhotoSeals($id)
    {
        $data = ['1'];
        $dto = \App\Lib\Queries\Admin\GetBooking::GetBookingAttachments($id, $data);

        return view('booking/edit/photoseal', ['data' => $dto]);
    }

    public function GetTypeAttachments($id, $type)
    {
        $data = [$type];
        $dto = \App\Lib\Queries\Admin\GetBooking::GetBookingAttachments($id, $data);

        return view('booking/edit/attachments', ['data' => $dto]);
    }

    public function GetLoadingAttachments($id)
    {
        $data = ['2', '3', '6'];
        $dto = \App\Lib\Queries\Admin\GetBooking::GetBookingAttachments($id, $data);

        return view('booking/edit/attachments', ['data' => $dto]);
    }

    public function GetDeclarationAttachments($id)
    {
        $data = ['4', '5'];
        $dto = \App\Lib\Queries\Admin\GetBooking::GetBookingAttachments($id, $data);

        return view('booking/edit/attachments', ['data' => $dto]);
    }

    public function GetFees($id, $type)
    {
        $dto = \App\Lib\Queries\Admin\GetBooking::GetBookingFees($id, $type);

        return view('booking/edit/fees', ['data' => $dto]);
    }

    public function UplaodFilePost(Request $request)
    {
        $data = $request->all();
        if ($request->hasFile('file_file')) {
            // $image = $request->file('file_file');
            // $input['imagename'] = time().'.'.$image->extension();
            //
            //  $destinationPath = storage_path('app/upload');
            //  $img = Image::make($image->path());
            //  $img->resize(100, 100, function ($constraint) {
            //     $constraint->aspectRatio();
            // })->save($destinationPath.'/'.$input['imagename']);
            // $destinationPath = storage_path('/app/public');
            // $image->move($destinationPath, $input['imagename']);
            // $image = Image::make($request->file('file_file'))
            // ->resize(750, null, function ($constraint) {
            //     $constraint->aspectRatio();
            // })->encode('jpg');
            // $ext = $request->file('file_name')->getMimeType();
            //
            // if($ext = '.png' || $ext = '.jpg' || $ext = '.gif'){
            // $path = $request->file('file_file')->store('upload');
            //
            // echo "<pre>";
            // print_r(storage_path('app/' . $path));
            // echo "</pre>";
            // $img = Image::make(storage_path('app/' . $path));
            // $img->resize(1000, null, function ($constraint) {
            //     $constraint->aspectRatio();
            // });

            // resize the image to a width of 300 and constrain aspect ratio (auto height)

            // }

            // $request->file('file_file')->getSize();

            // $ext = $request->file('file_file')->extension();
            // $filename = uniqid() . md5_file($request->file('file_file')->getRealPath());
            // $ext = $request->file('file_file')->guessExtension();

            // $path =  $request->file('file_file')->storeAs('upload', uniqid() .'.'.$ext  );

            $filename = date('YmdHi').$this->generateRandomString(8);
            $ext = $request->file('file_file')->extension();
            // echo "<pre>";
            // print_r($ext);
            // echo "</pre>";
            if ($ext == 'zip') {
                $ext = 'xlsx';
            }
            $path = $request->file('file_file')->storeAs('upload', $filename.'.'.$ext);

            // $path = $request->file('file_file')->store('upload');
            unset($data['file_file']);
            $data['attachment'] = $path;
        }
        dispatch(new \App\Lib\Commands\Admin\CreateBookingAttachment($data));

        return $this->JsonOk(['filenames' => $path, 'path' => $path]);
    }

    // private function resizeImage($request){
    //
    //   if ($request->hasFile('file_file')) {
    //     $image = Image::make($request->file('file_file'))
    //     ->resize(750, null, function ($constraint) {
    //         $constraint->aspectRatio();
    //     })->encode('jpg');
    //     $path = "upload/test.jpg";
    //     Storage::disk('upload')->put($path, $image->encoded);
    //   }
    //
    //   return $path;
    // }

    // Supplier
    public function Suppliers(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'name';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
        ];

        $dto = \App\Lib\Queries\Admin\GetSuppliers::Result($params);
        $paging = \App\Lib\Queries\Admin\GetSuppliers::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\SuppliersViewModel($dto, $paging);

        return view('supplier/list', ['vm' => $vm]);
    }

    public function AddSupplier(Request $request)
    {
        $vm = new \App\Http\ViewModels\Admin\SupplierViewModel();

        return view('supplier/add', ['vm' => $vm]);
    }

    public function AddSupplierPost(\App\Http\Requests\Admin\SupplierRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\CreateSupplier($data));

        return $this->JsonOk();
    }

    public function EditSupplier($id)
    {
        $dto = \App\Lib\Queries\Admin\GetSupplier::Result($id);
        $vm = new \App\Http\ViewModels\Admin\SupplierEditViewModel($dto);

        return view('supplier/edit', ['vm' => $vm]);
    }

    public function EditSupplierPost(\App\Http\Requests\Admin\SupplierInfoRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\UpdateSupplier($data));

        return $this->JsonOk();
    }

    public function DeleteSupplierPost(Request $request)
    {
        $data['id'] = $request->input('id');
        if ($this->ValidateDeleteSupplier($request->input('id'))) {
            dispatch(new \App\Lib\Commands\Admin\DeleteSupplier($data));

            return $this->JsonOk();
        } else {
            return $this->JsonError('无法删除此供应商。');
        }
    }

    private function ValidateDeleteSupplier($id)
    {
        $result = \App\Lib\Queries\Admin\GetSupplier::ValidateDelete($id);

        return $result;
    }

    // Beneficiary
    public function Beneficiaries(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'name';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
        ];

        $dto = \App\Lib\Queries\Admin\GetBeneficiaries::Result($params);
        $paging = \App\Lib\Queries\Admin\GetBeneficiaries::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\BeneficiariesViewModel($dto, $paging);

        return view('beneficiary/list', ['vm' => $vm]);
    }

    public function AddBeneficiary(Request $request)
    {
        $vm = new \App\Http\ViewModels\Admin\BeneficiaryViewModel();

        return view('beneficiary/add', ['vm' => $vm]);
    }

    public function AddBeneficiaryPost(\App\Http\Requests\Admin\BeneficiaryRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\CreateBeneficiary($data));

        return $this->JsonOk();
    }

    public function EditBeneficiary($id)
    {
        $dto = \App\Lib\Queries\Admin\GetBeneficiary::Result($id);
        $vm = new \App\Http\ViewModels\Admin\BeneficiaryEditViewModel($dto);

        return view('beneficiary/edit', ['vm' => $vm]);
    }

    public function EditBeneficiaryPost(\App\Http\Requests\Admin\BeneficiaryInfoRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\UpdateBeneficiary($data));

        return $this->JsonOk();
    }

    public function DeleteBeneficiaryPost(Request $request)
    {
        $data['id'] = $request->input('id');
        if ($this->ValidateDeleteBeneficiary($request->input('id'))) {
            dispatch(new \App\Lib\Commands\Admin\DeleteBeneficiary($data));

            return $this->JsonOk();
        } else {
            return $this->JsonError('无法删除此收款人。');
        }
    }

    private function ValidateDeleteBeneficiary($id)
    {
        $result = \App\Lib\Queries\Admin\GetBeneficiary::ValidateDelete($id);

        return $result;
    }

    // Payment Transfer
    public function PaymentTransfers(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'updated_at';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'desc';
        }

        if (null !== $request->input('keyword')) {
            $keyword = '%'.strtolower($request->input('keyword')).'%';
            $inputs['keyword'] = $request->input('keyword');
        } else {
            $keyword = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'keyword' => $keyword,
        ];

        $dto = \App\Lib\Queries\Admin\GetPaymentTransfers::Result($params);
        $paging = \App\Lib\Queries\Admin\GetPaymentTransfers::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\PaymentTransfersViewModel($dto, $paging);

        return view('payment_transfer/list', ['vm' => $vm]);
    }

    public function AddPaymentTransfer()
    {
        $vm = new \App\Http\ViewModels\Admin\PaymentTransferViewModel();

        return view('payment_transfer/add', ['vm' => $vm]);
    }

    public function AddPaymentTransferPost(\App\Http\Requests\Admin\PaymentTransferRequest $request)
    {
        $data = $request->validated();
        $data['owner_id'] = Auth::user()->id;
        $data['created_by'] = Auth::user()->name;
        $data['updated_by'] = Auth::user()->name;

        if ($request->hasFile('file_customer_attachment')) {
            $data['customer_attachment'] = $request->file('file_customer_attachment')->store('downloadfile/transfer');
            unset($data['file_customer_attachment']);
        }

        if ($request->hasFile('file_supplier_attachment')) {
            $data['supplier_attachment'] = $request->file('file_supplier_attachment')->store('downloadfile/transfer');
            unset($data['file_supplier_attachment']);
        }

        if (isset($data['payment_date'])) {
            $data['payment_date'] = str_replace('/', '-', $data['payment_date']);
            $data['payment_date'] = date('Y-m-d', strtotime($data['payment_date']));
        }

        if (isset($data['date_received'])) {
            $data['date_received'] = str_replace('/', '-', $data['date_received']);
            $data['date_received'] = date('Y-m-d', strtotime($data['date_received']));
        }

        dispatch(new \App\Lib\Commands\Admin\CreatePaymentTransfer($data));

        return $this->JsonOk();
    }

    public function EditPaymentTransfer($id)
    {
        $dto = \App\Lib\Queries\Admin\GetPaymentTransfer::Result($id);
        $vm = new \App\Http\ViewModels\Admin\PaymentTransferEditViewModel($dto);

        return view('payment_transfer/edit', ['vm' => $vm]);
    }

    public function EditPaymentTransferPost(\App\Http\Requests\Admin\PaymentTransferInfoRequest $request)
    {
        $data = $request->validated();

        if ($request->hasFile('file_customer_attachment')) {
            $data['customer_attachment'] = $request->file('file_customer_attachment')->store('downloadfile/transfer');
            unset($data['file_customer_attachment']);
        }

        if ($request->hasFile('file_supplier_attachment')) {
            $data['supplier_attachment'] = $request->file('file_supplier_attachment')->store('downloadfile/transfer');
            unset($data['file_supplier_attachment']);
        }

        if (isset($data['payment_date'])) {
            $data['payment_date'] = str_replace('/', '-', $data['payment_date']);
            $data['payment_date'] = date('Y-m-d', strtotime($data['payment_date']));
        }

        if (isset($data['date_received'])) {
            $data['date_received'] = str_replace('/', '-', $data['date_received']);
            $data['date_received'] = date('Y-m-d', strtotime($data['date_received']));
        }

        $data['updated_by'] = Auth::user()->name;

        dispatch(new \App\Lib\Commands\Admin\UpdatePaymentTransfer($data));

        return $this->JsonOk();
    }

    public function DeletePaymentTransferPost(Request $request)
    {
        $data['id'] = $request->input('id');
        dispatch(new \App\Lib\Commands\Admin\DeletePaymentTransfer($data));

        return $this->JsonOk();
    }

    public function DeletePaymentTransferAttachmentPost(Request $request)
    {
        $data['id'] = $request->input('payment_transfer_id');
        $data['type'] = $request->input('type');
        $dto = \App\Lib\Queries\Admin\GetPaymentTransfer::Result($data['id']);
        if ($data['type'] == 1) {
            Storage::delete($dto->customer_attachment);
        } elseif ($data['type'] == 2) {
            Storage::delete($dto->supplier_attachment);
        }
        dispatch(new \App\Lib\Commands\Admin\DeletePaymentTransferAttachment($data));

        return $this->JsonOk();
    }

    // Payment Status
    public function PaymentStatuses(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'sort_order';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
        ];

        $dto = \App\Lib\Queries\Admin\GetPaymentStatuses::Result($params);
        $paging = \App\Lib\Queries\Admin\GetPaymentStatuses::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\PaymentStatusesViewModel($dto, $paging);

        return view('payment_status/list', ['vm' => $vm]);
    }

    public function AddPaymentStatus()
    {
        $vm = new \App\Http\ViewModels\Admin\PaymentStatusViewModel();

        return view('payment_status/add', ['vm' => $vm]);
    }

    public function AddPaymentStatusPost(\App\Http\Requests\Admin\PaymentStatusRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\CreatePaymentStatus($data));

        return $this->JsonOk();
    }

    public function EditPaymentStatus($id)
    {
        $dto = \App\Lib\Queries\Admin\GetPaymentStatus::Result($id);
        $vm = new \App\Http\ViewModels\Admin\PaymentStatusEditViewModel($dto);

        return view('payment_status/edit', ['vm' => $vm]);
    }

    public function EditPaymentStatusPost(\App\Http\Requests\Admin\PaymentStatusInfoRequest $request)
    {
        $data = $request->validated();
        dispatch(new \App\Lib\Commands\Admin\UpdatePaymentStatus($data));

        return $this->JsonOk();
    }

    public function DeletePaymentStatusPost(Request $request)
    {
        $data['id'] = $request->input('id');
        if ($this->ValidateDeletePaymentStatus($request->input('id'))) {
            dispatch(new \App\Lib\Commands\Admin\DeletePaymentStatus($data));

            return $this->JsonOk();
        } else {
            return $this->JsonError('无法删除此码头。');
        }
    }

    private function ValidateDeletePaymentStatus($id)
    {
        $result = \App\Lib\Queries\Admin\GetPaymentStatus::ValidateDelete($id);

        return $result;
    }

    // Payment Transfer
    public function Payments(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'updated_at';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'desc';
        }

        if (null !== $request->input('keyword')) {
            $keyword = '%'.strtolower($request->input('keyword')).'%';
            $inputs['keyword'] = $request->input('keyword');
        } else {
            $keyword = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'keyword' => $keyword,
        ];

        $dto = \App\Lib\Queries\Admin\GetPayments::Result($params);

        // echo "<pre>";
        // print_r($dto);
        // echo "</pre>";
        $paging = \App\Lib\Queries\Admin\GetPayments::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\PaymentsViewModel($dto, $paging);

        return view('payment/list', ['vm' => $vm]);
    }

    public function AddPayment()
    {
        $vm = new \App\Http\ViewModels\Admin\PaymentViewModel();

        return view('payment/add', ['vm' => $vm]);
    }

    public function AddPaymentPost(\App\Http\Requests\Admin\PaymentRequest $request)
    {
        $data = $request->validated();
        $data['owner_id'] = Auth::user()->id;
        $data['created_by'] = Auth::user()->name;
        $data['updated_by'] = Auth::user()->name;

        if ($request->hasFile('file_customer_attachment')) {
            $data['customer_attachment'] = $request->file('file_customer_attachment')->store('downloadfile/transfer');
            unset($data['file_customer_attachment']);
        }

        // if ($request->hasFile('file_supplier_attachment')) {
        //     $data['supplier_attachment'] = $request->file('file_supplier_attachment')->store('downloadfile/transfer');
        //     unset($data['file_supplier_attachment']);
        // }

        if (isset($data['payment_date'])) {
            $data['payment_date'] = str_replace('/', '-', $data['payment_date']);
            $data['payment_date'] = date('Y-m-d', strtotime($data['payment_date']));
        }

        if (isset($data['remittance_date'])) {
            $data['remittance_date'] = str_replace('/', '-', $data['remittance_date']);
            $data['remittance_date'] = date('Y-m-d', strtotime($data['remittance_date']));
        }

        // if(isset($data['date_received'])){
        //   $data['date_received'] = str_replace('/', '-', $data['date_received']);
        //   $data['date_received'] = date('Y-m-d', strtotime($data['date_received']));
        // }

        $id = DB::table('payment')->insertGetId([
            'deleted' => 0,
            'payment_date' => $data['payment_date'],
            'remittance_date' => $data['remittance_date'],
            'status_id' => $data['status'],
            'owner_id' => $data['owner_id'],
            'customer_id' => $data['customer_id'],
            'customer_name' => $data['customer_name'],
            'customer_code' => $data['customer_code'],
            'customer_amount' => $data['customer_amount'],
            'customer_exchange_rate' => $data['customer_exchange_rate'],
            'customer_total' => $data['customer_total'],
            'purpose_payment' => empty($data['purpose_payment']) ? null : $data['purpose_payment'],
            'created_at' => now(),
            'updated_at' => now(),
            'created_by' => $data['created_by'],
            'updated_by' => $data['updated_by'],
        ]);
        // $id = dispatch(new \App\Lib\Commands\Admin\CreatePayment($data));

        return $this->JsonOk(['id' => $id]);
    }

    public function EditPayment($id)
    {
        $dto = \App\Lib\Queries\Admin\GetPayment::Result($id);
        $vm = new \App\Http\ViewModels\Admin\PaymentEditViewModel($dto);

        return view('payment/edit', ['vm' => $vm]);
    }

    public function EditPaymentPost(\App\Http\Requests\Admin\PaymentInfoRequest $request)
    {
        $data = $request->validated();

        if ($request->hasFile('file_customer_attachment')) {
            $data['customer_attachment'] = $request->file('file_customer_attachment')->store('downloadfile/transfer');
            unset($data['file_customer_attachment']);
        }

        if (isset($data['payment_date'])) {
            $data['payment_date'] = str_replace('/', '-', $data['payment_date']);
            $data['payment_date'] = date('Y-m-d', strtotime($data['payment_date']));
        }

        if (isset($data['remittance_date'])) {
            $data['remittance_date'] = str_replace('/', '-', $data['remittance_date']);
            $data['remittance_date'] = date('Y-m-d', strtotime($data['remittance_date']));
        }

        $data['updated_by'] = Auth::user()->name;

        dispatch(new \App\Lib\Commands\Admin\UpdatePayment($data));

        return $this->JsonOk();
    }

    public function DeletePaymentPost(Request $request)
    {
        $data['id'] = $request->input('id');
        dispatch(new \App\Lib\Commands\Admin\DeletePayment2($data));

        return $this->JsonOk();
    }

    public function DeletePaymentAttachmentPost(Request $request)
    {
        $data['id'] = $request->input('payment_id');
        $data['type'] = $request->input('type');
        $dto = \App\Lib\Queries\Admin\GetPayment::Result($data['id']);
        if ($data['type'] == 1) {
            Storage::delete($dto->customer_attachment);
        } elseif ($data['type'] == 2) {
            Storage::delete($dto->supplier_attachment);
        }
        dispatch(new \App\Lib\Commands\Admin\DeletePaymentAttachment($data));

        return $this->JsonOk();
    }

    // Payment Supplier
    public function GetPaymentSuppliers($id)
    {
        $dto = \App\Lib\Queries\Admin\GetPayment::GetPaymentSuppliers($id);

        return view('payment/suppliers', ['data' => $dto]);
    }

    public function GetPaymentSupplier($id)
    {
        $dto = \App\Lib\Queries\Admin\GetPayment::GetPaymentSupplier($id);

        return response()->json(['json' => $dto], 200);
    }

    public function AddPaymentSupplierPost(\App\Http\Requests\Admin\PaymentSupplierRequest $request)
    {
        $data = $request->validated();
        $data['owner_id'] = Auth::user()->id;
        $data['created_by'] = Auth::user()->name;
        $data['updated_by'] = Auth::user()->name;

        dispatch(new \App\Lib\Commands\Admin\CreatePaymentSupplier($data));

        return $this->JsonOk();
    }

    public function EditPaymentSupplierPost(\App\Http\Requests\Admin\PaymentSupplierRequest $request)
    {
        $data = $request->validated();
        $data['owner_id'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->name;

        // if ($request->hasFile('file_supplier_attachment')) {
        //     $data['supplier_attachment'] = $request->file('file_supplier_attachment')->store('downloadfile/transfer');
        //     unset($data['file_supplier_attachment']);
        // }
        //
        // if(isset($data['received_date'])){
        //   $data['received_date'] = str_replace('/', '-', $data['received_date']);
        //   $data['received_date'] = date('Y-m-d', strtotime($data['received_date']));
        // }

        dispatch(new \App\Lib\Commands\Admin\UpdatePaymentSupplier($data));

        return $this->JsonOk();
    }

    public function DeletePaymentSupplierPost(Request $request)
    {
        $data['id'] = $request->input('id');
        dispatch(new \App\Lib\Commands\Admin\DeletePaymentSupplier($data));

        return $this->JsonOk();
    }

    public function DeletePaymentSupplierAttachmentPost(Request $request)
    {
        $data['id'] = $request->input('payment_supplier_id');
        $dto = \App\Lib\Queries\Admin\GetPayment::GetPaymentSupplier($data['id']);
        Storage::delete($dto->supplier_attachment);
        dispatch(new \App\Lib\Commands\Admin\DeletePaymentSupplierAttachment($data));

        return $this->JsonOk();
    }

    // Payment Beneficiary
    public function AddPaymentBeneficiaryPost(\App\Http\Requests\Admin\PaymentBeneficiaryRequest $request)
    {
        $data = $request->validated();
        if ($request->hasFile('file_attachment')) {
            $filename = date('YmdHi').$this->generateRandomString(8);
            $ext = $request->file('file_attachment')->guessExtension();
            if ($ext == 'zip') {
                $ext = 'xlsx';
            }
            $data['attachment'] = $request->file('file_attachment')->storeAs('downloadfile/transfer', $filename.'.'.$ext);
            // $data['attachment'] = $request->file('file_attachment')->store('downloadfile/transfer');
            unset($data['file_attachment']);
        }

        if (isset($data['received_date'])) {
            $data['received_date'] = str_replace('/', '-', $data['received_date']);
            $data['received_date'] = date('Y-m-d', strtotime($data['received_date']));
        }

        dispatch(new \App\Lib\Commands\Admin\CreatePaymentBeneficiary($data));

        return $this->JsonOk();
    }

    public function EditPaymentBeneficiaryPost(\App\Http\Requests\Admin\PaymentBeneficiaryRequest $request)
    {
        $data = $request->validated();
        $data['owner_id'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->name;

        if ($request->hasFile('file_attachment')) {
            $data['attachment'] = $request->file('file_attachment')->store('downloadfile/transfer');
            unset($data['file_attachment']);
        }

        if (isset($data['received_date'])) {
            $data['received_date'] = str_replace('/', '-', $data['received_date']);
            $data['received_date'] = date('Y-m-d', strtotime($data['received_date']));
        }

        dispatch(new \App\Lib\Commands\Admin\UpdatePaymentBeneficiary($data));

        return $this->JsonOk();
    }

    public function DeletePaymentBeneficiaryPost(Request $request)
    {
        $data['id'] = $request->input('payment_beneficiary_id');
        dispatch(new \App\Lib\Commands\Admin\DeletePaymentBeneficiary($data));

        return $this->JsonOk();
    }

    public function GetPaymentBeneficiary($id)
    {
        $dto = \App\Lib\Queries\Admin\GetPayment::GetPaymentBeneficiary($id);

        return response()->json(['json' => $dto], 200);
    }

    public function DeletePaymentBeneficiaryAttachmentPost(Request $request)
    {
        $data['id'] = $request->input('payment_beneficiary_id');
        $dto = \App\Lib\Queries\Admin\GetPayment::GetPaymentBeneficiary($data['id']);
        Storage::delete($dto->attachment);
        dispatch(new \App\Lib\Commands\Admin\DeletePaymentBeneficiaryAttachment($data));

        return $this->JsonOk();
    }

    // Cargo
    public function Cargos(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'updated_at';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'desc';
        }

        if (null !== $request->input('keyword')) {
            $keyword = '%'.strtolower($request->input('keyword')).'%';
            $inputs['keyword'] = $request->input('keyword');
        } else {
            $keyword = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'keyword' => $keyword,
        ];

        $dto = \App\Lib\Queries\Admin\GetCargos::Result($params);

        $paging = \App\Lib\Queries\Admin\GetCargos::Paging($params);
        $vm = new \App\Http\ViewModels\Admin\CargosViewModel($dto, $paging);

        return view('cargo/list', ['vm' => $vm]);
    }

    public function AddCargo()
    {
        $vm = new \App\Http\ViewModels\Admin\CargoViewModel();

        return view('cargo/add', ['vm' => $vm]);
    }

    public function AddCargoPost(\App\Http\Requests\Admin\CargoRequest $request)
    {
        $data = $request->validated();
        // $data['owner_id'] = Auth::user()->id;
        $data['created_by'] = Auth::user()->name;
        $data['updated_by'] = Auth::user()->name;

        if (isset($data['date'])) {
            $data['date'] = str_replace('/', '-', $data['date']);
            $data['date'] = date('Y-m-d', strtotime($data['date']));
        }

        if (isset($data['arrival_date'])) {
            $data['arrival_date'] = str_replace('/', '-', $data['arrival_date']);
            $data['arrival_date'] = date('Y-m-d', strtotime($data['arrival_date']));
        }

        if (isset($data['loading_date'])) {
            $data['loading_date'] = str_replace('/', '-', $data['loading_date']);
            $data['loading_date'] = date('Y-m-d', strtotime($data['loading_date']));
        }

        $id = DB::table('cargo')->insertGetId([
            'deleted' => 0,
            'date' => $data['date'],
            'arrival_date' => empty($data['arrival_date']) ? null : $data['arrival_date'],
            'loading_date' => empty($data['loading_date']) ? null : $data['loading_date'],
            'status_id' => $data['status_id'],
            'owner_id' => $data['owner_id'],
            'customer_id' => $data['customer_id'],
            'customs_broker_id' => empty($data['customs_broker_id']) ? null : $data['customs_broker_id'],
            'warehouse_id' => $data['warehouse_id'],
            'tracking_no' => empty($data['tracking_no']) ? null : $data['tracking_no'],
            'supplier_inv_no' => empty($data['supplier_inv_no']) ? null : $data['supplier_inv_no'],
            'cbm' => empty($data['cbm']) ? null : $data['cbm'],
            'supplier_inv_amount' => empty($data['supplier_inv_amount']) ? null : $data['supplier_inv_amount'],
            'pack' => empty($data['pack']) ? null : $data['pack'],
            'supplier_goods_inv' => empty($data['supplier_goods_inv']) ? null : $data['supplier_goods_inv'],
            'supplier_goods_amount' => empty($data['supplier_goods_amount']) ? null : $data['supplier_goods_amount'],
            'customer_goods_inv' => empty($data['customer_goods_inv']) ? null : $data['customer_goods_inv'],
            'customer_goods_amount' => empty($data['customer_goods_amount']) ? null : $data['customer_goods_amount'],
            'transport_inv' => empty($data['transport_inv']) ? null : $data['transport_inv'],
            'transport_inv_amount' => empty($data['transport_inv_amount']) ? null : $data['transport_inv_amount'],
            'created_at' => now(),
            'updated_at' => now(),
            'created_by' => $data['created_by'],
            'updated_by' => $data['updated_by'],
        ]);

        return $this->JsonOk(['id' => $id]);
    }

    public function EditCargo($id)
    {
        $dto = \App\Lib\Queries\Admin\GetCargo::Result($id);
        $vm = new \App\Http\ViewModels\Admin\CargoEditViewModel($dto);

        return view('cargo/edit', ['vm' => $vm]);
    }

    public function EditCargoPost(\App\Http\Requests\Admin\CargoInfoRequest $request)
    {
        $data = $request->validated();

        $data['updated_by'] = Auth::user()->name;

        if (isset($data['date'])) {
            $data['date'] = str_replace('/', '-', $data['date']);
            $data['date'] = date('Y-m-d', strtotime($data['date']));
        }

        if (isset($data['arrival_date'])) {
            $data['arrival_date'] = str_replace('/', '-', $data['arrival_date']);
            $data['arrival_date'] = date('Y-m-d', strtotime($data['arrival_date']));
        }

        if (isset($data['loading_date'])) {
            $data['loading_date'] = str_replace('/', '-', $data['loading_date']);
            $data['loading_date'] = date('Y-m-d', strtotime($data['loading_date']));
        }

        dispatch(new \App\Lib\Commands\Admin\UpdateCargo($data));

        return $this->JsonOk();
    }

    public function DeleteCargoPost(Request $request)
    {
        $data['id'] = $request->input('id');
        dispatch(new \App\Lib\Commands\Admin\DeleteCargo($data));

        return $this->JsonOk();
    }

    public function CopyCargo($id)
    {
        $dto = \App\Lib\Queries\Admin\GetCargo::Result($id);
        $vm = new \App\Http\ViewModels\Admin\CargoEditViewModel($dto);

        return view('cargo/copy', ['vm' => $vm]);
    }

    // Report
    public function ReportOverview()
    {
        return view('report/overview');
    }

    public function ReportStaffCommission(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        // if(null !== ($request->input('date_from'))){
        //   $dtf = str_replace('/', '-', $request->input('date_from'));
        //   $date_from = date('Y-m-d', strtotime($dtf));
        //   $inputs['date_from'] = $request->input('date_from');
        // }else{
        //   $date_from = date('Y-m-1');
        //   $inputs['date_from'] = date('01/m/Y');
        // }
        //
        // if(null !== ($request->input('date_to'))){
        //   $dtt = str_replace('/', '-', $request->input('date_to'));
        //   // $date_to =  date('Y-m-d', strtotime($dtt . ' + 1 days'));
        //   $date_to =  date('Y-m-d', strtotime($dtt));
        //   $inputs['date_to'] = $request->input('date_to');
        // }else{
        //   $date_to = date('Y-m-d');
        //   $inputs['date_to'] = date('d/m/Y');
        // }

        // if(null !== ($request->input('customer_id'))){
        //   $customer_id = $request->input('customer_id');
        //   $inputs['customer_id'] = $request->input('customer_id');
        // }else{
        //   $customer_id = '';
        //   $inputs['customer_id'] = '';
        // }
        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = date('Y');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        if (null !== $request->input('employee_id')) {
            $employee_id = $request->input('employee_id');
            $inputs['employee_id'] = $request->input('employee_id');
        } else {
            $employee_id = '';
            $inputs['employee_id'] = '';
            $inputs['employee'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            // 'customer_id' => $customer_id,
            'employee_id' => $employee_id,
        ];

        $header = ['No.', '装柜日期', '客户名称', '货柜号码', '预订号'];
        $footer = [];
        $data = [];

        $employees = \App\Lib\Queries\Admin\GetEmployees::Status(1);

        if ($employee_id != '') {
            $dto = \App\Lib\Queries\Admin\GetReportStaffCommissionEmployee::Result($params);
            // $employees = \App\Lib\Queries\Admin\GetEmployees::Status(1);
            $employee = \App\Lib\Queries\Admin\GetUser::Result($employee_id);
        // array_push($header,$employee->name);
        } else {
            $dto = \App\Lib\Queries\Admin\GetReportStaffCommission::Result($params);
            // foreach($employees as $employee){
            //   array_push($header,$employee->name);
            // }
        }

        if (!empty($dto)) {
            foreach ($dto as $k => $v) {
                $workers = [];

                if ($employee_id != '') {
                    $jobs = '';
                    foreach ($v->employees as $e) {
                        $jobs .= $e->job.'<br/> ';
                    }
                    $workers[$employee_id] = $jobs;
                } else {
                    foreach ($employees as $ey) {
                        $jobs = '';

                        foreach ($v->employees as $e) {
                            if ($ey->user_id == $e->employee_id) {
                                $jobs .= $e->job.'<br/>';
                            }
                        }

                        $jobs = rtrim($jobs, '<br/>');
                        $workers[$ey->user_id] = $jobs;
                    }
                }
                // echo "<pre>";
                // print_r($workers);
                // echo "</pre>";

                $data[] = [
                    'no' => $k + 1,
                    'loading_date' => $v->loading_date,
                    'customer_name' => $v->customer_name,
                    'container_no' => $v->container_no,
                    'booking_no' => $v->booking_no,
                    'employees' => $workers,
                ];
            }
        }

        if ($employee_id == '') {
            foreach ($employees as $e) {
                $j = [];
                foreach ($dto as $d) {
                    foreach ($d->employees as $ee) {
                        if ($ee->employee_id == $e->user_id) {
                            $j[] = $ee->job;
                        }
                    }
                }
                // echo "<pre>";
                // print_r($j);
                // echo "</pre>";
                if (!empty($j)) {
                    $footer[$e->user_id] = [
                        'user_id' => $e->user_id,
                        'name' => $e->name,
                        'job' => array_count_values($j),
                    ];
                }
            }
        } else {
            $employee = \App\Lib\Queries\Admin\GetUser::Result($employee_id);
            $inputs['employee'] = ' - '.$employee->name;
            $j = [];
            foreach ($dto as $d) {
                foreach ($d->employees as $ee) {
                    if ($ee->employee_id == $employee_id) {
                        $j[] = $ee->job;
                    }
                }
            }

            // echo "<pre>";
            // print_r($j);
            // echo "</pre>";
            if (!empty($j)) {
                $footer[$employee_id] = [
                    'user_id' => $employee_id,
                    'name' => $employee->name,
                    'job' => array_count_values($j),
                ];
            }
        }

        // get only got job employee
        foreach ($footer as $ft) {
            if (count($ft['job']) > 0) {
                array_push($header, $ft['name']);
            }
        }
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";

        // echo "<pre>";
        // print_r($header);
        // echo "</pre>";
        //
        // echo "<pre>";
        // print_r($footer);
        // echo "</pre>";
        $vm = new \App\Http\ViewModels\Admin\ReportStaffCommissionViewModel($data, $inputs, $header, $footer);

        return view('report/staff_commission', ['vm' => $vm]);
    }

    public function ReportBookingOrder(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'booking_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            // $date_to =  date('Y-m-d', strtotime($dtt . ' + 1 days'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            // $date_to = date('Y-m-d', strtotime(date('Y-m-d'). ' + 1 days'));
            $date_to = date('Y-m-d', strtotime(date('Y-m-d').' + 14 days'));
            $inputs['date_to'] = date('d/m/Y', strtotime(date('Y-m-d').' + 14 days'));
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportBookingOrder::Result($params);

        $vm = new \App\Http\ViewModels\Admin\ReportBookingOrderViewModel($dto, $inputs);

        return view('report/booking_order', ['vm' => $vm]);
    }

    public function ReportContainerLoading(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = $request->input('year');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        // if(null !== ($request->input('date_from'))){
        //   $dtf = str_replace('/', '-', $request->input('date_from'));
        //   $date_from = date('Y-m-d', strtotime($dtf));
        //   $inputs['date_from'] = $request->input('date_from');
        // }else{
        //   $date_from = date('Y-m-1');
        //   $inputs['date_from'] = date('01/m/Y');
        // }
        //
        // if(null !== ($request->input('date_to'))){
        //   $dtt = str_replace('/', '-', $request->input('date_to'));
        //   $date_to =  date('Y-m-d', strtotime($dtt));
        //   $inputs['date_to'] = $request->input('date_to');
        // }else{
        //   $date_to = date('Y-m-d');
        //   $inputs['date_to'] = date('d/m/Y');
        // }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = [];
            $inputs['customer_id'] = [];
        }

        if (null !== $request->input('agent_id')) {
            $agent_id = $request->input('agent_id');
            $inputs['agent_id'] = $request->input('agent_id');
            $agent = \App\Lib\Queries\Admin\GetAgent::Result($agent_id);
            $inputs['agent'] = ' - '.$agent->name;
        } else {
            $agent_id = '';
            $inputs['agent_id'] = '';
            $inputs['agent'] = '';
        }

        if (null !== $request->input('user_id')) {
            $user_id = $request->input('user_id');
            $inputs['user_id'] = $request->input('user_id');
        } else {
            $user_id = '';
            $inputs['user_id'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
            'agent_id' => $agent_id,
            'user_id' => $user_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportContainerLoading::Result($params);
        $mth = $this->GetChineseMonthName($month);
        $title = $year.'年 '.$mth.'份';
        $vm = new \App\Http\ViewModels\Admin\ReportContainerLoadingViewModel($dto, $inputs, $title);

        return view('report/container_loading', ['vm' => $vm]);
    }

    public function ReportBillingCompany(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = $request->input('year');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        if (null !== $request->input('billing_company_id')) {
            $billing_company_id = $request->input('billing_company_id');

            $inputs['billing_company_id'] = $request->input('billing_company_id');
            $company = '';
            $billing_companies = [];
            if (!empty($billing_company_id)) {
                foreach ($billing_company_id as $k => $v) {
                    if ($v != '') {
                        $company = $company.' '.$v;
                        $billing_company = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($v);
                        $billing_companies[] = $billing_company->id;
                    }
                }
            }
            // echo "<pre>";
            // print_r($billing_companies);
            // echo "</pre>";
            $title = $company.' Container list for the month of '.strtoupper($mth).' '.$year;
        } else {
            $billing_company_id = [];
            $inputs['billing_company_id'] = [];
            $billing_companies = [];
            $title = 'All Container list for the month of  '.strtoupper($mth).' '.$year;
        }
        // if(null !== ($request->input('billing_company_id'))){
        //   $billing_company_id = '%' . $request->input('billing_company_id') . '%';
        //   $inputs['billing_company_id'] = $request->input('billing_company_id');
        //   $billing_company  = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($request->input('billing_company_id'));
        //
        //   if(!empty($billing_company)){
        //     $company = $billing_company->name;
        //   }else{
        //     $company = "";
        //   }
        //
        //   $title = $company . ' For the month ' . strtoupper($mth) . ' ' . $year;
        //
        // }else{
        //   $billing_company_id = '';
        //   $inputs['billing_company_id'] = '';
        //   $title = 'All company For the month ' . strtoupper($mth) . ' ' .  $year;
        // }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'billing_company_id' => $billing_company_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportBillingCompany::Result($params);
        $company_inv_amount = \App\Lib\Queries\Admin\GetReportBillingCompany::GetCompanyInvAmount($params);
        $supplier_inv_amount = \App\Lib\Queries\Admin\GetReportBillingCompany::GetSupplierInvAmount($params);
        $net_profit = $company_inv_amount - $supplier_inv_amount;

        $vm = new \App\Http\ViewModels\Admin\ReportBillingCompanyViewModel($dto, $inputs, $title, number_format($company_inv_amount, 2, '.', ','), number_format($supplier_inv_amount, 2, '.', ','), number_format($net_profit, 2, '.', ','));

        return view('report/billing_company', ['vm' => $vm]);
    }

    public function ReportMonthlyStatement(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = $request->input('year');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
        }

        if (null !== $request->input('billing_company_id')) {
            $billing_company_id = $request->input('billing_company_id');

            $inputs['billing_company_id'] = $request->input('billing_company_id');
            $company = '';
            $billing_companies = [];
            if (!empty($billing_company_id)) {
                foreach ($billing_company_id as $k => $v) {
                    if ($v != '') {
                        $company = $company.' '.$v;
                        $billing_company = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($v);
                        $billing_companies[] = $billing_company->id;
                    }
                }
            }
            // echo "<pre>";
            // print_r($billing_companies);
            // echo "</pre>";
            $title = $company.' Container list for the month of '.strtoupper($mth).' '.$year;
        } else {
            $billing_company_id = [];
            $inputs['billing_company_id'] = [];
            $billing_companies = [];
            $title = 'All Container list for the month of  '.strtoupper($mth).' '.$year;
        }

        if (null !== $request->input('rate')) {
            $rate = $request->input('rate');
            $inputs['rate'] = $request->input('rate');
        } else {
            $rate = $this->CurrentCurrencyRate();
            $inputs['rate'] = $rate;
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
            'billing_company_id' => $billing_company_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportMonthlyStatement::Result($params);

        // echo "<pre>";
        // print_r($dto);
        // echo "</pre>";
        $total_net_total = 0;
        $total_total2 = 0;
        $total_billing_handling = 0;
        $total_customs_handling = 0;
        $total_billing_other = 0;
        $total_customs_other = 0;
        $total_handling_charges = 0;
        $total_commission = 0;
        $total_freight_charges = 0;
        $total_net_freight_charges = 0;
        $total_loadingfee = 0;

        foreach ($dto as $d) {
            $net_total = $d->total + ($d->total_charges / $rate);
            $total_net_total += $net_total;
            $total_total2 += $d->total_charges;
            // total
            $total_billing_handling += $d->billing_handling_prices;
            $total_customs_handling += $d->custom_handling_prices;
            $total_billing_other += $d->billing_other_prices;
            $total_customs_other += $d->custom_other_prices;
            $total_handling_charges += $d->handling_charges;
            $total_commission += $d->commission;
            $total_freight_charges += $d->freight_charges;
            $total_net_freight_charges += $d->net_freight_charges;
            $total_loadingfee += $d->container_loading_charges;

            $d->custom_handling_prices = number_format($d->custom_handling_prices, 2);
            $d->billing_handling_prices = number_format($d->billing_handling_prices, 2);
            $d->custom_other_prices = number_format($d->custom_other_prices, 2);
            $d->billing_other_prices = number_format($d->billing_other_prices, 2);
            $d->handling_charges = number_format($d->handling_charges, 2);
            $d->commission = number_format($d->commission, 2);
            $d->freight_charges = number_format($d->freight_charges, 2);
            $d->net_freight_charges = number_format($d->net_freight_charges, 2);
            $d->container_loading_charges = number_format($d->container_loading_charges, 2);
            $d->total = number_format($d->total, 2);
            $d->total_charges = number_format($d->total_charges, 2);
            $d->net_total = number_format($net_total, 2);
        }

        // $total_billing_handling = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalBillingHandling($params);
        // $total_customs_handling = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalCustomsHandling($params);
        // $total_billing_other = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalBillingOther($params);
        // $total_customs_other = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalCustomsOther($params);
        $total_total = ($total_billing_handling + $total_billing_other) - ($total_customs_handling + $total_customs_other);

        // $handling_charges = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalHandlingCharges($params);
        // $commission = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalCommission($params);
        // $freight_charges = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalFreightCharges($params);
        // $loadingfee = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalLoadingFee($params);

        // $total_total2 = $handling_charges + $commission - $freight_charges - $loadingfee;
        $total = [
            'billing_handling' => number_format($total_billing_handling, 2),
            'billing_other' => number_format($total_billing_other, 2),
            'customs_handling' => number_format($total_customs_handling, 2),
            'customs_other' => number_format($total_customs_other, 2),
            'total' => number_format($total_total, 2),
            'handling_charges' => number_format($total_handling_charges, 2),
            'commission' => number_format($total_commission, 2),
            'freight_charges' => number_format($total_freight_charges, 2),
            'net_freight_charges' => number_format($total_net_freight_charges, 2),
            'loadingfee' => number_format($total_loadingfee, 2),
            'total_charges' => number_format($total_total2, 2),
            'net_total' => number_format($total_net_total, 2),
        ];

        $vm = new \App\Http\ViewModels\Admin\ReportMonthlyStatementViewModel($dto, $total, $inputs, $title, $rate);

        return view('report/monthly_statement', ['vm' => $vm]);
    }

    public function ReportYearlyStatement(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = $request->input('year');
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
            $_customer = \App\Lib\Queries\Admin\GetCustomer::Result($customer_id);
            if (!empty($_customer)) {
                $customer = $_customer->name;
            } else {
                $customer = '';
            }
        } else {
            $customer_id = '';
            $customer = '';
            $inputs['customer_id'] = '';
        }

        if (null !== $request->input('billing_company_id')) {
            $billing_company_id = $request->input('billing_company_id');

            $inputs['billing_company_id'] = $request->input('billing_company_id');
            $company = '';
            $billing_companies = [];
            if (!empty($billing_company_id)) {
                foreach ($billing_company_id as $k => $v) {
                    if ($v != '') {
                        $company = $company.' '.$v;
                        $billing_company = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($v);
                        $billing_companies[] = $billing_company->id;
                    }
                }
            }
        } else {
            $billing_company_id = [];
            $inputs['billing_company_id'] = [];
            $billing_companies = [];
        }

        if (null !== $request->input('rate')) {
            $rate = $request->input('rate');
            $inputs['rate'] = $request->input('rate');
        } else {
            $rate = $this->CurrentCurrencyRate();
            $inputs['rate'] = $rate;
        }

        $title = $customer.' Container list for the year of '.$year;

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'year' => $year,
            'customer_id' => $customer_id,
            'billing_company_id' => $billing_company_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportYearlyStatement::Result($params);

        $total_net_total = 0;
        $total_total2 = 0;
        $total_billing_handling = 0;
        $total_customs_handling = 0;
        $total_billing_other = 0;
        $total_customs_other = 0;
        $total_handling_charges = 0;
        $total_commission = 0;
        $total_freight_charges = 0;
        $total_net_freight_charges = 0;
        $total_loadingfee = 0;

        foreach ($dto as $d) {
            $net_total = $d->total + ($d->total_charges / $rate);
            $total_net_total += $net_total;
            $total_total2 += $d->total_charges;
            // total
            $total_billing_handling += $d->billing_handling_prices;
            $total_customs_handling += $d->custom_handling_prices;
            $total_billing_other += $d->billing_other_prices;
            $total_customs_other += $d->custom_other_prices;
            $total_handling_charges += $d->handling_charges;
            $total_commission += $d->commission;
            $total_freight_charges += $d->freight_charges;
            $total_net_freight_charges += $d->net_freight_charges;
            $total_loadingfee += $d->container_loading_charges;

            $d->custom_handling_prices = number_format($d->custom_handling_prices, 2);
            $d->billing_handling_prices = number_format($d->billing_handling_prices, 2);
            $d->custom_other_prices = number_format($d->custom_other_prices, 2);
            $d->billing_other_prices = number_format($d->billing_other_prices, 2);
            $d->handling_charges = number_format($d->handling_charges, 2);
            $d->commission = number_format($d->commission, 2);
            $d->freight_charges = number_format($d->freight_charges, 2);
            $d->net_freight_charges = number_format($d->net_freight_charges, 2);
            $d->container_loading_charges = number_format($d->container_loading_charges, 2);
            $d->total = number_format($d->total, 2);
            $d->total_charges = number_format($d->total_charges, 2);
            $d->net_total = number_format($net_total, 2);
        }

        $total_total = ($total_billing_handling + $total_billing_other) - ($total_customs_handling + $total_customs_other);

        $total = [
            'billing_handling' => number_format($total_billing_handling, 2),
            'billing_other' => number_format($total_billing_other, 2),
            'customs_handling' => number_format($total_customs_handling, 2),
            'customs_other' => number_format($total_customs_other, 2),
            'total' => number_format($total_total, 2),
            'handling_charges' => number_format($total_handling_charges, 2),
            'commission' => number_format($total_commission, 2),
            'freight_charges' => number_format($total_freight_charges, 2),
            'net_freight_charges' => number_format($total_net_freight_charges, 2),
            'loadingfee' => number_format($total_loadingfee, 2),
            'total_charges' => number_format($total_total2, 2),
            'net_total' => number_format($total_net_total, 2),
        ];

        $vm = new \App\Http\ViewModels\Admin\ReportYearlyStatementViewModel($dto, $total, $inputs, $title, $rate);

        return view('report/yearly_statement', ['vm' => $vm]);
    }

    public function ReportDailyPaymentTransfer(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'payment_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d');
            $inputs['date_to'] = date('d/m/Y');
        }

        if (null !== $request->input('supplier_id')) {
            $supplier_id = $request->input('supplier_id');
            $inputs['supplier_id'] = $request->input('supplier_id');
            $supplier = \App\Lib\Queries\Admin\GetSupplier::Result($supplier_id);
            $inputs['supplier'] = ' - '.$supplier->name;
        } else {
            $supplier_id = '';
            $inputs['supplier_id'] = '';
            $inputs['supplier'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'supplier_id' => $supplier_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::Result($params);
        $total_customer_amount = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalCustomerAmount($params);
        $total_customer_total = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalCustomerTotal($params);
        $total_supplier_amount = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalSupplierAmount($params);
        $total_supplier_total = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalSupplierTotal($params);
        $total_supplier_bank_charges = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalSupplierBankCharges($params);
        $total_supplier_net_amount = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalSupplierNetAmount($params);
        $total = [
            'total_customer_amount' => number_format($total_customer_amount, 2),
            'total_customer_total' => number_format($total_customer_total, 2),
            'total_supplier_amount' => number_format($total_supplier_amount, 2),
            'total_supplier_total' => number_format($total_supplier_total, 2),
            'total_supplier_bank_charges' => number_format($total_supplier_bank_charges, 2),
            'total_supplier_net_amount' => number_format($total_supplier_net_amount, 2),
        ];

        $vm = new \App\Http\ViewModels\Admin\ReportDailyPaymentTransferViewModel($dto, $total, $inputs);

        return view('report/daily_payment_transfer', ['vm' => $vm]);
    }

    public function ReportDailyPayment(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'payment_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d');
            $inputs['date_to'] = date('d/m/Y');
        }

        if (null !== $request->input('supplier_id')) {
            $supplier_id = $request->input('supplier_id');
            $inputs['supplier_id'] = $request->input('supplier_id');
            $supplier = \App\Lib\Queries\Admin\GetCustomsBroker::Result($supplier_id);
            // echo "<pre>";
            // print_r($supplier);
            // echo "</pre>";
            $inputs['supplier'] = ' - '.$supplier->name;
        } else {
            $supplier_id = '';
            $inputs['supplier_id'] = '';
            $inputs['supplier'] = '';
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        // $customer = \App\Lib\Queries\Admin\GetCustomer::Result($customer_id);
        // $inputs['customer'] = ' - ' .$customer->name;
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
            // $inputs['customer'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'supplier_id' => $supplier_id,
            'customer_id' => $customer_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportDailyPayment::Result($params);
        $total_customer_amount = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalCustomerAmount($params);
        $total_customer_total = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalCustomerTotal($params);
        $total_supplier_amount = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalSupplierAmount($params);
        $total_supplier_total = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalSupplierTotal($params);
        $total_supplier_bank_charges = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalSupplierBankCharges($params);
        $total_supplier_net_amount = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalSupplierNetAmount($params);
        $total = [
            'total_customer_amount' => number_format($total_customer_amount, 2),
            'total_customer_total' => number_format($total_customer_total, 2),
            'total_supplier_amount' => number_format($total_supplier_amount, 2),
            'total_supplier_total' => number_format($total_supplier_total, 2),
            'total_supplier_bank_charges' => number_format($total_supplier_bank_charges, 2),
            'total_supplier_net_amount' => number_format($total_supplier_net_amount, 2),
        ];

        $vm = new \App\Http\ViewModels\Admin\ReportDailyPaymentViewModel($dto, $total, $inputs);

        return view('report/daily_payment', ['vm' => $vm]);
    }

    public function ReportCargoRecord(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d');
            $inputs['date_to'] = date('d/m/Y');
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
        }

        if (null !== $request->input('customs_broker_id')) {
            $customs_broker_id = $request->input('customs_broker_id');
            $inputs['customs_broker_id'] = $request->input('customs_broker_id');
        } else {
            $customs_broker_id = '';
            $inputs['customs_broker_id'] = '';
        }

        if (null !== $request->input('owner_id')) {
            $owner_id = $request->input('owner_id');
            $inputs['owner_id'] = $request->input('owner_id');
        } else {
            $owner_id = '';
            $inputs['owner_id'] = '';
        }

        if (null !== $request->input('warehouse_id')) {
            $warehouse_id = $request->input('warehouse_id');
            $inputs['warehouse_id'] = $request->input('warehouse_id');
        } else {
            $warehouse_id = '';
            $inputs['warehouse_id'] = '';
        }

        if (null !== $request->input('tracking_no')) {
            $tracking_no = $request->input('tracking_no');
            $inputs['tracking_no'] = $request->input('tracking_no');
        } else {
            $tracking_no = '';
            $inputs['tracking_no'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
            'customs_broker_id' => $customs_broker_id,
            'owner_id' => $owner_id,
            'warehouse_id' => $warehouse_id,
            'tracking_no' => $tracking_no,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportCargoRecord::Result($params);

        $total_cbm = 0;
        $total_supplier_inv_amount = 0;
        $total_supplier_goods_amount = 0;
        $total_customer_goods_amount = 0;
        $total_transport_inv_amount = 0;

        foreach ($dto as $d) {
            // total
            $total_cbm += $d->cbm;
            $total_supplier_inv_amount += $d->supplier_inv_amount;
            $total_supplier_goods_amount += $d->supplier_goods_amount;
            $total_customer_goods_amount += $d->customer_goods_amount;
            $total_transport_inv_amount += $d->transport_inv_amount;
        }

        $net_total_supplier_goods_amount = $total_customer_goods_amount - $total_supplier_goods_amount;

        $net_total_transport_inv_amount = $total_transport_inv_amount - $total_supplier_inv_amount;

        $total = [
            'cbm' => number_format($total_cbm, 3),
            'supplier_inv_amount' => number_format($total_supplier_inv_amount, 2),
            'supplier_goods_amount' => number_format($total_supplier_goods_amount, 2),
            'customer_goods_amount' => number_format($total_customer_goods_amount, 2),
            'transport_inv_amount' => number_format($total_transport_inv_amount, 2),
            'net_total_supplier_goods_amount' => number_format($net_total_supplier_goods_amount, 2),
            'net_total_transport_inv_amount' => number_format($net_total_transport_inv_amount, 2),
        ];

        $title = $inputs['date_from'].' - '.$inputs['date_to'];
        $vm = new \App\Http\ViewModels\Admin\ReportCargoRecordViewModel($dto, $total, $inputs, $title);

        return view('report/cargo_record', ['vm' => $vm]);
    }

    public function ReportBalanceSheetCustomer(Request $request)
    {
        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');

            if (null !== $request->input('year')) {
                $year = $request->input('year');
                $inputs['year'] = $request->input('year');
            } else {
                $year = date('Y');
                $inputs['year'] = date('Y');
            }

            $params[] = [
                'customer_id' => $customer_id,
                'year' => $year,
            ];

            // Calculate existing year balance
            $inAmt = \App\Lib\Queries\Admin\GetReportBalanceSheetCustomer::GetTotalInAmount($params);
            $outAmt = \App\Lib\Queries\Admin\GetReportBalanceSheetCustomer::GetTotalOutAmount($params);

            $dto = \App\Lib\Queries\Admin\GetReportBalanceSheetCustomer::GetInRecords($params);
            $dto2 = \App\Lib\Queries\Admin\GetReportBalanceSheetCustomer::GetOutRecords($params);

            foreach ($dto as $d) {
                $d->type = 'in';
            }

            foreach ($dto2 as $d) {
                $d->type = 'out';
            }
            $data = [];
            $data = array_merge($dto, $dto2);
            usort($data, function ($dto, $dto2) {
                return strtotime($dto->date) - strtotime($dto2->date);
            });

            $customer = \App\Lib\Queries\Admin\GetCustomer::Result($customer_id);
            $balance = $customer->initial_balance;
            $initial_balance = $customer->initial_balance;

            $balance = $balance + $inAmt - $outAmt;
            $initial_balance = $initial_balance + $inAmt - $outAmt;
            // $initial_balance = number_format($initial_balance, 2);
            if ($initial_balance < 0) {
                $initial_balance = '('.str_replace('-', '', number_format($initial_balance, 2)).')';
            } else {
                $initial_balance = number_format($initial_balance, 2);
            }
            foreach ($data as $d) {
                if ($d->type == 'in') {
                    $d->balance = $balance + $d->in;
                    $d->in = number_format($d->in, 2);
                    $d->out = '';
                } elseif ($d->type == 'out') {
                    $d->balance = $balance - $d->out;
                    $d->in = '';
                    $d->out = number_format($d->out, 2);
                } else {
                    $d->in = '';
                    $d->out = '';
                }

                $balance = $d->balance;
                $d->balance = number_format($d->balance, 2);
                if ($d->balance < 0) {
                    $d->balance = '('.str_replace('-', '', $d->balance).')';
                }
                $d->date = date('d/m/Y', strtotime($d->date));
            }

            $title = $customer->name;
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
            $inputs['year'] = date('Y');
            $title = '';
            $data = [];
            $initial_balance = '';
        }

        $vm = new \App\Http\ViewModels\Admin\ReportBalanceSheetCustomerViewModel($data, $inputs, $title, $initial_balance);

        return view('report/balance_sheet_customer', ['vm' => $vm]);
    }

    public function ReportBalanceSheetSupplier(Request $request)
    {
        if (null !== $request->input('customsbroker_id')) {
            $customsbroker_id = $request->input('customsbroker_id');
            $inputs['customsbroker_id'] = $request->input('customsbroker_id');

            if (null !== $request->input('billing_company_id')) {
                $billing_company_id = $request->input('billing_company_id');
                $inputs['billing_company_id'] = $request->input('billing_company_id');
            } else {
                $billing_company_id = '';
                $inputs['billing_company_id'] = '';
            }

            if (null !== $request->input('year')) {
                $year = $request->input('year');
                $inputs['year'] = $request->input('year');
            } else {
                $year = date('Y');
                $inputs['year'] = date('Y');
            }

            $params[] = [
                'customsbroker_id' => $customsbroker_id,
                'billing_company_id' => $billing_company_id,
                'year' => $year,
            ];

            // Calculate existing year balance
            $inAmt = \App\Lib\Queries\Admin\GetReportBalanceSheetSupplier::GetTotalInAmount($params);
            $outAmt = \App\Lib\Queries\Admin\GetReportBalanceSheetSupplier::GetTotalOutAmount($params);

            $dto = \App\Lib\Queries\Admin\GetReportBalanceSheetSupplier::GetInRecords($params);
            $dto2 = \App\Lib\Queries\Admin\GetReportBalanceSheetSupplier::GetOutRecords($params);

            foreach ($dto as $d) {
                $d->type = 'in';
            }

            foreach ($dto2 as $d) {
                $d->type = 'out';
            }
            $data = [];
            $data = array_merge($dto, $dto2);
            usort($data, function ($dto, $dto2) {
                return strtotime($dto->date) - strtotime($dto2->date);
            });

            $customsbroker = \App\Lib\Queries\Admin\GetCustomsBroker::Result($customsbroker_id);

            if ($billing_company_id != '') {
                $balance = $this->GetSupplierInitialValue($customsbroker->balance, $billing_company_id);
                $initial_balance = $this->GetSupplierInitialValue($customsbroker->balance, $billing_company_id);
            } else {
                $_bal = 0;
                if (!empty($customsbroker->balance)) {
                    foreach ($customsbroker->balance as $ib) {
                        $_bal += $ib->balance;
                    }
                }
                $balance = $_bal;
                $initial_balance = $_bal;
            }

            // $balance = $customsbroker->initial_balance;
            // $initial_balance = $customsbroker->initial_balance;

            $balance = $balance + $inAmt - $outAmt;
            $initial_balance = $initial_balance + $inAmt - $outAmt;
            // $initial_balance = number_format($initial_balance, 2);
            if ($initial_balance < 0) {
                $initial_balance = '('.str_replace('-', '', number_format($initial_balance, 2)).')';
            } else {
                $initial_balance = number_format($initial_balance, 2);
            }
            foreach ($data as $d) {
                if ($d->type == 'in') {
                    $d->balance = $balance + $d->in;
                    $d->in = number_format($d->in, 2);
                    $d->out = '';
                } elseif ($d->type == 'out') {
                    $d->balance = $balance - $d->out;
                    $d->in = '';
                    $d->out = number_format($d->out, 2);
                } else {
                    $d->in = '';
                    $d->out = '';
                }

                $balance = $d->balance;
                $d->balance = number_format($d->balance, 2);
                if ($d->balance < 0) {
                    $d->balance = '('.str_replace('-', '', $d->balance).')';
                }
                $d->date = date('d/m/Y', strtotime($d->date));
            }

            $title = $customsbroker->name;
        } else {
            $customer_id = '';
            $inputs['customsbroker_id'] = '';
            $inputs['billing_company_id'] = '';
            $inputs['year'] = date('Y');
            $title = '';
            $data = [];
            $initial_balance = '';
        }

        $vm = new \App\Http\ViewModels\Admin\ReportBalanceSheetSupplierViewModel($data, $inputs, $title, $initial_balance);

        return view('report/balance_sheet_supplier', ['vm' => $vm]);
    }

    /* Export */
    public function ReportExportStaffCommission(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        // if(null !== ($request->input('date_from'))){
        //   $dtf = str_replace('/', '-', $request->input('date_from'));
        //   $date_from = date('Y-m-d', strtotime($dtf));
        //   $inputs['date_from'] = $request->input('date_from');
        // }else{
        //   $date_from = date('Y-m-1');
        //   $inputs['date_from'] = date('01/m/Y');
        // }
        //
        // if(null !== ($request->input('date_to'))){
        //   $dtt = str_replace('/', '-', $request->input('date_to'));
        //   $date_to =  date('Y-m-d', strtotime($dtt));
        //   $inputs['date_to'] = $request->input('date_to');
        // }else{
        //   $date_to = date('Y-m-d');
        //   $inputs['date_to'] = date('d/m/Y');
        // }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
        } else {
            $year = date('Y');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
        } else {
            $month = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = date('Y-m-t', strtotime($date_from));

        if (null !== $request->input('employee_id')) {
            $employee_id = $request->input('employee_id');
            $inputs['employee_id'] = $request->input('employee_id');
        } else {
            $employee_id = '';
            $inputs['employee_id'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'employee_id' => $employee_id,
        ];

        // $dto = \App\Lib\Queries\Admin\GetReportStaffCommission::Result($params);
        // $employees = \App\Lib\Queries\Admin\GetEmployees::Status(1);
        // $data = array();
        // $data['results'] = array();
        // $data['employees'] = $employees;
        // if(count($dto) > 0){
        //   foreach ($dto as $k => $v){
        //
        //     $employ = array();
        //     foreach($employees as $e){
        //       $jobs = "";
        //
        //       foreach($v->employees as $p){
        //
        //         if($e->user_id == $p->employee_id){
        //             $jobs .= $p->job . " + ";
        //         }
        //       }
        //       $jobs = rtrim($jobs, "+ ");
        //       $employ[] = array('job'=>$jobs);
        //     }
        //
        //     $data['results'][] = array(
        //         'no' => $k +1,
        //         'loading_date'  => $v->loading_date,
        //         'customer_name'  => $v->customer_name,
        //         'container_no'  => $v->container_no,
        //         'order_no'  => $v->order_no,
        //         'booking_no'  => $v->booking_no,
        //         'employees'  => $employ
        //     );
        //   }//foreach
        // }

        $header = ['No.', '装柜日期', '客户名称', '货柜号码', '预订号'];
        $footer = [];
        $results = [];

        $employees = \App\Lib\Queries\Admin\GetEmployees::Status(1);

        if ($employee_id != '') {
            $dto = \App\Lib\Queries\Admin\GetReportStaffCommissionEmployee::Result($params);
            // $employees = \App\Lib\Queries\Admin\GetEmployees::Status(1);
            $employee = \App\Lib\Queries\Admin\GetUser::Result($employee_id);
        // array_push($header,$employee->name);
        } else {
            $dto = \App\Lib\Queries\Admin\GetReportStaffCommission::Result($params);
            // foreach($employees as $employee){
            //   array_push($header,$employee->name);
            // }
        }

        if (!empty($dto)) {
            foreach ($dto as $k => $v) {
                $workers = [];

                if ($employee_id != '') {
                    $jobs = '';
                    foreach ($v->employees as $e) {
                        $jobs .= $e->job.'<br/> ';
                    }
                    $workers[$employee_id] = $jobs;
                } else {
                    foreach ($employees as $ey) {
                        $jobs = '';

                        foreach ($v->employees as $e) {
                            if ($ey->user_id == $e->employee_id) {
                                $jobs .= $e->job.'<br/>';
                            }
                        }

                        $jobs = rtrim($jobs, '<br/>');
                        $workers[$ey->user_id] = $jobs;
                    }
                }
                $results[] = [
                    'no' => $k + 1,
                    'loading_date' => $v->loading_date,
                    'customer_name' => $v->customer_name,
                    'container_no' => $v->container_no,
                    'booking_no' => $v->booking_no,
                    'employees' => $workers,
                ];
            }
        }

        if ($employee_id == '') {
            foreach ($employees as $e) {
                $j = [];
                foreach ($dto as $d) {
                    foreach ($d->employees as $ee) {
                        if ($ee->employee_id == $e->user_id) {
                            $j[] = $ee->job;
                        }
                    }
                }
                // echo "<pre>";
                // print_r($j);
                // echo "</pre>";
                if (!empty($j)) {
                    $footer[$e->user_id] = [
                        'user_id' => $e->user_id,
                        'name' => $e->name,
                        'job' => array_count_values($j),
                    ];
                }
            }
        } else {
            $employee = \App\Lib\Queries\Admin\GetUser::Result($employee_id);
            $inputs['employee'] = ' - '.$employee->name;
            $j = [];
            foreach ($dto as $d) {
                foreach ($d->employees as $ee) {
                    if ($ee->employee_id == $employee_id) {
                        $j[] = $ee->job;
                    }
                }
            }

            // echo "<pre>";
            // print_r($j);
            // echo "</pre>";
            if (!empty($j)) {
                $footer[$employee_id] = [
                    'user_id' => $employee_id,
                    'name' => $employee->name,
                    'job' => array_count_values($j),
                ];
            }
        }

        // get only got job employee
        foreach ($footer as $ft) {
            if (count($ft['job']) > 0) {
                array_push($header, $ft['name']);
            }
        }

        $data = [
            'results' => $results,
            'header' => $header,
            'footer' => $footer,
            'date' => $year.'年'.$month.'月份',
        ];
        $filename = 'StaffCommission-'.date('ymd').'.xlsx';

        // return view('report.export.staff_commission', ['data' => $data]);
        return Excel::download(new ReportExportStaffCommission('report.export.staff_commission', $data), $filename);
    }

    public function ReportExportBookingOrder(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'booking_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d', strtotime(date('Y-m-d').' + 14 days'));
            $inputs['date_to'] = date('d/m/Y', strtotime(date('Y-m-d').' + 14 days'));
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportBookingOrder::Result($params);

        $data = [];
        $data['results'] = [];

        if (count($dto) > 0) {
            foreach ($dto as $k => $v) {
                $data['results'][] = [
                    'no' => $k + 1,
                    'booking_date' => $v->booking_date,
                    'eta_date' => $v->eta_date,
                    'customer_name' => $v->customer_name,
                    'order_no' => $v->order_no,
                    'booking_no' => $v->booking_no,
                    'goods_value' => $v->goods_value,
                    'agent_name' => $v->agent_name,
                    'shipping_company' => $v->shipping_company,
                    'loading_port' => $v->loading_port,
                    'destination_port' => $v->destination_port,
                    'handling_charges' => $v->handling_charges,
                    'freight_charges' => $v->freight_charges,
                ];
            }// foreach
        }

        $filename = 'BookingOrder-'.date('ymd').'.xlsx';

        // return view('report.export.booking_order', ['data' => $data]);
        return Excel::download(new ReportExportBookingOrder('report.export.booking_order', $data), $filename);
    }

    public function ReportExportContainerLoading(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        // if(null !== ($request->input('date_from'))){
        //   $dtf = str_replace('/', '-', $request->input('date_from'));
        //   $date_from = date('Y-m-d', strtotime($dtf));
        //   $inputs['date_from'] = $request->input('date_from');
        // }else{
        //   $date_from = date('Y-m-1');
        //   $inputs['date_from'] = date('01/m/Y');
        // }
        //
        // if(null !== ($request->input('date_to'))){
        //   $dtt = str_replace('/', '-', $request->input('date_to'));
        //   $date_to =  date('Y-m-d', strtotime($dtt));
        //   $inputs['date_to'] = $request->input('date_to');
        // }else{
        //   $date_to = date('Y-m-d');
        //   $inputs['date_to'] = date('d/m/Y');
        // }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = $request->input('year');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
            $customer_id = explode(',', $customer_id);
        } else {
            $customer_id = [];
            $inputs['customer_id'] = [];
        }

        if (null !== $request->input('agent_id')) {
            $agent_id = $request->input('agent_id');
            $inputs['agent_id'] = $request->input('agent_id');
            $agent = \App\Lib\Queries\Admin\GetAgent::Result($agent_id);
            $inputs['agent'] = ' - '.$agent->name;
        } else {
            $agent_id = '';
            $inputs['agent_id'] = '';
            $inputs['agent'] = '';
        }

        if (null !== $request->input('user_id')) {
            $user_id = $request->input('user_id');
            $inputs['user_id'] = $request->input('user_id');
        } else {
            $user_id = '';
            $inputs['user_id'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
            'agent_id' => $agent_id,
            'user_id' => $user_id
        ];

        $dto = \App\Lib\Queries\Admin\GetReportContainerLoading::Result($params);
        $data = [];
        $data['results'] = [];

        if (count($dto) > 0) {
            foreach ($dto as $k => $v) {
                if ($v->forme_apply == 1) {
                    $forme_apply = 'YES';
                } else {
                    $forme_apply = 'NO';
                }
                $data['results'][] = [
                    'no' => $k + 1,
                    'loading_date' => $v->loading_date,
                    'eta_date' => $v->eta_date,
                    'goods_value' => $v->goods_value,
                    'handling_charges' => $v->handling_charges,
                    'total_other_fee' => number_format($v->total_other_fee, 2, '.', ''),
                    'customer_name' => $v->customer_name,
                    'container_no' => $v->container_no,
                    'order_no' => $v->order_no,
                    'booking_no' => $v->booking_no,
                    'forme_apply' => $forme_apply,
                    'agent_name' => $v->agent_name,
                    'freight_charges' => $v->freight_charges,
                    'net_freight_charges' => $v->net_freight_charges,
                    'shipping_company' => $v->shipping_company,
                    'remote_fee' => $v->remote_fee,
                ];
            }// foreach
        }

        $mth = $this->GetChineseMonthName($month);
        $data['title'] = $year.'年 '.$mth.'份';
        $data['agent'] = $inputs['agent'];
        $filename = 'ContainerLoading-'.date('ymd').'.xlsx';

        // return view('report.export.container_loading', ['data' => $data]);
        return Excel::download(new ReportExportContainerLoading('report.export.container_loading', $data), $filename);
    }

    public function ReportExportBillingCompany(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = $request->input('year');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        // if(null !== ($request->input('billing_company_id'))){
        //   $billing_company_id = '%' . $request->input('billing_company_id') . '%';
        //   $inputs['billing_company_id'] = $request->input('billing_company_id');
        //   $billing_company  = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($request->input('billing_company_id'));
        //   if(!empty($billing_company)){
        //     $company = $billing_company->name;
        //   }else{
        //     $company = "";
        //   }
        //   $title = $company . ' For the month ' .  strtoupper($mth) . ' ' . $year;
        //
        // }else{
        //   $billing_company_id = '';
        //   $inputs['billing_company_id'] = '';
        //   $title = 'All company For the month ' . strtoupper($mth) . ' ' .  $year;
        // }

        if (null !== $request->input('billing_company_id')) {
            // $billing_company_id = '%' . $request->input('billing_company_id') . '%';
            // $inputs['billing_company_id'] = $request->input('billing_company_id');
            // $billing_company  = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($request->input('billing_company_id'));
            // if(!empty($billing_company)){
            //   $company = $billing_company->name;
            // }else{
            //   $company = "";
            // }
            // $title = $company . ' Container list for the month of ' . strtoupper($mth) . ' ' . $year;
            $billing_company_id = $request->input('billing_company_id');
            $inputs['billing_company_id'] = $request->input('billing_company_id');
            $company = '';
            $billing_companies = [];

            $billing_company_id = explode(',', $billing_company_id);
            foreach ($billing_company_id as $k => $v) {
                if ($v != '') {
                    $company = $company.' '.$v;
                    $billing_company = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($v);
                    $billing_companies[] = $billing_company->id;
                }
            }
            $title = $company.' Container list for the month of '.strtoupper($mth).' '.$year;
        } else {
            $billing_company_id = [];
            $inputs['billing_company_id'] = [];
            $billing_companies = [];
            $title = 'All Container list for the month of  '.strtoupper($mth).' '.$year;
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'billing_company_id' => $billing_company_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportBillingCompany::Result($params);
        $company_inv_amount = \App\Lib\Queries\Admin\GetReportBillingCompany::GetCompanyInvAmount($params);
        $supplier_inv_amount = \App\Lib\Queries\Admin\GetReportBillingCompany::GetSupplierInvAmount($params);

        $data = [];
        $data['results'] = [];
        $data['title'] = $title;
        $data['company_inv_amount'] = number_format($company_inv_amount, 2, '.', ',');
        $data['supplier_inv_amount'] = number_format($supplier_inv_amount, 2, '.', ',');

        $data['net_profit'] = $company_inv_amount - $supplier_inv_amount;

        if (count($dto) > 0) {
            foreach ($dto as $k => $v) {
                if ($v->sst == 1) {
                    $sst = 'YES';
                } else {
                    $sst = 'NO';
                }
                $data['results'][] = [
                    'no' => $k + 1,
                    'declaration_date' => $v->declaration_date,
                    'loading_date' => $v->loading_date,
                    'eta_date' => $v->eta_date,
                    'customer_name' => $v->customer_name,
                    'order_no' => $v->order_no,
                    'booking_no' => $v->booking_no,
                    'container_no' => $v->container_no,
                    'billing_goods_inv_date' => $v->billing_goods_inv_date,
                    'billing_goods_inv_no' => $v->billing_goods_inv_no,
                    'billing_goods_amount' => number_format($v->billing_goods_amount, 2, '.', ','),
                    'custom_goods_inv_date' => $v->custom_goods_inv_date,
                    'custom_goods_inv_no' => $v->custom_goods_inv_no,
                    'custom_goods_amount' => number_format($v->custom_goods_amount, 2, '.', ','),
                    'custom_handling_inv_no' => $v->custom_handling_inv_no,
                    'sst' => $sst,
                    'customs_broker' => $v->custom_borker,
                ];
            }// foreach
        }

        $filename = 'BillingCompany-'.date('ymd').'.xlsx';

        // return view('report.export.billing_company', ['data' => $data]);
        return Excel::download(new ReportExportBillingCompany('report.export.billing_company', $data), $filename);
    }

    public function ReportExportMonthlyStatement(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
        } else {
            $year = date('Y');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
        } else {
            $month = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
        }

        if (null !== $request->input('billing_company_id')) {
            // $billing_company_id = '%' . $request->input('billing_company_id') . '%';
            // $inputs['billing_company_id'] = $request->input('billing_company_id');
            // $billing_company  = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($request->input('billing_company_id'));
            // if(!empty($billing_company)){
            //   $company = $billing_company->name;
            // }else{
            //   $company = "";
            // }
            // $title = $company . ' Container list for the month of ' . strtoupper($mth) . ' ' . $year;
            $billing_company_id = $request->input('billing_company_id');
            $inputs['billing_company_id'] = $request->input('billing_company_id');
            $company = '';
            $billing_companies = [];

            $billing_company_id = explode(',', $billing_company_id);
            foreach ($billing_company_id as $k => $v) {
                if ($v != '') {
                    $company = $company.' '.$v;
                    $billing_company = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($v);
                    $billing_companies[] = $billing_company->id;
                }
            }
            $title = $company.' Container list for the month of '.strtoupper($mth).' '.$year;
        } else {
            $billing_company_id = [];
            $inputs['billing_company_id'] = [];
            $billing_companies = [];
            $title = 'All Container list for the month of  '.strtoupper($mth).' '.$year;
        }

        if (null !== $request->input('rate')) {
            $rate = $request->input('rate');
        } else {
            $rate = $this->CurrentCurrencyRate();
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
            'billing_company_id' => $billing_company_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportMonthlyStatement::Result($params);

        $net_total = 0;

        $data = [];
        $data['results'] = [];

        $total_net_total = 0;
        $total_total2 = 0;
        $total_billing_handling = 0;
        $total_customs_handling = 0;
        $total_billing_other = 0;
        $total_customs_other = 0;
        $total_handling_charges = 0;
        $total_commission = 0;
        $total_freight_charges = 0;
        $total_net_freight_charges = 0;
        $total_loadingfee = 0;

        if (count($dto) > 0) {
            foreach ($dto as $k => $v) {
                $net_total = $v->total + ($v->total_charges / $rate);
                $total_net_total += $net_total;
                $total_total2 += $v->total_charges;

                // total
                $total_billing_handling += $v->billing_handling_prices;
                $total_customs_handling += $v->custom_handling_prices;
                $total_billing_other += $v->billing_other_prices;
                $total_customs_other += $v->custom_other_prices;
                $total_handling_charges += $v->handling_charges;
                $total_commission += $v->commission;
                $total_freight_charges += $v->freight_charges;
                $total_net_freight_charges += $v->net_freight_charges;
                $total_loadingfee += $v->container_loading_charges;

                $data['result'][] = [
                    'no' => $k + 1,
                    'loading_date' => $v->loading_date,
                    'created_at' => $v->created_at,
                    'customer_name' => $v->customer_name,
                    'container_no' => $v->container_no,
                    'order_no' => $v->order_no,
                    'booking_no' => $v->booking_no,
                    'billing_handling_prices' => number_format($v->billing_handling_prices, 2),
                    'billing_other_prices' => number_format($v->billing_other_prices, 2),
                    'custom_handling_prices' => number_format($v->custom_handling_prices, 2),
                    'custom_other_prices' => number_format($v->custom_other_prices, 2),
                    'total' => number_format($v->total, 2),
                    'handling_charges' => number_format($v->handling_charges, 2),
                    'commission' => number_format($v->commission, 2),
                    'freight_charges' => number_format($v->freight_charges, 2),
                    'net_freight_charges' => number_format($v->net_freight_charges, 2),
                    'container_loading_charges' => number_format($v->container_loading_charges, 2),
                    'total_charges' => number_format($v->total_charges, 2),
                    'net_total' => number_format($net_total, 2),
                ];
            }// foreach
        }

        // $total_billing_handling = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalBillingHandling($params);
        // $total_customs_handling = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalCustomsHandling($params);
        // $total_billing_other = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalBillingOther($params);
        // $total_customs_other = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalCustomsOther($params);
        $total_total = ($total_billing_handling + $total_billing_other) - ($total_customs_handling + $total_customs_other);

        // $handling_charges = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalHandlingCharges($params);
        // $commission = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalCommission($params);
        // $freight_charges = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalFreightCharges($params);
        // $loadingfee = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalLoadingFee($params);

        // $total_total2 = $handling_charges + $commission - $freight_charges - $loadingfee;

        $total = [
            'billing_handling' => number_format($total_billing_handling, 2),
            'billing_other' => number_format($total_billing_other, 2),
            'customs_handling' => number_format($total_customs_handling, 2),
            'customs_other' => number_format($total_customs_other, 2),
            'total' => number_format($total_total, 2),
            'handling_charges' => number_format($total_handling_charges, 2),
            'commission' => number_format($total_commission, 2),
            'freight_charges' => number_format($total_freight_charges, 2),
            'net_freight_charges' => number_format($total_net_freight_charges, 2),
            'loadingfee' => number_format($total_loadingfee, 2),
            'total_charges' => number_format($total_total2, 2),
            'net_total' => number_format($total_net_total, 2),
        ];

        $data['rate'] = $rate;
        $data['title'] = strtoupper($title);
        $filename = 'MonthlyStatement-'.date('ymd').'.xlsx';

        // return view('report.export.monthly_statement', ['data' => $data, 'total' => $total]);
        return Excel::download(new ReportExportMonthlyStatement('report.export.monthly_statement', $data, $total), $filename);
    }

    public function ReportExportYearlyStatement(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
        } else {
            $year = date('Y');
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
            $_customer = \App\Lib\Queries\Admin\GetCustomer::Result($customer_id);
            if (!empty($_customer)) {
                $customer = $_customer->name;
            } else {
                $customer = '';
            }
        } else {
            $customer_id = '';
            $customer = '';
            $inputs['customer_id'] = '';
        }

        if (null !== $request->input('billing_company_id')) {
            $billing_company_id = $request->input('billing_company_id');
            $inputs['billing_company_id'] = $request->input('billing_company_id');
            $company = '';
            $billing_companies = [];

            $billing_company_id = explode(',', $billing_company_id);
            foreach ($billing_company_id as $k => $v) {
                if ($v != '') {
                    $company = $company.' '.$v;
                    $billing_company = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($v);
                    $billing_companies[] = $billing_company->id;
                }
            }
        } else {
            $billing_company_id = [];
            $inputs['billing_company_id'] = [];
            $billing_companies = [];
        }

        if (null !== $request->input('rate')) {
            $rate = $request->input('rate');
        } else {
            $rate = $this->CurrentCurrencyRate();
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'year' => $year,
            'customer_id' => $customer_id,
            'billing_company_id' => $billing_company_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportYearlyStatement::Result($params);

        $net_total = 0;

        $data = [];
        $data['results'] = [];

        $total_net_total = 0;
        $total_total2 = 0;
        $total_billing_handling = 0;
        $total_customs_handling = 0;
        $total_billing_other = 0;
        $total_customs_other = 0;
        $total_handling_charges = 0;
        $total_commission = 0;
        $total_freight_charges = 0;
        $total_net_freight_charges = 0;
        $total_loadingfee = 0;

        if (count($dto) > 0) {
            foreach ($dto as $k => $v) {
                $net_total = $v->total + ($v->total_charges / $rate);
                $total_net_total += $net_total;
                $total_total2 += $v->total_charges;

                // total
                $total_billing_handling += $v->billing_handling_prices;
                $total_customs_handling += $v->custom_handling_prices;
                $total_billing_other += $v->billing_other_prices;
                $total_customs_other += $v->custom_other_prices;
                $total_handling_charges += $v->handling_charges;
                $total_commission += $v->commission;
                $total_freight_charges += $v->freight_charges;
                $total_net_freight_charges += $v->net_freight_charges;
                $total_loadingfee += $v->container_loading_charges;

                $data['result'][] = [
                    'no' => $k + 1,
                    'loading_date' => $v->loading_date,
                    'created_at' => $v->created_at,
                    'customer_name' => $v->customer_name,
                    'container_no' => $v->container_no,
                    'order_no' => $v->order_no,
                    'booking_no' => $v->booking_no,
                    'billing_handling_prices' => number_format($v->billing_handling_prices, 2),
                    'billing_other_prices' => number_format($v->billing_other_prices, 2),
                    'custom_handling_prices' => number_format($v->custom_handling_prices, 2),
                    'custom_other_prices' => number_format($v->custom_other_prices, 2),
                    'total' => number_format($v->total, 2),
                    'handling_charges' => number_format($v->handling_charges, 2),
                    'commission' => number_format($v->commission, 2),
                    'freight_charges' => number_format($v->freight_charges, 2),
                    'net_freight_charges' => number_format($v->net_freight_charges, 2),
                    'container_loading_charges' => number_format($v->container_loading_charges, 2),
                    'total_charges' => number_format($v->total_charges, 2),
                    'net_total' => number_format($net_total, 2),
                ];
            }// foreach
        }

        $total_total = ($total_billing_handling + $total_billing_other) - ($total_customs_handling + $total_customs_other);

        $total = [
            'billing_handling' => number_format($total_billing_handling, 2),
            'billing_other' => number_format($total_billing_other, 2),
            'customs_handling' => number_format($total_customs_handling, 2),
            'customs_other' => number_format($total_customs_other, 2),
            'total' => number_format($total_total, 2),
            'handling_charges' => number_format($total_handling_charges, 2),
            'commission' => number_format($total_commission, 2),
            'freight_charges' => number_format($total_freight_charges, 2),
            'net_freight_charges' => number_format($total_net_freight_charges, 2),
            'loadingfee' => number_format($total_loadingfee, 2),
            'total_charges' => number_format($total_total2, 2),
            'net_total' => number_format($total_net_total, 2),
        ];

        $data['rate'] = $rate;
        $title = $customer.' Container list for the year of '.$year;
        $data['title'] = strtoupper($title);

        $filename = 'YearlyStatement-'.date('ymd').'.xlsx';

        return Excel::download(new ReportExportYearlyStatement('report.export.yearly_statement', $data, $total), $filename);
    }

    public function ReportExportDailyPaymentTransfer(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'payment_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d');
            $inputs['date_to'] = date('d/m/Y');
        }

        if (null !== $request->input('supplier_id')) {
            $supplier_id = $request->input('supplier_id');
            $inputs['supplier_id'] = $request->input('supplier_id');
            $supplier = \App\Lib\Queries\Admin\GetSupplier::Result($supplier_id);
            $inputs['supplier'] = ' - '.$supplier->name;
        } else {
            $supplier_id = '';
            $inputs['supplier_id'] = '';
            $inputs['supplier'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'supplier_id' => $supplier_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::Result($params);
        $total_customer_amount = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalCustomerAmount($params);
        $total_customer_total = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalCustomerTotal($params);
        $total_supplier_amount = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalSupplierAmount($params);
        $total_supplier_total = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalSupplierTotal($params);
        $total_supplier_bank_charges = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalSupplierBankCharges($params);
        $total_supplier_net_amount = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalSupplierNetAmount($params);

        $data = [];
        $data['results'] = [];

        if (count($dto) > 0) {
            foreach ($dto as $k => $v) {
                $data['results'][] = [
                    'payment_date' => $v->payment_date,
                    'customer_code' => $v->customer_code,
                    'customer_amount' => number_format($v->customer_amount, 2),
                    'customer_exchange_rate' => number_format($v->customer_exchange_rate, 3),
                    'customer_total' => number_format($v->customer_total, 2),
                    'supplier_code' => $v->supplier_code,
                    'supplier_amount' => number_format($v->supplier_amount, 2),
                    'supplier_exchange_rate' => number_format($v->supplier_exchange_rate, 3),
                    'supplier_total' => number_format($v->supplier_total, 2),
                    'supplier_bank_charges' => number_format($v->supplier_bank_charges, 2),
                    'supplier_net_amount' => number_format($v->supplier_net_amount, 2),
                    'date_received' => $v->date_received,
                    'beneficiary' => $v->beneficiary,
                    'slip' => $v->supplier_code,
                ];
            }// foreach
        }

        $total = [
            'total_customer_amount' => number_format($total_customer_amount, 2),
            'total_customer_total' => number_format($total_customer_total, 2),
            'total_supplier_amount' => number_format($total_supplier_amount, 2),
            'total_supplier_total' => number_format($total_supplier_total, 2),
            'total_supplier_bank_charges' => number_format($total_supplier_bank_charges, 2),
            'total_supplier_net_amount' => number_format($total_supplier_net_amount, 2),
        ];

        $filename = 'DailyPaymentTransfer-'.date('ymd').'.xlsx';

        // return view('report.export.daily_payment_transfer', ['data' => $data]);
        return Excel::download(new ReportExportDailyPaymentTransfer('report.export.daily_payment_transfer', $data, $total), $filename);
    }

    public function ReportExportDailyPayment(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'payment_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d');
            $inputs['date_to'] = date('d/m/Y');
        }

        if (null !== $request->input('supplier_id')) {
            $supplier_id = $request->input('supplier_id');
            $inputs['supplier_id'] = $request->input('supplier_id');
            $supplier = \App\Lib\Queries\Admin\GetCustomsBroker::Result($supplier_id);
            $inputs['supplier'] = ' - '.$supplier->name;
        } else {
            $supplier_id = '';
            $inputs['supplier_id'] = '';
            $inputs['supplier'] = '';
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        // $customer = \App\Lib\Queries\Admin\GetCustomer::Result($customer_id);
        // $inputs['customer'] = ' - ' .$customer->name;
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
            // $inputs['customer'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'supplier_id' => $supplier_id,
            'customer_id' => $customer_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportDailyPayment::Result($params);
        $total_customer_amount = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalCustomerAmount($params);
        $total_customer_total = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalCustomerTotal($params);
        $total_supplier_amount = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalSupplierAmount($params);
        $total_supplier_total = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalSupplierTotal($params);
        $total_supplier_bank_charges = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalSupplierBankCharges($params);
        $total_supplier_net_amount = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalSupplierNetAmount($params);

        $data = [];
        $data['results'] = [];

        if (count($dto) > 0) {
            foreach ($dto as $k => $v) {
                $suppliers = [];
                foreach ($v->suppliers as $s) {
                    $beneficiaries = [];
                    foreach ($s->beneficiaries as $b) {
                        $beneficiaries[] = [
                            'beneficiary' => $b->beneficiary,
                            'amount' => number_format($b->amount, 2),
                            'bank_charges' => number_format($b->bank_charges, 2),
                            'net_amount' => number_format($b->net_amount, 2),
                            'received_date' => $b->received_date,
                            'slip' => $b->slip,
                        ];
                    }
                    $suppliers[] = [
                        'supplier_code' => $s->supplier_code,
                        'supplier_amount' => number_format($s->supplier_amount, 2),
                        'supplier_exchange_rate' => number_format($s->supplier_exchange_rate, 3),
                        'supplier_total' => number_format($s->supplier_total, 2),
                        'rowspan' => $s->rowspan,
                        'beneficiaries' => $beneficiaries,
                    ];
                }

                $data['results'][] = [
                    'payment_date' => $v->payment_date,
                    'remittance_date' => $v->remittance_date,
                    'customer_code' => $v->customer_code,
                    'customer_amount' => number_format($v->customer_amount, 2),
                    'customer_exchange_rate' => number_format($v->customer_exchange_rate, 3),
                    'customer_total' => number_format($v->customer_total, 2),
                    'suppliers' => $suppliers,
                    'rowspan' => $v->rowspan,
                ];
            }// foreach
        }

        $total = [
            'total_customer_amount' => number_format($total_customer_amount, 2),
            'total_customer_total' => number_format($total_customer_total, 2),
            'total_supplier_amount' => number_format($total_supplier_amount, 2),
            'total_supplier_total' => number_format($total_supplier_total, 2),
            'total_supplier_bank_charges' => number_format($total_supplier_bank_charges, 2),
            'total_supplier_net_amount' => number_format($total_supplier_net_amount, 2),
        ];

        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        $filename = 'DailyPaymentTransfer-'.date('ymd').'.xlsx';

        // return view('report.export.daily_payment', ['data' => $data, 'total' => $total]);
        return Excel::download(new ReportExportDailyPayment('report.export.daily_payment', $data, $total), $filename);
    }

    public function ReportExportCargoRecord(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d');
            $inputs['date_to'] = date('d/m/Y');
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
        }

        if (null !== $request->input('customs_broker_id')) {
            $customs_broker_id = $request->input('customs_broker_id');
            $inputs['customs_broker_id'] = $request->input('customs_broker_id');
        } else {
            $customs_broker_id = '';
            $inputs['customs_broker_id'] = '';
        }

        if (null !== $request->input('owner_id')) {
            $owner_id = $request->input('owner_id');
            $inputs['owner_id'] = $request->input('owner_id');
        } else {
            $owner_id = '';
            $inputs['owner_id'] = '';
        }

        if (null !== $request->input('warehouse_id')) {
            $warehouse_id = $request->input('warehouse_id');
            $inputs['warehouse_id'] = $request->input('warehouse_id');
        } else {
            $warehouse_id = '';
            $inputs['warehouse_id'] = '';
        }

        if (null !== $request->input('tracking_no')) {
            $tracking_no = $request->input('tracking_no');
            $inputs['tracking_no'] = $request->input('tracking_no');
        } else {
            $tracking_no = '';
            $inputs['tracking_no'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
            'customs_broker_id' => $customs_broker_id,
            'owner_id' => $owner_id,
            'warehouse_id' => $warehouse_id,
            'tracking_no' => $tracking_no,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportCargoRecord::Result($params);

        $data = [];
        $data['results'] = [];

        $total_cbm = 0;
        $total_supplier_inv_amount = 0;
        $total_supplier_goods_amount = 0;
        $total_customer_goods_amount = 0;
        $total_transport_inv_amount = 0;

        if (count($dto) > 0) {
            foreach ($dto as $k => $v) {  // total
                $total_cbm += $v->cbm;
                $total_supplier_inv_amount += $v->supplier_inv_amount;
                $total_supplier_goods_amount += $v->supplier_goods_amount;
                $total_customer_goods_amount += $v->customer_goods_amount;
                $total_transport_inv_amount += $v->transport_inv_amount;

                $data['results'][] = [
                    'no' => $k + 1,
                    'date' => $v->date,
                    'loading_date' => $v->loading_date,
                    'arrival_date' => $v->arrival_date,
                    'customer' => $v->customer,
                    'customs_broker' => $v->customs_broker,
                    'warehouse' => $v->warehouse,
                    'owner' => $v->owner,
                    'tracking_no' => $v->tracking_no,
                    'cbm' => $v->cbm,
                    'pack' => $v->pack,
                    'supplier_inv_no' => $v->supplier_inv_no,
                    'supplier_inv_amount' => $v->supplier_inv_amount,
                    'supplier_goods_inv' => $v->supplier_goods_inv,
                    'supplier_goods_amount' => $v->supplier_goods_amount,
                    'customer_goods_inv' => $v->customer_goods_inv,
                    'customer_goods_amount' => $v->customer_goods_amount,
                    'transport_inv' => $v->transport_inv,
                    'transport_inv_amount' => $v->transport_inv_amount,
                ];
            }// foreach
        }

        $net_total_supplier_goods_amount = $total_customer_goods_amount - $total_supplier_goods_amount;

        $net_total_transport_inv_amount = $total_transport_inv_amount - $total_supplier_inv_amount;

        $total = [
            'cbm' => number_format($total_cbm, 3),
            'supplier_inv_amount' => number_format($total_supplier_inv_amount, 2),
            'supplier_goods_amount' => number_format($total_supplier_goods_amount, 2),
            'customer_goods_amount' => number_format($total_customer_goods_amount, 2),
            'transport_inv_amount' => number_format($total_transport_inv_amount, 2),
            'net_total_supplier_goods_amount' => number_format($net_total_supplier_goods_amount, 2),
            'net_total_transport_inv_amount' => number_format($net_total_transport_inv_amount, 2),
        ];

        // title
        $month_from = date('m', strtotime($dtf));
        $month_to = date('m', strtotime($dtt));

        $mf = $this->GetMonthName($month_from);
        $mt = $this->GetMonthName($month_to);

        if ($mf == $mt) {
            $title = $mf.' '.date('Y', strtotime($dtf));
        } else {
            $title = $mf.' - '.$mt.' '.date('Y', strtotime($dtf));
        }
        $data['title'] = '散货记录表 - '.$title;
        $filename = 'CargoRecord-'.date('ymd').'.xlsx';

        // return view('report.export.cargo_record', ['data' => $data, 'total' => $total]);
        return Excel::download(new ReportExportCargoRecord('report.export.cargo_record', $data, $total), $filename);
    }

    public function ReportExportBalanceSheetCustomer(Request $request)
    {
        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');

            if (null !== $request->input('year')) {
                $year = $request->input('year');
                $inputs['year'] = $request->input('year');
            } else {
                $year = date('Y');
                $inputs['year'] = date('Y');
            }

            $params[] = [
                'customer_id' => $customer_id,
                'year' => $year,
            ];

            // Calculate existing year balance
            $inAmt = \App\Lib\Queries\Admin\GetReportBalanceSheetCustomer::GetTotalInAmount($params);
            $outAmt = \App\Lib\Queries\Admin\GetReportBalanceSheetCustomer::GetTotalOutAmount($params);

            $dto = \App\Lib\Queries\Admin\GetReportBalanceSheetCustomer::GetInRecords($params);
            $dto2 = \App\Lib\Queries\Admin\GetReportBalanceSheetCustomer::GetOutRecords($params);

            foreach ($dto as $d) {
                $d->type = 'in';
            }

            foreach ($dto2 as $d) {
                $d->type = 'out';
            }
            $data = [];
            $data = array_merge($dto, $dto2);
            usort($data, function ($dto, $dto2) {
                return strtotime($dto->date) - strtotime($dto2->date);
            });

            $customer = \App\Lib\Queries\Admin\GetCustomer::Result($customer_id);
            $balance = $customer->initial_balance;
            $initial_balance = $customer->initial_balance;

            $balance = $balance + $inAmt - $outAmt;
            $initial_balance = $initial_balance + $inAmt - $outAmt;
            // $initial_balance = number_format($initial_balance, 2);
            if ($initial_balance < 0) {
                $initial_balance = '('.str_replace('-', '', number_format($initial_balance, 2, '.', '')).')';
            } else {
                $initial_balance = number_format($initial_balance, 2, '.', '');
            }
            foreach ($data as $d) {
                if ($d->type == 'in') {
                    $d->balance = $balance + $d->in;
                    $d->in = number_format($d->in, 2, '.', '');
                    $d->out = '';
                } elseif ($d->type == 'out') {
                    $d->balance = $balance - $d->out;
                    $d->in = '';
                    $d->out = number_format($d->out, 2, '.', '');
                } else {
                    $d->in = '';
                    $d->out = '';
                }

                $balance = $d->balance;
                // $d->balance = number_format($d->balance,2);
                // $d->date = date("d/m/Y", strtotime($d->date));
                if ($d->balance < 0) {
                    $bal = '('.str_replace('-', '', number_format($d->balance, 2, '.', '')).')';
                } else {
                    $bal = number_format($d->balance, 2, '.', '');
                }
                $data['results'][] = [
                    'type' => $d->type,
                    'date' => date('d/m/Y', strtotime($d->date)),
                    'description' => $d->description,
                    'in' => $d->in,
                    'out' => $d->out,
                    'balance' => $bal,
                ];
            }

            $title = $customer->name;
            $customer = $customer->code;
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
            $inputs['year'] = date('Y');
            $title = '';
            $customer = '';
            $data = [];
            $initial_balance = '';
        }
        $data['title'] = '客户月份结单 - '.$title;
        $data['initial_balance'] = $initial_balance;
        $filename = 'BalanceSheetCustomer-'.$customer.'.xlsx';

        // return view('report.export.balance_sheet_customer', ['data' => $data]);
        return Excel::download(new ReportExportBalanceSheetCustomer('report.export.balance_sheet_customer', $data), $filename);

        // $vm = new \App\Http\ViewModels\Admin\ReportBalanceSheetCustomerViewModel($data,  $inputs, $title,$initial_balance);
        // return view('report/balance_sheet_customer', ['vm' => $vm]);
    }

    public function ReportExportBalanceSheetSupplier(Request $request)
    {
        if (null !== $request->input('customsbroker_id')) {
            $customsbroker_id = $request->input('customsbroker_id');
            $inputs['customsbroker_id'] = $request->input('customsbroker_id');

            if (null !== $request->input('billing_company_id')) {
                $billing_company_id = $request->input('billing_company_id');
                $inputs['billing_company_id'] = $request->input('billing_company_id');
            } else {
                $billing_company_id = '';
                $inputs['billing_company_id'] = '';
            }

            if (null !== $request->input('year')) {
                $year = $request->input('year');
                $inputs['year'] = $request->input('year');
            } else {
                $year = date('Y');
                $inputs['year'] = date('Y');
            }

            $params[] = [
                'customsbroker_id' => $customsbroker_id,
                'billing_company_id' => $billing_company_id,
                'year' => $year,
            ];

            // Calculate existing year balance
            $inAmt = \App\Lib\Queries\Admin\GetReportBalanceSheetSupplier::GetTotalInAmount($params);
            $outAmt = \App\Lib\Queries\Admin\GetReportBalanceSheetSupplier::GetTotalOutAmount($params);

            $dto = \App\Lib\Queries\Admin\GetReportBalanceSheetSupplier::GetInRecords($params);
            $dto2 = \App\Lib\Queries\Admin\GetReportBalanceSheetSupplier::GetOutRecords($params);

            foreach ($dto as $d) {
                $d->type = 'in';
            }

            foreach ($dto2 as $d) {
                $d->type = 'out';
            }
            $data = [];
            $data = array_merge($dto, $dto2);
            usort($data, function ($dto, $dto2) {
                return strtotime($dto->date) - strtotime($dto2->date);
            });

            $customsbroker = \App\Lib\Queries\Admin\GetCustomsBroker::Result($customsbroker_id);
            // $balance = $customsbroker->initial_balance;
            // $initial_balance = $customsbroker->initial_balance;

            if ($billing_company_id != '') {
                $balance = $this->GetSupplierInitialValue($customsbroker->balance, $billing_company_id);
                $initial_balance = $this->GetSupplierInitialValue($customsbroker->balance, $billing_company_id);
            } else {
                $_bal = 0;
                if (!empty($customsbroker->balance)) {
                    foreach ($customsbroker->balance as $ib) {
                        $_bal += $ib->balance;
                    }
                }
                $balance = $_bal;
                $initial_balance = $_bal;
            }

            $balance = $balance + $inAmt - $outAmt;
            $initial_balance = $initial_balance + $inAmt - $outAmt;
            // $initial_balance = number_format($initial_balance, 2, '.', '');
            if ($initial_balance < 0) {
                $initial_balance = '('.str_replace('-', '', number_format($initial_balance, 2, '.', '')).')';
            } else {
                $initial_balance = number_format($initial_balance, 2, '.', '');
            }
            foreach ($data as $d) {
                if ($d->type == 'in') {
                    $d->balance = $balance + $d->in;
                    $d->in = number_format($d->in, 2, '.', '');
                    $d->out = '';
                } elseif ($d->type == 'out') {
                    $d->balance = $balance - $d->out;
                    $d->in = '';
                    $d->out = number_format($d->out, 2, '.', '');
                } else {
                    $d->in = '';
                    $d->out = '';
                }

                $balance = $d->balance;
                if ($d->balance < 0) {
                    $bal = '('.str_replace('-', '', number_format($d->balance, 2, '.', '')).')';
                } else {
                    $bal = number_format($d->balance, 2, '.', '');
                }
                $data['results'][] = [
                    'type' => $d->type,
                    'date' => date('d/m/Y', strtotime($d->date)),
                    'description' => $d->description,
                    'in' => $d->in,
                    'out' => $d->out,
                    'balance' => $bal, // number_format($d->balance,2, '.', ''),
                ];
            }
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
            $inputs['year'] = date('Y');
            $data = [];
            $initial_balance = '';
        }

        $data['title'] = '供应商月份结单 - '.$customsbroker->name;
        $data['initial_balance'] = $initial_balance;
        $filename = 'BalanceSheetSupplier-'.$customsbroker->code.'.xlsx';

        // return view('report.export.balance_sheet_supplier', ['data' => $data]);
        return Excel::download(new ReportExportBalanceSheetSupplier('report.export.balance_sheet_supplier', $data), $filename);
    }

    /* Print */
    public function ReportPrintStaffCommission(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direotion')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        // if(null !== ($request->input('date_from'))){
        //   $dtf = str_replace('/', '-', $request->input('date_from'));
        //   $date_from = date('Y-m-d', strtotime($dtf));
        //   $inputs['date_from'] = $request->input('date_from');
        // }else{
        //   $date_from = date('Y-m-1');
        //   $inputs['date_from'] = date('01/m/Y');
        // }
        //
        // if(null !== ($request->input('date_to'))){
        //   $dtt = str_replace('/', '-', $request->input('date_to'));
        //   $date_to =  date('Y-m-d', strtotime($dtt));
        //   $inputs['date_to'] = $request->input('date_to');
        // }else{
        //   $date_to = date('Y-m-d');
        //   $inputs['date_to'] = date('d/m/Y');
        // }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $year;
        } else {
            $year = date('Y');
            $inputs['year'] = $year;
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $month;
        } else {
            $month = date('m');
            $inputs['month'] = $month;
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = date('Y-m-t', strtotime($date_from));

        $inputs['date_from'] = $date_from;
        $inputs['date_to'] = $date_to;

        if (null !== $request->input('employee_id')) {
            $employee_id = $request->input('employee_id');
            $inputs['employee_id'] = $request->input('employee_id');
        } else {
            $employee_id = '';
            $inputs['employee_id'] = '';
            $inputs['employee'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            // 'customer_id' => $customer_id,
            'employee_id' => $employee_id,
        ];

        $header = ['No.', '装柜日期', '客户名称', '货柜号码', '预订号'];
        $footer = [];
        $data = [];

        $employees = \App\Lib\Queries\Admin\GetEmployees::Status(1);

        if ($employee_id != '') {
            $dto = \App\Lib\Queries\Admin\GetReportStaffCommissionEmployee::Result($params);
            // $employees = \App\Lib\Queries\Admin\GetEmployees::Status(1);
            $employee = \App\Lib\Queries\Admin\GetUser::Result($employee_id);
        // array_push($header,$employee->name);
        } else {
            $dto = \App\Lib\Queries\Admin\GetReportStaffCommission::Result($params);
            // foreach($employees as $employee){
            //   array_push($header,$employee->name);
            // }
        }

        // echo "<pre>";
        // print_r($dto);
        // echo "</pre>";
        if (!empty($dto)) {
            foreach ($dto as $k => $v) {
                $workers = [];

                if ($employee_id != '') {
                    $jobs = '';
                    foreach ($v->employees as $e) {
                        $jobs .= $e->job.'<br/> ';
                    }
                    $workers[$employee_id] = $jobs;
                } else {
                    foreach ($employees as $ey) {
                        $jobs = '';

                        foreach ($v->employees as $e) {
                            if ($ey->user_id == $e->employee_id) {
                                $jobs .= $e->job.'<br/>';
                            }
                        }

                        $jobs = rtrim($jobs, '<br/>');
                        $workers[$ey->user_id] = $jobs;
                    }
                }
                // echo "<pre>";
                // print_r($workers);
                // echo "</pre>";

                $data[] = [
                    'no' => $k + 1,
                    'loading_date' => $v->loading_date,
                    'customer_name' => $v->customer_name,
                    'container_no' => $v->container_no,
                    'booking_no' => $v->booking_no,
                    'employees' => $workers,
                ];
            }
        }

        if ($employee_id == '') {
            foreach ($employees as $e) {
                $j = [];
                foreach ($dto as $d) {
                    foreach ($d->employees as $ee) {
                        if ($ee->employee_id == $e->user_id) {
                            $j[] = $ee->job;
                        }
                    }
                }
                // echo "<pre>";
                // print_r($j);
                // echo "</pre>";
                if (!empty($j)) {
                    $footer[$e->user_id] = [
                        'user_id' => $e->user_id,
                        'name' => $e->name,
                        'job' => array_count_values($j),
                    ];
                }
            }
        } else {
            $employee = \App\Lib\Queries\Admin\GetUser::Result($employee_id);
            $inputs['employee'] = ' - '.$employee->name;
            $j = [];
            foreach ($dto as $d) {
                foreach ($d->employees as $ee) {
                    if ($ee->employee_id == $employee_id) {
                        $j[] = $ee->job;
                    }
                }
            }

            // echo "<pre>";
            // print_r($j);
            // echo "</pre>";
            if (!empty($j)) {
                $footer[$employee_id] = [
                    'user_id' => $employee_id,
                    'name' => $employee->name,
                    'job' => array_count_values($j),
                ];
            }
        }

        // get only got job employee
        foreach ($footer as $ft) {
            if (count($ft['job']) > 0) {
                array_push($header, $ft['name']);
            }
        }

        $vm = new \App\Http\ViewModels\Admin\ReportStaffCommissionViewModel($data, $inputs, $header, $footer);

        return view('report/print/staff_commission', ['vm' => $vm]);
    }

    public function ReportPrintBookingOrder(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'booking_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d', strtotime(date('Y-m-d').' + 14 days'));
            $inputs['date_to'] = date('d/m/Y', strtotime(date('Y-m-d').' + 14 days'));
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportBookingOrder::Result($params);

        $vm = new \App\Http\ViewModels\Admin\ReportBookingOrderViewModel($dto, $inputs);

        return view('report/print/booking_order', ['vm' => $vm]);
    }

    public function ReportPrintContainerLoading(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        // if(null !== ($request->input('date_from'))){
        //   $dtf = str_replace('/', '-', $request->input('date_from'));
        //   $date_from = date('Y-m-d', strtotime($dtf));
        //   $inputs['date_from'] = $request->input('date_from');
        // }else{
        //   $date_from = date('Y-m-1');
        //   $inputs['date_from'] = date('01/m/Y');
        // }
        //
        // if(null !== ($request->input('date_to'))){
        //   $dtt = str_replace('/', '-', $request->input('date_to'));
        //   $date_to =  date('Y-m-d', strtotime($dtt));
        //   $inputs['date_to'] = $request->input('date_to');
        // }else{
        //   $date_to = date('Y-m-d');
        //   $inputs['date_to'] = date('d/m/Y');
        // }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = $request->input('year');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
            $customer_id = explode(',', $customer_id);
        } else {
            $customer_id = [];
            $inputs['customer_id'] = [];
        }

        // if(null !== ($request->input('customer_id'))){
        //   $customer_id = $request->input('customer_id');
        //   $inputs['customer_id'] = $request->input('customer_id');
        // }else{
        //   $customer_id = array();
        //   $inputs['customer_id'] = array();
        // }

        if (null !== $request->input('agent_id')) {
            $agent_id = $request->input('agent_id');
            $inputs['agent_id'] = $request->input('agent_id');
            $agent = \App\Lib\Queries\Admin\GetAgent::Result($agent_id);
            $inputs['agent'] = ' - '.$agent->name;
        } else {
            $agent_id = '';
            $inputs['agent_id'] = '';
            $inputs['agent'] = '';
        }

        if (null !== $request->input('user_id')) {
            $user_id = $request->input('user_id');
            $inputs['user_id'] = $request->input('user_id');
        } else {
            $user_id = '';
            $inputs['user_id'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
            'agent_id' => $agent_id,
            'user_id' => $user_id
        ];

        $dto = \App\Lib\Queries\Admin\GetReportContainerLoading::Result($params);
        $mth = $this->GetChineseMonthName($month);
        $title = $year.'年 '.$mth.'份';
        $vm = new \App\Http\ViewModels\Admin\ReportContainerLoadingViewModel($dto, $inputs, $title);

        return view('report/print/container_loading', ['vm' => $vm]);
    }

    public function ReportPrintBillingCompany(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = $request->input('year');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        // if(null !== ($request->input('billing_company_id'))){
        //   $billing_company_id = '%' . $request->input('billing_company_id') . '%';
        //   $inputs['billing_company_id'] = $request->input('billing_company_id');
        //     $billing_company  = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($request->input('billing_company_id'));
        //   if(!empty($billing_company)){
        //     $company = $billing_company->name;
        //   }else{
        //     $company = "";
        //   }
        //   $title = $company . ' For the month ' . strtoupper($mth) . ' ' . $year;
        //
        // }else{
        //   $billing_company_id = '';
        //   $inputs['billing_company_id'] = '';
        //   $title = 'All company For the month ' . strtoupper($mth) . ' ' .  $year;
        // }

        if (null !== $request->input('billing_company_id')) {
            $billing_company_id = $request->input('billing_company_id');
            // $billing_company_id = '%' . $request->input('billing_company_id') . '%';
            $inputs['billing_company_id'] = $request->input('billing_company_id');
            // $billing_company  = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($request->input('billing_company_id'));

            // if(!empty($billing_company)){
            //   $company = $billing_company->name;
            // }else{
            //   $company = "";
            // }
            $company = '';
            $billing_companies = [];

            $billing_company_id = explode(',', $billing_company_id);
            foreach ($billing_company_id as $k => $v) {
                if ($v != '') {
                    $company = $company.' '.$v;
                    $billing_company = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($v);
                    $billing_companies[] = $billing_company->id;
                }
            }
            // echo "<pre>";
            // print_r($billing_companies);
            // echo "</pre>";
            $title = $company.' Container list for the month of '.strtoupper($mth).' '.$year;
        } else {
            $billing_company_id = [];
            $inputs['billing_company_id'] = [];
            $billing_companies = [];
            $title = 'All Container list for the month of  '.strtoupper($mth).' '.$year;
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'billing_company_id' => $billing_company_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportBillingCompany::Result($params);
        $company_inv_amount = \App\Lib\Queries\Admin\GetReportBillingCompany::GetCompanyInvAmount($params);
        $supplier_inv_amount = \App\Lib\Queries\Admin\GetReportBillingCompany::GetSupplierInvAmount($params);

        $net_profit = $company_inv_amount - $supplier_inv_amount;

        $vm = new \App\Http\ViewModels\Admin\ReportBillingCompanyViewModel($dto, $inputs, $title, number_format($company_inv_amount, 2, '.', ','), number_format($supplier_inv_amount, 2, '.', ','), number_format($net_profit, 2, '.', ','));

        return view('report/print/billing_company', ['vm' => $vm]);
    }

    public function ReportPrintMonthlyStatement(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = $request->input('year');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
        }

        if (null !== $request->input('billing_company_id')) {
            $billing_company_id = $request->input('billing_company_id');
            // $billing_company_id = '%' . $request->input('billing_company_id') . '%';
            $inputs['billing_company_id'] = $request->input('billing_company_id');
            // $billing_company  = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($request->input('billing_company_id'));

            // if(!empty($billing_company)){
            //   $company = $billing_company->name;
            // }else{
            //   $company = "";
            // }
            $company = '';
            $billing_companies = [];

            $billing_company_id = explode(',', $billing_company_id);
            foreach ($billing_company_id as $k => $v) {
                if ($v != '') {
                    $company = $company.' '.$v;
                    $billing_company = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($v);
                    $billing_companies[] = $billing_company->id;
                }
            }
            // echo "<pre>";
            // print_r($billing_companies);
            // echo "</pre>";
            $title = $company.' Container list for the month of '.strtoupper($mth).' '.$year;
        } else {
            $billing_company_id = [];
            $inputs['billing_company_id'] = [];
            $billing_companies = [];
            $title = 'All Container list for the month of  '.strtoupper($mth).' '.$year;
        }

        if (null !== $request->input('rate')) {
            $rate = $request->input('rate');
            $inputs['rate'] = $request->input('rate');
        } else {
            $rate = $this->CurrentCurrencyRate();
            $inputs['rate'] = $rate;
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
            'billing_company_id' => $billing_company_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportMonthlyStatement::Result($params);

        $total_net_total = 0;
        $total_total2 = 0;
        $total_billing_handling = 0;
        $total_customs_handling = 0;
        $total_billing_other = 0;
        $total_customs_other = 0;
        $total_handling_charges = 0;
        $total_commission = 0;
        $total_freight_charges = 0;
        $total_net_freight_charges = 0;
        $total_loadingfee = 0;

        foreach ($dto as $d) {
            $net_total = $d->total + ($d->total_charges / $rate);
            $total_net_total += $net_total;
            $total_total2 += $d->total_charges;
            // total
            $total_billing_handling += $d->billing_handling_prices;
            $total_customs_handling += $d->custom_handling_prices;
            $total_billing_other += $d->billing_other_prices;
            $total_customs_other += $d->custom_other_prices;
            $total_handling_charges += $d->handling_charges;
            $total_commission += $d->commission;
            $total_freight_charges += $d->freight_charges;
            $total_net_freight_charges += $d->net_freight_charges;
            $total_loadingfee += $d->container_loading_charges;

            $d->custom_handling_prices = number_format($d->custom_handling_prices, 2);
            $d->billing_handling_prices = number_format($d->billing_handling_prices, 2);
            $d->handling_charges = number_format($d->handling_charges, 2);
            $d->commission = number_format($d->commission, 2);
            $d->freight_charges = number_format($d->freight_charges, 2);
            $d->net_freight_charges = number_format($d->net_freight_charges, 2);
            $d->container_loading_charges = number_format($d->container_loading_charges, 2);
            $d->total = number_format($d->total, 2);
            $d->total_charges = number_format($d->total_charges, 2);
            $d->net_total = number_format($net_total, 2);
        }

        // $total_billing_handling = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalBillingHandling($params);
        // $total_customs_handling = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalCustomsHandling($params);
        $total_total = ($total_billing_handling + $total_billing_other) - ($total_customs_handling + $total_customs_other);
        // $handling_charges = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalHandlingCharges($params);
        // $commission = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalCommission($params);
        // $freight_charges = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalFreightCharges($params);
        // $loadingfee = \App\Lib\Queries\Admin\GetReportMonthlyStatement::TotalLoadingFee($params);

        // $total_total2 = $handling_charges + $commission - $freight_charges - $loadingfee;

        $total = [
            'billing_handling' => number_format($total_billing_handling, 2),
            'billing_other' => number_format($total_billing_other, 2),
            'customs_handling' => number_format($total_customs_handling, 2),
            'customs_other' => number_format($total_customs_other, 2),
            'total' => number_format($total_total, 2),
            'handling_charges' => number_format($total_handling_charges, 2),
            'commission' => number_format($total_commission, 2),
            'freight_charges' => number_format($total_freight_charges, 2),
            'net_freight_charges' => number_format($total_net_freight_charges, 2),
            'loadingfee' => number_format($total_loadingfee, 2),
            'total_charges' => number_format($total_total2, 2),
            'net_total' => number_format($total_net_total, 2),
        ];

        $vm = new \App\Http\ViewModels\Admin\ReportMonthlyStatementViewModel($dto, $total, $inputs, $title, $rate);

        return view('report/print/monthly_statement', ['vm' => $vm]);
    }

    public function ReportPrintYearlyStatement(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = $request->input('year');
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
            $_customer = \App\Lib\Queries\Admin\GetCustomer::Result($customer_id);
            if (!empty($_customer)) {
                $customer = $_customer->name;
            } else {
                $customer = '';
            }
        } else {
            $customer_id = '';
            $customer = '';
            $inputs['customer_id'] = '';
        }

        if (null !== $request->input('billing_company_id')) {
            $billing_company_id = $request->input('billing_company_id');

            $inputs['billing_company_id'] = $request->input('billing_company_id');

            $company = '';
            $billing_companies = [];

            $billing_company_id = explode(',', $billing_company_id);
            foreach ($billing_company_id as $k => $v) {
                if ($v != '') {
                    $company = $company.' '.$v;
                    $billing_company = \App\Lib\Queries\Admin\GetCompany::ResultbyCode($v);
                    $billing_companies[] = $billing_company->id;
                }
            }
        } else {
            $billing_company_id = [];
            $inputs['billing_company_id'] = [];
            $billing_companies = [];
        }

        if (null !== $request->input('rate')) {
            $rate = $request->input('rate');
            $inputs['rate'] = $request->input('rate');
        } else {
            $rate = $this->CurrentCurrencyRate();
            $inputs['rate'] = $rate;
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'year' => $year,
            'customer_id' => $customer_id,
            'billing_company_id' => $billing_company_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportYearlyStatement::Result($params);

        $total_net_total = 0;
        $total_total2 = 0;
        $total_billing_handling = 0;
        $total_customs_handling = 0;
        $total_billing_other = 0;
        $total_customs_other = 0;
        $total_handling_charges = 0;
        $total_commission = 0;
        $total_freight_charges = 0;
        $total_net_freight_charges = 0;
        $total_loadingfee = 0;

        foreach ($dto as $d) {
            $net_total = $d->total + ($d->total_charges / $rate);
            $total_net_total += $net_total;
            $total_total2 += $d->total_charges;
            // total
            $total_billing_handling += $d->billing_handling_prices;
            $total_customs_handling += $d->custom_handling_prices;
            $total_billing_other += $d->billing_other_prices;
            $total_customs_other += $d->custom_other_prices;
            $total_handling_charges += $d->handling_charges;
            $total_commission += $d->commission;
            $total_freight_charges += $d->freight_charges;
            $total_net_freight_charges += $d->net_freight_charges;
            $total_loadingfee += $d->container_loading_charges;

            $d->custom_handling_prices = number_format($d->custom_handling_prices, 2);
            $d->billing_handling_prices = number_format($d->billing_handling_prices, 2);
            $d->handling_charges = number_format($d->handling_charges, 2);
            $d->commission = number_format($d->commission, 2);
            $d->freight_charges = number_format($d->freight_charges, 2);
            $d->net_freight_charges = number_format($d->net_freight_charges, 2);
            $d->container_loading_charges = number_format($d->container_loading_charges, 2);
            $d->total = number_format($d->total, 2);
            $d->total_charges = number_format($d->total_charges, 2);
            $d->net_total = number_format($net_total, 2);
        }

        $total_total = ($total_billing_handling + $total_billing_other) - ($total_customs_handling + $total_customs_other);
        $total = [
            'billing_handling' => number_format($total_billing_handling, 2),
            'billing_other' => number_format($total_billing_other, 2),
            'customs_handling' => number_format($total_customs_handling, 2),
            'customs_other' => number_format($total_customs_other, 2),
            'total' => number_format($total_total, 2),
            'handling_charges' => number_format($total_handling_charges, 2),
            'commission' => number_format($total_commission, 2),
            'freight_charges' => number_format($total_freight_charges, 2),
            'net_freight_charges' => number_format($total_net_freight_charges, 2),
            'loadingfee' => number_format($total_loadingfee, 2),
            'total_charges' => number_format($total_total2, 2),
            'net_total' => number_format($total_net_total, 2),
        ];

        $title = $customer.' Container list for the year of '.$year;

        $vm = new \App\Http\ViewModels\Admin\ReportYearlyStatementViewModel($dto, $total, $inputs, $title, $rate);

        return view('report/print/yearly_statement', ['vm' => $vm]);
    }

    public function ReportPrintDailyPaymentTransfer(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'payment_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d');
            $inputs['date_to'] = date('d/m/Y');
        }

        if (null !== $request->input('supplier_id')) {
            $supplier_id = $request->input('supplier_id');
            $inputs['supplier_id'] = $request->input('supplier_id');
            $supplier = \App\Lib\Queries\Admin\GetSupplier::Result($supplier_id);
            $inputs['supplier'] = ' - '.$supplier->name;
        } else {
            $supplier_id = '';
            $inputs['supplier_id'] = '';
            $inputs['supplier'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'supplier_id' => $supplier_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::Result($params);
        $total_customer_amount = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalCustomerAmount($params);
        $total_customer_total = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalCustomerTotal($params);
        $total_supplier_amount = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalSupplierAmount($params);
        $total_supplier_total = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalSupplierTotal($params);
        $total_supplier_bank_charges = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalSupplierBankCharges($params);
        $total_supplier_net_amount = \App\Lib\Queries\Admin\GetReportDailyPaymentTransfer::GetTotalSupplierNetAmount($params);
        $total = [
            'total_customer_amount' => number_format($total_customer_amount, 2),
            'total_customer_total' => number_format($total_customer_total, 2),
            'total_supplier_amount' => number_format($total_supplier_amount, 2),
            'total_supplier_total' => number_format($total_supplier_total, 2),
            'total_supplier_bank_charges' => number_format($total_supplier_bank_charges, 2),
            'total_supplier_net_amount' => number_format($total_supplier_net_amount, 2),
        ];

        $vm = new \App\Http\ViewModels\Admin\ReportDailyPaymentTransferViewModel($dto, $total, $inputs);

        return view('report/print/daily_payment_transfer', ['vm' => $vm]);
    }

    public function ReportPrintDailyPayment(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'payment_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d');
            $inputs['date_to'] = date('d/m/Y');
        }

        if (null !== $request->input('supplier_id')) {
            $supplier_id = $request->input('supplier_id');
            $inputs['supplier_id'] = $request->input('supplier_id');
            $supplier = \App\Lib\Queries\Admin\GetCustomsBroker::Result($supplier_id);
            $inputs['supplier'] = ' - '.$supplier->name;
        } else {
            $supplier_id = '';
            $inputs['supplier_id'] = '';
            $inputs['supplier'] = '';
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        // $customer = \App\Lib\Queries\Admin\GetCustomer::Result($customer_id);
        // $inputs['customer'] = ' - ' .$customer->name;
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
            // $inputs['customer'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'supplier_id' => $supplier_id,
            'customer_id' => $customer_id,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportDailyPayment::Result($params);
        $total_customer_amount = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalCustomerAmount($params);
        $total_customer_total = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalCustomerTotal($params);
        $total_supplier_amount = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalSupplierAmount($params);
        $total_supplier_total = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalSupplierTotal($params);
        $total_supplier_bank_charges = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalSupplierBankCharges($params);
        $total_supplier_net_amount = \App\Lib\Queries\Admin\GetReportDailyPayment::GetTotalSupplierNetAmount($params);
        $total = [
            'total_customer_amount' => number_format($total_customer_amount, 2),
            'total_customer_total' => number_format($total_customer_total, 2),
            'total_supplier_amount' => number_format($total_supplier_amount, 2),
            'total_supplier_total' => number_format($total_supplier_total, 2),
            'total_supplier_bank_charges' => number_format($total_supplier_bank_charges, 2),
            'total_supplier_net_amount' => number_format($total_supplier_net_amount, 2),
        ];

        $vm = new \App\Http\ViewModels\Admin\ReportDailyPaymentViewModel($dto, $total, $inputs);

        return view('report/print/daily_payment', ['vm' => $vm]);
    }

    public function ReportPrintCargoRecord(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d');
            $inputs['date_to'] = date('d/m/Y');
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
        }

        if (null !== $request->input('customs_broker_id')) {
            $customs_broker_id = $request->input('customs_broker_id');
            $inputs['customs_broker_id'] = $request->input('customs_broker_id');
        } else {
            $customs_broker_id = '';
            $inputs['customs_broker_id'] = '';
        }

        if (null !== $request->input('owner_id')) {
            $owner_id = $request->input('owner_id');
            $inputs['owner_id'] = $request->input('owner_id');
        } else {
            $owner_id = '';
            $inputs['owner_id'] = '';
        }

        if (null !== $request->input('warehouse_id')) {
            $warehouse_id = $request->input('warehouse_id');
            $inputs['warehouse_id'] = $request->input('warehouse_id');
        } else {
            $warehouse_id = '';
            $inputs['warehouse_id'] = '';
        }

        if (null !== $request->input('tracking_no')) {
            $tracking_no = $request->input('tracking_no');
            $inputs['tracking_no'] = $request->input('tracking_no');
        } else {
            $tracking_no = '';
            $inputs['tracking_no'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
            'customs_broker_id' => $customs_broker_id,
            'owner_id' => $owner_id,
            'warehouse_id' => $warehouse_id,
            'tracking_no' => $tracking_no,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportCargoRecord::Result($params);

        $total_cbm = 0;
        $total_supplier_inv_amount = 0;
        $total_supplier_goods_amount = 0;
        $total_customer_goods_amount = 0;
        $total_transport_inv_amount = 0;

        foreach ($dto as $d) {
            // total
            $total_cbm += $d->cbm;
            $total_supplier_inv_amount += $d->supplier_inv_amount;
            $total_supplier_goods_amount += $d->supplier_goods_amount;
            $total_customer_goods_amount += $d->customer_goods_amount;
            $total_transport_inv_amount += $d->transport_inv_amount;
        }

        $net_total_supplier_goods_amount = $total_customer_goods_amount - $total_supplier_goods_amount;

        $net_total_transport_inv_amount = $total_transport_inv_amount - $total_supplier_inv_amount;

        $total = [
            'cbm' => number_format($total_cbm, 3),
            'supplier_inv_amount' => number_format($total_supplier_inv_amount, 2),
            'supplier_goods_amount' => number_format($total_supplier_goods_amount, 2),
            'customer_goods_amount' => number_format($total_customer_goods_amount, 2),
            'transport_inv_amount' => number_format($total_transport_inv_amount, 2),
            'net_total_supplier_goods_amount' => number_format($net_total_supplier_goods_amount, 2),
            'net_total_transport_inv_amount' => number_format($net_total_transport_inv_amount, 2),
        ];

        // title
        $month_from = date('m', strtotime($dtf));
        $month_to = date('m', strtotime($dtt));

        $mf = $this->GetMonthName($month_from);
        $mt = $this->GetMonthName($month_to);

        if ($mf == $mt) {
            $title = $mf.' '.date('Y', strtotime($dtf));
        } else {
            $title = $mf.' - '.$mt.' '.date('Y', strtotime($dtf));
        }

        $vm = new \App\Http\ViewModels\Admin\ReportCargoRecordViewModel($dto, $total, $inputs, $title);

        return view('report/print/cargo_record', ['vm' => $vm]);
    }

    public function ReportPrintBalanceSheetCustomer(Request $request)
    {
        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');

            $inputs['customer_id'] = $request->input('customer_id');

            if (null !== $request->input('year')) {
                $year = $request->input('year');
                $inputs['year'] = $request->input('year');
            } else {
                $year = date('Y');
                $inputs['year'] = date('Y');
            }

            $params[] = [
                'customer_id' => $customer_id,
                'year' => $year,
            ];

            // Calculate existing year balance
            $inAmt = \App\Lib\Queries\Admin\GetReportBalanceSheetCustomer::GetTotalInAmount($params);
            $outAmt = \App\Lib\Queries\Admin\GetReportBalanceSheetCustomer::GetTotalOutAmount($params);

            $dto = \App\Lib\Queries\Admin\GetReportBalanceSheetCustomer::GetInRecords($params);
            $dto2 = \App\Lib\Queries\Admin\GetReportBalanceSheetCustomer::GetOutRecords($params);

            foreach ($dto as $d) {
                $d->type = 'in';
            }

            foreach ($dto2 as $d) {
                $d->type = 'out';
            }
            $data = [];
            $data = array_merge($dto, $dto2);
            usort($data, function ($dto, $dto2) {
                return strtotime($dto->date) - strtotime($dto2->date);
            });

            $customer = \App\Lib\Queries\Admin\GetCustomer::Result($customer_id);
            $balance = $customer->initial_balance;
            $initial_balance = $customer->initial_balance;

            $balance = $balance + $inAmt - $outAmt;
            $initial_balance = $initial_balance + $inAmt - $outAmt;
            // $initial_balance = number_format($initial_balance, 2);
            if ($initial_balance < 0) {
                $initial_balance = '('.str_replace('-', '', number_format($initial_balance, 2)).')';
            } else {
                $initial_balance = number_format($initial_balance, 2);
            }
            foreach ($data as $d) {
                if ($d->type == 'in') {
                    $d->balance = $balance + $d->in;
                    $d->in = number_format($d->in, 2);
                    $d->out = '';
                } elseif ($d->type == 'out') {
                    $d->balance = $balance - $d->out;
                    $d->in = '';
                    $d->out = number_format($d->out, 2);
                } else {
                    $d->in = '';
                    $d->out = '';
                }

                $balance = $d->balance;
                $d->balance = number_format($d->balance, 2);
                if ($d->balance < 0) {
                    $d->balance = '('.str_replace('-', '', $d->balance).')';
                }
                $d->date = date('d/m/Y', strtotime($d->date));
            }

            $title = $customer->name;
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
            $inputs['year'] = date('Y');
            $title = '';
            $data = [];
            $initial_balance = '';
        }

        $vm = new \App\Http\ViewModels\Admin\ReportBalanceSheetCustomerViewModel($data, $inputs, $title, $initial_balance);

        return view('report/print/balance_sheet_customer', ['vm' => $vm]);
    }

    public function ReportPrintBalanceSheetSupplier(Request $request)
    {
        if (null !== $request->input('customsbroker_id')) {
            $customsbroker_id = $request->input('customsbroker_id');
            $inputs['customsbroker_id'] = $request->input('customsbroker_id');

            if (null !== $request->input('billing_company_id')) {
                $billing_company_id = $request->input('billing_company_id');
                $inputs['billing_company_id'] = $request->input('billing_company_id');
            } else {
                $billing_company_id = '';
                $inputs['billing_company_id'] = '';
            }

            if (null !== $request->input('year')) {
                $year = $request->input('year');
                $inputs['year'] = $request->input('year');
            } else {
                $year = date('Y');
                $inputs['year'] = date('Y');
            }

            $params[] = [
                'customsbroker_id' => $customsbroker_id,
                'billing_company_id' => $billing_company_id,
                'year' => $year,
            ];

            // Calculate existing year balance
            $inAmt = \App\Lib\Queries\Admin\GetReportBalanceSheetSupplier::GetTotalInAmount($params);
            $outAmt = \App\Lib\Queries\Admin\GetReportBalanceSheetSupplier::GetTotalOutAmount($params);

            $dto = \App\Lib\Queries\Admin\GetReportBalanceSheetSupplier::GetInRecords($params);
            $dto2 = \App\Lib\Queries\Admin\GetReportBalanceSheetSupplier::GetOutRecords($params);

            foreach ($dto as $d) {
                $d->type = 'in';
            }

            foreach ($dto2 as $d) {
                $d->type = 'out';
            }
            $data = [];
            $data = array_merge($dto, $dto2);
            usort($data, function ($dto, $dto2) {
                return strtotime($dto->date) - strtotime($dto2->date);
            });

            $customsbroker = \App\Lib\Queries\Admin\GetCustomsBroker::Result($customsbroker_id);
            // $balance = $customsbroker->initial_balance;
            // $initial_balance = $customsbroker->initial_balance;

            if ($billing_company_id != '') {
                $balance = $this->GetSupplierInitialValue($customsbroker->balance, $billing_company_id);
                $initial_balance = $this->GetSupplierInitialValue($customsbroker->balance, $billing_company_id);
            } else {
                $_bal = 0;
                if (!empty($customsbroker->balance)) {
                    foreach ($customsbroker->balance as $ib) {
                        $_bal += $ib->balance;
                    }
                }
                $balance = $_bal;
                $initial_balance = $_bal;
            }

            $balance = $balance + $inAmt - $outAmt;
            $initial_balance = $initial_balance + $inAmt - $outAmt;
            // $initial_balance = number_format($initial_balance, 2);
            if ($initial_balance < 0) {
                $initial_balance = '('.str_replace('-', '', number_format($initial_balance, 2)).')';
            } else {
                $initial_balance = number_format($initial_balance, 2);
            }
            foreach ($data as $d) {
                if ($d->type == 'in') {
                    $d->balance = $balance + $d->in;
                    $d->in = number_format($d->in, 2);
                    $d->out = '';
                } elseif ($d->type == 'out') {
                    $d->balance = $balance - $d->out;
                    $d->in = '';
                    $d->out = number_format($d->out, 2);
                } else {
                    $d->in = '';
                    $d->out = '';
                }

                $balance = $d->balance;
                $d->balance = number_format($d->balance, 2);
                if ($d->balance < 0) {
                    $d->balance = '('.str_replace('-', '', $d->balance).')';
                }
                $d->date = date('d/m/Y', strtotime($d->date));
            }

            $title = $customsbroker->name;
        } else {
            $customer_id = '';
            $inputs['customsbroker_id'] = '';
            $inputs['billing_company_id'] = '';
            $inputs['year'] = date('Y');
            $title = '';
            $data = [];
            $initial_balance = '';
        }

        $vm = new \App\Http\ViewModels\Admin\ReportBalanceSheetSupplierViewModel($data, $inputs, $title, $initial_balance);

        return view('report/print/balance_sheet_supplier', ['vm' => $vm]);
    }

    /* other */
    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public function CurrentOrderNo()
    {
        $order_no = 0;
        $dto = \App\Lib\Queries\Admin\GetSetting::Key('current_order_id');
        if (!empty($dto)) {
            $order_no = $dto->setting_value + 1;
        }

        return $order_no;
    }

    public function CurrentCurrencyRate()
    {
        $rate = 0;
        $dto = \App\Lib\Queries\Admin\GetSetting::Key('exchange_rate');
        if (!empty($dto)) {
            $rate = $dto->setting_value;
        }

        return $rate;
    }

    public function GetMonthName($month)
    {
        $title = '';
        switch ($month) {
            case 1:
                $title = 'January';
                break;
            case 2:
                $title = 'February';
                break;
            case 3:
                $title = 'March';
                break;
            case 4:
                $title = 'April';
                break;
            case 5:
                $title = 'May';
                break;
            case 6:
                $title = 'June';
                break;
            case 7:
                $title = 'July';
                break;
            case 8:
                $title = 'August';
                break;
            case 9:
                $title = 'September';
                break;
            case 10:
                $title = 'October';
                break;
            case 11:
                $title = 'November';
                break;
            case 12:
                $title = 'December';
                break;
        }

        return $title;
    }

    public function GetChineseMonthName($month)
    {
        $title = '';
        switch ($month) {
            case 1:
                $title = '一月';
                break;
            case 2:
                $title = '二月';
                break;
            case 3:
                $title = '三月';
                break;
            case 4:
                $title = '四月';
                break;
            case 5:
                $title = '五月';
                break;
            case 6:
                $title = '六月';
                break;
            case 7:
                $title = '七月';
                break;
            case 8:
                $title = '八月';
                break;
            case 9:
                $title = '九月';
                break;
            case 10:
                $title = '十月';
                break;
            case 11:
                $title = '十一月';
                break;
            case 12:
                $title = '十二月';
                break;
        }

        return $title;
    }

    public function sortBSC($a, $b)
    {
        echo '<pre>';
        print_r($a);
        echo '</pre>';
        // return strtotime($a->date) - strtotime($b->date);

        // usort($data, function($a, $b) {
        //     return $a->date <=> $b->date;
        // });
    }

    public function GetSupplierInitialValue($data, $company_id)
    {
        if (!empty($data)) {
            $balances = [$data];
            if (isset($balances[0]->$company_id->balance)) {
                $value = $balances[0]->$company_id->balance;
            } else {
                $value = 0;
            }
        } else {
            $value = 0;
        }

        return number_format($value, 2, '.', '');
    }
}
