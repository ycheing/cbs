<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommonController extends Controller
{

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function GetCountry(Request $request){
    $dto = "";
    if($request->input('country_id') != 0){
      $dto = \App\Lib\Queries\Common\GetCountry::Result($request->input('country_id'));
    }
    return response()->json(['json'=>$dto], 200);
  }

  public function GetCustomer(Request $request){
    $dto = "";
    if($request->input('customer_id') != 0){
      $dto = \App\Lib\Queries\Common\GetCustomer::Result($request->input('customer_id'));
    }
    return response()->json($dto, 200);
  }


  /*common*/
  public function UplaodFilePost(Request $request) {
    $path = $request->file('file')->store('upload');
    return $this->JsonOk(['filenames' =>$path, 'path' =>$path]);
  }

  public function DownloadFile($path, $name) {
    $diskPath = storage_path('app/upload/' . $path);
    return \Response::download($diskPath, 'image');
  }

  // public function DownloadFile($path, $name) {
  //     $diskPath = storage_path('app/downloadfile/' . $path);
  //     return \Response::download($diskPath, $name);
  // }

}
