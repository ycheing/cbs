<?php

namespace App\Http\Controllers;

use App\Export\ReportExportAgentBookingOrder;
use App\Export\ReportExportAgentCargoRecord;
use App\Export\ReportExportAgentContainerLoading;
use App\Export\ReportExportAgentStaffCommission;
use App\Export\ReportExportStaffCommission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class AgentController extends Controller
{
    private $limits = 15;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('dashboard/agent');
    }

    // Customer
    public function Customers(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'name';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('keyword')) {
            $keyword = '%'.strtolower($request->input('keyword')).'%';
            $inputs['keyword'] = $request->input('keyword');
        } else {
            $keyword = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'keyword' => $keyword,
        ];

        $dto = \App\Lib\Queries\Agent\GetCustomers::Result($params);
        $paging = \App\Lib\Queries\Agent\GetCustomers::Paging($params);
        $vm = new \App\Http\ViewModels\Agent\CustomersViewModel($dto, $paging);

        return view('agent/customer/list', ['vm' => $vm]);
    }

    public function AddCustomer()
    {
        $vm = new \App\Http\ViewModels\Agent\CustomerViewModel();

        return view('agent/customer/add', ['vm' => $vm]);
    }

    public function AddCustomerPost(\App\Http\Requests\Agent\CustomerRequest $request)
    {
        $data = $request->validated();
        // Biiling
        if ($data['billing_state_id'] != '' || $data['billing_state_id'] != 0) {
            $billing_state = \App\Lib\Queries\Common\GetState::Result($data['billing_state_id']);
            if (!empty($billing_state)) {
                $data['billing_state'] = $billing_state->name;
            } else {
                $data['billing_state'] = '';
            }
        } else {
            $data['billing_state'] = '';
        }

        if ($data['billing_country_id'] != '' || $data['billing_country_id'] != 0) {
            $billing_country = \App\Lib\Queries\Common\GetCountry::Result($data['billing_country_id']);
            $data['billing_country'] = $billing_country->name;
        } else {
            $data['billing_country'] = '';
        }

        // Shipping
        if ($data['shipping_state_id'] != 0) {
            $shipping_state = \App\Lib\Queries\Common\GetState::Result($data['shipping_state_id']);
            if (!empty($billing_state)) {
                $data['shipping_state'] = $billing_state->name;
            } else {
                $data['shipping_state'] = '';
            }
        } else {
            $data['shipping_state'] = '';
        }

        if ($data['shipping_country_id'] != '' || $data['shipping_country_id'] != 0) {
            $shipping_country = \App\Lib\Queries\Common\GetCountry::Result($data['shipping_country_id']);
            $data['shipping_country'] = $billing_country->name;
        } else {
            $data['shipping_country'] = '';
        }

        $data['created_by'] = Auth::user()->name;
        $data['updated_by'] = Auth::user()->name;

        dispatch(new \App\Lib\Commands\Agent\CreateCustomer($data));

        return $this->JsonOk();
    }

    public function EditCustomer($id)
    {
        $dto = \App\Lib\Queries\Agent\GetCustomer::Result($id);
        $vm = new \App\Http\ViewModels\Agent\CustomerEditViewModel($dto);

        return view('agent/customer/edit', ['vm' => $vm]);
    }

    public function EditCustomerPost(\App\Http\Requests\Agent\CustomerInfoRequest $request)
    {
        $data = $request->validated();

        // Biiling
        // if ($data['billing_state_id'] != '' || $data['billing_state_id'] != 0) {
        if (!empty($data['billing_state_id'])) {
            $billing_state = \App\Lib\Queries\Common\GetState::Result($data['billing_state_id']);
            if (!empty($billing_state)) {
                $data['billing_state'] = $billing_state->name;
            } else {
                $data['billing_state'] = '';
            }
        } else {
            $data['billing_state'] = '';
        }

        if ($data['billing_country_id'] != '' || $data['billing_country_id'] != 0) {
            $billing_country = \App\Lib\Queries\Common\GetCountry::Result($data['billing_country_id']);
            $data['billing_country'] = $billing_country->name;
        } else {
            $data['billing_country'] = '';
        }

        // Shipping
        if ($data['shipping_state_id'] != 0) {
            $shipping_state = \App\Lib\Queries\Common\GetState::Result($data['shipping_state_id']);
            if (!empty($shipping_state)) {
                $data['shipping_state'] = $shipping_state->name;
            } else {
                $data['shipping_state'] = '';
            }
        } else {
            $data['shipping_state'] = '';
        }

        if ($data['shipping_country_id'] != '' || $data['shipping_country_id'] != 0) {
            $shipping_country = \App\Lib\Queries\Common\GetCountry::Result($data['shipping_country_id']);
            $data['shipping_country'] = $shipping_country->name;
        } else {
            $data['shipping_country'] = '';
        }

        $data['updated_by'] = Auth::user()->name;

        dispatch(new \App\Lib\Commands\Agent\UpdateCustomer($data));

        return $this->JsonOk();
    }

    public function DeleteCustomerPost(Request $request)
    {
        $data['id'] = $request->input('id');
        if ($this->ValidateDeleteCustomer($request->input('id'))) {
            dispatch(new \App\Lib\Commands\Agent\DeleteCustomer($data));

            return $this->JsonOk();
        } else {
            return $this->JsonError('无法删除此客户。');
        }
    }

    private function ValidateDeleteCustomer($id)
    {
        $result = \App\Lib\Queries\Agent\GetCustomer::ValidateDelete($id);

        return $result;
    }

    public function CopyCustomer($id)
    {
        $dto = \App\Lib\Queries\Agent\GetCustomer::Result($id);
        $vm = new \App\Http\ViewModels\Agent\CustomerEditViewModel($dto);

        return view('agent/customer/copy', ['vm' => $vm]);
    }

    // Booking
    public function Bookings(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'updated_at';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'desc';
        }

        if (null !== $request->input('keyword')) {
            $keyword = '%'.strtolower($request->input('keyword')).'%';
            $inputs['keyword'] = $request->input('keyword');
        } else {
            $keyword = '';
            $inputs['keyword'] = '';
        }

        if (null !== $request->input('status_id')) {
            $status_id = $request->input('status_id');
            $inputs['status_id'] = $request->input('status_id');
        } else {
            $status_id = '';
            $inputs['status_id'] = '';
        }

        if (null !== $request->input('owner_id')) {
            $owner_id = Auth::user()->id;
            $inputs['owner_id'] = $request->input('owner_id');
        } else {
            $owner_id = '';
            $inputs['owner_id'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'keyword' => $keyword,
            'status_id' => $status_id,
            'owner_id' => $owner_id,
        ];

        $dto = \App\Lib\Queries\Agent\GetBookings::Result($params);
        $paging = \App\Lib\Queries\Agent\GetBookings::Paging($params);
        $vm = new \App\Http\ViewModels\Agent\BookingsViewModel($dto, $paging, $inputs);

        return view('agent/booking/list', ['vm' => $vm]);
    }

    public function BookingType()
    {
        return view('agent/booking/type');
    }

    public function BookingTypePost(\App\Http\Requests\Agent\BookingTypeRequest $request)
    {
        $data = $request->validated();
        if ($data['booking_type'] == 'booking') {
            $location = url('/agent/booking/add');
        } elseif ($data['booking_type'] == 'declaration') {
            $location = url('/agent/booking/declaration');
        }

        return $this->JsonOk(['location' => $location]);
    }

    public function AddBooking()
    {
        $vm = new \App\Http\ViewModels\Agent\BookingViewModel();

        return view('agent/booking/add', ['vm' => $vm]);
    }

    public function AddBookingPost(\App\Http\Requests\Agent\BookingRequest $request)
    {
        $data = $request->validated();

        $data['rate'] = $this->CurrentCurrencyRate();
        $data['order_no'] = 'OR'.date('Y').str_pad($this->currentOrderNo(), 4, '0', STR_PAD_LEFT);
        $data['order_num'] = $this->currentOrderNo();
        $data['booking_no'] = $data['customer_code'].'-'.$data['booking_no_1'].'-'.$data['booking_no_2'];
        $data['status'] = 1;
        $data['owner_id'] = Auth::user()->id;
        $data['created_by'] = Auth::user()->name;
        $data['updated_by'] = Auth::user()->name;

        if (isset($data['booking_date'])) {
            $data['booking_date'] = str_replace('/', '-', $data['booking_date']);
            $data['booking_date'] = date('Y-m-d', strtotime($data['booking_date']));
        }

        if (isset($data['closing_date'])) {
            $data['closing_date'] = str_replace('/', '-', $data['closing_date']);
            $data['closing_date'] = date('Y-m-d', strtotime($data['closing_date']));
        }

        if (isset($data['sailing_date'])) {
            $data['sailing_date'] = str_replace('/', '-', $data['sailing_date']);
            $data['sailing_date'] = date('Y-m-d', strtotime($data['sailing_date']));
        }

        // prevent empty country
        if ($data['delivery_state_id'] != '' || $data['delivery_state_id'] != 0) {
            $state = \App\Lib\Queries\Admin\GetState::Result($data['delivery_state_id']);
            if (!empty($state)) {
                $data['delivery_state'] = $state->name;
            }
        }

        // prevent empty state
        if ($data['delivery_country_id'] != '' || $data['delivery_country_id'] != 0) {
            $country = \App\Lib\Queries\Admin\GetCountry::Result($data['delivery_country_id']);
            $data['delivery_country'] = $country->name;
        }

        dispatch(new \App\Lib\Commands\Agent\CreateBooking($data));

        return $this->JsonOk();
    }

    public function AddBookingLoadingPost(Request $request)
    {
        $data['id'] = $request->input('id');
        $data['updated_by'] = Auth::user()->username;
        $dto = \App\Lib\Queries\Agent\GetBooking::Result($request->input('id'));

        if (!empty($dto)) {
            if (!empty($dto->booking_date)) {
                $booking_date = str_replace('/', '-', $dto->booking_date);
                $data['loading_date'] = date('Y-m-d', strtotime($booking_date));
            } else {
                $data['loading_date'] = date('Y-m-d');
            }
        }

        dispatch(new \App\Lib\Commands\Agent\CreateLoading($data));

        return $this->JsonOk();
    }

    public function AddDeclaration()
    {
        $vm = new \App\Http\ViewModels\Agent\BookingDeclarationViewModel();

        return view('/agent/booking/add/declaration', ['vm' => $vm]);
    }

    public function AddDeclarationPost(\App\Http\Requests\Agent\BookingDeclarationRequest $request)
    {
        $data = $request->validated();
        $data['rate'] = $this->CurrentCurrencyRate();
        $data['order_no'] = 'OR'.date('Y').str_pad($this->currentOrderNo(), 4, '0', STR_PAD_LEFT);
        $data['order_num'] = $this->currentOrderNo();
        $data['booking_no'] = $data['customer_code'].'-'.$data['booking_no_1'].'-'.$data['booking_no_2'];
        $data['status'] = 4;
        $data['owner_id'] = Auth::user()->id;
        $data['created_by'] = Auth::user()->name;
        $data['updated_by'] = Auth::user()->name;
        $data['agent_id'] = Auth::user()->id;

        if (isset($data['loading_date'])) {
            $data['loading_date'] = str_replace('/', '-', $data['loading_date']);
            $data['loading_date'] = date('Y-m-d', strtotime($data['loading_date']));
        }

        if (isset($data['sst'])) {
            $data['sst'] = 1;
        } else {
            $data['sst'] = 0;
        }

        if (isset($data['eta_date'])) {
            $data['eta_date'] = str_replace('/', '-', $data['eta_date']);
            $data['eta_date'] = date('Y-m-d', strtotime($data['eta_date']));
        }

        dispatch(new \App\Lib\Commands\Agent\CreateDeclaration($data));

        return $this->JsonOk();
    }

    // edit booking
    public function Booking($id)
    {
        $dto = \App\Lib\Queries\Agent\GetBooking::Result($id);
        if (!empty($dto)) {
            // check status return to right page
            if ($dto->status_id == 1) {
                return redirect('agent/booking/edit/booking/'.$id);
            } elseif ($dto->status_id == 2 || $dto->status_id == 3) {
                return redirect('agent/booking/loading/'.$id);
            } elseif ($dto->status_id == 4 || $dto->status_id == 5) {
                return redirect('agent/booking/edit/declaration/'.$id);
            }
        } else {
            return view('404');
        }
    }

    public function EditBooking($id)
    {
        $dto = \App\Lib\Queries\Agent\GetBooking::Result($id);
        if (!empty($dto)) {
            $vm = new \App\Http\ViewModels\Admin\BookingEditViewModel($dto);

            return view('/agent/booking/edit', ['vm' => $vm]);
        } else {
            return view('404');
        }
    }

    public function EditBookingPost(\App\Http\Requests\Agent\BookingEditRequest $request)
    {
        $data = $request->validated();

        if (isset($data['booking_date'])) {
            $data['booking_date'] = str_replace('/', '-', $data['booking_date']);
            $data['booking_date'] = date('Y-m-d', strtotime($data['booking_date']));
        }

        if (isset($data['closing_date'])) {
            $data['closing_date'] = str_replace('/', '-', $data['closing_date']);
            $data['closing_date'] = date('Y-m-d', strtotime($data['closing_date']));
        }

        if (isset($data['sailing_date'])) {
            $data['sailing_date'] = str_replace('/', '-', $data['sailing_date']);
            $data['sailing_date'] = date('Y-m-d', strtotime($data['sailing_date']));
        }

        // prevent empty country
        if ($data['delivery_state_id'] != '' || $data['delivery_state_id'] != 0) {
            $state = \App\Lib\Queries\Admin\GetState::Result($data['delivery_state_id']);
            if (!empty($state)) {
                $data['delivery_state'] = $state->name;
            }
        }

        // prevent empty state
        if ($data['delivery_country_id'] != '' || $data['delivery_country_id'] != 0) {
            $country = \App\Lib\Queries\Admin\GetCountry::Result($data['delivery_country_id']);
            $data['delivery_country'] = $country->name;
        }

        $data['updated_by'] = Auth::user()->username;
        dispatch(new \App\Lib\Commands\Agent\UpdateBooking($data));

        return $this->JsonOk();
    }

    // edit loading
    public function EditLoading($id)
    {
        $dto = \App\Lib\Queries\Agent\GetBooking::Result($id);
        if (!empty($dto)) {
            $vm = new \App\Http\ViewModels\Agent\BookingLoadingViewModel($dto);

            return view('/agent/booking/loading', ['vm' => $vm]);
        } else {
            return view('404');
        }
    }

    public function EditLoadingPost(\App\Http\Requests\Agent\BookingLoadingRequest $request)
    {
        $data = $request->validated();

        if (isset($data['forme_apply'])) {
            $data['forme_apply'] = 1;
        } else {
            $data['forme_apply'] = 0;
        }

        if (isset($data['loading_date'])) {
            $data['loading_date'] = str_replace('/', '-', $data['loading_date']);
            $data['loading_date'] = date('Y-m-d', strtotime($data['loading_date']));
        }

        if (isset($data['etd_date'])) {
            $data['etd_date'] = str_replace('/', '-', $data['etd_date']);
            $data['etd_date'] = date('Y-m-d', strtotime($data['etd_date']));
        }

        if (isset($data['eta_date'])) {
            $data['eta_date'] = str_replace('/', '-', $data['eta_date']);
            $data['eta_date'] = date('Y-m-d', strtotime($data['eta_date']));
        }

        $data['updated_by'] = Auth::user()->username;

        dispatch(new \App\Lib\Commands\Agent\UpdateLoading($data));

        return $this->JsonOk();
    }

    // edit declaration
    public function EditDeclaration($id)
    {
        $dto = \App\Lib\Queries\Admin\GetBooking::Result($id);
        if (!empty($dto)) {
            $vm = new \App\Http\ViewModels\Agent\BookingDeclarationEditViewModel($dto);

            return view('/agent/booking/edit/declaration', ['vm' => $vm]);
        } else {
            return view('404');
        }
    }

    public function EditDeclarationPost(\App\Http\Requests\Agent\BookingDeclarationEditRequest $request)
    {
        $data = $request->validated();

        // if(isset($data['declaration_date'])){
        //   $data['declaration_date'] = str_replace('/', '-', $data['declaration_date']);
        //   $data['declaration_date'] = date('Y-m-d', strtotime($data['declaration_date']));
        // }

        if (isset($data['loading_date'])) {
            $data['loading_date'] = str_replace('/', '-', $data['loading_date']);
            $data['loading_date'] = date('Y-m-d', strtotime($data['loading_date']));
        }

        if (isset($data['eta_date'])) {
            $data['eta_date'] = str_replace('/', '-', $data['eta_date']);
            $data['eta_date'] = date('Y-m-d', strtotime($data['eta_date']));
        }

        if (isset($data['sst'])) {
            $data['sst'] = 1;
        } else {
            $data['sst'] = 0;
        }

        $data['updated_by'] = Auth::user()->username;
        dispatch(new \App\Lib\Commands\Agent\UpdateDeclaration($data));

        return $this->JsonOk();
    }

    public function DeleteBookingPost(Request $request)
    {
        $data['id'] = $request->input('id');
        $data['deleted_by'] = Auth::user()->username;
        dispatch(new \App\Lib\Commands\Agent\DeleteBooking($data));

        return $this->JsonOk();
    }

    public function AddBookingPersonPost(\App\Http\Requests\Agent\BookingPersonRequest $request)
    {
        $data = $request->validated();

        dispatch(new \App\Lib\Commands\Agent\CreateBookingPerson($data));

        return $this->JsonOk();
    }

    public function AddBookingFeePost(\App\Http\Requests\Admin\BookingFeeRequest $request)
    {
        $data = $request->validated();

        dispatch(new \App\Lib\Commands\Admin\CreateBookingFee($data));

        return $this->JsonOk();
    }

    public function DeleteBookingPersonPost(Request $request)
    {
        $data['booking_person_id'] = $request->input('booking_person_id');
        dispatch(new \App\Lib\Commands\Agent\DeleteBookingPerson($data));

        return $this->JsonOk();
    }

    public function DeleteBookingAttachmentPost(Request $request)
    {
        $data['booking_attachment_id'] = $request->input('booking_attachment_id');
        dispatch(new \App\Lib\Commands\Agent\DeleteBookingAttachmentPost($data));

        return $this->JsonOk();
    }

    public function DeleteBookingFeePost(Request $request)
    {
        $data['booking_fee_id'] = $request->input('booking_fee_id');
        dispatch(new \App\Lib\Commands\Admin\DeleteBookingFee($data));

        return $this->JsonOk();
    }

    public function GetPersons($id)
    {
        $dto = \App\Lib\Queries\Agent\GetBooking::GetBookingPersons($id);

        return view('agent/booking/list/persons', ['data' => $dto]);
    }

    public function GetInchargePersons($id)
    {
        $dto = \App\Lib\Queries\Agent\GetBooking::GetBookingPersons($id, 1);

        return view('agent/booking/list/incharges', ['data' => $dto]);
    }

    public function GetInspectPersons($id)
    {
        $dto = \App\Lib\Queries\Agent\GetBooking::GetBookingPersons($id, 2);

        return view('agent/booking/list/inspects', ['data' => $dto]);
    }

    public function GetPhotoSeals($id)
    {
        $data = ['1'];
        $dto = \App\Lib\Queries\Agent\GetBooking::GetBookingAttachments($id, $data);

        return view('agent/booking/list/photoseal', ['data' => $dto]);
    }

    public function GetTypeAttachments($id, $type)
    {
        $data = [$type];
        $dto = \App\Lib\Queries\Agent\GetBooking::GetBookingAttachments($id, $data);

        return view('agent/booking/edit/attachments', ['data' => $dto]);
    }

    public function GetLoadingAttachments($id)
    {
        $data = ['2', '3', '6'];
        $dto = \App\Lib\Queries\Agent\GetBooking::GetBookingAttachments($id, $data);

        return view('agent/booking/edit/attachments', ['data' => $dto]);
    }

    public function GetDeclarationAttachments($id)
    {
        $data = ['4', '5'];
        $dto = \App\Lib\Queries\Agent\GetBooking::GetBookingAttachments($id, $data);

        return view('/agent/booking/edit/declaration-attachments', ['data' => $dto]);
    }

    public function GetFees($id, $type)
    {
        $dto = \App\Lib\Queries\Agent\GetBooking::GetBookingFees($id, $type);

        return view('agent/booking/edit/fees', ['data' => $dto]);
    }

    public function UplaodFilePost(Request $request)
    {
        $data = $request->all();
        if ($request->hasFile('file_file')) {
            $path = $request->file('file_file')->store('upload');
            unset($data['file_file']);
            $data['attachment'] = $path;
        }
        dispatch(new \App\Lib\Commands\Agent\CreateBookingAttachment($data));

        return $this->JsonOk(['filenames' => $path, 'path' => $path]);
    }

    public function CompleteBookingLoadingPost(Request $request)
    {
        $data['id'] = $request->input('id');
        $data['updated_by'] = Auth::user()->username;
        dispatch(new \App\Lib\Commands\Agent\CreateLoadingComplete($data));

        return $this->JsonOk();
    }

    public function UpdateDeclarationPost(Request $request)
    {
        $data['id'] = $request->input('id');
        $data['updated_by'] = Auth::user()->username;
        dispatch(new \App\Lib\Commands\Admin\UpdateDeclartionStatus($data));

        return $this->JsonOk();
    }

    public function UpdateStatusPost(Request $request)
    {
        $data['id'] = $request->input('id');
        $data['status_id'] = $request->input('status_id');
        $data['updated_by'] = Auth::user()->username;
        dispatch(new \App\Lib\Commands\Agent\UpdateBookingStatus($data));

        return $this->JsonOk();
    }

    // Cargo
    public function Cargos(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'updated_at';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
            $inputs['direction'] = $request->input('direction');
        } else {
            $direction = 'desc';
        }

        if (null !== $request->input('keyword')) {
            $keyword = '%'.strtolower($request->input('keyword')).'%';
            $inputs['keyword'] = $request->input('keyword');
        } else {
            $keyword = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'keyword' => $keyword,
        ];

        $dto = \App\Lib\Queries\Agent\GetCargos::Result($params);

        $paging = \App\Lib\Queries\Agent\GetCargos::Paging($params);
        $vm = new \App\Http\ViewModels\Agent\CargosViewModel($dto, $paging);

        return view('agent/cargo/list', ['vm' => $vm]);
    }

    public function AddCargo()
    {
        $vm = new \App\Http\ViewModels\Agent\CargoViewModel();

        return view('agent/cargo/add', ['vm' => $vm]);
    }

    public function AddCargoPost(\App\Http\Requests\Agent\CargoRequest $request)
    {
        $data = $request->validated();
        $data['owner_id'] = Auth::user()->id;
        $data['created_by'] = Auth::user()->name;
        $data['updated_by'] = Auth::user()->name;

        if (isset($data['date'])) {
            $data['date'] = str_replace('/', '-', $data['date']);
            $data['date'] = date('Y-m-d', strtotime($data['date']));
        }

        if (isset($data['arrival_date'])) {
            $data['arrival_date'] = str_replace('/', '-', $data['arrival_date']);
            $data['arrival_date'] = date('Y-m-d', strtotime($data['arrival_date']));
        }

        if (isset($data['loading_date'])) {
            $data['loading_date'] = str_replace('/', '-', $data['loading_date']);
            $data['loading_date'] = date('Y-m-d', strtotime($data['loading_date']));
        }

        $id = DB::table('cargo')->insertGetId([
            'deleted' => 0,
            'date' => $data['date'],
            'arrival_date' => empty($data['arrival_date']) ? null : $data['arrival_date'],
            'loading_date' => empty($data['loading_date']) ? null : $data['loading_date'],
            'status_id' => $data['status_id'],
            'owner_id' => $data['owner_id'],
            'customer_id' => $data['customer_id'],
            'customs_broker_id' => $data['customs_broker_id'],
            'warehouse_id' => $data['warehouse_id'],
            'tracking_no' => empty($data['tracking_no']) ? null : $data['tracking_no'],
            'cbm' => empty($data['cbm']) ? null : $data['cbm'],
            'pack' => empty($data['pack']) ? null : $data['pack'],
            'created_at' => now(),
            'updated_at' => now(),
            'created_by' => $data['created_by'],
            'updated_by' => $data['updated_by'],
        ]);

        return $this->JsonOk(['id' => $id]);
    }

    public function EditCargo($id)
    {
        $dto = \App\Lib\Queries\Agent\GetCargo::Result($id);
        $vm = new \App\Http\ViewModels\Agent\CargoEditViewModel($dto);

        return view('agent/cargo/edit', ['vm' => $vm]);
    }

    public function EditCargoPost(\App\Http\Requests\Agent\CargoInfoRequest $request)
    {
        $data = $request->validated();

        $data['updated_by'] = Auth::user()->name;

        if (isset($data['date'])) {
            $data['date'] = str_replace('/', '-', $data['date']);
            $data['date'] = date('Y-m-d', strtotime($data['date']));
        }

        if (isset($data['arrival_date'])) {
            $data['arrival_date'] = str_replace('/', '-', $data['arrival_date']);
            $data['arrival_date'] = date('Y-m-d', strtotime($data['arrival_date']));
        }

        if (isset($data['loading_date'])) {
            $data['loading_date'] = str_replace('/', '-', $data['loading_date']);
            $data['loading_date'] = date('Y-m-d', strtotime($data['loading_date']));
        }

        dispatch(new \App\Lib\Commands\Agent\UpdateCargo($data));

        return $this->JsonOk();
    }

    public function DeleteCargoPost(Request $request)
    {
        $data['id'] = $request->input('id');
        dispatch(new \App\Lib\Commands\Agent\DeleteCargo($data));

        return $this->JsonOk();
    }

    // REPORT
    public function ReportOverview()
    {
        return view('agent/report/overview');
    }

    public function ReportFullStaffCommission(Request $request)
    {
         // 检查当前用户是否有查看权限
         if (!auth()->user()->can('view full staff commission report')) {
            abort(403, '您没有权限查看完整的员工报表。');
        }
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }
        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = date('Y');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        if (null !== $request->input('employee_id')) {
            $employee_id = $request->input('employee_id');
            $inputs['employee_id'] = $request->input('employee_id');
        } else {
            $employee_id = '';
            $inputs['employee_id'] = '';
            $inputs['employee'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            // 'customer_id' => $customer_id,
            'employee_id' => $employee_id,
        ];

        $header = ['No.', '装柜日期', '客户名称', '货柜号码', '预订号'];
        $footer = [];
        $data = [];

        $employees = \App\Lib\Queries\Admin\GetEmployees::Status(1);

        if ($employee_id != '') {
            $dto = \App\Lib\Queries\Admin\GetReportStaffCommissionEmployee::Result($params);
            $employee = \App\Lib\Queries\Admin\GetUser::Result($employee_id);
        } else {
            $dto = \App\Lib\Queries\Admin\GetReportStaffCommission::Result($params);
        }

        if (!empty($dto)) {
            foreach ($dto as $k => $v) {
                $workers = [];

                if ($employee_id != '') {
                    $jobs = '';
                    foreach ($v->employees as $e) {
                        $jobs .= $e->job.'<br/> ';
                    }
                    $workers[$employee_id] = $jobs;
                } else {
                    foreach ($employees as $ey) {
                        $jobs = '';

                        foreach ($v->employees as $e) {
                            if ($ey->user_id == $e->employee_id) {
                                $jobs .= $e->job.'<br/>';
                            }
                        }

                        $jobs = rtrim($jobs, '<br/>');
                        $workers[$ey->user_id] = $jobs;
                    }
                }
                $data[] = [
                    'no' => $k + 1,
                    'loading_date' => $v->loading_date,
                    'customer_name' => $v->customer_name,
                    'container_no' => $v->container_no,
                    'booking_no' => $v->booking_no,
                    'employees' => $workers,
                ];
            }
        }

        if ($employee_id == '') {
            foreach ($employees as $e) {
                $j = [];
                foreach ($dto as $d) {
                    foreach ($d->employees as $ee) {
                        if ($ee->employee_id == $e->user_id) {
                            $j[] = $ee->job;
                        }
                    }
                }
                if (!empty($j)) {
                    $footer[$e->user_id] = [
                        'user_id' => $e->user_id,
                        'name' => $e->name,
                        'job' => array_count_values($j),
                    ];
                }
            }
        } else {
            $employee = \App\Lib\Queries\Admin\GetUser::Result($employee_id);
            $inputs['employee'] = ' - '.$employee->name;
            $j = [];
            foreach ($dto as $d) {
                foreach ($d->employees as $ee) {
                    if ($ee->employee_id == $employee_id) {
                        $j[] = $ee->job;
                    }
                }
            }

            if (!empty($j)) {
                $footer[$employee_id] = [
                    'user_id' => $employee_id,
                    'name' => $employee->name,
                    'job' => array_count_values($j),
                ];
            }
        }

        // get only got job employee
        foreach ($footer as $ft) {
            if (count($ft['job']) > 0) {
                array_push($header, $ft['name']);
            }
        }
        $vm = new \App\Http\ViewModels\Admin\ReportStaffCommissionViewModel($data, $inputs, $header, $footer);

        return view('agent/report/full_staff_commission', ['vm' => $vm]);

    }

    public function ReportStaffCommission(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = date('Y');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'user_id' => Auth::user()->id,
        ];

        $dto = \App\Lib\Queries\Agent\GetReportStaffCommission::Result($params);

        $footer = [];

        $inputs['employee'] = ' - '.Auth::user()->name;
        $j = [];
        foreach ($dto as $d) {
            foreach ($d->employees as $ee) {
                if ($ee->employee_id == Auth::user()->id) {
                    $j[] = $ee->job;
                }
            }
        }
        $footer[] = [
            'user_id' => Auth::user()->id,
            'name' => Auth::user()->name,
            'job' => array_count_values($j),
        ];

        $vm = new \App\Http\ViewModels\Agent\ReportStaffCommissionViewModel($dto, $inputs, $footer);

        return view('agent/report/staff_commission', ['vm' => $vm]);
    }

    public function ReportBookingOrder(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'booking_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            // $date_to =  date('Y-m-d', strtotime($dtt . ' + 1 days'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            // $date_to = date('Y-m-d', strtotime(date('Y-m-d'). ' + 1 days'));
            $date_to = date('Y-m-d', strtotime(date('Y-m-d').' + 14 days'));
            $inputs['date_to'] = date('d/m/Y', strtotime(date('Y-m-d').' + 14 days'));
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
        ];

        $dto = \App\Lib\Queries\Agent\GetReportBookingOrder::Result($params);

        $vm = new \App\Http\ViewModels\Agent\ReportBookingOrderViewModel($dto, $inputs);

        return view('agent/report/booking_order', ['vm' => $vm]);
    }

    public function ReportContainerLoading(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = $request->input('year');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = [];
            $inputs['customer_id'] = [];
        }

        if (null !== $request->input('agent_id')) {
            $agent_id = $request->input('agent_id');
            $inputs['agent_id'] = $request->input('agent_id');
            $agent = \App\Lib\Queries\Admin\GetAgent::Result($agent_id);
            $inputs['agent'] = ' - '.$agent->name;
        } else {
            $agent_id = '';
            $inputs['agent_id'] = '';
            $inputs['agent'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
            'agent_id' => $agent_id,
        ];

        $dto = \App\Lib\Queries\Agent\GetReportContainerLoading::Result($params);

        $vm = new \App\Http\ViewModels\Agent\ReportContainerLoadingViewModel($dto, $inputs);

        return view('agent/report/container_loading', ['vm' => $vm]);
    }

    public function ReportCargoRecord(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d');
            $inputs['date_to'] = date('d/m/Y');
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
        }

        if (null !== $request->input('customs_broker_id')) {
            $customs_broker_id = $request->input('customs_broker_id');
            $inputs['customs_broker_id'] = $request->input('customs_broker_id');
        } else {
            $customs_broker_id = '';
            $inputs['customs_broker_id'] = '';
        }

        if (null !== $request->input('owner_id')) {
            $owner_id = $request->input('owner_id');
            $inputs['owner_id'] = $request->input('owner_id');
        } else {
            $owner_id = '';
            $inputs['owner_id'] = '';
        }

        if (null !== $request->input('warehouse_id')) {
            $warehouse_id = $request->input('warehouse_id');
            $inputs['warehouse_id'] = $request->input('warehouse_id');
        } else {
            $warehouse_id = '';
            $inputs['warehouse_id'] = '';
        }

        if (null !== $request->input('tracking_no')) {
            $tracking_no = $request->input('tracking_no');
            $inputs['tracking_no'] = $request->input('tracking_no');
        } else {
            $tracking_no = '';
            $inputs['tracking_no'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
            'customs_broker_id' => $customs_broker_id,
            'owner_id' => $owner_id,
            'warehouse_id' => $warehouse_id,
            'tracking_no' => $tracking_no,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportCargoRecord::Result($params);

        $total_cbm = 0;
        $total_supplier_inv_amount = 0;
        $total_supplier_goods_amount = 0;
        $total_customer_goods_amount = 0;
        $total_transport_inv_amount = 0;

        foreach ($dto as $d) {
            // total
            $total_cbm += $d->cbm;
            $total_supplier_inv_amount += $d->supplier_inv_amount;
            $total_supplier_goods_amount += $d->supplier_goods_amount;
            $total_customer_goods_amount += $d->customer_goods_amount;
            $total_transport_inv_amount += $d->transport_inv_amount;
        }

        $net_total_supplier_goods_amount = $total_customer_goods_amount - $total_supplier_goods_amount;

        $net_total_transport_inv_amount = $total_transport_inv_amount - $total_supplier_inv_amount;

        $total = [
            'cbm' => number_format($total_cbm, 3),
            'supplier_inv_amount' => number_format($total_supplier_inv_amount, 2),
            'supplier_goods_amount' => number_format($total_supplier_goods_amount, 2),
            'customer_goods_amount' => number_format($total_customer_goods_amount, 2),
            'transport_inv_amount' => number_format($total_transport_inv_amount, 2),
            'net_total_supplier_goods_amount' => number_format($net_total_supplier_goods_amount, 2),
            'net_total_transport_inv_amount' => number_format($net_total_transport_inv_amount, 2),
        ];

        $title = $inputs['date_from'].' - '.$inputs['date_to'];
        $vm = new \App\Http\ViewModels\Admin\ReportCargoRecordViewModel($dto, $total, $inputs, $title);

        return view('agent/report/cargo_record', ['vm' => $vm]);
    }

    public function ReportExportFullStaffCommission(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
        } else {
            $year = date('Y');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
        } else {
            $month = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = date('Y-m-t', strtotime($date_from));

        if (null !== $request->input('employee_id')) {
            $employee_id = $request->input('employee_id');
            $inputs['employee_id'] = $request->input('employee_id');
        } else {
            $employee_id = '';
            $inputs['employee_id'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'employee_id' => $employee_id,
        ];

        $header = ['No.', '装柜日期', '客户名称', '货柜号码', '预订号'];
        $footer = [];
        $results = [];

        $employees = \App\Lib\Queries\Admin\GetEmployees::Status(1);

        if ($employee_id != '') {
            $dto = \App\Lib\Queries\Admin\GetReportStaffCommissionEmployee::Result($params);
            // $employees = \App\Lib\Queries\Admin\GetEmployees::Status(1);
            $employee = \App\Lib\Queries\Admin\GetUser::Result($employee_id);
        // array_push($header,$employee->name);
        } else {
            $dto = \App\Lib\Queries\Admin\GetReportStaffCommission::Result($params);
            // foreach($employees as $employee){
            //   array_push($header,$employee->name);
            // }
        }

        if (!empty($dto)) {
            foreach ($dto as $k => $v) {
                $workers = [];

                if ($employee_id != '') {
                    $jobs = '';
                    foreach ($v->employees as $e) {
                        $jobs .= $e->job.'<br/> ';
                    }
                    $workers[$employee_id] = $jobs;
                } else {
                    foreach ($employees as $ey) {
                        $jobs = '';

                        foreach ($v->employees as $e) {
                            if ($ey->user_id == $e->employee_id) {
                                $jobs .= $e->job.'<br/>';
                            }
                        }

                        $jobs = rtrim($jobs, '<br/>');
                        $workers[$ey->user_id] = $jobs;
                    }
                }
                $results[] = [
                    'no' => $k + 1,
                    'loading_date' => $v->loading_date,
                    'customer_name' => $v->customer_name,
                    'container_no' => $v->container_no,
                    'booking_no' => $v->booking_no,
                    'employees' => $workers,
                ];
            }
        }

        if ($employee_id == '') {
            foreach ($employees as $e) {
                $j = [];
                foreach ($dto as $d) {
                    foreach ($d->employees as $ee) {
                        if ($ee->employee_id == $e->user_id) {
                            $j[] = $ee->job;
                        }
                    }
                }
                // echo "<pre>";
                // print_r($j);
                // echo "</pre>";
                if (!empty($j)) {
                    $footer[$e->user_id] = [
                        'user_id' => $e->user_id,
                        'name' => $e->name,
                        'job' => array_count_values($j),
                    ];
                }
            }
        } else {
            $employee = \App\Lib\Queries\Admin\GetUser::Result($employee_id);
            $inputs['employee'] = ' - '.$employee->name;
            $j = [];
            foreach ($dto as $d) {
                foreach ($d->employees as $ee) {
                    if ($ee->employee_id == $employee_id) {
                        $j[] = $ee->job;
                    }
                }
            }

            // echo "<pre>";
            // print_r($j);
            // echo "</pre>";
            if (!empty($j)) {
                $footer[$employee_id] = [
                    'user_id' => $employee_id,
                    'name' => $employee->name,
                    'job' => array_count_values($j),
                ];
            }
        }

        // get only got job employee
        foreach ($footer as $ft) {
            if (count($ft['job']) > 0) {
                array_push($header, $ft['name']);
            }
        }

        $data = [
            'results' => $results,
            'header' => $header,
            'footer' => $footer,
            'date' => $year.'年'.$month.'月份',
        ];
        $filename = 'StaffCommission-'.date('ymd').'.xlsx';

        // return view('report.export.staff_commission', ['data' => $data]);
        return Excel::download(new ReportExportStaffCommission('report.export.staff_commission', $data), $filename);
    }

    public function ReportExportStaffCommission(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = date('Y');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'user_id' => Auth::user()->id,
        ];

        $dto = \App\Lib\Queries\Agent\GetReportStaffCommission::Result($params);
        $data = [];
        $data['results'] = [];
        if (count($dto) > 0) {
            foreach ($dto as $k => $v) {
                $data['results'][] = [
                    'no' => $k + 1,
                    'loading_date' => $v->loading_date,
                    'customer_name' => $v->customer_name,
                    'container_no' => $v->container_no,
                    'order_no' => $v->order_no,
                    'booking_no' => $v->booking_no,
                    'jobs' => $v->jobs,
                ];
            }// foreach
        }

        $footer = [];

        $inputs['employee'] = ' - '.Auth::user()->name;
        $j = [];
        foreach ($dto as $d) {
            foreach ($d->employees as $ee) {
                if ($ee->employee_id == Auth::user()->id) {
                    $j[] = $ee->job;
                }
            }
        }
        $footer[] = [
            'user_id' => Auth::user()->id,
            'name' => Auth::user()->name,
            'job' => array_count_values($j),
        ];

        $data['footer'] = $footer;
        $data['name'] = Auth::user()->name;
        $filename = 'Commission-'.date('ymd').'.xlsx';

        // return view('agent.report.export.staff_commission', ['data' => $data]);
        return Excel::download(new ReportExportAgentStaffCommission('agent.report.export.staff_commission', $data), $filename);
    }

    public function ReportExportBookingOrder(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'booking_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d', strtotime(date('Y-m-d').' + 14 days'));
            $inputs['date_to'] = date('d/m/Y', strtotime(date('Y-m-d').' + 14 days'));
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
        ];

        $dto = \App\Lib\Queries\Agent\GetReportBookingOrder::Result($params);

        $data = [];
        $data['results'] = [];

        if (count($dto) > 0) {
            foreach ($dto as $k => $v) {
                $data['results'][] = [
                    'no' => $k + 1,
                    'booking_date' => $v->booking_date,
                    'eta_date' => $v->eta_date,
                    'customer_name' => $v->customer_name,
                    'order_no' => $v->order_no,
                    'booking_no' => $v->booking_no,
                    'goods_value' => $v->goods_value,
                    'agent_name' => $v->agent_name,
                    'shipping_company' => $v->shipping_company,
                    'loading_port' => $v->loading_port,
                    'destination_port' => $v->destination_port,
                    'handling_charges' => $v->handling_charges,
                    'freight_charges' => $v->freight_charges,
                ];
            }// foreach
        }

        $filename = 'BookingOrder-'.date('ymd').'.xlsx';

        // return view('report.export.booking_order', ['data' => $data]);
        return Excel::download(new ReportExportAgentBookingOrder('agent.report.export.booking_order', $data), $filename);
    }

    public function ReportExportContainerLoading(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        // if(null !== ($request->input('date_from'))){
        //   $dtf = str_replace('/', '-', $request->input('date_from'));
        //   $date_from = date('Y-m-d', strtotime($dtf));
        //   $inputs['date_from'] = $request->input('date_from');
        // }else{
        //   $date_from = date('Y-m-1');
        //   $inputs['date_from'] = date('01/m/Y');
        // }
        //
        // if(null !== ($request->input('date_to'))){
        //   $dtt = str_replace('/', '-', $request->input('date_to'));
        //   $date_to =  date('Y-m-d', strtotime($dtt));
        //   $inputs['date_to'] = $request->input('date_to');
        // }else{
        //   $date_to = date('Y-m-d');
        //   $inputs['date_to'] = date('d/m/Y');
        // }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = $request->input('year');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
            $customer_id = explode(',', $customer_id);
        } else {
            $customer_id = [];
            $inputs['customer_id'] = [];
        }
        if (null !== $request->input('agent_id')) {
            $agent_id = $request->input('agent_id');
            $inputs['agent_id'] = $request->input('agent_id');
            $agent = \App\Lib\Queries\Admin\GetAgent::Result($agent_id);
            $inputs['agent'] = ' - '.$agent->name;
        } else {
            $agent_id = '';
            $inputs['agent_id'] = '';
            $inputs['agent'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
            'agent_id' => $agent_id,
        ];

        $dto = \App\Lib\Queries\Agent\GetReportContainerLoading::Result($params);
        $data = [];
        $data['results'] = [];

        if (count($dto) > 0) {
            foreach ($dto as $k => $v) {
                if ($v->forme_apply == 1) {
                    $forme_apply = 'YES';
                } else {
                    $forme_apply = 'NO';
                }
                $data['results'][] = [
                    'no' => $k + 1,
                    'loading_date' => $v->loading_date,
                    'eta_date' => $v->eta_date,
                    'goods_value' => $v->goods_value,
                    'handling_charges' => $v->handling_charges,
                    'customer_name' => $v->customer_name,
                    'container_no' => $v->container_no,
                    'order_no' => $v->order_no,
                    'booking_no' => $v->booking_no,
                    'forme_apply' => $forme_apply,
                    'agent_name' => $v->agent_name,
                    'freight_charges' => $v->freight_charges,
                    'shipping_company' => $v->shipping_company,
                    'remote_fee' => $v->remote_fee,
                ];
            }// foreach
        }

        $filename = 'ContainerLoading-'.date('ymd').'.xlsx';

        // return view('report.export.container_loading', ['data' => $data]);
        return Excel::download(new ReportExportAgentContainerLoading('agent.report.export.container_loading', $data), $filename);
    }

    public function ReportExportCargoRecord(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d');
            $inputs['date_to'] = date('d/m/Y');
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
        }

        if (null !== $request->input('customs_broker_id')) {
            $customs_broker_id = $request->input('customs_broker_id');
            $inputs['customs_broker_id'] = $request->input('customs_broker_id');
        } else {
            $customs_broker_id = '';
            $inputs['customs_broker_id'] = '';
        }

        if (null !== $request->input('owner_id')) {
            $owner_id = $request->input('owner_id');
            $inputs['owner_id'] = $request->input('owner_id');
        } else {
            $owner_id = '';
            $inputs['owner_id'] = '';
        }

        if (null !== $request->input('warehouse_id')) {
            $warehouse_id = $request->input('warehouse_id');
            $inputs['warehouse_id'] = $request->input('warehouse_id');
        } else {
            $warehouse_id = '';
            $inputs['warehouse_id'] = '';
        }

        if (null !== $request->input('tracking_no')) {
            $tracking_no = $request->input('tracking_no');
            $inputs['tracking_no'] = $request->input('tracking_no');
        } else {
            $tracking_no = '';
            $inputs['tracking_no'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
            'customs_broker_id' => $customs_broker_id,
            'owner_id' => $owner_id,
            'warehouse_id' => $warehouse_id,
            'tracking_no' => $tracking_no,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportCargoRecord::Result($params);

        $data = [];
        $data['results'] = [];

        $total_cbm = 0;
        $total_supplier_inv_amount = 0;
        $total_supplier_goods_amount = 0;
        $total_customer_goods_amount = 0;
        $total_transport_inv_amount = 0;

        if (count($dto) > 0) {
            foreach ($dto as $k => $v) {  // total
                $total_cbm += $v->cbm;
                $total_supplier_inv_amount += $v->supplier_inv_amount;
                $total_supplier_goods_amount += $v->supplier_goods_amount;
                $total_customer_goods_amount += $v->customer_goods_amount;
                $total_transport_inv_amount += $v->transport_inv_amount;

                $data['results'][] = [
                    'no' => $k + 1,
                    'date' => $v->date,
                    'loading_date' => $v->loading_date,
                    'arrival_date' => $v->arrival_date,
                    'customer' => $v->customer,
                    'customs_broker' => $v->customs_broker,
                    'warehouse' => $v->warehouse,
                    'owner' => $v->owner,
                    'tracking_no' => $v->tracking_no,
                    'cbm' => $v->cbm,
                    'pack' => $v->pack,
                    'supplier_inv_no' => $v->supplier_inv_no,
                    'supplier_inv_amount' => $v->supplier_inv_amount,
                    'supplier_goods_inv' => $v->supplier_goods_inv,
                    'supplier_goods_amount' => $v->supplier_goods_amount,
                    'customer_goods_inv' => $v->customer_goods_inv,
                    'customer_goods_amount' => $v->customer_goods_amount,
                    'transport_inv' => $v->transport_inv,
                    'transport_inv_amount' => $v->transport_inv_amount,
                ];
            }// foreach
        }

        $net_total_supplier_goods_amount = $total_customer_goods_amount - $total_supplier_goods_amount;

        $net_total_transport_inv_amount = $total_transport_inv_amount - $total_supplier_inv_amount;

        $total = [
            'cbm' => number_format($total_cbm, 3),
            'supplier_inv_amount' => number_format($total_supplier_inv_amount, 2),
            'supplier_goods_amount' => number_format($total_supplier_goods_amount, 2),
            'customer_goods_amount' => number_format($total_customer_goods_amount, 2),
            'transport_inv_amount' => number_format($total_transport_inv_amount, 2),
            'net_total_supplier_goods_amount' => number_format($net_total_supplier_goods_amount, 2),
            'net_total_transport_inv_amount' => number_format($net_total_transport_inv_amount, 2),
        ];

        // title
        $month_from = date('m', strtotime($dtf));
        $month_to = date('m', strtotime($dtt));

        $mf = $this->GetMonthName($month_from);
        $mt = $this->GetMonthName($month_to);

        if ($mf == $mt) {
            $title = $mf.' '.date('Y', strtotime($dtf));
        } else {
            $title = $mf.' - '.$mt.' '.date('Y', strtotime($dtf));
        }
        $data['title'] = '散货记录表 - '.$title;
        $filename = 'CargoRecord-'.date('ymd').'.xlsx';

        // return view('report.export.cargo_record', ['data' => $data, 'total' => $total]);
        return Excel::download(new ReportExportAgentCargoRecord('agent.report.export.cargo_record', $data, $total), $filename);
    }
    
    public function ReportPrintFullStaffCommission(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        // if(null !== ($request->input('date_from'))){
        //   $dtf = str_replace('/', '-', $request->input('date_from'));
        //   $date_from = date('Y-m-d', strtotime($dtf));
        //   $inputs['date_from'] = $request->input('date_from');
        // }else{
        //   $date_from = date('Y-m-1');
        //   $inputs['date_from'] = date('01/m/Y');
        // }
        //
        // if(null !== ($request->input('date_to'))){
        //   $dtt = str_replace('/', '-', $request->input('date_to'));
        //   $date_to =  date('Y-m-d', strtotime($dtt));
        //   $inputs['date_to'] = $request->input('date_to');
        // }else{
        //   $date_to = date('Y-m-d');
        //   $inputs['date_to'] = date('d/m/Y');
        // }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $year;
        } else {
            $year = date('Y');
            $inputs['year'] = $year;
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $month;
        } else {
            $month = date('m');
            $inputs['month'] = $month;
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = date('Y-m-t', strtotime($date_from));

        $inputs['date_from'] = $date_from;
        $inputs['date_to'] = $date_to;

        if (null !== $request->input('employee_id')) {
            $employee_id = $request->input('employee_id');
            $inputs['employee_id'] = $request->input('employee_id');
        } else {
            $employee_id = '';
            $inputs['employee_id'] = '';
            $inputs['employee'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            // 'customer_id' => $customer_id,
            'employee_id' => $employee_id,
        ];

        $header = ['No.', '装柜日期', '客户名称', '货柜号码', '预订号'];
        $footer = [];
        $data = [];

        $employees = \App\Lib\Queries\Admin\GetEmployees::Status(1);

        if ($employee_id != '') {
            $dto = \App\Lib\Queries\Admin\GetReportStaffCommissionEmployee::Result($params);
            // $employees = \App\Lib\Queries\Admin\GetEmployees::Status(1);
            $employee = \App\Lib\Queries\Admin\GetUser::Result($employee_id);
        // array_push($header,$employee->name);
        } else {
            $dto = \App\Lib\Queries\Admin\GetReportStaffCommission::Result($params);
            // foreach($employees as $employee){
            //   array_push($header,$employee->name);
            // }
        }

        // echo "<pre>";
        // print_r($dto);
        // echo "</pre>";
        if (!empty($dto)) {
            foreach ($dto as $k => $v) {
                $workers = [];

                if ($employee_id != '') {
                    $jobs = '';
                    foreach ($v->employees as $e) {
                        $jobs .= $e->job.'<br/> ';
                    }
                    $workers[$employee_id] = $jobs;
                } else {
                    foreach ($employees as $ey) {
                        $jobs = '';

                        foreach ($v->employees as $e) {
                            if ($ey->user_id == $e->employee_id) {
                                $jobs .= $e->job.'<br/>';
                            }
                        }

                        $jobs = rtrim($jobs, '<br/>');
                        $workers[$ey->user_id] = $jobs;
                    }
                }
                // echo "<pre>";
                // print_r($workers);
                // echo "</pre>";

                $data[] = [
                    'no' => $k + 1,
                    'loading_date' => $v->loading_date,
                    'customer_name' => $v->customer_name,
                    'container_no' => $v->container_no,
                    'booking_no' => $v->booking_no,
                    'employees' => $workers,
                ];
            }
        }

        if ($employee_id == '') {
            foreach ($employees as $e) {
                $j = [];
                foreach ($dto as $d) {
                    foreach ($d->employees as $ee) {
                        if ($ee->employee_id == $e->user_id) {
                            $j[] = $ee->job;
                        }
                    }
                }
                // echo "<pre>";
                // print_r($j);
                // echo "</pre>";
                if (!empty($j)) {
                    $footer[$e->user_id] = [
                        'user_id' => $e->user_id,
                        'name' => $e->name,
                        'job' => array_count_values($j),
                    ];
                }
            }
        } else {
            $employee = \App\Lib\Queries\Admin\GetUser::Result($employee_id);
            $inputs['employee'] = ' - '.$employee->name;
            $j = [];
            foreach ($dto as $d) {
                foreach ($d->employees as $ee) {
                    if ($ee->employee_id == $employee_id) {
                        $j[] = $ee->job;
                    }
                }
            }

            // echo "<pre>";
            // print_r($j);
            // echo "</pre>";
            if (!empty($j)) {
                $footer[$employee_id] = [
                    'user_id' => $employee_id,
                    'name' => $employee->name,
                    'job' => array_count_values($j),
                ];
            }
        }

        // get only got job employee
        foreach ($footer as $ft) {
            if (count($ft['job']) > 0) {
                array_push($header, $ft['name']);
            }
        }

        $vm = new \App\Http\ViewModels\Admin\ReportStaffCommissionViewModel($data, $inputs, $header, $footer);

        return view('report/print/staff_commission', ['vm' => $vm]);
    }

    public function ReportPrintStaffCommission(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = date('Y');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'user_id' => Auth::user()->id,
        ];

        $dto = \App\Lib\Queries\Agent\GetReportStaffCommission::Result($params);
        $footer = [];

        $inputs['employee'] = ' - '.Auth::user()->name;
        $j = [];
        foreach ($dto as $d) {
            foreach ($d->employees as $ee) {
                if ($ee->employee_id == Auth::user()->id) {
                    $j[] = $ee->job;
                }
            }
        }
        $footer[] = [
            'user_id' => Auth::user()->id,
            'name' => Auth::user()->name,
            'job' => array_count_values($j),
        ];

        $vm = new \App\Http\ViewModels\Agent\ReportStaffCommissionViewModel($dto, $inputs, $footer);

        return view('agent/report/print/staff_commission', ['vm' => $vm]);
    }

    public function ReportPrintBookingOrder(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'booking_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d', strtotime(date('Y-m-d').' + 14 days'));
            $inputs['date_to'] = date('d/m/Y', strtotime(date('Y-m-d').' + 14 days'));
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
        ];

        $dto = \App\Lib\Queries\Agent\GetReportBookingOrder::Result($params);

        $vm = new \App\Http\ViewModels\Agent\ReportBookingOrderViewModel($dto, $inputs);

        return view('agent/report/print/booking_order', ['vm' => $vm]);
    }

    public function ReportPrintContainerLoading(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'loading_date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        // if(null !== ($request->input('date_from'))){
        //   $dtf = str_replace('/', '-', $request->input('date_from'));
        //   $date_from = date('Y-m-d', strtotime($dtf));
        //   $inputs['date_from'] = $request->input('date_from');
        // }else{
        //   $date_from = date('Y-m-1');
        //   $inputs['date_from'] = date('01/m/Y');
        // }
        //
        // if(null !== ($request->input('date_to'))){
        //   $dtt = str_replace('/', '-', $request->input('date_to'));
        //   $date_to =  date('Y-m-d', strtotime($dtt));
        //   $inputs['date_to'] = $request->input('date_to');
        // }else{
        //   $date_to = date('Y-m-d');
        //   $inputs['date_to'] = date('d/m/Y');
        // }

        if (null !== $request->input('year')) {
            $year = $request->input('year');
            $inputs['year'] = $request->input('year');
        } else {
            $year = date('Y');
            $inputs['year'] = $request->input('year');
        }

        if (null !== $request->input('month')) {
            $month = $request->input('month');
            $inputs['month'] = $request->input('month');
        } else {
            $month = date('m');
            $inputs['month'] = date('m');
        }
        $mth = $this->GetMonthName($month);

        $date_from = $year.'-'.$month.'-1';
        $date_to = $year.'-'.$month.'-31';

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
            $customer_id = explode(',', $customer_id);
        } else {
            $customer_id = [];
            $inputs['customer_id'] = [];
        }

        if (null !== $request->input('agent_id')) {
            $agent_id = $request->input('agent_id');
            $inputs['agent_id'] = $request->input('agent_id');
            $agent = \App\Lib\Queries\Admin\GetAgent::Result($agent_id);
            $inputs['agent'] = ' - '.$agent->name;
        } else {
            $agent_id = '';
            $inputs['agent_id'] = '';
            $inputs['agent'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
            'agent_id' => $agent_id,
        ];

        $dto = \App\Lib\Queries\Agent\GetReportContainerLoading::Result($params);

        $vm = new \App\Http\ViewModels\Agent\ReportContainerLoadingViewModel($dto, $inputs);

        return view('agent/report/print/container_loading', ['vm' => $vm]);
    }

    public function ReportPrintCargoRecord(Request $request)
    {
        if (null !== $request->input('sort')) {
            $sort = $request->input('sort');
        } else {
            $sort = 'date';
        }

        if (null !== $request->input('direction')) {
            $direction = $request->input('direction');
        } else {
            $direction = 'asc';
        }

        if (null !== $request->input('date_from')) {
            $dtf = str_replace('/', '-', $request->input('date_from'));
            $date_from = date('Y-m-d', strtotime($dtf));
            $inputs['date_from'] = $request->input('date_from');
        } else {
            $date_from = date('Y-m-1');
            $inputs['date_from'] = date('01/m/Y');
        }

        if (null !== $request->input('date_to')) {
            $dtt = str_replace('/', '-', $request->input('date_to'));
            $date_to = date('Y-m-d', strtotime($dtt));
            $inputs['date_to'] = $request->input('date_to');
        } else {
            $date_to = date('Y-m-d');
            $inputs['date_to'] = date('d/m/Y');
        }

        if (null !== $request->input('customer_id')) {
            $customer_id = $request->input('customer_id');
            $inputs['customer_id'] = $request->input('customer_id');
        } else {
            $customer_id = '';
            $inputs['customer_id'] = '';
        }

        if (null !== $request->input('customs_broker_id')) {
            $customs_broker_id = $request->input('customs_broker_id');
            $inputs['customs_broker_id'] = $request->input('customs_broker_id');
        } else {
            $customs_broker_id = '';
            $inputs['customs_broker_id'] = '';
        }

        if (null !== $request->input('owner_id')) {
            $owner_id = $request->input('owner_id');
            $inputs['owner_id'] = $request->input('owner_id');
        } else {
            $owner_id = '';
            $inputs['owner_id'] = '';
        }

        if (null !== $request->input('warehouse_id')) {
            $warehouse_id = $request->input('warehouse_id');
            $inputs['warehouse_id'] = $request->input('warehouse_id');
        } else {
            $warehouse_id = '';
            $inputs['warehouse_id'] = '';
        }

        if (null !== $request->input('tracking_no')) {
            $tracking_no = $request->input('tracking_no');
            $inputs['tracking_no'] = $request->input('tracking_no');
        } else {
            $tracking_no = '';
            $inputs['tracking_no'] = '';
        }

        $params[] = [
            'limits' => $this->limits,
            'sort' => $sort,
            'direction' => $direction,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'customer_id' => $customer_id,
            'customs_broker_id' => $customs_broker_id,
            'owner_id' => $owner_id,
            'warehouse_id' => $warehouse_id,
            'tracking_no' => $tracking_no,
        ];

        $dto = \App\Lib\Queries\Admin\GetReportCargoRecord::Result($params);

        $total_cbm = 0;
        $total_supplier_inv_amount = 0;
        $total_supplier_goods_amount = 0;
        $total_customer_goods_amount = 0;
        $total_transport_inv_amount = 0;

        foreach ($dto as $d) {
            // total
            $total_cbm += $d->cbm;
            $total_supplier_inv_amount += $d->supplier_inv_amount;
            $total_supplier_goods_amount += $d->supplier_goods_amount;
            $total_customer_goods_amount += $d->customer_goods_amount;
            $total_transport_inv_amount += $d->transport_inv_amount;
        }

        $net_total_supplier_goods_amount = $total_customer_goods_amount - $total_supplier_goods_amount;

        $net_total_transport_inv_amount = $total_transport_inv_amount - $total_supplier_inv_amount;

        $total = [
            'cbm' => number_format($total_cbm, 3),
            'supplier_inv_amount' => number_format($total_supplier_inv_amount, 2),
            'supplier_goods_amount' => number_format($total_supplier_goods_amount, 2),
            'customer_goods_amount' => number_format($total_customer_goods_amount, 2),
            'transport_inv_amount' => number_format($total_transport_inv_amount, 2),
            'net_total_supplier_goods_amount' => number_format($net_total_supplier_goods_amount, 2),
            'net_total_transport_inv_amount' => number_format($net_total_transport_inv_amount, 2),
        ];

        // title
        $month_from = date('m', strtotime($dtf));
        $month_to = date('m', strtotime($dtt));

        $mf = $this->GetMonthName($month_from);
        $mt = $this->GetMonthName($month_to);

        if ($mf == $mt) {
            $title = $mf.' '.date('Y', strtotime($dtf));
        } else {
            $title = $mf.' - '.$mt.' '.date('Y', strtotime($dtf));
        }

        $vm = new \App\Http\ViewModels\Admin\ReportCargoRecordViewModel($dto, $total, $inputs, $title);

        return view('agent/report/print/cargo_record', ['vm' => $vm]);
    }

    public function CurrentOrderNo()
    {
        $order_no = 0;
        $dto = \App\Lib\Queries\Admin\GetSetting::Key('current_order_id');
        if (!empty($dto)) {
            $order_no = $dto->setting_value + 1;
        }

        return $order_no;
    }

    public function CurrentCurrencyRate()
    {
        $rate = 0;
        $dto = \App\Lib\Queries\Admin\GetSetting::Key('exchange_rate');
        if (!empty($dto)) {
            $rate = $dto->setting_value;
        }

        return $rate;
    }

    public function GetMonthName($month)
    {
        $title = '';
        switch ($month) {
            case 1:
                $title = 'January';
                break;
            case 2:
                $title = 'February';
                break;
            case 3:
                $title = 'March';
                break;
            case 4:
                $title = 'April';
                break;
            case 5:
                $title = 'May';
                break;
            case 6:
                $title = 'June';
                break;
            case 7:
                $title = 'July';
                break;
            case 8:
                $title = 'August';
                break;
            case 9:
                $title = 'September';
                break;
            case 10:
                $title = 'October';
                break;
            case 11:
                $title = 'November';
                break;
            case 12:
                $title = 'December';
                break;
        }

        return $title;
    }
}
