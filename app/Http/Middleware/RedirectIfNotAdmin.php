<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;

class RedirectIfNotAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param string|null              $guard
     *
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if (Auth::check() && Auth::user()->role == 2) { // 如果是 agent
            return redirect('/agent'); // 重定向到 agent 仪表板
        }

        return $next($request);
    }
}
