<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class BookingTypeRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            // 'billing_company_id' => 'required|max:8',
            'booking_type' => 'required|max:50',
        ];
    }

    public function attributes() {
        return [
            // 'billing_company_id' => 'Code',
          'booking_type' => '订单类型',
        ];
    }


}
