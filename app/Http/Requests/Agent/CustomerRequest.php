<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'code' => 'required|max:16',
            'name' => 'required|max:50',
            'nickname' => 'nullable|max:12',
            'status' => 'required',
            'id_number' => 'nullable|max:50',
            'vat_number' => 'nullable|max:50',
            'website' => 'nullable|max:250',
            'phone' => 'nullable|max:32',
            'phone2' => 'nullable|max:32',
            'attn' => 'nullable|max:250',
            'email' => 'nullable|max:96|email',
            'mobile' => 'nullable|max:32',
            'company_id' => 'required|max:11',
            'billing_company_name' => 'nullable|max:255',
            'billing_address_1' => 'nullable|max:255',
            'billing_address_2' => 'nullable|max:255',
            'billing_city' => 'nullable|max:255',
            'billing_postcode' => 'nullable|max:10',
            'billing_country_id' => 'nullable|max:11',
            'billing_state_id' => 'nullable|max:11',
            'shipping_company_name' => 'nullable|max:255',
            'shipping_address_1' => 'nullable|max:255',
            'shipping_address_2' => 'nullable|max:255',
            'shipping_city' => 'nullable|max:255',
            'shipping_postcode' => 'nullable|max:10',
            'shipping_country_id' => 'nullable|max:11',
            'shipping_state_id' => 'nullable|max:11',
            'shipping_name' => 'nullable|max:255',
            'shipping_phone_1' => 'nullable|max:255',
            'shipping_phone_2' => 'nullable|max:255',
        ];
    }

    public function attributes()
    {
        return [
          'code' => '客户编号',
          'name' => '客户名称',
          'nickname' => '客户昵称',
          'status' => '状态',
          'id_number' => '注册号码',
          'vat_number' => '税号',
          'website' => '网站',
          'phone' => '电话号码',
          'attn' => '联系人姓名',
          'email' => '电邮',
          'mobile' => '行动电话号码',
          'billing_company_name' => '公司名字',
          'billing_address_1' => '地址第一行',
          'billing_address_2' => '地址第二行',
          'billing_city' => '城市',
          'billing_postcode' => '邮编',
          'billing_country_id' => '国家',
          'billing_state_id' => '州属',
          'shipping_company_name' => '公司名字',
          'shipping_address_1' => '地址第一行',
          'shipping_address_2' => '地址第二行',
          'shipping_city' => '城市',
          'shipping_postcode' => '邮编',
          'shipping_country_id' => '国家',
          'shipping_state_id' => '州属',
          'shipping_name' => '收货联系人',
          'shipping_phone_1' => '电话号码',
          'shipping_phone_2' => '电话号码',
        ];
    }
}
