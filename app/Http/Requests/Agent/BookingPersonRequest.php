<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class BookingPersonRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'booking_id' => 'required|max:11',
            'type_id' => 'required|max:1',
            'job_id' => 'required|max:11',
            'employee_id' => 'required|max:11',
        ];
    }

    public function attributes() {
        return [
          'booking_id' => '订单ID',
          'type_id' => '类型',
          'job_id' => '工作',
          'employee_id' => '员工',
        ];
    }


}
