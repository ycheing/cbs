<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class BookingAttachmentRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'booking_id' => 'required|max:11',
            'type_id' => 'required|max:1',
            'file_file' => 'required',
        ];
    }

    public function attributes() {
        return [
          'booking_id' => '订单ID',
          'type_id' => '类型',
          'file_file' => '附件'
        ];
    }


}
