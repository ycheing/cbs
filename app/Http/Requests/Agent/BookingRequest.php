<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
          'booking_date' => 'nullable|date_format:d/m/Y',
          'booking_no_1' => 'required|digits:3|numeric',
          'booking_no_2' => 'required|max:5|min:4',
          'customer_id' => 'required|max:11',
          'customer_code' => 'required|max:32',
          'customer_name' => 'required|max:255',
          'delivery_address_1' => 'nullable|max:255',
          'delivery_address_2' => 'nullable|max:255',
          'delivery_city' => 'nullable|max:255',
          'delivery_postcode' => 'nullable|max:10',
          'delivery_country_id' => 'nullable|max:11',
          'delivery_country' => 'nullable|max:255',
          'delivery_state_id' => 'nullable|max:11',
          'delivery_state' => 'nullable|max:255',
          'goods_value' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          'agent_id' => 'nullable|max:11',
          'shipping_company_id' => 'nullable|max:11',
          'loading_port_id' => 'nullable|max:11',
          'destination_port_id' => 'nullable|max:11',
          'closing_date' => 'nullable|date_format:d/m/Y',
          'sailing_date' => 'nullable|date_format:d/m/Y',
          'sailing_day' => 'nullable|numeric',
          'handling_charges' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          'rental_period' => 'nullable|max:255',
        ];
    }

    public function attributes()
    {
        return [
          'booking_date' => '预定装柜日期',
          'booking_no_1' => '订单号',
          'booking_no_2' => '订单号',
          'customer_id' => '客户编号 ',
          'customer_name' => '客户名字',
          'delivery_address_1' => '配送地址 1',
          'delivery_address_2' => '配送地址 2',
          'delivery_city' => '城市',
          'delivery_postcode' => '邮编',
          'delivery_country_id' => '国家',
          'delivery_country' => '国家',
          'delivery_state_id' => '州',
          'delivery_state' => '州',
          'goods_value' => '货值',
          'agent_id' => '代理',
          'shipping_company_id' => '船公司',
          'loading_port_id' => '起运港',
          'destination_port_id' => '目的港',
          'closing_date' => '结关日期',
          'sailing_date' => '开船日期',
          'sailing_day' => '航行时间',
          'handling_charges' => '货柜费用',
          'rental_period' => '免柜租期',
        ];
    }
}
