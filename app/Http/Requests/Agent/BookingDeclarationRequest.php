<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class BookingDeclarationRequest  extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'booking_no_1' => 'required|digits:3|numeric',
            'booking_no_2' => 'required|max:5|min:4',
            'loading_date' => 'nullable|date_format:d/m/Y',
            'customer_id' => 'required|max:11',
            'customer_code' => 'required|max:32',
            'customer_name' => 'required|max:255',
            'container_no' => 'nullable|max:255',
            'seal_no' => 'nullable|max:255',
            'shipping_company_id' => 'nullable|max:11',
            'eta_date' => 'nullable|date_format:d/m/Y',
            'loading_port_id' => 'nullable|max:11',
            'destination_port_id' => 'nullable|max:11',
            'inv_plist' => 'nullable|max:255',
            'sst' => 'nullable|max:1',
            'forme_apply' => 'nullable|max:1',
            'custom_borker_id' => 'nullable|max:11',
            'billing_company_id' => 'nullable|max:11',
        ];
    }

    public function attributes() {
        return [
          'booking_no_1' => '订单号',
          'booking_no_2' => '订单号',
          'loading_date' => '装柜日期',
          'customer_id' => '客户编号 ',
          'customer_name' => '客户姓名',
          'container_no' => '货柜号码',
          'seal_no' => '封条号',
          'shipping_company_id' => '船公司',
          'eta_date' => '到港日期',
          'loading_port_id' => '起运港',
          'destination_port_id' => '目的港',
          'inv_plist' => '清单 & 装箱单',
          'forme_apply' => 'Form E 申请',
          'custom_borker_id' => '清关行',
          'billing_company_id' => '开票公司',
        ];
    }


}
