<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PaymentBeneficiaryRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
          'payment_id' => 'required|max:11',
          'payment_supplier_id' => 'required|max:11',
          'payment_beneficiary_id' => 'required|max:11',
          'beneficiary_id' => 'required|max:11',
          'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/',
          'bank_charges' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          'net_amount' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          'received_date' => 'nullable|date_format:d/m/Y',
          'file_attachment' => 'nullable',
        ];
    }

    public function attributes() {
        return [
          'payment_id' => '货款ID',
          'payment_supplier_id' => '供应商ID',
          'payment_beneficiary_id' => '收款人ID',
          'beneficiary_id' => '收款人',
          'amount' => '金额',
          'bank_charges' => '银行收费',
          'net_amount' => '净额 (RMB)',
          'date_received' => ' 到账日期',
          'file_attachment' => '附件',
        ];
    }


}
