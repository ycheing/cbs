<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CustomsBrokerInfoRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'id' => 'required|max:11',
            'code' => 'required|max:8',
            'name' => 'required|max:50',
            'status' => 'required',
            // 'initial_balance' =>  'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'balance.*.company_id' => 'required',
            'balance.*.balance' => 'nullable|regex:/^-?\\d{1,9}(\\.\\d{1,2})?$/',
        ];
    }

    public function attributes() {
        return [
            'id' => 'ID',
            'code' => '清关行编号',
            'name' => '	清关行名称',
            'status' => '状态',
            // 'initial_balance' => '初始余额',
            'balance.*.company_id' => '初始余额',
            'balance.*.balance' => '初始余额',
        ];
    }


}
