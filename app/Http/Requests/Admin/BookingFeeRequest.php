<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class BookingFeeRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'booking_id' => 'required|max:11',
            'fee_type' => 'required|max:1',
            'fee_description' => 'required|max:255',
            'fee_amount' => 'required|regex:/^-?\d+(\.\d{1,2})?$/',
        ];
    }

    public function attributes() {
        return [
          'booking_id' => 'Booking ID',
          'fee_type' => '费用类型',
          'fee_description' => '说明',
          'fee_amount' => '金额',
        ];
    }


}
