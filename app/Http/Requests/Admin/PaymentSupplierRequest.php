<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PaymentSupplierRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
          'payment_id' => 'required|max:11',
          'payment_supplier_id' => 'required|max:11',
          'supplier_id' => 'nullable|max:11',
          'supplier_amount' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          'supplier_exchange_rate' => 'nullable|regex:/^\d+(\.\d{1,4})?$/',
          'supplier_total' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          // 'supplier_bank_charges' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          // 'supplier_net_amount' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          // 'received_date' => 'nullable|date_format:d/m/Y',
          // 'file_supplier_attachment' => 'nullable',
        ];
    }

    public function attributes() {
        return [
          'payment_id' => '货款ID',
          'payment_supplier_id' => '供应商ID',
          'supplier_id' => '供应商',
          'supplier_amount' => '金额 (RM)',
          'supplier_exchange_rate' => '汇率',
          'supplier_total' => '金额 (RMB)',
          'supplier_bank_charges' => '银行收费',
          'supplier_net_amount' => '净额 (RMB)',
          'date_received' => ' 到账日期',
          'supplier_attachment' => '附件',
        ];
    }


}
