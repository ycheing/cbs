<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'setting[0][setting_value]' => 'required',
        ];
    }

    public function attributes() {
        return [
            'setting[0][setting_value]' => '汇率',
        ];
    }


}
