<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PortInfoRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'id' => 'required|max:11',
            'country_id' => 'required|max:11',
            'name' => 'required|max:50',
            'status' => 'required',
        ];
    }

    public function attributes() {
        return [
            'id' => 'ID',
            'country_id' => '国家',
            'name' => '码头',
            'status' => '状态',
        ];
    }


}
