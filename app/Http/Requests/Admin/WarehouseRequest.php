<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class WarehouseRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'code' => 'required|max:8',
            'name' => 'required|max:50',
            'status' => 'required',
        ];
    }

    public function attributes() {
        return [
          'code' => '仓库编号',
          'name' => '	仓库名称',
          'status' => '状态',
        ];
    }


}
