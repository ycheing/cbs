<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
          'payment_date' => 'nullable|date_format:d/m/Y',
          'remittance_date' => 'nullable|date_format:d/m/Y',
          'customer_id' => 'required|max:11',
          'customer_name' => 'required|max:250',
          'customer_code' => 'required|max:32',
          'status' => 'required',
          'customer_amount' => 'required|regex:/^\d+(\.\d{1,2})?$/',
          'customer_exchange_rate' => 'nullable|regex:/^\d+(\.\d{1,4})?$/',
          'customer_total' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          // 'file_customer_attachment' => 'nullable',
          'purpose_payment' => 'nullable|max:250',
          // 'deposits' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          // 'beneficiary_id' => 'nullable|max:11',
          // 'supplier_id' => 'nullable|max:11',
          // 'supplier_amount' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          // 'supplier_exchange_rate' => 'nullable|regex:/^\d+(\.\d{1,3})?$/',
          // 'supplier_total' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          // 'supplier_bank_charges' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          // 'supplier_net_amount' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          // 'date_received' => 'nullable|date_format:d/m/Y',
          // 'file_supplier_attachment' => 'nullable',
        ];
    }

    public function attributes()
    {
        return [
          'payment_date' => ' 收款日期',
          'remittance_date' => ' 汇款日期',
          'customer_id' => '客户编号',
          'customer_name' => '客户名称',
          'customer_code' => '客户编号',
          'status' => '状态',
          'customer_amount' => '金额  (RM)',
          'customer_exchange_rate' => '汇率',
          'customer_total' => '金额  (RMB)',
          'customer_attachment' => '附件',
          'purpose_payment' => '付款目的',
          'deposits' => '定金',
          'beneficiary_id' => '收款人',
          'supplier_id' => '供应商',
          'supplier_amount' => '金额 (RM)',
          'supplier_exchange_rate' => '汇率',
          'supplier_total' => '金额 (RMB)',
          'supplier_bank_charges' => '银行收费',
          'supplier_net_amount' => '净额 (RMB)',
          'date_received' => ' 接收日期',
          'supplier_attachment' => '附件',
        ];
    }
}
