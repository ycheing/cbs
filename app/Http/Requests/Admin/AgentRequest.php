<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AgentRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'name' => 'required|max:50',
            'email' => 'nullable|max:96|email',
            'status' => 'required',
        ];
    }

    public function attributes() {
        return [
            'name' => '代理姓名',
            'email' => '电邮',
            'status' => '状态',
        ];
    }


}
