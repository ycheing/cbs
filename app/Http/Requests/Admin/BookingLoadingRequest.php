<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class BookingLoadingRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'id' => 'required|max:11',
            'booking_no' => 'required|max:256',
            'loading_date' => 'nullable|date_format:d/m/Y',
            'container_no' => 'nullable|max:255',
            'seal_no' => 'nullable|max:255',
            'loading_place' => 'nullable',
            'loading_photo' => 'nullable',
            'forme_apply' => 'nullable|max:1',
            'etd_date' => 'nullable|date_format:d/m/Y',
            'eta_date' => 'nullable|date_format:d/m/Y',
            'custom_borker_id' =>'nullable|max:11',
            'handling_charges' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'apply_charges' =>  'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'commission' =>  'nullable|regex:/^\d+(\.\d{1,2})?$/',
            // 'other_fee' =>  'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'freight_charges' =>  'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'document_fee' =>  'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'remote_fee' =>  'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'charges' =>  'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'container_loading_charges' =>  'nullable|regex:/^\d+(\.\d{1,2})?$/',
            // 'total_charges' =>  'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'total_charges' =>  'nullable|regex:/^-?\d+(\.\d{1,2})?$/',
            'offsite_fee' =>  'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'entry_fee' =>  'nullable|regex:/^\d+(\.\d{1,2})?$/',
        ];
    }

    public function attributes() {
        return [
          'id' => 'ID',
          'loading_date' => '装柜日期',
          'container_no' => '货柜号码',
          'seal_no' => '封条号',
          'loading_place' => '装柜区域',
          'loading_photo' => '封柜图',
          'forme_apply' => 'Form E 申请',
          'etd_date' => '离港日期',
          'eta_date' => '到港日期',
          'custom_borker_id' => '清关行',
          'handling_charges' => '货柜费用',
          'apply_charges' =>  'Form E 申请费用',
          'commission' =>  '佣金',
          'other_fee' =>  '其他费用',
          'freight_charges' =>  '全包价',
          'document_fee' =>  '文件费',
          'remote_fee' =>  '异地费',
          'charges' =>  'Form E 费用',
          'container_loading_charges' =>  '装柜费',
          'total_charges' =>  '总费用',
          'offsite_fee' =>  '异地费用',
          'entry_fee' =>  '进场费',
        ];
    }


}
