<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'code' => 'required|max:8',
            'name' => 'required|max:50',
            'status' => 'required',
        ];
    }

    public function attributes() {
        return [
            'code' => '供应商编号',
            'name' => '供应商名称',
            'status' => '状态',
        ];
    }


}
