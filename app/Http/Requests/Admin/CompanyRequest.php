<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'code' => 'required|max:8',
            'name' => 'required|max:50',
            'status' => 'required',
        ];
    }

    public function attributes() {
        return [
          'code' => '公司编号',
          'name' => '公司名称',
          'status' => '状态',
        ];
    }


}
