<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class BookingDeclarationEditRequest  extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'id' => 'required|max:11',
            'booking_no' => 'required|max:256',
            // 'declaration_date' => 'required|date_format:d/m/Y',
            'loading_date' => 'nullable|date_format:d/m/Y',
            'customer_id' => 'required|max:11',
            'customer_code' => 'required|max:32',
            'customer_name' => 'required|max:255',
            'container_no' => 'required|max:255',
            'seal_no' => 'nullable|max:255',
            'shipping_company_id' => 'required|max:11',
            'eta_date' => 'required|date_format:d/m/Y',
            'loading_port_id' => 'required|max:11',
            'destination_port_id' => 'required|max:11',
            'inv_plist' => 'nullable|max:255',
            'forme_apply' => 'nullable|max:1',
            'sst' => 'nullable|max:1',
            //form E attachment
            //Inv Attachment
            'custom_borker_id' => 'nullable|max:11',
            'custom_goods_inv_date' => 'nullable|date_format:d/m/Y',
            'custom_goods_inv_no' => 'nullable|max:255',
            'custom_goods_amount' => 'nullable|numeric',
            'custom_handling_inv_date' => 'nullable|date_format:d/m/Y',
            'custom_handling_inv_no' => 'nullable|max:255',
            'custom_handling_prices' => 'nullable|numeric',
            'custom_other_prices' => 'nullable|numeric',
            'billing_company_id' => 'nullable|max:11',
            'billing_goods_inv_date' => 'nullable|date_format:d/m/Y',
            'billing_goods_inv_no' => 'nullable|max:255',
            'billing_goods_amount' => 'nullable|numeric',
            'billing_handling_inv_date' => 'nullable|date_format:d/m/Y',
            'billing_handling_inv_no' => 'nullable|max:255',
            'billing_handling_prices' => 'nullable|numeric',
            'billing_other_prices' => 'nullable|numeric',
        ];
    }

    public function attributes() {
        return [
          'booking_no_1' => '订单号',
          'booking_no_2' => '订单号',
          'loading_date' => '装柜日期',
          'customer_id' => '客户编号 ',
          'customer_name' => '客户姓名',
          'container_no' => '货柜号码',
          'seal_no' => '封条号',
          'shipping_company_id' => '船公司',
          'eta_date' => '到港日期',
          'loading_port_id' => '起运港',
          'destination_port_id' => '目的港',
          'inv_plist' => '清单 & 装箱单',
          'forme_apply' => 'Form E 申请',
          //form E attachment
          //Inv Attachment
          'custom_borker_id' => '清关行',
          'custom_goods_inv_date' => '货单日期',
          'custom_goods_inv_no' => '货单号',
          'custom_goods_amount' => '货单金额',
          'custom_handling_inv_date' => '费用单日期',
          'custom_handling_inv_no' => '费用单号',
          'custom_handling_prices' => '清关价格',
          'custom_other_prices' => '其他',
          'billing_company_id' => '开票公司',
          'billing_goods_inv_date' => '货单日期',
          'billing_goods_inv_no' => '货单号',
          'billing_goods_amount' => '货单金额',
          'billing_handling_inv_date' => '费用单日期',
          'billing_handling_inv_no' => '费用单号',
          'billing_handling_prices' => '清关价格',
          'billing_other_prices' => '其他',
        ];
    }


}
