<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ShippingCompanyInfoRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'id' => 'required|max:11',
            'code' => 'required|max:8',
            'name' => 'required|max:50',
            'email' => 'nullable|max:96',
            'status' => 'required',
        ];
    }

    public function attributes() {
        return [
            'id' => 'ID',
            'code' => '清关行编号',
            'name' => '清关行名称',
            'email' => '电邮',
            'status' => '状态',
        ];
    }


}
