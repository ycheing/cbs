<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserInfoRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'id'  => 'required',
            'name' => 'required|max:50',
            'role' => 'required',
            'email' => 'required|max:50',
            'status' => 'required',
        ];
    }

    public function attributes() {
        return [
            'name' => '姓名',
            'email' => '电邮',
            'role' => '角色',
            'status' => '状态',
        ];
    }


}
