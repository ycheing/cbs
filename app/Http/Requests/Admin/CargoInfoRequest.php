<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CargoInfoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
          'id' => 'required|max:11',
          'date' => 'nullable|date_format:d/m/Y',
          'arrival_date' => 'nullable|date_format:d/m/Y',
          'loading_date' => 'nullable|date_format:d/m/Y',
          'status_id' => 'required',
          'customer_id' => 'required|max:11',
          'customs_broker_id' => 'required|max:11',
          'owner_id' => 'nullable|max:11',
          'warehouse_id' => 'required|max:11',
          'tracking_no' => 'nullable|max:250',
          'supplier_inv_no' => 'nullable|max:250',
          'cbm' => 'nullable|regex:/^\d+(\.\d{1,3})?$/',
          'supplier_inv_amount' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          'pack' => 'nullable|max:99999|integer',
          'supplier_goods_inv' => 'nullable|max:250',
          'supplier_goods_amount' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          'customer_goods_inv' => 'nullable|max:250',
          'customer_goods_amount' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          'transport_inv' => 'nullable|max:250',
          'transport_inv_amount' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
        ];
    }

    public function attributes()
    {
        return [
          'id' => 'ID',
          'date' => ' 日期',
          'arrival_date' => '预计到港日期',
          'loading_date' => '装柜日期',
          'status_id' => '状态',
          'customer_id' => '客户编号',
          'supplier_id' => '供应商',
          'warehouse_id' => '收货仓库',
          'tracking_no' => '追踪编号',
          'supplier_inv_no' => '供应商运费单',
          'cbm' => '立方数',
          'supplier_inv_amount' => '供应商运费单 金额 (RM)',
          'pack' => '箱数',
          'supplier_goods_inv' => '供应商货单',
          'supplier_goods_amount' => '供应商货单 金额 (RM)',
          'customer_goods_inv' => '客户货单',
          'customer_goods_amount' => '客户货单 金额 (RM)',
          'transport_inv' => '运费单',
          'transport_inv_amount' => '运费单 金额 (RM)',
        ];
    }
}
