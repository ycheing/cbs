<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class JobInfoRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'id' => 'required|max:11',
            'name' => 'required|max:50',
            'status' => 'required',
        ];
    }

    public function attributes() {
        return [
            'id' => 'ID',
            'name' => '工作',
            'status' => '状态',
        ];
    }


}
