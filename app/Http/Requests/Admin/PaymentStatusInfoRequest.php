<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PaymentStatusInfoRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'id' => 'required|max:11',
            'name' => 'required|max:50',
            'status' => 'required',
            'sort_order' => 'required|max:4',
        ];
    }

    public function attributes() {
        return [
            'id' => 'ID',
            'name' => '货款状态',
            'status' => '状态',
            'sort_order' => '排列',
        ];
    }


}
