<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeInfoRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'id'  => 'required',
            'name' => 'required|max:50',
            'email' => 'required|max:50',
            'status' => 'required',
            'permissions' => 'array', // 验证为数组
            'permissions.*' => 'string|exists:permissions,name', // 确保数组中的每个权限名称都在数据库的权限表中
   
        ];
    }

    public function attributes() {
        return [
            'name' => '员工姓名',
            'email' => '电邮',
            'status' => '状态',
            'permissions' => '权限'
        ];
    }


}
