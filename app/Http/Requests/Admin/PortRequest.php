<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PortRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'country_id' => 'required|max:11',
            'name' => 'required|max:50',
            'status' => 'required',
        ];
    }

    public function attributes() {
        return [
          'country_id' => '国家',
          'name' => '码头',
          'status' => '状态',
        ];
    }


}
