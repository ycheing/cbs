<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AgentInfoRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'id' => 'required|max:11',
            'name' => 'required|max:50',
            'email' => 'nullable|max:96|email',
            'status' => 'required',
        ];
    }

    public function attributes() {
        return [
            'id' => 'ID',
            'name' => '代理姓名',
            'email' => '电邮',
            'status' => '状态',
        ];
    }


}
