<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PaymentInfoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
          'id' => 'required|max:11',
          'payment_date' => 'nullable|date_format:d/m/Y',
          'remittance_date' => 'nullable|date_format:d/m/Y',
          'customer_id' => 'required|max:11',
          'customer_name' => 'required|max:250',
          'customer_code' => 'required|max:32',
          'status_id' => 'required',
          'customer_amount' => 'required|regex:/^\d+(\.\d{1,2})?$/',
          'customer_exchange_rate' => 'nullable|regex:/^\d+(\.\d{1,4})?$/',
          'customer_total' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
          // 'file_customer_attachment' => 'nullable',
          'purpose_payment' => 'nullable|max:250',
          // 'deposits' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
        ];
    }

    public function attributes()
    {
        return [
          'payment_date' => ' 收款日期',
          'remittance_date' => ' 汇款日期',
          'customer_id' => '客户编号',
          'customer_name' => '客户名称',
          'customer_code' => '客户编号',
          'status_id' => '状态',
          'customer_amount' => '金额  (RM)',
          'customer_exchange_rate' => '汇率',
          'customer_total' => '金额  (RMB)',
          'customer_attachment' => '附件',
          'purpose_payment' => '付款目的',
        ];
    }
}
