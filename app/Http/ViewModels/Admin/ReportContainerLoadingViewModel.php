<?php

namespace App\Http\ViewModels\Admin;

use App\Http\ViewModels\ViewModelBase;

class ReportContainerLoadingViewModel extends ViewModelBase
{
    public $dto;
    public $inputs;
    public $title;

    public function __construct($dto, $inputs, $title)
    {
        $this->dto = $dto;
        $this->inputs = $inputs;
        $this->title = $title;
    }

    public function GetCustomers()
    {
        $records = \App\Lib\Queries\Admin\GetCustomers::Active();

        return $records;
    }

    public function GetAgents()
    {
        $records = \App\Lib\Queries\Admin\GetAgents::Active();

        return $records;
    }

    public function GetUsers()
    {
        $records = \App\Lib\Queries\Admin\GetUsers::Active();

        return $records;
    }

    public function GetYears()
    {
        $records = [];
        $x = date('Y');
        do {
            $records[] = $x;
            --$x;
        } while ($x >= 2020);

        return $records;
    }

    public function GetMonths()
    {
        $records = \Config::get('custom')['months'];

        return $records;
    }
}
