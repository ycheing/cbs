<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;

class SettingViewModel extends ViewModelBase {

  public $dto;

  public function __construct($dto){
    $this->dto = $dto;
  }

  public function GetValue($setting_key){
    $key = array_search($setting_key, array_column((array)$this->dto, 'setting_key'));
    $value  = $this->dto[$key]->setting_value;
    return $value;
  }

}
