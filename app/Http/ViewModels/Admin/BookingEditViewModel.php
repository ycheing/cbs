<?php

namespace App\Http\ViewModels\Admin;

use App\Http\ViewModels\ViewModelBase;

class BookingEditViewModel extends ViewModelBase
{
    public $dto;

    public function __construct($dto)
    {
        $this->dto = $dto;
    }

    public function checkAddLoadingButton()
    {
        $result = !empty($this->dto->booking_date) && !empty($this->dto->booking_no) && !empty($this->dto->customer_id) && !empty($this->dto->customer_code) && !empty($this->dto->customer_name) && !empty($this->dto->agent_id) && !empty($this->dto->shipping_company_id) && !empty($this->dto->loading_port_id) && !empty($this->dto->destination_port_id) && !empty($this->dto->closing_date) && !empty($this->dto->sailing_date) && !empty($this->dto->rental_period);

        return $result;
    }

    public function GetCountries()
    {
        $records = \App\Lib\Queries\Admin\GetCountries::All();

        return $records;
    }

    public function GetBookingStatuses()
    {
        $records = \Config::get('custom')['booking_statuses'];

        return $records;
    }

    public function GetBillingCompanies()
    {
        $records = \App\Lib\Queries\Admin\GetCompanies::All();

        return $records;
    }

    public function GetCustomers()
    {
        $records = \App\Lib\Queries\Admin\GetCustomers::All();

        return $records;
    }

    public function GetEmployees()
    {
        $records = \App\Lib\Queries\Admin\GetEmployees::All();

        return $records;
    }

    public function GetShippingCompanies()
    {
        $records = \App\Lib\Queries\Admin\GetShippingCompanies::All();

        return $records;
    }

    public function GetLoadingPorts()
    {
        $records = \App\Lib\Queries\Admin\GetPorts::ByCountryID(2);

        return $records;
    }

    public function GetDestinationPorts()
    {
        $records = \App\Lib\Queries\Admin\GetPorts::GetDestinationPorts(2);

        return $records;
    }

    public function GetAgents()
    {
        $records = \App\Lib\Queries\Admin\GetAgents::All();

        return $records;
    }
}
