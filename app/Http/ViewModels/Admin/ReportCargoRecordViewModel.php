<?php

namespace App\Http\ViewModels\Admin;

use App\Http\ViewModels\ViewModelBase;

class ReportCargoRecordViewModel extends ViewModelBase
{
    public $dto;
    public $total;
    public $inputs;
    public $title;

    public function __construct($dto, $total, $inputs, $title)
    {
        $this->dto = $dto;
        $this->total = $total;
        $this->inputs = $inputs;
        $this->title = $title;
    }

    public function GetCustomers()
    {
        $records = \App\Lib\Queries\Admin\GetCustomers::Active();

        return $records;
    }

    public function GetCustomsBrokers()
    {
        $records = \App\Lib\Queries\Admin\GetCustomsBrokers::Active();

        return $records;
    }

    public function GetWarehouses()
    {
        $records = \App\Lib\Queries\Admin\GetWarehouses::Active();

        return $records;
    }

    public function GetEmployees()
    {
        $records = \App\Lib\Queries\Admin\GetEmployees::Status(1);

        return $records;
    }
}
