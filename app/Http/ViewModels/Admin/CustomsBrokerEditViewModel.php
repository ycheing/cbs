<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class CustomsBrokerEditViewModel extends ViewModelBase {
  public $dto;

  public function __construct($dto){
  	$this->dto = $dto;
	}
  public function GetStatuses(){
      $records = \Config::get('custom')['statuses'];
      return $records;
  }

  public function GetBillingCompanies(){
      $records = \App\Lib\Queries\Admin\GetCompanies::All();
      return $records;
  }

  public function GetBalance($company_id){

    if(!empty($this->dto->balance)){
      $balances = array($this->dto->balance);
      if(isset($balances[0]->$company_id->balance)){
        $value = $balances[0]->$company_id->balance;
      }else{
        $value = 0;
      }
    }else{
      $value = 0;
    }

    return number_format($value,2, '.','');
  }
}
