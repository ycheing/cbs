<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class StateViewModel extends ViewModelBase {
  public $country_id;


  public function __construct($country_id){
		$this->country_id = $country_id;
	}

  public function GetCountries(){
      $records = \App\Lib\Queries\Admin\GetCountries::All();
      return $records;
  }

  public function GetStatuses(){
      $records = \Config::get('custom')['statuses'];
      return $records;
  }

}
