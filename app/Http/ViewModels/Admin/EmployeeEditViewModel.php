<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class EmployeeEditViewModel extends ViewModelBase {
  public $dto;
  public $user;

  public function __construct($dto, $user){
  	$this->dto = $dto;
  	$this->user = $user;

  }

  public function GetStatuses(){
      $records = \Config::get('custom')['statuses'];
      return $records;
  }

}
