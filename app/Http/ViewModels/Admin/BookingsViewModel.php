<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class BookingsViewModel extends ViewModelBase {
  public $dto;
  public $paging;
  public $inputs;

  public function __construct($dto, $paging, $inputs){
		$this->dto = $dto;
    $this->paging = $paging;
    $this->inputs = $inputs;
	}

  public function GetBookingStatuses(){
      $records = \Config::get('custom')['booking_statuses'];
      return $records;
  }


  public function GetUsers(){
      $records = \App\Lib\Queries\Admin\GetUsers::All();
      return $records;
  }

}
