<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class PaymentViewModel extends ViewModelBase {

  public function GetStatuses(){
    $records = \App\Lib\Queries\Admin\GetPaymentStatuses::All();
    return $records;
  }

  public function GetCustomers(){
    $records = \App\Lib\Queries\Admin\GetCustomers::Active();
    return $records;
  }

  public function GetEmployees(){
    $records = \App\Lib\Queries\Admin\GetEmployees::Status(1);
    return $records;
  }

  public function GetSuppliers(){
      $records = \App\Lib\Queries\Admin\GetSuppliers::Active();
      return $records;
  }

  public function GetBeneficiaries(){
      $records = \App\Lib\Queries\Admin\GetBeneficiaries::ByStatus(1);
      return $records;
  }

}
