<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class StatesViewModel extends ViewModelBase {
  public $dto;
  public $paging;
  public $country_id;

  public function __construct($dto, $paging, $country_id){
		$this->dto = $dto;
    $this->paging = $paging;
    $this->country_id = $country_id;
	}



}
