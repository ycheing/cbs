<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class ReportDailyPaymentViewModel extends ViewModelBase {
  public $dto;
  public $total;
  public $inputs;

  public function __construct($dto, $total, $inputs){
  	$this->dto = $dto;
    $this->total = $total;
    $this->inputs = $inputs;
	}

  public function GetCustomers(){
    $records = \App\Lib\Queries\Admin\GetCustomers::Active();
    return $records;
  }

  public function GetSuppliers(){
      $records = \App\Lib\Queries\Admin\GetCustomsBrokers::Active();
      return $records;
  }

  public function GetYears(){
    $records = array();
    $x = date('Y');
    do {
      $records[] = $x;
      $x--;
    } while ($x >= 2020);
    return $records;
  }

  public function GetMonths(){
      $records = \Config::get('custom')['months'];
      return $records;
  }

}
