<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class BookingViewModel extends ViewModelBase {

  public function GetCountries(){
      $records = \App\Lib\Queries\Admin\GetCountries::All();
      return $records;
  }

  public function GetBookingStatuses(){
      $records = \Config::get('custom')['booking_statuses'];
      return $records;
  }

  public function GetBillingCompanies(){
      $records = \App\Lib\Queries\Admin\GetCompanies::All();
      return $records;
  }
}
