<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class ReportBookingOrderViewModel extends ViewModelBase {
  public $dto;
  public $inputs;

  public function __construct($dto, $inputs){
  	$this->dto = $dto;
    $this->inputs = $inputs;
	}

  public function GetCustomers(){
    $records = \App\Lib\Queries\Admin\GetCustomers::Active();
    return $records;
  }

}
