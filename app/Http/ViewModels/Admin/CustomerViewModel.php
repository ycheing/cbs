<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class CustomerViewModel extends ViewModelBase {

  public function GetCountries(){
      $records = \App\Lib\Queries\Admin\GetCountries::All();
      return $records;
  }

  public function GetStatuses(){
      $records = \Config::get('custom')['statuses'];
      return $records;
  }

  public function GetBillingCompanies(){
      $records = \App\Lib\Queries\Admin\GetCompanies::All();
      return $records;
  }

}
