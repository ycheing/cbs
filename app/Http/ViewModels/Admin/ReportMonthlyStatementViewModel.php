<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class ReportMonthlyStatementViewModel extends ViewModelBase {
  public $dto;
  public $total;
  public $inputs;
  public $title;
  public $rate;

  public function __construct($dto, $total, $inputs, $title, $rate){
  	$this->dto = $dto;
  	$this->total = $total;
    $this->inputs = $inputs;
    $this->title = $title;
    $this->rate = $rate;
	}

  public function GetCustomers(){
    $records = \App\Lib\Queries\Admin\GetCustomers::Active();
    return $records;
  }

  public function GetBillingCompanies(){
      $records = \App\Lib\Queries\Admin\GetCompanies::Active();
      return $records;
  }

  public function GetYears(){
    $records = array();
    $x = date('Y');
    do {
      $records[] = $x;
      $x--;
    } while ($x >= 2020);
    return $records;
  }

  public function GetMonths(){
      $records = \Config::get('custom')['months'];
      return $records;
  }
}
