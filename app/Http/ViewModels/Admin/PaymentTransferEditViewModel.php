<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class PaymentTransferEditViewModel extends ViewModelBase {
  public $dto;

  public function __construct($dto){
  	$this->dto = $dto;
	}

  public function GetStatuses(){
      $records = \Config::get('custom')['statuses'];
      return $records;
  }

  public function GetCustomers(){
    $records = \App\Lib\Queries\Admin\GetCustomers::All();
    return $records;
  }

  public function GetEmployees(){
    $records = \App\Lib\Queries\Admin\GetEmployees::All();
    return $records;
  }

  public function GetSuppliers(){
      $records = \App\Lib\Queries\Admin\GetSuppliers::All();
      return $records;
  }

  public function GetBeneficiaries(){
      $records = \App\Lib\Queries\Admin\GetBeneficiaries::All();
      return $records;
  }

}
