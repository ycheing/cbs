<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class ReportStaffCommissionViewModel extends ViewModelBase {
  public $data;
  // public $data;
  public $inputs;
  // public $employees;
  public $header;
  public $footer;

  public function __construct($data,  $inputs, $header, $footer){
  	$this->data = $data;
    // $this->data = $data;
    $this->inputs = $inputs;
    $this->header = $header;
    $this->footer = $footer;
	}

  public function GetCustomers(){
    $records = \App\Lib\Queries\Admin\GetCustomers::All();
    return $records;
  }

  public function GetEmployees(){
    $records = \App\Lib\Queries\Admin\GetEmployees::Status(1);
    return $records;
  }


  public function GetYears(){
    $records = array();
    $x = date('Y');
    do {
      $records[] = $x;
      $x--;
    } while ($x >= 2020);
    return $records;
  }

  public function GetMonths(){
      $records = \Config::get('custom')['months'];
      return $records;
  }

}
