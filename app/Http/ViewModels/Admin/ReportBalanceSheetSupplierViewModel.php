<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class ReportBalanceSheetSupplierViewModel extends ViewModelBase {
  public $dto;
  public $inputs;
  public $title;
  public $initial_balance;

  public function __construct($dto, $inputs, $title,$initial_balance){
  	$this->dto = $dto;
    $this->inputs = $inputs;
    $this->title = $title;
    $this->initial_balance = $initial_balance;
	}

  public function GetCustomsBrokers(){
    $records = \App\Lib\Queries\Admin\GetCustomsBrokers::Active();
    return $records;
  }

  public function GetBillingCompanies(){
      $records = \App\Lib\Queries\Admin\GetCompanies::Active();
      return $records;
  }

  public function GetYears(){
    $records = array();
    $x = date('Y');
    do {
      $records[] = $x;
      $x--;
    } while ($x >= 2020);
    return $records;
  }

  public function GetMonths(){
      $records = \Config::get('custom')['months'];
      return $records;
  }
}
