<?php

namespace App\Http\ViewModels\Admin;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class ReportBillingCompanyViewModel extends ViewModelBase {
  public $dto;
  public $inputs;
  public $title;
  public $company_inv_amount;
  public $supplier_inv_amount;
  public $net_profit;

  public function __construct($dto, $inputs,$title,$company_inv_amount,$supplier_inv_amount,$net_profit){
  	$this->dto = $dto;
    $this->inputs = $inputs;
    $this->title = $title;
    $this->company_inv_amount = $company_inv_amount;
    $this->supplier_inv_amount = $supplier_inv_amount;
    $this->net_profit = $net_profit;
	}

  public function GetCustomers(){
    $records = \App\Lib\Queries\Admin\GetCustomers::Active();
    return $records;
  }

  public function GetBillingCompanies(){
      $records = \App\Lib\Queries\Admin\GetCompanies::Active();
      return $records;
  }

  public function GetYears(){
    $records = array();
    $x = date('Y');
    do {
      $records[] = $x;
      $x--;
    } while ($x >= 2020);
    return $records;
  }

  public function GetMonths(){
      $records = \Config::get('custom')['months'];
      return $records;
  }
}
