<?php

namespace App\Http\ViewModels\Agent;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class CargoViewModel extends ViewModelBase {

  public function GetStatuses(){
    $records = \Config::get('custom')['cargo_statuses'];
    return $records;
  }

  public function GetCustomers(){
    $records = \App\Lib\Queries\Agent\GetCustomers::Active();
    return $records;
  }

  public function GetCustomsBrokers(){
      $records = \App\Lib\Queries\Admin\GetCustomsBrokers::Active();
      return $records;
  }


  public function GetWarehouses(){
      $records = \App\Lib\Queries\Agent\GetWarehouses::Active();
      return $records;
  }

}
