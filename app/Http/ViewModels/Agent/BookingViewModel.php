<?php

namespace App\Http\ViewModels\Agent;

use App\Http\ViewModels\ViewModelBase;

class BookingViewModel extends ViewModelBase
{
    public function GetCountries()
    {
        $records = \App\Lib\Queries\Admin\GetCountries::Active();

        return $records;
    }

    public function GetBookingStatuses()
    {
        $records = \Config::get('custom')['booking_statuses'];

        return $records;
    }

    public function GetBillingCompanies()
    {
        $records = \App\Lib\Queries\Admin\GetCompanies::Active();

        return $records;
    }

    public function GetCustomers()
    {
        $records = \App\Lib\Queries\Admin\GetCustomers::Active();

        return $records;
    }

    public function GetEmployees()
    {
        $records = \App\Lib\Queries\Admin\GetEmployees::Status(1);

        return $records;
    }

    public function GetShippingCompanies()
    {
        $records = \App\Lib\Queries\Admin\GetShippingCompanies::Active();

        return $records;
    }

    public function GetLoadingPorts()
    {
        $records = \App\Lib\Queries\Admin\GetPorts::ByCountryID(2);
        $filteredRecords = $records->filter(function($record) {
            return $record->status == 1;
        });
        return $filteredRecords;
        // return $records;
    }

    public function GetDestinationPorts()
    {
        $records = \App\Lib\Queries\Admin\GetPorts::GetDestinationPorts(2);
        $filteredRecords = $records->filter(function($record) {
            return $record->status == 1;
        });
        return $filteredRecords;
        // return $records;
    }

    public function GetAgents()
    {
        $records = \App\Lib\Queries\Admin\GetAgents::Active();

        return $records;
    }
}
