<?php

namespace App\Http\ViewModels\Agent;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class CargoEditViewModel extends ViewModelBase {
  public $dto;

  public function __construct($dto){
  	$this->dto = $dto;
	}

  public function GetStatuses(){
    $records = \Config::get('custom')['cargo_statuses'];
    return $records;
  }

  public function GetCustomers(){
    $records = \App\Lib\Queries\Agent\GetCustomers::All();
    return $records;
  }

  public function GetCustomsBrokers(){
      $records = \App\Lib\Queries\Admin\GetCustomsBrokers::All();
      return $records;
  }


  public function GetWarehouses(){
      $records = \App\Lib\Queries\Agent\GetWarehouses::All();
      return $records;
  }

}
