<?php

namespace App\Http\ViewModels\Agent;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class CustomerEditViewModel extends ViewModelBase {
  public $dto;

  public function __construct($dto){
  	$this->dto = $dto;
	}

  public function GetCountries(){
      $records = \App\Lib\Queries\Common\GetCountries::Result();
      return $records;
  }

  public function GetStatuses(){
      $records = \Config::get('custom')['statuses_zh'];
      return $records;
  }

  public function GetBillingCompanies(){
      $records = \App\Lib\Queries\Admin\GetCompanies::All();
      return $records;
  }

}
