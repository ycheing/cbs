<?php

namespace App\Http\ViewModels\Agent;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class ReportStaffCommissionViewModel extends ViewModelBase {
  public $dto;
  public $inputs;
  public $footer;

  public function __construct($dto,  $inputs, $footer){
  	$this->dto = $dto;
    $this->inputs = $inputs;
    // $this->header = $header;
    $this->footer = $footer;
	}

  public function GetCustomers(){
    $records = \App\Lib\Queries\Admin\GetCustomers::Active();
    return $records;
  }

  public function GetEmployees(){
    $records = \App\Lib\Queries\Admin\GetEmployees::Active();
    return $records;
  }


  public function GetYears(){
    $records = array();
    $x = date('Y');
    do {
      $records[] = $x;
      $x--;
    } while ($x >= 2020);
    return $records;
  }

  public function GetMonths(){
      $records = \Config::get('custom')['months'];
      return $records;
  }
}
