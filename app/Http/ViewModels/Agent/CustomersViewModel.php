<?php

namespace App\Http\ViewModels\Agent;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class CustomersViewModel extends ViewModelBase {
  public $dto;
  public $paging;

  public function __construct($dto, $paging){
		$this->dto = $dto;
    $this->paging = $paging;
	}

  public function GetStatuses(){
      $records = \Config::get('custom')['statuses_zh'];
      return $records;
  }

}
