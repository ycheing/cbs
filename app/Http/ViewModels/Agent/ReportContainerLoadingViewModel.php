<?php

namespace App\Http\ViewModels\Agent;
use Illuminate\Support\Facades\Log;
use App\Http\ViewModels\ViewModelBase;
use Illuminate\Support\Facades\Auth;

class ReportContainerLoadingViewModel extends ViewModelBase {
  public $dto;
  public $inputs;

  public function __construct($dto, $inputs){
  	$this->dto = $dto;
    $this->inputs = $inputs;
	}

  public function GetCustomers(){
    $records = \App\Lib\Queries\Admin\GetCustomers::Active();
    return $records;
  }

  public function GetAgents(){
    $records = \App\Lib\Queries\Admin\GetAgents::Active();
    return $records;
  }

  public function GetYears(){
    $records = array();
    $x = date('Y');
    do {
      $records[] = $x;
      $x--;
    } while ($x >= 2020);
    return $records;
  }

  public function GetMonths(){
      $records = \Config::get('custom')['months'];
      return $records;
  }
}
