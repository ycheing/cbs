<?php

namespace App\Export;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Exports\Concerns\WithCustomProperties;
use Maatwebsite\Excel\Writer;
// use PhpOffice\PhpSpreadsheet\Shared\Date;
// use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
// use Maatwebsite\Excel\Concerns\WithColumnFormatting;


class ReportExportYearlyStatement implements FromView,  ShouldAutoSize
{
  public $view;
  public $data;
  public $total;

  public function __construct($view, $data, $total){
      $this->view = $view;
      $this->data = $data;
      $this->total = $total;
  }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View{
       return view('report.export.yearly_statement',['data' => $this->data, 'total' => $this->total]);
    }

    // public function headingRow(): int
    // {
    //     return 2;
    // }
   //  public function columnFormats(): array {
   //     return [
   //         'A' => NumberFormat::FORMAT_DATE_DDMMYYYY,
   //         'E' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
   //     ];
   // }

}
