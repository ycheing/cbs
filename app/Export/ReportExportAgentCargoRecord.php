<?php

namespace App\Export;

// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ReportExportAgentCargoRecord implements FromView, ShouldAutoSize
{
    public $view;
    public $data;
    public $total;

    public function __construct($view, $data, $total)
    {
        $this->view = $view;
        $this->data = $data;
        $this->total = $total;
    }

      /**
       * @return \Illuminate\Support\Collection
       */
      public function view(): View
      {
          return view('agent.report.export.cargo_record', ['data' => $this->data, 'total' => $this->total]);
      }

      public function registerEvents(): array
      {
          return [
              AfterSheet::class => function (AfterSheet $event) {
                  $event->sheet->styleCells(
                      'A1',
                      [
                          'alignment' => [
                              'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                          ],
                      ]
                  );
              },
          ];
      }
}
