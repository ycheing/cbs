<?php

namespace App\Lib\Commands\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateLoading implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle(){
        DB::table('booking')->where('id', $this->data['id'])->update([
          'booking_no' => $this->data['booking_no'],
          'loading_date' =>  empty($this->data['loading_date']) ? null : $this->data['loading_date'],
          'container_no' =>  empty($this->data['container_no']) ? null : $this->data['container_no'],
          'seal_no' => empty($this->data['seal_no']) ? null : $this->data['seal_no'],
          'loading_place' => empty($this->data['loading_place']) ? null : $this->data['loading_place'],
          'forme_apply' => empty($this->data['forme_apply']) ? null : $this->data['forme_apply'],
          'etd_date' =>  empty($this->data['etd_date']) ? null : $this->data['etd_date'],
          'eta_date' => empty($this->data['eta_date']) ? null : $this->data['eta_date'],
          'custom_borker_id' => empty($this->data['custom_borker_id']) ? null : $this->data['custom_borker_id'],
          'freight_charges' => empty($this->data['freight_charges']) ? null : $this->data['freight_charges'],
          'document_fee' => empty($this->data['document_fee']) ? null : $this->data['document_fee'],
          'handling_charges' => empty($this->data['handling_charges']) ? null : $this->data['handling_charges'],
          'apply_charges' => empty($this->data['apply_charges']) ? null : $this->data['apply_charges'],
          'commission' => empty($this->data['commission']) ? null : $this->data['commission'],
          'other_fee' => empty($this->data['other_fee']) ? null : $this->data['other_fee'],
          'remote_fee' => empty($this->data['remote_fee']) ? null : $this->data['remote_fee'],
          'charges' => empty($this->data['charges']) ? null : $this->data['charges'],
          'container_loading_charges' => empty($this->data['container_loading_charges']) ? null : $this->data['container_loading_charges'],
          'total_charges' => empty($this->data['total_charges']) ? null : $this->data['total_charges'],
          //add on 2022-02-07
          'offsite_fee'  => empty($this->data['offsite_fee']) ? null : $this->data['offsite_fee'],
          'entry_fee'  => empty($this->data['entry_fee']) ? null : $this->data['entry_fee'],
          'updated_at' => now(),
          'updated_by' => $this->data['updated_by']
         ]);

         //add booking fee
         //add attachment
    }

}
