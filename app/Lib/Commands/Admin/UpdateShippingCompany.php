<?php

namespace App\Lib\Commands\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateShippingCompany implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle(){
        DB::table('shipping_company')->where('id', $this->data['id'])->update([
            'code' => $this->data['code'],
            'name' => $this->data['name'],
            'email' => empty($this->data['email']) ? null : $this->data['email'],
            'status' => $this->data['status'],
            'updated_at' => now(),
         ]);
    }

}
