<?php

namespace App\Lib\Commands\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;


class UpdatePaymentSupplier implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle() {
      DB::table('payment_supplier')->where('id', $this->data['payment_supplier_id'])->update([
          'supplier_id' => $this->data['supplier_id'],
          'supplier_amount' => $this->data['supplier_amount'],
          'supplier_exchange_rate' => $this->data['supplier_exchange_rate'],
          'supplier_total' => $this->data['supplier_total'],
          // 'supplier_bank_charges' => $this->data['supplier_bank_charges'],
          // 'supplier_net_amount' => $this->data['supplier_net_amount'],
          // 'received_date' =>  empty($this->data['received_date']) ? null : $this->data['received_date'],
          'updated_at' => now(),
          'updated_by' => $this->data['updated_by']
      ]);

      // if(!empty($this->data['supplier_attachment'])){
      //   DB::table('payment_supplier')->where('id', $this->data['payment_supplier_id'])->update([
      //       'supplier_attachment' => $this->data['supplier_attachment']
      //   ]);
      // }
    }
}
