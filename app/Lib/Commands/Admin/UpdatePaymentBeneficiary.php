<?php

namespace App\Lib\Commands\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;


class UpdatePaymentBeneficiary implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle() {
      DB::table('payment_beneficiary')->where('id', $this->data['payment_beneficiary_id'])->update([
          'beneficiary_id' => $this->data['beneficiary_id'],
          'amount' => $this->data['amount'],
          'bank_charges' => $this->data['bank_charges'],
          'net_amount' => $this->data['net_amount'],
          'received_date' =>  empty($this->data['received_date']) ? null : $this->data['received_date'],
          // 'attachment' => empty($this->data['attachment']) ? null : $this->data['attachment'],
          // 'updated_at' => now(),
          // 'updated_by' => $this->data['updated_by']
      ]);

      if(!empty($this->data['attachment'])){
        DB::table('payment_beneficiary')->where('id', $this->data['payment_beneficiary_id'])->update([
            'attachment' => $this->data['attachment']
        ]);
      }
    }
}
