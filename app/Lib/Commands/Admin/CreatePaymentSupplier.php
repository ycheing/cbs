<?php

namespace App\Lib\Commands\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;


class CreatePaymentSupplier implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle() {
      DB::table('payment_supplier')->insert([
          'payment_id' => $this->data['payment_id'],
          'supplier_id' => $this->data['supplier_id'],
          'supplier_amount' => $this->data['supplier_amount'],
          'supplier_exchange_rate' => $this->data['supplier_exchange_rate'],
          'supplier_total' => $this->data['supplier_total'],        
          'created_at' => now(),
          'updated_at' => now(),
          'created_by' => $this->data['created_by'],
          'updated_by' => $this->data['updated_by']
      ]);

    }
}
