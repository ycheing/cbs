<?php

namespace App\Lib\Commands\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DeletePayment2 implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle() {
      DB::table('payment_beneficiary')->where('payment_id', '=', $this->data['id'])->delete();

      DB::table('payment_supplier')->where('payment_id', '=', $this->data['id'])->delete();

      DB::table('payment')->where('id', '=', $this->data['id'])->delete();
    }
}
