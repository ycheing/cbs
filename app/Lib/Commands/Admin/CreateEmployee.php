<?php

namespace App\Lib\Commands\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;


class CreateEmployee implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle() {
      $user_id = DB::table('users')->insertGetId([
          'username' => $this->data['username'],
          'name' => $this->data['name'],
          'email' => $this->data['email'],
          'status' => $this->data['status'],
          'role'  => '2',
          'password' => Hash::make($this->data['password']),
          'created_at' => now(),
          'updated_at' => now(),
      ]);

      DB::table('employee')->insert([
          'user_id' => $user_id,
          'agent' => $this->data['agent'],
      ]);
    }
}
