<?php

namespace App\Lib\Commands\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class UpdateCargo implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle()
    {
        DB::table('cargo')->where('id', $this->data['id'])->update([
          'date' => $this->data['date'],
          'arrival_date' => empty($this->data['arrival_date']) ? null : $this->data['arrival_date'],
          'loading_date' => empty($this->data['loading_date']) ? null : $this->data['loading_date'],
          'status_id' => $this->data['status_id'],
          'customer_id' => $this->data['customer_id'],
          'customs_broker_id' => empty($this->data['customs_broker_id']) ? null : $this->data['customs_broker_id'],
          'warehouse_id' => $this->data['warehouse_id'],
          'owner_id' => $this->data['owner_id'],
          'tracking_no' => empty($this->data['tracking_no']) ? null : $this->data['tracking_no'],
          'supplier_inv_no' => empty($this->data['supplier_inv_no']) ? null : $this->data['supplier_inv_no'],
          'cbm' => empty($this->data['cbm']) ? null : $this->data['cbm'],
          'supplier_inv_amount' => empty($this->data['supplier_inv_amount']) ? null : $this->data['supplier_inv_amount'],
          'pack' => empty($this->data['pack']) ? null : $this->data['pack'],
          'supplier_goods_inv' => empty($this->data['supplier_goods_inv']) ? null : $this->data['supplier_goods_inv'],
          'supplier_goods_amount' => empty($this->data['supplier_goods_amount']) ? null : $this->data['supplier_goods_amount'],
          'customer_goods_inv' => empty($this->data['customer_goods_inv']) ? null : $this->data['customer_goods_inv'],
          'customer_goods_amount' => empty($this->data['customer_goods_amount']) ? null : $this->data['customer_goods_amount'],
          'transport_inv' => empty($this->data['transport_inv']) ? null : $this->data['transport_inv'],
          'transport_inv_amount' => empty($this->data['transport_inv_amount']) ? null : $this->data['transport_inv_amount'],
          'updated_by' => $this->data['updated_by'],
          'updated_at' => now(),
         ]);
    }
}
