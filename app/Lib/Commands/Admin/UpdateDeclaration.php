<?php

namespace App\Lib\Commands\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateDeclaration implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle(){
        DB::table('booking')->where('id', $this->data['id'])->update([
          'booking_no' => $this->data['booking_no'],
          // 'declaration_date' => $this->data['declaration_date'],
          'loading_date' => $this->data['loading_date'],
          'customer_id' => $this->data['customer_id'],
          'customer_code' => $this->data['customer_code'],
          'customer_name' => $this->data['customer_name'],
          'container_no' =>  empty($this->data['container_no']) ? null : $this->data['container_no'],
          'seal_no' => empty($this->data['seal_no']) ? null : $this->data['seal_no'],
          'shipping_company_id' => empty($this->data['shipping_company_id']) ? null : $this->data['shipping_company_id'],
          'eta_date' => empty($this->data['eta_date']) ? null : $this->data['eta_date'],
          'loading_port_id' => empty($this->data['loading_port_id']) ? null : $this->data['loading_port_id'],
          'destination_port_id' => empty($this->data['destination_port_id']) ? null : $this->data['destination_port_id'],
          'inv_plist' => empty($this->data['inv_plist']) ? null : $this->data['inv_plist'],
          'forme_apply' => empty($this->data['forme_apply']) ? null : $this->data['forme_apply'],
          'sst' => empty($this->data['sst']) ? null : $this->data['sst'],
          'custom_borker_id' =>  empty($this->data['custom_borker_id']) ? null : $this->data['custom_borker_id'],
          'custom_goods_inv_date' => empty($this->data['custom_goods_inv_date']) ? null : $this->data['custom_goods_inv_date'],
          'custom_goods_inv_no' => empty($this->data['custom_goods_inv_no']) ? null : $this->data['custom_goods_inv_no'],
          'custom_goods_amount' => empty($this->data['custom_goods_amount']) ? null : $this->data['custom_goods_amount'],
          'custom_handling_inv_date' => empty($this->data['custom_handling_inv_date']) ? null : $this->data['custom_handling_inv_date'],
          'custom_handling_inv_no' => empty($this->data['custom_handling_inv_no']) ? null : $this->data['custom_handling_inv_no'],
          'custom_handling_prices' => empty($this->data['custom_handling_prices']) ? null : $this->data['custom_handling_prices'],
          'custom_other_prices' => empty($this->data['custom_other_prices']) ? null : $this->data['custom_other_prices'],
          'billing_company_id' => empty($this->data['billing_company_id']) ? null : $this->data['billing_company_id'],
          'billing_goods_inv_date' => empty($this->data['billing_goods_inv_date']) ? null : $this->data['billing_goods_inv_date'],
          'billing_goods_inv_no' => empty($this->data['billing_goods_inv_no']) ? null : $this->data['billing_goods_inv_no'],
          'billing_goods_amount' => empty($this->data['billing_goods_amount']) ? null : $this->data['billing_goods_amount'],
          'billing_handling_inv_date' => empty($this->data['billing_handling_inv_date']) ? null : $this->data['billing_handling_inv_date'],
          'billing_handling_inv_no' => empty($this->data['billing_handling_inv_no']) ? null : $this->data['billing_handling_inv_no'],
          'billing_handling_prices' => empty($this->data['billing_handling_prices']) ? null : $this->data['billing_handling_prices'],
          'billing_other_prices' => empty($this->data['billing_other_prices']) ? null : $this->data['billing_other_prices'],
          'updated_at' => now(),
          'updated_by' => $this->data['updated_by']
         ]);

         //add booking fee
         //add attachment
    }

}
