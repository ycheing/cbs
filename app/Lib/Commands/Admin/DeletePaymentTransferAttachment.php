<?php

namespace App\Lib\Commands\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;


class DeletePaymentTransferAttachment implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle() {

      if($this->data['type'] == 1){
        DB::table('payment_transfer')->where('id', $this->data['id'])->update([
            'customer_attachment' => ''
        ]);
      }elseif($this->data['type'] == 2){
        DB::table('payment_transfer')->where('id', $this->data['id'])->update([
            'supplier_attachment' => ''
        ]);
      }

    }
}
