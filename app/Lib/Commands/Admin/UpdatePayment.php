<?php

namespace App\Lib\Commands\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;


class UpdatePayment implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle() {
      DB::table('payment')->where('id', $this->data['id'])->update([
          'status_id' =>  $this->data['status_id'],
          'payment_date' => $this->data['payment_date'],
          'remittance_date' => $this->data['remittance_date'],
          'customer_id' =>  $this->data['customer_id'],
          'customer_name' =>  $this->data['customer_name'],
          'customer_code' =>  $this->data['customer_code'],
          'customer_amount' =>  $this->data['customer_amount'],
          'customer_exchange_rate' =>  $this->data['customer_exchange_rate'],
          'customer_total' =>  $this->data['customer_total'],
          'purpose_payment' => empty($this->data['purpose_payment']) ? null : $this->data['purpose_payment'],
          // 'deposits' => empty($this->data['deposits']) ? null : $this->data['deposits'],
          'updated_at' => now(),
          'updated_by' => $this->data['updated_by']
      ]);

    }
}
