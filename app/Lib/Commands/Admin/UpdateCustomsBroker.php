<?php

namespace App\Lib\Commands\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateCustomsBroker implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle(){
        $initial_balance = json_encode($this->data['balance']);
        DB::table('customsbroker')->where('id', $this->data['id'])->update([
            'code' => $this->data['code'],
            'name' => $this->data['name'],
            'status' => $this->data['status'],
            // 'initial_balance' => $this->data['initial_balance'],
            'initial_balance' => $initial_balance,
            'updated_at' => now(),
         ]);
    }

}
