<?php

namespace App\Lib\Commands\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;


class CreatePaymentTransfer implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle() {
      DB::table('payment_transfer')->insert([
          'deleted' => 0,
          'payment_date' => $this->data['payment_date'],
          'status' =>  $this->data['status'],
          'owner_id' =>  $this->data['owner_id'],
          'customer_id' =>  $this->data['customer_id'],
          'customer_name' =>  $this->data['customer_name'],
          'customer_code' =>  $this->data['customer_code'],
          'customer_amount' =>  $this->data['customer_amount'],
          'customer_exchange_rate' =>  $this->data['customer_exchange_rate'],
          'customer_total' =>  $this->data['customer_total'],
          'customer_attachment' => empty($this->data['customer_attachment']) ? null : $this->data['customer_attachment'],
          'purpose_payment' => empty($this->data['purpose_payment']) ? null : $this->data['purpose_payment'],
          'deposits' => empty($this->data['deposits']) ? null : $this->data['deposits'],
          'beneficiary_id' => $this->data['beneficiary_id'],
          //'beneficiary_name' => $this->data['beneficiary_name'],
          'supplier_id' => $this->data['supplier_id'],
          //'supplier_name' => $this->data['supplier_name'],
          'supplier_amount' => $this->data['supplier_amount'],
          'supplier_exchange_rate' => $this->data['supplier_exchange_rate'],
          'supplier_total' => $this->data['supplier_total'],
          'supplier_bank_charges' => $this->data['supplier_bank_charges'],
          'supplier_net_amount' => $this->data['supplier_net_amount'],
          'date_received' =>  empty($this->data['date_received']) ? null : $this->data['date_received'],
          'supplier_attachment' => empty($this->data['supplier_attachment']) ? null : $this->data['supplier_attachment'],
          'created_at' => now(),
          'updated_at' => now(),
          'created_by' => $this->data['created_by'],
          'updated_by' => $this->data['updated_by']
      ]);

    }
}
