<?php

namespace App\Lib\Commands\Agent;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;


class UpdateBookingStatus implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle() {
      DB::table('booking')->where('id', $this->data['id'])->update([
        'status' => $this->data['status_id'],
        'completed_date' => now(),
        'updated_at' => now(),
        'updated_by' => $this->data['updated_by']
       ]);
    }
}
