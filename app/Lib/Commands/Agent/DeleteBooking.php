<?php

namespace App\Lib\Commands\Agent;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DeleteBooking implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle() {
      DB::table('booking')->where('id', $this->data['id'])->update([
          'deleted' => 1,
          'deleted_by' => $this->data['deleted_by'],
          'deleted_at' => now(),
       ]);
    }
}
