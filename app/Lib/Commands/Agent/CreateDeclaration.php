<?php

namespace App\Lib\Commands\Agent;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;


class CreateDeclaration implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle() {
      DB::table('booking')->insert([
          'booking' => 0,
          'loading' => 0,
          'declaration' => 1,
          'deleted' => 0,
          'loading_date' => $this->data['loading_date'],
          'currency_rate' => $this->data['rate'],
          'order_no' => $this->data['order_no'],
          'order_num' => $this->data['order_num'],
          'booking_no' => $this->data['booking_no'],
          'status' => $this->data['status'],
          'agent_id' => $this->data['agent_id'],
          'customer_id' => $this->data['customer_id'],
          'customer_code' => $this->data['customer_code'],
          'customer_name' => $this->data['customer_name'],
          'container_no' =>  empty($this->data['container_no']) ? null : $this->data['container_no'],
          'seal_no' => empty($this->data['seal_no']) ? null : $this->data['seal_no'],
          'shipping_company_id' => empty($this->data['shipping_company_id']) ? null : $this->data['shipping_company_id'],
          'eta_date' => empty($this->data['eta_date']) ? null : $this->data['eta_date'],
          'loading_port_id' => empty($this->data['loading_port_id']) ? null : $this->data['loading_port_id'],
          'destination_port_id' => empty($this->data['destination_port_id']) ? null : $this->data['destination_port_id'],
          'inv_plist' => empty($this->data['inv_plist']) ? null : $this->data['inv_plist'],
          'forme_apply' => empty($this->data['forme_apply']) ? null : $this->data['forme_apply'],
          'sst' => empty($this->data['sst']) ? null : $this->data['sst'],
          'custom_borker_id' => empty($this->data['custom_borker_id']) ? null : $this->data['custom_borker_id'],
          'billing_company_id' => empty($this->data['billing_company_id']) ? null : $this->data['billing_company_id'],
          'owner_id' => $this->data['owner_id'],
          'created_by' => $this->data['created_by'],
          'updated_by' => $this->data['updated_by'],
          'created_at' => now(),
          'updated_at' => now(),
          'declaration_date' => now(),
      ]);

      //update current_order_id
      DB::table('setting')->where('setting_key', 'current_order_id')->update([
        'setting_value' => $this->data['order_num'],
        'updated_at' => now(),
     ]);

      //add attachment
    }
}
