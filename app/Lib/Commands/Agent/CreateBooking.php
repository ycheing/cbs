<?php

namespace App\Lib\Commands\Agent;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;


class CreateBooking implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function handle() {
      DB::table('booking')->insert([
          'booking' => 1,
          'loading' => 0,
          'declaration' => 0,
          'deleted' => 0,
          'booking_date' =>  empty($this->data['booking_date']) ? null : $this->data['booking_date'],
          'currency_rate' => $this->data['rate'],
          'order_no' => $this->data['order_no'],
          'order_num' => $this->data['order_num'],
          'booking_no' => $this->data['booking_no'],
          'status' => $this->data['status'],
          'agent_id' => $this->data['agent_id'],
          'customer_id' => $this->data['customer_id'],
          'customer_code' => $this->data['customer_code'],
          'customer_name' => $this->data['customer_name'],
          'delivery_address_1' => empty($this->data['delivery_address_1']) ? null : $this->data['delivery_address_1'],
          'delivery_address_2' =>  empty($this->data['delivery_address_2']) ? null : $this->data['delivery_address_2'],
          'delivery_city' => empty($this->data['delivery_city']) ? null : $this->data['delivery_city'],
          'delivery_postcode' => empty($this->data['delivery_postcode']) ? null : $this->data['delivery_postcode'],
          'delivery_country_id' => empty($this->data['delivery_country_id']) ? null : $this->data['delivery_country_id'],
          'delivery_country' => empty($this->data['delivery_country']) ? null : $this->data['delivery_country'],
          'delivery_state_id' => empty($this->data['delivery_state_id']) ? null : $this->data['delivery_state_id'],
          'delivery_state' => empty($this->data['delivery_state']) ? null : $this->data['delivery_state'],
          'goods_value' => empty($this->data['goods_value']) ? null : $this->data['goods_value'],
          'shipping_company_id' => empty($this->data['shipping_company_id']) ? null : $this->data['shipping_company_id'],
          'loading_port_id' => empty($this->data['loading_port_id']) ? null : $this->data['loading_port_id'],
          'destination_port_id' => empty($this->data['destination_port_id']) ? null : $this->data['destination_port_id'],
          'closing_date' => empty($this->data['closing_date']) ? null : $this->data['closing_date'],
          'sailing_date' => empty($this->data['sailing_date']) ? null : $this->data['sailing_date'],
          'sailing_day' => empty($this->data['sailing_day']) ? null : $this->data['sailing_day'],
          'handling_charges' => empty($this->data['handling_charges']) ? null : $this->data['handling_charges'],
          'rental_period' => empty($this->data['rental_period']) ? null : $this->data['rental_period'],
          'created_at' => now(),
          'updated_at' => now(),
          'owner_id' => $this->data['owner_id'],
          'created_by' => $this->data['created_by'],
          'updated_by' => $this->data['updated_by']
      ]);

      //update current_order_id
      DB::table('setting')->where('setting_key', 'current_order_id')->update([
        'setting_value' => $this->data['order_num'],
        'updated_at' => now(),
     ]);
    }
}
