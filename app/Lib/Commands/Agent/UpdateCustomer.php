<?php

namespace App\Lib\Commands\Agent;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class UpdateCustomer implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle()
    {
        DB::table('customer')->where('id', $this->data['id'])->update([
            'code' => $this->data['code'],
            'name' => $this->data['name'],
            'nickname' => $this->data['nickname'],
            'status' => $this->data['status'],
            'id_number' => $this->data['id_number'],
            'vat_number' => $this->data['vat_number'],
            'website' => $this->data['website'],
            'phone' => $this->data['phone'],
            'phone2' => $this->data['phone2'],
            'attn' => $this->data['attn'],
            'email' => $this->data['email'],
            'mobile' => $this->data['mobile'],
            'company_id' => $this->data['company_id'],
            'shipping_name' => $this->data['shipping_name'],
            'shipping_phone_1' => $this->data['shipping_phone_1'],
            'shipping_phone_2' => $this->data['shipping_phone_2'],
            'updated_at' => now(),
            'updated_by' => $this->data['updated_by'],
         ]);

        // Billing Address
        DB::table('address')->where('customer_id', $this->data['id'])->where('type', '1')->update([
         'company_name' => $this->data['billing_company_name'],
         'address_1' => $this->data['billing_address_1'],
         'address_2' => $this->data['billing_address_2'],
         'city' => $this->data['billing_city'],
         'postcode' => $this->data['billing_postcode'],
         'state_id' => $this->data['billing_state_id'],
         'state' => $this->data['billing_state'],
         'country_id' => $this->data['billing_country_id'],
         'country' => $this->data['billing_country'],
         'updated_at' => now(),
        ]);

        // Shipping Address
        DB::table('address')->where('customer_id', $this->data['id'])->where('type', '2')->update([
        'company_name' => $this->data['shipping_company_name'],
        'address_1' => $this->data['shipping_address_1'],
        'address_2' => $this->data['shipping_address_2'],
        'city' => $this->data['shipping_city'],
        'postcode' => $this->data['shipping_postcode'],
        'state_id' => $this->data['shipping_state_id'],
        'state' => $this->data['shipping_state'],
        'country_id' => $this->data['shipping_country_id'],
        'country' => $this->data['shipping_country'],
        'updated_at' => now(),
       ]);
    }
}
