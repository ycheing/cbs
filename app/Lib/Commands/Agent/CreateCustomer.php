<?php

namespace App\Lib\Commands\Agent;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class CreateCustomer implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle()
    {
        $customer_id = DB::table('customer')->insertGetId([
          'code' => $this->data['code'],
          'name' => $this->data['name'],
          'nickname' => $this->data['nickname'],
          'status' => $this->data['status'],
          'id_number' => $this->data['id_number'],
          'vat_number' => $this->data['vat_number'],
          'website' => $this->data['website'],
          'phone' => $this->data['phone'],
          'phone2' => $this->data['phone2'],
          'attn' => $this->data['attn'],
          'email' => $this->data['email'],
          'mobile' => $this->data['mobile'],
          'company_id' => $this->data['company_id'],
          'shipping_name' => $this->data['shipping_name'],
          'shipping_phone_1' => $this->data['shipping_phone_1'],
          'shipping_phone_2' => $this->data['shipping_phone_2'],
          'created_at' => now(),
          'updated_at' => now(),
          'created_by' => $this->data['created_by'],
          'updated_by' => $this->data['updated_by'],
      ]);

        // Billing Address
        DB::table('address')->insert([
         'customer_id' => $customer_id,
         'type' => '1',
         'address_1' => $this->data['billing_address_1'],
         'address_2' => $this->data['billing_address_2'],
         'city' => $this->data['billing_city'],
         'postcode' => $this->data['billing_postcode'],
         'state_id' => $this->data['billing_state_id'],
         'state' => $this->data['billing_state'],
         'country_id' => $this->data['billing_country_id'],
         'country' => $this->data['billing_country'],
         'created_at' => now(),
         'updated_at' => now(),
     ]);

        // Shipping Address
        DB::table('address')->insert([
         'customer_id' => $customer_id,
         'type' => '2',
         'address_1' => $this->data['shipping_address_1'],
         'address_2' => $this->data['shipping_address_2'],
         'city' => $this->data['shipping_city'],
         'postcode' => $this->data['shipping_postcode'],
         'state_id' => $this->data['shipping_state_id'],
         'state' => $this->data['shipping_state'],
         'country_id' => $this->data['shipping_country_id'],
         'country' => $this->data['shipping_country'],
         'created_at' => now(),
         'updated_at' => now(),
    ]);
    }
}
