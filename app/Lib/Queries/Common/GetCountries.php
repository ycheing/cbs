<?php

namespace App\Lib\Queries\Common;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetCountries extends QueryBase {
    public static function Result(){
       $record =  DB::table('country')->where('status', '=', 1)->orderBy('name')->get();
       return \App\Lib\Dto\Common\CountryDto::Collection($record);
    }

}
