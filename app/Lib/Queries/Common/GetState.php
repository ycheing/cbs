<?php

namespace App\Lib\Queries\Common;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetState extends QueryBase {
    public static function Result($id){
       $record = DB::table('state')
              ->where('id', $id)
              ->first();
              if(!empty($record))
       return new \App\Lib\Dto\Common\StateDto($record);
    }
}
