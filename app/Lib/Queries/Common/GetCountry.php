<?php

namespace App\Lib\Queries\Common;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetCountry extends QueryBase {
    public static function Result($country_id){
       $record = DB::table('country')->where('id', $country_id)->first();
       $record->states = DB::table('state')->where('country_id', '=', $country_id)
       ->orderBy('name')->get();
       if(!empty($record)){
         return new \App\Lib\Dto\Common\CountryDto($record);
       }
    }

}
