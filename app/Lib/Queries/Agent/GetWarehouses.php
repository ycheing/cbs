<?php

namespace App\Lib\Queries\Agent;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetWarehouses extends QueryBase {
    public $limits;
    public $sort;
    public $direction;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
      }

       $record =  DB::table('warehouse')
       ->orderBy($sort, $direction)
       ->paginate($limits);

       return \App\Lib\Dto\Agent\WarehouseDto::Collection($record);
    }

    public static function Paging($data){
        foreach ($data as $key ) {
          $limits =  $key['limits'];
          $sort =  $key['sort'];
          $direction =  $key['direction'];
       }

       $record =  DB::table('warehouse')
       ->paginate($limits)
       ->appends(['limits' => $limits, 'sort' => $sort, 'direction' => $direction]);

       if(!empty($record)){
         return $record;
       }
    }

    public static function All(){
      $record =  DB::table('warehouse')->orderBy('code')->get();
      if(!empty($record)){
        return $record;
      }
    }

    public static function Active(){
      $record =  DB::table('warehouse')->where('status',1)->orderBy('code')->get();
      if(!empty($record)){
        return $record;
      }
    }
}
