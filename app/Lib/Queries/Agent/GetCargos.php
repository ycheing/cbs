<?php

namespace App\Lib\Queries\Agent;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetCargos extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $keyword;
    // public $status_id;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $keyword =  $key['keyword'];
         // $status_id =  $key['status_id'];
      }

       $record =  DB::table('cargo')
       ->leftjoin('users', 'cargo.owner_id', '=', 'users.id')
       ->leftjoin('customer', 'cargo.customer_id', '=', 'customer.id')
      ->leftjoin('customsbroker', 'cargo.customs_broker_id', '=', 'customsbroker.id')
      ->leftjoin('warehouse', 'cargo.warehouse_id', '=', 'warehouse.id')
       ->select('cargo.*','users.name as owner','customer.code as customer', 'customsbroker.code as customs_broker', 'warehouse.code as warehouse')
       ->where('deleted', '=', 0)
       ->when($keyword != "", function ($q)  use ($keyword){
         return $q->whereRaw('LOWER(customer.code) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(customsbroker.code) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(warehouse.code) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(cargo.tracking_no) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(cargo.supplier_inv_no) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(cargo.supplier_goods_inv) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(cargo.customer_goods_inv) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(cargo.transport_inv) LIKE ?', $keyword);
       })
       ->orderBy($sort, $direction)
       ->paginate($limits);

       if(!empty($record)){
         return \App\Lib\Dto\Agent\CargoDto::Collection($record);
       }


    }

    public static function Paging($data){
        foreach ($data as $key ) {
          $limits =  $key['limits'];
          $sort =  $key['sort'];
          $direction =  $key['direction'];
          $keyword =  $key['keyword'];
       }

       $record =  DB::table('cargo')
       ->leftjoin('users', 'cargo.owner_id', '=', 'users.id')
       ->leftjoin('customer', 'cargo.customer_id', '=', 'customer.id')
      ->leftjoin('customsbroker', 'cargo.customs_broker_id', '=', 'customsbroker.id')
      ->leftjoin('warehouse', 'cargo.warehouse_id', '=', 'warehouse.id')
       ->when($keyword != "", function ($q)  use ($keyword){
         return $q->whereRaw('LOWER(customer.code) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(customsbroker.code) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(warehouse.code) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(cargo.tracking_no) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(cargo.supplier_inv_no) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(cargo.supplier_goods_inv) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(cargo.customer_goods_inv) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(cargo.transport_inv) LIKE ?', $keyword);
       })
        ->paginate($limits)
        ->appends(['limits' => $limits, 'sort' => $sort, 'direction' => $direction, 'keyword' => $keyword]);

       if(!empty($record)){
         return $record;
       }
    }


}
