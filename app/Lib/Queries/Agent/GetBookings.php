<?php

namespace App\Lib\Queries\Agent;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetBookings extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $keyword;
    public $status_id;
    public $owner_id;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $keyword =  $key['keyword'];
         $status_id =  $key['status_id'];
         $owner_id =  $key['owner_id'];
      }

       $record =  DB::table('booking')
       ->leftjoin('agent', 'booking.agent_id', '=', 'agent.id')
       ->leftjoin('users', 'booking.owner_id', '=', 'users.id')
       ->select('booking.*','agent.name as agent_name','users.name as created_by')
       ->when($keyword != "", function ($q)  use ($keyword){
         // $q->whereRaw('LOWER(order_no) LIKE ?', $keyword)
         // ->orWhereRaw('LOWER(booking_no) LIKE ?', $keyword)
         // ->orWhereRaw('LOWER(customer_code) LIKE ?', $keyword)
         // ->orWhereRaw('LOWER(customer_name) LIKE ?', $keyword);
          return $q->whereRaw('concat(LOWER(customer_name), LOWER(container_no)) LIKE ?', $keyword);

       })
       ->when($status_id != "", function ($q)  use ($status_id){
         return $q->where('booking.status', $status_id);
       })
       ->when($owner_id != "", function ($q)  use ($owner_id){
         return $q->where('booking.owner_id', $owner_id);
       })
      ->where('deleted', '=', 0)
       ->orderBy($sort, $direction)
       ->paginate($limits);

       return \App\Lib\Dto\Agent\BookingDto::Collection($record);
    }

    public static function Paging($data){
        foreach ($data as $key ) {
          $limits =  $key['limits'];
          $sort =  $key['sort'];
          $direction =  $key['direction'];
          $keyword =  $key['keyword'];
          $status_id =  $key['status_id'];
          $owner_id =  $key['owner_id'];
       }

       $record =  DB::table('booking')
       ->when($keyword != "", function ($q)  use ($keyword){
           return $q->whereRaw('concat(LOWER(customer_name), LOWER(container_no)) LIKE ?', $keyword);
         // return $q->whereRaw('LOWER(order_no) LIKE ?', $keyword)
         //   ->orWhereRaw('LOWER(booking_no) LIKE ?', $keyword)
         //  ->orWhereRaw('LOWER(customer_code) LIKE ?', $keyword)
         //   ->orWhereRaw('LOWER(customer_name) LIKE ?', $keyword);
       })
       ->when($status_id != "", function ($q)  use ($status_id){
         return $q->where('booking.status', $status_id);
       })
       ->when($owner_id != "", function ($q)  use ($owner_id){
         return $q->where('booking.owner_id', $owner_id);
       })
       ->where('deleted', '=', 0)
       ->paginate($limits)
       ->appends(['limits' => $limits, 'sort' => $sort, 'direction' => $direction, 'keyword' => $keyword, 'status_id' => $status_id, 'owner_id' => $owner_id]);

       if(!empty($record)){
         return $record;
       }
    }


}
