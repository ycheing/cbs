<?php

namespace App\Lib\Queries\Agent;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetCustomer extends QueryBase {
    public static function Result($id){
       $record = DB::table('customer')
              ->where('id', $id)
              ->first();

       if(!empty($record)){
          //Get Billing address
           $record->billing_address = DB::table('address')->where('customer_id', $record->id)->where('type', '1')->first();

           //Get Billing address
          $record->shipping_address = DB::table('address')->where('customer_id', $record->id)->where('type', '2')->first();
       }
       return new \App\Lib\Dto\Agent\CustomerDto($record);
    }

    //Validated result
    public static function ValidateDelete($id){
      $result = true;
      //Booking
       $booking =  DB::table('booking')->where('customer_id', '=', $id)
          ->count();

        if($booking > 0){
          $result = false;
        }

      //Payment Transfer
       $payment_transfer =  DB::table('payment_transfer')->where('customer_id', '=', $id)
          ->count();

        if($payment_transfer > 0){
          $result = false;
        }

       return $result;
    }
}
