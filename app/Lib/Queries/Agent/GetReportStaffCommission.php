<?php

namespace App\Lib\Queries\Agent;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetReportStaffCommission extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $date_from;
    public $date_to;
    // public $customer_id;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $user_id =  $key['user_id'];
         // $customer_id =  $key['customer_id'];
      }

       $record =  DB::table('booking_person')
       ->leftjoin('booking', 'booking.id', '=', 'booking_person.booking_id')
       ->where('booking.deleted', '=', 0)
       ->where('booking.loading', '=', '1')
       ->where('booking_person.employee_id', '=', $user_id)
       // ->whereBetween('booking.loading_date',[$date_from, $date_to])
       ->whereDate('loading_date', '>=', $date_from)
       ->whereDate('loading_date', '<=', $date_to)
       ->select('booking.id', 'booking.loading_date', 'booking.customer_name','booking.order_no', 'booking.booking_no', 'booking.container_no')
       ->groupBy('booking.id', 'booking.loading_date', 'booking.customer_name','booking.order_no', 'booking.booking_no',  'booking.container_no')
       ->orderBy($sort, $direction)
       ->get();

       if(!empty($record)){
         foreach($record as $r){
         $r->employees =  DB::table('booking_person')
               ->leftjoin('users', 'booking_person.employee_id', '=', 'users.id')
               ->leftjoin('job', 'booking_person.job_id', '=', 'job.id')
               ->select('booking_person.*','users.name as employee', 'job.name as job')
               ->where('booking_person.booking_id', '=', $r->id)
               ->where('booking_person.employee_id', '=', $user_id)
               ->orderBy('users.name')
               ->get();
         }

         return \App\Lib\Dto\Agent\StaffCommissionDto::Collection($record);
       }
    }


}
