<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetPayment extends QueryBase {
    public static function Result($id){
       $record = DB::table('payment')
       ->leftjoin('users', 'payment.owner_id', '=', 'users.id')
       ->leftjoin('payment_status', 'payment.status_id', '=', 'payment_status.id')
       ->select('payment.*','users.name as owner','payment_status.name as status')
        ->where('payment.id', $id)
        ->first();
        if(!empty($record))
       return new \App\Lib\Dto\Admin\PaymentDto($record);
    }

    public static function GetPaymentSuppliers($id){
       $record = DB::table('payment_supplier')
              ->where('payment_supplier.payment_id', $id)
              ->get();
        if(!empty($record)){
          foreach($record as $r){
            $r->beneficiaries =  DB::table('payment_beneficiary')
            ->leftjoin('beneficiary', 'payment_beneficiary.beneficiary_id', '=', 'beneficiary.id')
            ->select('payment_beneficiary.*','beneficiary.name as beneficiary')
            ->where('payment_supplier_id', '=', $r->id)
            ->orderBy('id')
            ->get();
          }
          return \App\Lib\Dto\Admin\PaymentSupplierDto::Collection($record);
        }

    }

    public static function GetPaymentSupplier($id){
       $record = DB::table('payment_supplier')
        ->where('payment_supplier.id', $id)
        ->first();
        if(!empty($record)){
          $record->beneficiaries =  DB::table('payment_beneficiary')
          ->leftjoin('beneficiary', 'payment_beneficiary.beneficiary_id', '=', 'beneficiary.id')
          ->select('payment_beneficiary.*','beneficiary.name as beneficiary')
          ->where('payment_supplier_id', '=', $record->id)
          ->orderBy('id')
          ->get();
         return new \App\Lib\Dto\Admin\PaymentSupplierDto($record);
        }

    }

    public static function GetPaymentBeneficiary($id){
       $record = DB::table('payment_beneficiary')
        ->where('payment_beneficiary.id', $id)
        ->first();


        if(!empty($record)){
         return new \App\Lib\Dto\Admin\PaymentBeneficiaryDto($record);
        }

    }


}
