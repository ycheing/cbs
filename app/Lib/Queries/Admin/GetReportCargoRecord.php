<?php

namespace App\Lib\Queries\Admin;

use App\Lib\Queries\QueryBase;
use Illuminate\Support\Facades\DB;

class GetReportCargoRecord extends QueryBase
{
    public $limits;
    public $sort;
    public $direction;
    public $date_from;
    public $date_to;
    public $customer_id;
    public $customs_broker_id;
    public $tracking_no;

    public static function Result($data)
    {
        foreach ($data as $key) {
            $limits = $key['limits'];
            $sort = $key['sort'];
            $direction = $key['direction'];
            $date_from = $key['date_from'];
            $date_to = $key['date_to'];
            $customer_id = $key['customer_id'];
            $customs_broker_id = $key['customs_broker_id'];
            $owner_id = $key['owner_id'];
            $warehouse_id = $key['warehouse_id'];
            $tracking_no = $key['tracking_no'];
        }
        $record = DB::table('cargo')
        ->leftjoin('users', 'cargo.owner_id', '=', 'users.id')
        ->leftjoin('customer', 'cargo.customer_id', '=', 'customer.id')
        ->leftjoin('customsbroker', 'cargo.customs_broker_id', '=', 'customsbroker.id')
        ->leftjoin('warehouse', 'cargo.warehouse_id', '=', 'warehouse.id')
        ->select('cargo.*', 'users.name as owner', 'customer.code as customer', 'customsbroker.code as customs_broker', 'warehouse.code as warehouse')
        ->where('deleted', '=', 0)
        ->whereDate('date', '>=', $date_from)
        ->whereDate('date', '<=', $date_to)
        ->when($customer_id != '', function ($q) use ($customer_id) {
            return $q->where('customer_id', $customer_id);
        })
        ->when($customs_broker_id != '', function ($q) use ($customs_broker_id) {
            return $q->where('customs_broker_id', $customs_broker_id);
        })
        ->when($tracking_no != '', function ($q) use ($tracking_no) {
            return $q->where('tracking_no', 'like', '%'.$tracking_no.'%');
        })
        ->when($owner_id != '', function ($q) use ($owner_id) {
            return $q->where('owner_id', $owner_id);
        })
        ->when($warehouse_id != '', function ($q) use ($warehouse_id) {
            return $q->where('warehouse_id', $warehouse_id);
        })
        ->orderBy($sort, $direction)
        ->get();

        if (!empty($record)) {
            return \App\Lib\Dto\Admin\CargoRecordDto::Collection($record);
        }
    }
}
