<?php

namespace App\Lib\Queries\Admin;

use App\Lib\Queries\QueryBase;
use Illuminate\Support\Facades\DB;

class GetReportContainerLoading extends QueryBase
{
    public $limits;
    public $sort;
    public $direction;
    public $date_from;
    public $date_to;
    public $customer_id;
    public $user_id;

    public static function Result($data)
    {
        foreach ($data as $key) {
            $limits = $key['limits'];
            $sort = $key['sort'];
            $direction = $key['direction'];
            $date_from = $key['date_from'];
            $date_to = $key['date_to'];
            $customer_id = $key['customer_id'];
            $agent_id = $key['agent_id'];
            $user_id = $key['user_id'];
        }

        $record = DB::table('booking')
        ->leftjoin('agent', 'booking.agent_id', '=', 'agent.id')
        ->select('booking.*', 'agent.name as agent_name')
        ->where('deleted', '=', 0)
        ->where('loading', '=', '1')
        ->whereDate('loading_date', '>=', $date_from)
        ->whereDate('loading_date', '<=', $date_to)
        // ->whereBetween('loading_date',[$date_from, $date_to])
        // ->when($customer_id != "", function ($q)  use ($customer_id){
        //   return $q->where('customer_id', $customer_id);
        // })
        ->where(function ($q) use ($customer_id) {
            foreach ($customer_id as $c) {
                $q->orwhere('customer_id', $customer_id);
            }
        })
        ->when($agent_id != '', function ($q) use ($agent_id) {
            return $q->where('agent_id', $agent_id);
        })
        ->when($user_id != '', function ($q) use ($user_id) {
            return $q->where('owner_id', $user_id);
        })

        ->orderBy($sort, $direction)
        ->get();

        if (!empty($record)) {
            foreach ($record as $r) {
                $r->other_fee = DB::table('booking_fee')
                       ->where('booking_fee.booking_id', $r->id)
                       ->where('booking_fee.type', 1)
                       ->sum('amount');
            }

            return \App\Lib\Dto\Admin\ContainerLoadingDto::Collection($record);
        }
    }
}
