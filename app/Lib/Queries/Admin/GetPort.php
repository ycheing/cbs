<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetPort extends QueryBase {
    public static function Result($id){
       $record = DB::table('port')
             ->leftjoin('country', 'port.country_id', '=', 'country.id')
             ->select('port.*', 'country.name as country')
              ->where('port.id', $id)
              ->first();
              if(!empty($record))
       return new \App\Lib\Dto\Admin\PortDto($record);
    }

    //Validated result
    public static function ValidateDelete($id){
      $result = true;
      //Booking
       $booking =  DB::table('booking')->where('loading_port_id', '=', $id)
       ->orWhere('destination_port_id', '=', $id)
      ->count();

        if($booking > 0){
          $result = false;
        }

       return $result;
    }
}
