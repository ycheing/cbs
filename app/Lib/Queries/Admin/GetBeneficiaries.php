<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetBeneficiaries extends QueryBase {
    public $limits;
    public $sort;
    public $direction;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
      }

       $record =  DB::table('beneficiary')
       ->orderBy($sort, $direction)
       ->paginate($limits);

       return \App\Lib\Dto\Admin\BeneficiaryDto::Collection($record);
    }

    public static function Paging($data){
        foreach ($data as $key ) {
          $limits =  $key['limits'];
          $sort =  $key['sort'];
          $direction =  $key['direction'];
       }

       $record =  DB::table('beneficiary')
       ->paginate($limits)
        ->appends(['limits' => $limits, 'sort' => $sort, 'direction' => $direction]);

       if(!empty($record)){
         return $record;
       }
    }

    public static function All(){
      $record =  DB::table('beneficiary')->orderBy('name')->get();
      if(!empty($record)){
         return \App\Lib\Dto\Admin\BeneficiaryDto::Collection($record);
      }
    }

    public static function ByStatus($status){
      $record =  DB::table('beneficiary')
      ->where('status', '=', $status)
      ->orderBy('name')->get();
      if(!empty($record)){
         return \App\Lib\Dto\Admin\BeneficiaryDto::Collection($record);
      }
    }
}
