<?php

namespace App\Lib\Queries\Admin;

use App\Lib\Queries\QueryBase;
use Illuminate\Support\Facades\DB;

class GetCustomers extends QueryBase
{
    public $limits;
    public $sort;
    public $direction;
    public $keyword;

    public static function Result($data)
    {
        foreach ($data as $key) {
            $limits = $key['limits'];
            $sort = $key['sort'];
            $direction = $key['direction'];
            $keyword = $key['keyword'];
        }

        $record = DB::table('customer')
        ->when($keyword != '', function ($q) use ($keyword) {
            return $q->whereRaw('LOWER(code) LIKE ?', $keyword)
              ->orWhereRaw('LOWER(name) LIKE ?', $keyword)
             ->orWhereRaw('LOWER(id_number) LIKE ?', $keyword)
              ->orWhereRaw('LOWER(attn) LIKE ?', $keyword);
        })
        ->orderBy($sort, $direction)
        ->paginate($limits);

        if (!empty($record)) {
            foreach ($record as $r) {
                // Get Billing address
                $r->billing_address = DB::table('address')->where('customer_id', $r->id)->where('type', '1')->first();

                // Get Billing address
                $r->shipping_address = DB::table('address')->where('customer_id', $r->id)->where('type', '2')->first();
            }
        }

        return \App\Lib\Dto\Admin\CustomerDto::Collection($record);
    }

    public static function Paging($data)
    {
        foreach ($data as $key) {
            $limits = $key['limits'];
            $sort = $key['sort'];
            $direction = $key['direction'];
            $keyword = $key['keyword'];
        }

        $record = DB::table('customer')
        ->when($keyword != '', function ($q) use ($keyword) {
            return $q->whereRaw('LOWER(code) LIKE ?', $keyword)
              ->orWhereRaw('LOWER(name) LIKE ?', $keyword)
             ->orWhereRaw('LOWER(id_number) LIKE ?', $keyword)
              ->orWhereRaw('LOWER(attn) LIKE ?', $keyword);
        })
         ->paginate($limits)
        ->appends(['limits' => $limits, 'sort' => $sort, 'direction' => $direction, 'keyword' => $keyword]);

        if (!empty($record)) {
            return $record;
        }
    }

    public static function All()
    {
        $record = DB::table('customer')->orderBy('code')->get();
        if (!empty($record)) {
            return $record;
        }
    }


    public static function Active()
    {
        $record = DB::table('customer')->orderBy('code')->where('status', 1)->get();
        if (!empty($record)) {
            return $record;
        }
    }

    public static function ExcludeCode($code)
    {
        $record = DB::table('customer')
        ->where('code', 'NOT LIKE', $code)
        ->where('status', 1)
        ->orderBy('code')->get();
        if (!empty($record)) {
            return $record;
        }
    }
}
