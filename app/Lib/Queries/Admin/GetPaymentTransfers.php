<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetPaymentTransfers extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $keyword;
    // public $status_id;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $keyword =  $key['keyword'];
         // $status_id =  $key['status_id'];
      }

       $record =  DB::table('payment_transfer')
       ->leftjoin('users', 'payment_transfer.owner_id', '=', 'users.id')
       ->select('payment_transfer.*','users.name as owner')
       ->where('deleted', '=', 0)
       ->when($keyword != "", function ($q)  use ($keyword){
         return $q->whereRaw('LOWER(payment_transfer.customer_code) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(payment_transfer.customer_name) LIKE ?', $keyword);
       })
       ->orderBy($sort, $direction)
       ->paginate($limits);

       return \App\Lib\Dto\Admin\PaymentTransferDto::Collection($record);
    }

    public static function Paging($data){
        foreach ($data as $key ) {
          $limits =  $key['limits'];
          $sort =  $key['sort'];
          $direction =  $key['direction'];
          $keyword =  $key['keyword'];
          // $status_id =  $key['status_id'];
       }

       $record =  DB::table('payment_transfer')
       ->when($keyword != "", function ($q)  use ($keyword){
         return $q->whereRaw('LOWER(payment_transfer.customer_code) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(payment_transfer.customer_name) LIKE ?', $keyword);
       })
        ->paginate($limits)
        ->appends(['limits' => $limits, 'sort' => $sort, 'direction' => $direction, 'keyword' => $keyword]);

       if(!empty($record)){
         return $record;
       }
    }


}
