<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetStates extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $country_id;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $country_id = $key['country_id'];
      }

       $record =  DB::table('state')->where('country_id', $country_id)->orderBy($sort, $direction)
        ->paginate($limits);

       return \App\Lib\Dto\Admin\StateDto::Collection($record);
    }

    public static function Paging($data){
        foreach ($data as $key ) {
          $limits =  $key['limits'];
          $sort =  $key['sort'];
          $direction =  $key['direction'];
          $country_id = $key['country_id'];
       }

       $record =  DB::table('state')->where('country_id', $country_id)
       ->paginate($limits)
       ->appends(['limits' => $limits, 'sort' => $sort, 'direction' => $direction]);

       if(!empty($record)){
         return $record;
       }
    }

    public static function AllStates($country_id){
       $record =  DB::table('state')->where('country_id', $country_id)->orderBy($sort, $direction)
        ->get();

       return \App\Lib\Dto\Admin\StateDto::Collection($record);
    }
}
