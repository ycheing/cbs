<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetCargo extends QueryBase {
    public static function Result($id){
       $record = DB::table('cargo')
       ->leftjoin('users', 'cargo.owner_id', '=', 'users.id')
       ->leftjoin('customer', 'cargo.customer_id', '=', 'customer.id')
       ->leftjoin('customsbroker', 'cargo.customs_broker_id', '=', 'customsbroker.id')
       ->leftjoin('warehouse', 'cargo.warehouse_id', '=', 'warehouse.id')
       ->select('cargo.*','users.name as owner','customer.code as customer', 'customsbroker.code as customs_broker', 'warehouse.code as warehouse')
        ->where('cargo.id', $id)
        ->first();
        if(!empty($record))
       return new \App\Lib\Dto\Admin\CargoDto($record);
    }


}
