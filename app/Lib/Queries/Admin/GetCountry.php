<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetCountry extends QueryBase {
    public static function Result($id){
       $record = DB::table('country')
              ->where('id', $id)
              ->first();
              if(!empty($record))
       return new \App\Lib\Dto\Admin\CountryDto($record);
    }

    //Validated result
    public static function ValidateDelete($id){
      $result = true;
      //State
       $state =  DB::table('state')->where('country_id', '=', $id)
          ->count();

        if($state > 0){
          $result = false;
        }

      //Port
       $port =  DB::table('port')->where('country_id', '=', $id)
          ->count();

        if($port > 0){
          $result = false;
        }

      //Address
       $address =  DB::table('address')->where('country_id', '=', $id)
          ->count();

        if($address > 0){
          $result = false;
        }

      //Booking
       $booking =  DB::table('booking')->where('delivery_country_id', '=', $id)
          ->count();

        if($booking > 0){
          $result = false;
        }
       return $result;
    }
}
