<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetBookings extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $keyword;
    public $status_id;
    public $user_id;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $keyword =  $key['keyword'];
         $status_id =  $key['status_id'];
         $user_id =  $key['user_id'];
      }

       $record =  DB::table('booking')
       ->leftjoin('agent', 'booking.agent_id', '=', 'agent.id')
       ->leftjoin('users', 'booking.owner_id', '=', 'users.id')
       ->select('booking.*','agent.name as agent_name','users.name as created_by')
       ->where('deleted', '=', 0)
       ->when($keyword != "", function ($q)  use ($keyword){
            return $q->whereRaw('concat(LOWER(customer_name), LOWER(container_no)) LIKE ?', $keyword);
         // return $q->whereRaw('LOWER(order_no) LIKE ?', $keyword)
         //   ->orWhereRaw('LOWER(booking_no) LIKE ?', $keyword)
         //  ->orWhereRaw('LOWER(customer_code) LIKE ?', $keyword)
         //   ->orWhereRaw('LOWER(customer_name) LIKE ?', $keyword);
       })
       ->when($status_id != "", function ($q)  use ($status_id){
         return $q->where('booking.status', $status_id);
       })
       ->when($user_id != "", function ($q)  use ($user_id){
         return $q->where('booking.owner_id', $user_id);
       })
       ->orderBy($sort, $direction)
       ->paginate($limits);

       return \App\Lib\Dto\Admin\BookingDto::Collection($record);
    }

    public static function Paging($data){
        foreach ($data as $key ) {
          $limits =  $key['limits'];
          $sort =  $key['sort'];
          $direction =  $key['direction'];
          $keyword =  $key['keyword'];
          $status_id =  $key['status_id'];
          $user_id =  $key['user_id'];
       }

       $record =  DB::table('booking')
        ->where('deleted', '=', 0)
       ->when($keyword != "", function ($q)  use ($keyword){
            return $q->whereRaw('concat(LOWER(customer_name), LOWER(container_no)) LIKE ?', $keyword);
          // return $q->whereRaw('LOWER(order_no) LIKE ?', $keyword)
          //   ->orWhereRaw('LOWER(booking_no) LIKE ?', $keyword)
          //  ->orWhereRaw('LOWER(customer_code) LIKE ?', $keyword)
          //   ->orWhereRaw('LOWER(customer_name) LIKE ?', $keyword);
        })
        ->when($status_id != "", function ($q)  use ($status_id){
          return $q->where('booking.status', $status_id);
        })
        ->when($user_id != "", function ($q)  use ($user_id){
          return $q->where('booking.owner_id', $user_id);
        })
        ->paginate($limits)
        ->appends(['limits' => $limits, 'sort' => $sort, 'direction' => $direction, 'keyword' => $keyword, 'status_id' => $status_id, 'user_id' => $user_id]);


       if(!empty($record)){
         return $record;
       }
    }


}
