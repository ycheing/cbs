<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetReportDailyPayment extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $date_from;
    public $date_to;
    // public $customer_id;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $supplier_id =  $key['supplier_id'];
         $customer_id =  $key['customer_id'];
      }
      if($supplier_id != ''){
        $record =  DB::table('payment_supplier')
        ->join('payment', 'payment_supplier.payment_id', '=', 'payment.id')
        ->leftjoin('users', 'payment.owner_id', '=', 'users.id')
        ->leftjoin('payment_status', 'payment.status_id', '=', 'payment_status.id')
        ->select('payment.*','users.name as owner','payment_status.name as status')
        ->where('deleted', '=', 0)
        ->whereDate('payment.remittance_date', '>=', $date_from)
        ->whereDate('payment.remittance_date', '<=', $date_to)
        ->when($supplier_id != "", function ($q)  use ($supplier_id){
          return $q->where('payment_supplier.supplier_id', $supplier_id);
        })
        ->when($customer_id != "", function ($q)  use ($customer_id){
          return $q->where('payment.customer_id', $customer_id);
        })
        ->orderBy($sort, $direction)
        ->get();
      }else{
        $record =  DB::table('payment')
        ->leftjoin('users', 'payment.owner_id', '=', 'users.id')
        ->leftjoin('payment_status', 'payment.status_id', '=', 'payment_status.id')
        ->select('payment.*','users.name as owner','payment_status.name as status')
        ->where('deleted', '=', 0)
        ->whereDate('payment.remittance_date', '>=', $date_from)
        ->whereDate('payment.remittance_date', '<=', $date_to)
        ->when($customer_id != "", function ($q)  use ($customer_id){
          return $q->where('payment.customer_id', $customer_id);
        })
        ->orderBy($sort, $direction)
        ->get();
      }
       if(!empty($record)){
         foreach($record as $r){
           if($supplier_id != ''){
             $r->suppliers =  DB::table('payment_supplier')
             ->where('payment_id', '=', $r->id)
             ->when($supplier_id != "", function ($q)  use ($supplier_id){
               return $q->where('payment_supplier.supplier_id', $supplier_id);
             })
             ->orderBy('created_at')
             ->get();
           }else{
             $r->suppliers =  DB::table('payment_supplier')
             ->where('payment_id', '=', $r->id)
             ->orderBy('created_at')
             ->get();
           }

           foreach($r->suppliers as $s){
             $s->beneficiaries =  DB::table('payment_beneficiary')
             ->leftjoin('beneficiary', 'payment_beneficiary.beneficiary_id', '=', 'beneficiary.id')
             ->select('payment_beneficiary.*','beneficiary.name as beneficiary')
             ->where('payment_supplier_id', '=', $s->id)
             ->orderBy('created_at')
             ->get();
           }

         }
        return \App\Lib\Dto\Admin\DailyPaymentDto::Collection($record);
       }

    }

    public static function GetTotalCustomerAmount($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $supplier_id =  $key['supplier_id'];
         $customer_id =  $key['customer_id'];
      }

      if($supplier_id != ''){
        $record =  DB::table('payment_supplier')
        ->join('payment', 'payment_supplier.payment_id', '=', 'payment.id')
        ->where('deleted', '=', 0)
        ->whereDate('payment.remittance_date', '>=', $date_from)
        ->whereDate('payment.remittance_date', '<=', $date_to)
        ->when($supplier_id != "", function ($q)  use ($supplier_id){
          return $q->where('payment_supplier.supplier_id', $supplier_id);
        })
        ->when($customer_id != "", function ($q)  use ($customer_id){
          return $q->where('payment.customer_id', $customer_id);
        })
        ->sum('payment.customer_amount');
      }else{
        $record =  DB::table('payment')
        ->select('payment.*','users.name as owner','payment_status.name as status')
        ->where('deleted', '=', 0)
        ->whereDate('payment.remittance_date', '>=', $date_from)
        ->whereDate('payment.remittance_date', '<=', $date_to)
        ->when($customer_id != "", function ($q)  use ($customer_id){
          return $q->where('payment.customer_id', $customer_id);
        })
        ->sum('payment.customer_amount');
      }

       return $record;

    }

    public static function GetTotalCustomerTotal($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $supplier_id =  $key['supplier_id'];
         $customer_id =  $key['customer_id'];
      }
      if($supplier_id != ''){
        $record =  DB::table('payment_supplier')
        ->join('payment', 'payment_supplier.payment_id', '=', 'payment.id')
        ->where('deleted', '=', 0)
        ->whereDate('payment.remittance_date', '>=', $date_from)
        ->whereDate('payment.remittance_date', '<=', $date_to)
        ->when($supplier_id != "", function ($q)  use ($supplier_id){
          return $q->where('payment_supplier.supplier_id', $supplier_id);
        })
        ->when($customer_id != "", function ($q)  use ($customer_id){
          return $q->where('payment.customer_id', $customer_id);
        })
        ->sum('payment.customer_total');
      }else{
        $record =  DB::table('payment')
        ->where('deleted', '=', 0)
        ->whereDate('payment.remittance_date', '>=', $date_from)
        ->whereDate('payment.remittance_date', '<=', $date_to)
        ->when($customer_id != "", function ($q)  use ($customer_id){
          return $q->where('payment.customer_id', $customer_id);
        })
        ->sum('payment.customer_total');
      }

       return $record;

    }

    public static function GetTotalSupplierAmount($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $supplier_id =  $key['supplier_id'];
         $customer_id =  $key['customer_id'];
      }

      // if($supplier_id != ''){
        $record =  DB::table('payment_supplier')
        ->join('payment', 'payment_supplier.payment_id', '=', 'payment.id')
        ->where('deleted', '=', 0)
        ->whereDate('payment.remittance_date', '>=', $date_from)
        ->whereDate('payment.remittance_date', '<=', $date_to)
        ->when($supplier_id != "", function ($q)  use ($supplier_id){
          return $q->where('payment_supplier.supplier_id', $supplier_id);
        })
        ->when($customer_id != "", function ($q)  use ($customer_id){
          return $q->where('payment.customer_id', $customer_id);
        })
        ->sum('payment_supplier.supplier_amount');
      // }else{
      //   $record =  DB::table('payment_supplier')
      //   ->join('payment', 'payment_supplier.payment_id', '=', 'payment.id')
      //   ->where('deleted', '=', 0)
      //   ->whereDate('payment.remittance_date', '>=', $date_from)
      //   ->whereDate('payment.remittance_date', '<=', $date_to)
      //   ->sum('payment_supplier.supplier_amount');
      // }
       return $record;

    }

    public static function GetTotalSupplierTotal($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $supplier_id =  $key['supplier_id'];
         $customer_id =  $key['customer_id'];
      }

      // if($supplier_id != ''){
        $record =  DB::table('payment_supplier')
        ->join('payment', 'payment_supplier.payment_id', '=', 'payment.id')
        ->where('deleted', '=', 0)
        ->whereDate('payment.remittance_date', '>=', $date_from)
        ->whereDate('payment.remittance_date', '<=', $date_to)
        ->when($supplier_id != "", function ($q)  use ($supplier_id){
          return $q->where('payment_supplier.supplier_id', $supplier_id);
        })
        ->when($customer_id != "", function ($q)  use ($customer_id){
          return $q->where('payment.customer_id', $customer_id);
        })
        ->sum('payment_supplier.supplier_total');
      // }else{
      //   $record =  DB::table('payment_supplier')
      //   ->join('payment', 'payment_supplier.payment_id', '=', 'payment.id')
      //   ->where('deleted', '=', 0)
      //   ->whereDate('payment.remittance_date', '>=', $date_from)
      //   ->whereDate('payment.remittance_date', '<=', $date_to)
      //   ->sum('payment_supplier.supplier_total');
      // }

     return $record;

    }

    public static function GetTotalSupplierBankCharges($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $supplier_id =  $key['supplier_id'];
         $customer_id =  $key['customer_id'];
      }

      $record =  DB::table('payment_beneficiary')
      ->join('payment', 'payment_beneficiary.payment_id', '=', 'payment.id')
      ->join('payment_supplier', 'payment_beneficiary.payment_supplier_id', '=', 'payment_supplier.id')
      ->where('payment.deleted', '=', 0)
      ->whereDate('payment.remittance_date', '>=', $date_from)
      ->whereDate('payment.remittance_date', '<=', $date_to)
      ->when($supplier_id != "", function ($q)  use ($supplier_id){
        return $q->where('payment_supplier.supplier_id', $supplier_id);
      })
      ->when($customer_id != "", function ($q)  use ($customer_id){
        return $q->where('payment.customer_id', $customer_id);
      })
       ->sum('payment_beneficiary.bank_charges');

       return $record;

    }

    public static function GetTotalSupplierNetAmount($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $supplier_id =  $key['supplier_id'];
         $customer_id =  $key['customer_id'];
      }

      $record =  DB::table('payment_beneficiary')
      ->join('payment', 'payment_beneficiary.payment_id', '=', 'payment.id')
      ->join('payment_supplier', 'payment_beneficiary.payment_supplier_id', '=', 'payment_supplier.id')
      ->where('payment.deleted', '=', 0)
      ->whereDate('payment.remittance_date', '>=', $date_from)
      ->whereDate('payment.remittance_date', '<=', $date_to)
      ->when($supplier_id != "", function ($q)  use ($supplier_id){
        return $q->where('payment_supplier.supplier_id', $supplier_id);
      })
      ->when($customer_id != "", function ($q)  use ($customer_id){
        return $q->where('payment.customer_id', $customer_id);
      })
     ->sum('payment_beneficiary.net_amount');

     return $record;

    }
}
