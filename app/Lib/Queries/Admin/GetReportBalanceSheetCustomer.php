<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetReportBalanceSheetCustomer extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $date_from;
    public $date_to;
    public $customer_id;
    public $year;

    public static function GetInRecords($data){
      foreach ($data as $key ) {
         $customer_id =  $key['customer_id'];
         $year =  $key['year'];
      }

      $record =  DB::table('payment')
      ->select('payment.payment_date as date', 'payment.purpose_payment as description','payment.customer_amount as in','payment.customer_id as customer_id')
      ->where('deleted', '=', 0)
      ->where('customer_id', '=', $customer_id)
      ->whereYear('payment_date', '=', $year)
      ->orderBy('payment_date', 'asc')
      ->get();

       if(!empty($record)){
          return \App\Lib\Dto\Admin\BalanceSheetCustomerDto::Collection($record);
       }
    }

    public static function GetOutRecords($data){
      foreach ($data as $key ) {
         $customer_id =  $key['customer_id'];
         $year =  $key['year'];
      }

      $record =  DB::table('booking')
      ->select('booking.billing_goods_inv_date as date', 'booking.container_no',  'booking.booking_no', 'booking.billing_goods_inv_no', 'booking.billing_goods_amount as out', 'booking.customer_id as customer_id')
      ->where('deleted', '=', 0)
      ->where('customer_id', '=', $customer_id)
      ->where('declaration', '=', 1)
      // ->where('status', '=', 5)
      ->where('billing_goods_inv_date', '!=', '')
      ->whereYear('billing_goods_inv_date', '=', $year)
      // ->whereDate('payment_date', '<=', $date_to)
      ->orderBy('billing_goods_inv_date', 'asc')
      ->get();

       if(!empty($record)){
          return \App\Lib\Dto\Admin\BalanceSheetCustomerDto::Collection($record);
       }
    }

    public static function GetTotalInAmount($data){
      foreach ($data as $key ) {
         $customer_id =  $key['customer_id'];
         $year =  $key['year'];
      }

      $record =  DB::table('payment')
      ->where('deleted', '=', 0)
      ->where('customer_id', '=', $customer_id)
      ->where(function ($q)  use ($year){
        return $q->whereYear('payment_date', '<', $year)->orwhereNull('payment_date');
      })
      ->orderBy('payment_date', 'asc')
      ->sum('customer_amount');
       return $record;
    }

    public static function GetTotalOutAmount($data){
      foreach ($data as $key ) {
         $customer_id =  $key['customer_id'];
         $year =  $key['year'];
      }

      $record =  DB::table('booking')
      ->where('deleted', '=', 0)
      ->where('customer_id', '=', $customer_id)
      ->where('declaration', '=', 1)
      // ->where('status', '=', 5)
      ->where('billing_goods_inv_date', '!=', '')
      ->whereYear('billing_goods_inv_date', '<', $year)
      // ->orwhereNull('billing_goods_inv_date')
      ->where(function ($q)  use ($year){
        return $q->whereYear('billing_goods_inv_date', '<', $year)->orwhereNull('billing_goods_inv_date');
      })
      ->sum('billing_goods_amount');

      return $record;
    }
}
