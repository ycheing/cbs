<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetShippingCompany extends QueryBase {
    public static function Result($id){
       $record = DB::table('shipping_company')
              ->where('id', $id)
              ->first();
              if(!empty($record))
       return new \App\Lib\Dto\Admin\ShippingCompanyDto($record);
    }

    //Validated result
    public static function ValidateDelete($id){
      $result = true;
      //Booking
       $booking =  DB::table('booking')->where('shipping_company_id', '=', $id)
          ->count();

        if($booking > 0){
          $result = false;
        }

       return $result;
    }
}
