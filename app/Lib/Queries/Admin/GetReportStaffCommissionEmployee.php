<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetReportStaffCommissionEmployee extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $date_from;
    public $date_to;
    // public $customer_id;
    public $employee_id;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $employee_id =  $key['employee_id'];
      }

       $record =  DB::table('booking_person')
       ->leftjoin('booking', 'booking_person.booking_id', '=', 'booking.id')
       ->leftjoin('agent', 'booking.agent_id', '=', 'agent.id')
       ->select('booking.*','agent.name as agent_name')
       ->where('booking.deleted', '=', 0)
       ->where('booking.loading', '=', '1')
       ->whereDate('booking.loading_date', '>=', $date_from)
       ->whereDate('booking.loading_date', '<=', $date_to)
       ->where('booking_person.employee_id', '=', $employee_id)
       ->orderBy($sort, $direction)
       ->get();

       // echo "<pre>";
       // print_r($record);
       // echo "</pre>";
       if(!empty($record)){
         foreach($record as $r){
           $r->employees =  DB::table('booking_person')
           ->leftjoin('users', 'booking_person.employee_id', '=', 'users.id')
           ->leftjoin('job', 'booking_person.job_id', '=', 'job.id')
           ->select('booking_person.*','users.name as employee', 'job.name as job')
           ->where('booking_person.booking_id', '=', $r->id)
           ->where('booking_person.employee_id', '=', $employee_id)
           ->orderBy('users.name')
           ->get();
         }
         return \App\Lib\Dto\Admin\StaffCommissionDto::Collection($record);
       }

    }


}
