<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetJob extends QueryBase {
    public static function Result($id){
       $record = DB::table('job')
              ->where('id', $id)
              ->first();
              if(!empty($record))
       return new \App\Lib\Dto\Admin\JobDto($record);
    }

    //Validated result
    public static function ValidateDelete($id){
      $result = true;
      //Booking Person
       $booking =  DB::table('booking_person')->where('job_id', '=', $id)
          ->count();

        if($booking > 0){
          $result = false;
        }

       return $result;
    }
}
