<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetReportMonthlyStatement extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $date_from;
    public $date_to;
    public $customer_id;
    public $billing_company_id;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $customer_id =  $key['customer_id'];
         $billing_company_id =  $key['billing_company_id'];
      }

       $record =  DB::table('booking')
       ->where('booking.deleted', '=', 0)
       ->where(function($query) {
               $query->where('booking.declaration', '=', '1')
                     ->orWhere('booking.loading', '=', '1');
       })
       // ->where('booking.declaration', '=', '1')
       // ->Where('booking.loading', '=', '1')
       // ->whereBetween('booking.created_at',[$date_from, $date_to])
       ->whereDate('loading_date', '>=', $date_from)
       ->whereDate('loading_date', '<=', $date_to)
       // ->when(!empty($billing_company_id), function ($q)  use ($billing_company_id){
       //   // return $q->whereRaw('concat(booking_no) LIKE ?', $billing_company_id);
       //   return $q->whereIn('billing_company_id', $billing_company_id);
       // })
       ->when($customer_id != "", function ($q)  use ($customer_id){
          return $q->where('customer_id', $customer_id);
       })
       ->where(function ($q) use ($billing_company_id){
         foreach($billing_company_id as $bc){
           $q->orwhereRaw('concat(booking_no) LIKE ?', '%' . $bc . '%');
         }
       })
       ->orderBy($sort, $direction)
       ->get();

       if(!empty($record)){

         foreach($record as $r){
           //other fee
           $r->other_fee_1 = DB::table('booking_fee')
                  ->where('booking_fee.booking_id', $r->id)
                  ->where('booking_fee.type','1')
                  ->sum('amount');

          $r->other_fee_2 = DB::table('booking_fee')
                 ->where('booking_fee.booking_id', $r->id)
                 ->where('booking_fee.type','2')
                 ->sum('amount');
         }

          return \App\Lib\Dto\Admin\MonthlyStatementDto::Collection($record);
       }
    }

    public static function TotalBillingHandling($data){
      foreach ($data as $key ) {
        $limits =  $key['limits'];
        $sort =  $key['sort'];
        $direction =  $key['direction'];
        $date_from =  $key['date_from'];
        $date_to =  $key['date_to'];
        $billing_company_id =  $key['billing_company_id'];
      }
       $record =  DB::table('booking')
       ->where('booking.deleted', '=', 0)
       ->where(function($query) {
               $query->where('booking.declaration', '=', '1')
                     ->orWhere('booking.loading', '=', '1');
       })
       // ->where('booking.declaration', '=', '1')
       // ->Where('booking.loading', '=', '1')
       // ->whereBetween('booking.created_at',[$date_from, $date_to])
       ->whereDate('loading_date', '>=', $date_from)
       ->whereDate('loading_date', '<=', $date_to)
       // ->when($billing_company_id != "", function ($q)  use ($billing_company_id){
       //   return $q->whereIn('billing_company_id', $billing_company_id);
       //
       //   // return $q->whereRaw('concat(booking_no) LIKE ?', $billing_company_id);
       //   // return $q->where('billing_company_id', $billing_company_id);
       // })
       ->where(function ($q) use ($billing_company_id){
         foreach($billing_company_id as $bc){
           $q->orwhereRaw('concat(booking_no) LIKE ?', '%' . $bc . '%');
         }
       })
       ->sum('billing_handling_prices');

       return $record;

    }

    public static function TotalBillingOther($data){
      foreach ($data as $key ) {
        $limits =  $key['limits'];
        $sort =  $key['sort'];
        $direction =  $key['direction'];
        $date_from =  $key['date_from'];
        $date_to =  $key['date_to'];
        $billing_company_id =  $key['billing_company_id'];
      }
       $record =  DB::table('booking')
       ->where('booking.deleted', '=', 0)
       ->where(function($query) {
               $query->where('booking.declaration', '=', '1')
                     ->orWhere('booking.loading', '=', '1');
       })
       // ->where('booking.declaration', '=', '1')
       // ->Where('booking.loading', '=', '1')
       // ->whereBetween('booking.created_at',[$date_from, $date_to])
       ->whereDate('loading_date', '>=', $date_from)
       ->whereDate('loading_date', '<=', $date_to)
       // ->when($billing_company_id != "", function ($q)  use ($billing_company_id){
       //   // return $q->whereRaw('concat(booking_no) LIKE ?', $billing_company_id);
       //   // return $q->where('billing_company_id', $billing_company_id);
       //   return $q->whereIn('billing_company_id', $billing_company_id);
       //
       // })
       ->where(function ($q) use ($billing_company_id){
         foreach($billing_company_id as $bc){
           $q->orwhereRaw('concat(booking_no) LIKE ?', '%' . $bc . '%');
         }
       })
       ->sum('billing_other_prices');

       return $record;

    }

    public static function TotalCustomsHandling($data){
      foreach ($data as $key ) {
        $limits =  $key['limits'];
        $sort =  $key['sort'];
        $direction =  $key['direction'];
        $date_from =  $key['date_from'];
        $date_to =  $key['date_to'];
        $billing_company_id =  $key['billing_company_id'];
      }
       $record =  DB::table('booking')
       ->where('booking.deleted', '=', 0)
       ->where(function($query) {
               $query->where('booking.declaration', '=', '1')
                     ->orWhere('booking.loading', '=', '1');
       })
       // ->where('booking.declaration', '=', '1')
       // ->Where('booking.loading', '=', '1')
       // ->whereBetween('booking.created_at',[$date_from, $date_to])
       ->whereDate('loading_date', '>=', $date_from)
       ->whereDate('loading_date', '<=', $date_to)
       // ->when($billing_company_id != "", function ($q)  use ($billing_company_id){
       //   // return $q->whereRaw('concat(booking_no) LIKE ?', $billing_company_id);
       //   // return $q->where('billing_company_id', $billing_company_id);
       //   return $q->whereIn('billing_company_id', $billing_company_id);
       //
       // })
       ->where(function ($q) use ($billing_company_id){
         foreach($billing_company_id as $bc){
           $q->orwhereRaw('concat(booking_no) LIKE ?', '%' . $bc . '%');
         }
       })
       ->sum('custom_handling_prices');

       return $record;

    }

    public static function TotalCustomsOther($data){
      foreach ($data as $key ) {
        $limits =  $key['limits'];
        $sort =  $key['sort'];
        $direction =  $key['direction'];
        $date_from =  $key['date_from'];
        $date_to =  $key['date_to'];
        $billing_company_id =  $key['billing_company_id'];
      }
       $record =  DB::table('booking')
       ->where('booking.deleted', '=', 0)
       ->where(function($query) {
               $query->where('booking.declaration', '=', '1')
                     ->orWhere('booking.loading', '=', '1');
       })
       ->whereDate('loading_date', '>=', $date_from)
       ->whereDate('loading_date', '<=', $date_to)
       // ->when($billing_company_id != "", function ($q)  use ($billing_company_id){
       //   // return $q->whereRaw('concat(booking_no) LIKE ?', $billing_company_id);
       //   // return $q->where('billing_company_id', $billing_company_id);
       //   return $q->whereIn('billing_company_id', $billing_company_id);
       //
       // })
       ->where(function ($q) use ($billing_company_id){
         foreach($billing_company_id as $bc){
           $q->orwhereRaw('concat(booking_no) LIKE ?', '%' . $bc . '%');
         }
       })
       ->sum('custom_other_prices');

       return $record;

    }

    public static function TotalHandlingCharges($data){
      foreach ($data as $key ) {
        $limits =  $key['limits'];
        $sort =  $key['sort'];
        $direction =  $key['direction'];
        $date_from =  $key['date_from'];
        $date_to =  $key['date_to'];
        $billing_company_id =  $key['billing_company_id'];
      }
       $record =  DB::table('booking')
       ->where('booking.deleted', '=', 0)
       ->where(function($query) {
               $query->where('booking.declaration', '=', '1')
                     ->orWhere('booking.loading', '=', '1');
       })
       // ->where('booking.declaration', '=', '1')
       // ->Where('booking.loading', '=', '1')
       // ->whereBetween('booking.created_at',[$date_from, $date_to])
       ->whereDate('loading_date', '>=', $date_from)
       ->whereDate('loading_date', '<=', $date_to)
       // ->when($billing_company_id != "", function ($q)  use ($billing_company_id){
       //   // return $q->whereRaw('concat(booking_no) LIKE ?', $billing_company_id);
       //   // return $q->where('billing_company_id', $billing_company_id);
       //   return $q->whereIn('billing_company_id', $billing_company_id);
       //
       // })
       ->where(function ($q) use ($billing_company_id){
         foreach($billing_company_id as $bc){
           $q->orwhereRaw('concat(booking_no) LIKE ?', '%' . $bc . '%');
         }
       })
       ->sum('handling_charges');

       return $record;

    }

    public static function TotalCommission($data){
      foreach ($data as $key ) {
        $limits =  $key['limits'];
        $sort =  $key['sort'];
        $direction =  $key['direction'];
        $date_from =  $key['date_from'];
        $date_to =  $key['date_to'];
        $billing_company_id =  $key['billing_company_id'];
      }
       $record =  DB::table('booking')
       ->where('booking.deleted', '=', 0)
       ->where(function($query) {
               $query->where('booking.declaration', '=', '1')
                     ->orWhere('booking.loading', '=', '1');
       })
       // ->where('booking.declaration', '=', '1')
       // ->Where('booking.loading', '=', '1')
       // ->whereBetween('booking.created_at',[$date_from, $date_to])
       ->whereDate('loading_date', '>=', $date_from)
       ->whereDate('loading_date', '<=', $date_to)
       // ->when($billing_company_id != "", function ($q)  use ($billing_company_id){
       //   // return $q->whereRaw('concat(booking_no) LIKE ?', $billing_company_id);
       //   // return $q->where('billing_company_id', $billing_company_id);
       //   return $q->whereIn('billing_company_id', $billing_company_id);
       //
       // })
       ->where(function ($q) use ($billing_company_id){
         foreach($billing_company_id as $bc){
           $q->orwhereRaw('concat(booking_no) LIKE ?', '%' . $bc . '%');
         }
       })
       ->sum('commission');

       return $record;

    }

    public static function TotalFreightCharges($data){
      foreach ($data as $key ) {
        $limits =  $key['limits'];
        $sort =  $key['sort'];
        $direction =  $key['direction'];
        $date_from =  $key['date_from'];
        $date_to =  $key['date_to'];
        $billing_company_id =  $key['billing_company_id'];
      }
       $record =  DB::table('booking')
       ->where('booking.deleted', '=', 0)
       ->where(function($query) {
               $query->where('booking.declaration', '=', '1')
                     ->orWhere('booking.loading', '=', '1');
       })
       // ->where('booking.declaration', '=', '1')
       // ->Where('booking.loading', '=', '1')
       // ->whereBetween('booking.created_at',[$date_from, $date_to])
       ->whereDate('loading_date', '>=', $date_from)
       ->whereDate('loading_date', '<=', $date_to)
       // ->when($billing_company_id != "", function ($q)  use ($billing_company_id){
       //   // return $q->whereRaw('concat(booking_no) LIKE ?', $billing_company_id);
       //   // return $q->where('billing_company_id', $billing_company_id);
       //   return $q->whereIn('billing_company_id', $billing_company_id);
       //
       // })
       ->where(function ($q) use ($billing_company_id){
         foreach($billing_company_id as $bc){
           $q->orwhereRaw('concat(booking_no) LIKE ?', '%' . $bc . '%');
         }
       })
       ->sum('freight_charges');

       return $record;

    }

    public static function TotalLoadingFee($data){
      foreach ($data as $key ) {
        $limits =  $key['limits'];
        $sort =  $key['sort'];
        $direction =  $key['direction'];
        $date_from =  $key['date_from'];
        $date_to =  $key['date_to'];
        $billing_company_id =  $key['billing_company_id'];
      }
       $record =  DB::table('booking')
       ->where('booking.deleted', '=', 0)
       ->where(function($query) {
               $query->where('booking.declaration', '=', '1')
                     ->orWhere('booking.loading', '=', '1');
       })
       // ->where('booking.declaration', '=', '1')
       // ->Where('booking.loading', '=', '1')
       // ->whereBetween('booking.created_at',[$date_from, $date_to])
       ->whereDate('loading_date', '>=', $date_from)
       ->whereDate('loading_date', '<=', $date_to)
       // ->when($billing_company_id != "", function ($q)  use ($billing_company_id){
       //   // return $q->whereRaw('concat(booking_no) LIKE ?', $billing_company_id);
       //   // return $q->where('billing_company_id', $billing_company_id);
       //   return $q->whereIn('billing_company_id', $billing_company_id);
       //
       // })
       ->where(function ($q) use ($billing_company_id){
         foreach($billing_company_id as $bc){
           $q->orwhereRaw('concat(booking_no) LIKE ?', '%' . $bc . '%');
         }
       })
       ->sum('container_loading_charges');

       return $record;

    }


}
