<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetBooking extends QueryBase {
    public static function Result($id){
       $record = DB::table('booking')
              ->leftjoin('agent', 'booking.agent_id', '=', 'agent.id')
              ->leftjoin('users', 'booking.owner_id', '=', 'users.id')
              ->select('booking.*','agent.name as agent_name','users.name as created_by')
              ->where('booking.id', $id)
              ->where('deleted', '=', 0)
              ->first();
      if(!empty($record)){

        //get attachments

        //get inchargeperson
        $record->incharges =  DB::table('booking_person')
        ->leftjoin('users', 'booking_person.employee_id', '=', 'users.id')
        ->leftjoin('job', 'booking_person.job_id', '=', 'job.id')
        ->select('booking_person.*','users.name as employee', 'job.name as job')
        ->where('booking_id', '=', $record->id)
        ->where('type_id', '=', 1)
        ->orderBy('created_at')
        ->get();

        $record->inspect =  DB::table('booking_person')
        ->leftjoin('users', 'booking_person.employee_id', '=', 'users.id')
        ->leftjoin('job', 'booking_person.job_id', '=', 'job.id')
        ->select('booking_person.*','users.name as employee', 'job.name as job')
        ->where('booking_person.booking_id', '=', $record->id)
        ->where('booking_person.type_id', '=', 2)
        ->orderBy('booking_person.created_at')
        ->get();

         return new \App\Lib\Dto\Admin\BookingDto($record);
       }
    }

    public static function GetBookingPersons($id){
       $record = DB::table('booking_person')
             ->leftjoin('users', 'booking_person.employee_id', '=', 'users.id')
             ->leftjoin('job', 'booking_person.job_id', '=', 'job.id')
             ->select('booking_person.*','users.name as employee', 'job.name as job')
              ->where('booking_person.booking_id', $id)
              // ->where('booking_person.type_id', $type_id)
              ->get();
        if(!empty($record)){
          return \App\Lib\Dto\Admin\BookingPersonDto::Collection($record);
        }

    }

    public static function GetBookingAttachments($id, $data){
       $record = DB::table('booking_attachment')
              ->where('booking_attachment.booking_id', $id)
              ->whereIn('booking_attachment.type_id', $data)
              ->get();
        if(!empty($record)){
          return \App\Lib\Dto\Admin\BookingAttachmentDto::Collection($record);
        }

    }

    public static function GetBookingFees($id, $type){
       $record = DB::table('booking_fee')
              ->where('booking_fee.booking_id', $id)
              ->where('booking_fee.type', $type)
              ->get();
        if(!empty($record)){
          return \App\Lib\Dto\Admin\BookingFeeDto::Collection($record);
        }

    }

}
