<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetState extends QueryBase {
    public static function Result($id){
       $record = DB::table('state')
              ->where('id', $id)
              ->first();
              if(!empty($record))
       return new \App\Lib\Dto\Admin\StateDto($record);
    }

    //Validated result
    public static function ValidateDelete($id){
      $result = true;
      //Address
       $address =  DB::table('address')->where('state_id', '=', $id)
          ->count();

        if($address > 0){
          $result = false;
        }

      //Booking
       $booking =  DB::table('booking')->where('delivery_state_id', '=', $id)
          ->count();

        if($booking > 0){
          $result = false;
        }
       return $result;
    }
}
