<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetReportYearlyStatement extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $year;
    public $customer_id;
    public $billing_company_id;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $year =  $key['year'];
         $customer_id =  $key['customer_id'];
         $billing_company_id =  $key['billing_company_id'];
      }

       $record =  DB::table('booking')
       ->where('booking.deleted', '=', 0)
       ->where(function($query) {
           $query->where('booking.declaration', '=', '1')
                 ->orWhere('booking.loading', '=', '1');
       })
       ->whereYear('loading_date',  $year)
       ->when($customer_id != "", function ($q)  use ($customer_id){
          return $q->where('customer_id', $customer_id);
       })
       ->where(function ($q) use ($billing_company_id){
         foreach($billing_company_id as $bc){
           $q->orwhereRaw('concat(booking_no) LIKE ?', '%' . $bc . '%');
         }
       })
       ->orderBy($sort, $direction)
       ->get();

       if(!empty($record)){

         foreach($record as $r){
           //other fee
           $r->other_fee_1 = DB::table('booking_fee')
                  ->where('booking_fee.booking_id', $r->id)
                  ->where('booking_fee.type','1')
                  ->sum('amount');

          $r->other_fee_2 = DB::table('booking_fee')
                 ->where('booking_fee.booking_id', $r->id)
                 ->where('booking_fee.type','2')
                 ->sum('amount');
         }

          return \App\Lib\Dto\Admin\YearlyStatementDto::Collection($record);
       }
    }

}
