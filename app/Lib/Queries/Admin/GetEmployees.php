<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetEmployees extends QueryBase {
    public $limits;
    public $sort;
    public $direction;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
      }

       $record =  DB::table('employee')
       ->leftjoin('users', 'employee.user_id', '=', 'users.id')
         ->select('users.*','employee.user_id as user_id', 'employee.id as id', 'employee.agent')
        ->orderBy($sort, $direction)
        ->paginate($limits);

       return \App\Lib\Dto\Admin\EmployeeDto::Collection($record);
    }

    public static function Paging($data){
        foreach ($data as $key ) {
          $limits =  $key['limits'];
          $sort =  $key['sort'];
          $direction =  $key['direction'];
       }

       $record =  DB::table('employee')
       ->paginate($limits)
       ->appends(['limits' => $limits, 'sort' => $sort, 'direction' => $direction]);

       if(!empty($record)){
         return $record;
       }
    }

    public static function All(){
      $record =   DB::table('employee')
       ->leftjoin('users', 'employee.user_id', '=', 'users.id')
         ->select('users.*','employee.user_id as user_id', 'employee.id as id', 'employee.agent')
         ->get();
      if(!empty($record)){
           return \App\Lib\Dto\Admin\EmployeeDto::Collection($record);
      }
    }

    public static function Status($status){
      $record =   DB::table('employee')
       ->leftjoin('users', 'employee.user_id', '=', 'users.id')
       ->where('users.status', '=', $status)
       ->select('users.*','employee.user_id as user_id', 'employee.id as id', 'employee.agent')
       ->get();
      if(!empty($record)){
        return \App\Lib\Dto\Admin\EmployeeDto::Collection($record);
      }
    }

  
}
