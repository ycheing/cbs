<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetReportBalanceSheetSupplier extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $date_from;
    public $date_to;
    public $customsbroker_id;
    public $billing_company_id;
    public $year;

    public static function GetInRecords($data){
      foreach ($data as $key ) {
         $customsbroker_id =  $key['customsbroker_id'];
         $billing_company_id =  $key['billing_company_id'];
         $year =  $key['year'];
      }

      $record =  DB::table('payment_supplier')
      ->leftjoin('payment', 'payment.id', '=', 'payment_supplier.payment_id')
      ->leftjoin('customer', 'customer.id', '=', 'payment.customer_id')
      ->select('payment.remittance_date as date', 'payment.purpose_payment as description','payment_supplier.supplier_amount as in','payment_supplier.supplier_id as customsbroker_id','payment.customer_id as customer_id', 'customer.company_id')
      ->where('payment_supplier.supplier_id', '=', $customsbroker_id)
      ->whereYear('payment.remittance_date', '=', $year)
      ->when($billing_company_id != "", function ($q)  use ($billing_company_id){
         // return $q->whereRaw('concat(LOWER(customer.company_id)) LIKE ?', '%' . $billing_company_id . '%');
         return $q->where('customer.company_id', '=', $billing_company_id);
         //   return $q->whereIn('payment.customer_code', $customer);
      })
      ->orderBy('payment.remittance_date', 'asc')
      ->get();

       if(!empty($record)){
          return \App\Lib\Dto\Admin\BalanceSheetSupplierDto::Collection($record);
       }
    }

    public static function GetOutRecords($data){
      foreach ($data as $key ) {
         $customsbroker_id =  $key['customsbroker_id'];
         $billing_company_id =  $key['billing_company_id'];
         $year =  $key['year'];
      }

      // echo "<pre>";
      // print_r($customer);
      // echo "</pre>";

      $record =  DB::table('booking')
      ->select('booking.custom_goods_inv_date as date', 'booking.container_no',  'booking.booking_no', 'booking.custom_goods_inv_no', 'booking.custom_goods_amount as out', 'booking.custom_borker_id as customsbroker_id', 'booking.customer_id as customer_id')
      ->where('deleted', '=', 0)
      ->where('custom_borker_id', '=', $customsbroker_id)
      ->where('declaration', '=', 1)
      ->where('status', '=', 5)
      ->where('custom_goods_inv_date', '!=', '')
      ->whereYear('custom_goods_inv_date', '=', $year)
      ->when($billing_company_id != "", function ($q)  use ($billing_company_id){
        return $q->where('booking.billing_company_id', '=', $billing_company_id);
         // return $q->whereRaw('concat(LOWER(booking.customer_code)) LIKE ?', '%' . $customer . '%');
         //  return $q->whereIn('booking.customer_code', $customer);
      })
      ->orderBy('custom_goods_inv_date', 'asc')
      ->get();

       if(!empty($record)){
          return \App\Lib\Dto\Admin\BalanceSheetSupplierDto::Collection($record);
       }
    }

    public static function GetTotalInAmount($data){
      foreach ($data as $key ) {
         $customsbroker_id =  $key['customsbroker_id'];
         $billing_company_id =  $key['billing_company_id'];
         $year =  $key['year'];
      }

      $record =  DB::table('payment_supplier')
      ->leftjoin('payment', 'payment.id', '=', 'payment_supplier.payment_id')
      ->leftjoin('customer', 'customer.id', '=', 'payment.customer_id')
      ->where('payment_supplier.supplier_id', '=', $customsbroker_id)
      ->where(function ($q)  use ($year){
        return $q->whereYear('payment.remittance_date', '<', $year)->orwhereNull('payment.remittance_date');
      })
      ->when($billing_company_id != "", function ($q)  use ($billing_company_id){
         return $q->where('customer.company_id', '=', $billing_company_id);
      })
      ->sum('payment_supplier.supplier_amount');

       return $record;
    }

    public static function GetTotalOutAmount($data){
      foreach ($data as $key ) {
         $customsbroker_id =  $key['customsbroker_id'];
         $billing_company_id =  $key['billing_company_id'];
         $year =  $key['year'];
      }

      $record =  DB::table('booking')
      ->select('booking.custom_goods_inv_date as date', 'booking.container_no',  'booking.booking_no', 'booking.custom_goods_inv_no', 'booking.custom_goods_amount as out', 'booking.custom_borker_id as customsbroker_id', 'booking.customer_id as customer_id')
      ->where('deleted', '=', 0)
      ->where('custom_borker_id', '=', $customsbroker_id)
      ->where('declaration', '=', 1)
      ->where('status', '=', 5)
      ->where('custom_goods_inv_date', '!=', '')
      ->where(function ($q)  use ($year){
        return $q->whereYear('custom_goods_inv_date', '<', $year)->orwhereNull('custom_goods_inv_date');
      })
      ->when($billing_company_id != "", function ($q)  use ($billing_company_id){
        return $q->where('booking.billing_company_id', '=', $billing_company_id);
      })
      ->sum('booking.custom_goods_amount');

      return $record;
    }
}
