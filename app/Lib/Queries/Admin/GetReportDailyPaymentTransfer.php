<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetReportDailyPaymentTransfer extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $date_from;
    public $date_to;
    // public $customer_id;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $supplier_id =  $key['supplier_id'];
      }

      $record =  DB::table('payment_transfer')
      ->leftjoin('users', 'payment_transfer.owner_id', '=', 'users.id')
      ->select('payment_transfer.*','users.name as owner')
      ->where('deleted', '=', 0)
      ->whereDate('payment_transfer.payment_date', '>=', $date_from)
      ->whereDate('payment_transfer.payment_date', '<=', $date_to)
      ->when($supplier_id != "", function ($q)  use ($supplier_id){
        return $q->where('supplier_id', $supplier_id);
      })
      // ->whereBetween('payment_transfer.payment_date',[$date_from, $date_to])
      ->orderBy($sort, $direction)
      ->get();

       if(!empty($record)){
          return \App\Lib\Dto\Admin\DailyPaymentTransferDto::Collection($record);
       }

    }

    public static function GetTotalCustomerAmount($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $supplier_id =  $key['supplier_id'];
      }

      $record =  DB::table('payment_transfer')
      ->where('deleted', '=', 0)
      // ->whereBetween('payment_transfer.payment_date',[$date_from, $date_to])
      ->whereDate('payment_transfer.payment_date', '>=', $date_from)
      ->whereDate('payment_transfer.payment_date', '<=', $date_to)
      ->when($supplier_id != "", function ($q)  use ($supplier_id){
        return $q->where('supplier_id', $supplier_id);
      })
       ->sum('customer_amount');

       return $record;

    }

    public static function GetTotalCustomerTotal($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $supplier_id =  $key['supplier_id'];
      }

      $record =  DB::table('payment_transfer')
      ->where('deleted', '=', 0)
      // ->whereBetween('payment_transfer.payment_date',[$date_from, $date_to])
      ->whereDate('payment_transfer.payment_date', '>=', $date_from)
      ->whereDate('payment_transfer.payment_date', '<=', $date_to)
      ->when($supplier_id != "", function ($q)  use ($supplier_id){
        return $q->where('supplier_id', $supplier_id);
      })
       ->sum('customer_total');

       return $record;

    }

    public static function GetTotalSupplierAmount($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $supplier_id =  $key['supplier_id'];
      }

      $record =  DB::table('payment_transfer')
      ->where('deleted', '=', 0)
      // ->whereBetween('payment_transfer.payment_date',[$date_from, $date_to])
      ->whereDate('payment_transfer.payment_date', '>=', $date_from)
      ->whereDate('payment_transfer.payment_date', '<=', $date_to)
      ->when($supplier_id != "", function ($q)  use ($supplier_id){
        return $q->where('supplier_id', $supplier_id);
      })
       ->sum('supplier_amount');

       return $record;

    }

    public static function GetTotalSupplierTotal($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $supplier_id =  $key['supplier_id'];
      }

      $record =  DB::table('payment_transfer')
      ->where('deleted', '=', 0)
      // ->whereBetween('payment_transfer.payment_date',[$date_from, $date_to])
      ->whereDate('payment_transfer.payment_date', '>=', $date_from)
      ->whereDate('payment_transfer.payment_date', '<=', $date_to)
      ->when($supplier_id != "", function ($q)  use ($supplier_id){
        return $q->where('supplier_id', $supplier_id);
      })
       ->sum('supplier_total');

       return $record;

    }

    public static function GetTotalSupplierBankCharges($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $supplier_id =  $key['supplier_id'];
      }

      $record =  DB::table('payment_transfer')
      ->where('deleted', '=', 0)
      // ->whereBetween('payment_transfer.payment_date',[$date_from, $date_to])
      ->whereDate('payment_transfer.payment_date', '>=', $date_from)
      ->whereDate('payment_transfer.payment_date', '<=', $date_to)
      ->when($supplier_id != "", function ($q)  use ($supplier_id){
        return $q->where('supplier_id', $supplier_id);
      })
       ->sum('supplier_bank_charges');

       return $record;

    }

    public static function GetTotalSupplierNetAmount($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $supplier_id =  $key['supplier_id'];
      }

      $record =  DB::table('payment_transfer')
      ->where('deleted', '=', 0)
      // ->whereBetween('payment_transfer.payment_date',[$date_from, $date_to])
      ->whereDate('payment_transfer.payment_date', '>=', $date_from)
      ->whereDate('payment_transfer.payment_date', '<=', $date_to)
      ->when($supplier_id != "", function ($q)  use ($supplier_id){
        return $q->where('supplier_id', $supplier_id);
      })
     ->sum('supplier_net_amount');

     return $record;

    }
}
