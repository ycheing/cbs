<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetCustomsBroker extends QueryBase {
    public static function Result($id){
       $record = DB::table('customsbroker')
              ->where('id', $id)
              ->first();
        if(!empty($record))
       return new \App\Lib\Dto\Admin\CustomsBrokerDto($record);
    }

    //Validated result
    public static function ValidateDelete($id){
      $result = true;
      //Booking
       $booking =  DB::table('booking')->where('custom_borker_id', '=', $id)
          ->count();

      $payment_supplier =  DB::table('payment_supplier')->where('supplier_id', '=', $id)
         ->count();

      if($booking > 0 || $payment_supplier > 0){
        $result = false;
      }

       return $result;
    }
}
