<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetSetting extends QueryBase {
    public static function Result($id){
       $record = DB::table('setting')
              ->where('id', $id)
              ->first();
      if(!empty($record))
       return new \App\Lib\Dto\Admin\SettingDto($record);
    }

    public static function Key($key){
       $record = DB::table('setting')
              ->where('setting_key', $key)
              ->first();
      if(!empty($record))
       return new \App\Lib\Dto\Admin\SettingDto($record);
    }
}
