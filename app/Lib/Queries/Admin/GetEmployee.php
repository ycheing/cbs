<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetEmployee extends QueryBase {
      public static function Result($id) {
        // 先通过 `employee` 表获取 `user_id`
        $record = DB::table('employee')
        ->leftjoin('users', 'employee.user_id', '=', 'users.id')
        ->where('employee.id', $id)
        ->select('users.*','employee.user_id as user_id', 'employee.id as id', 'employee.agent')
        ->first();

        if (!empty($record)) {
          return new \App\Lib\Dto\Admin\EmployeeDto($record);
        }

        return null;
    }

    //Validated result
    public static function ValidateDelete($id){
      $result = true;
      //Booking
      $booking =  DB::table('booking')->where('owner_id', '=', $id)
        ->count();

      if($booking > 0){
        $result = false;
      }

      return $result;
    }
}
