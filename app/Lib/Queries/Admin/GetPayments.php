<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetPayments extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $keyword;
    // public $status_id;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $keyword =  $key['keyword'];
         // $status_id =  $key['status_id'];
      }

       $record =  DB::table('payment')
       ->leftjoin('users', 'payment.owner_id', '=', 'users.id')
       ->leftjoin('payment_status', 'payment.status_id', '=', 'payment_status.id')
       ->select('payment.*','users.name as owner','payment_status.name as status')
       ->where('deleted', '=', 0)
       ->when($keyword != "", function ($q)  use ($keyword){
         return $q->whereRaw('LOWER(payment.customer_code) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(payment.customer_name) LIKE ?', $keyword);
       })
       ->orderBy($sort, $direction)
       ->paginate($limits);

       if(!empty($record)){
         foreach($record as $r){
           $r->suppliers =  DB::table('payment_supplier')
           ->where('payment_id', '=', $r->id)
           ->orderBy('created_at')
           ->get();


           foreach($r->suppliers as $s){
             $s->beneficiaries =  DB::table('payment_beneficiary')
             ->leftjoin('beneficiary', 'payment_beneficiary.beneficiary_id', '=', 'beneficiary.id')
             ->select('payment_beneficiary.*','beneficiary.name as beneficiary')
             ->where('payment_supplier_id', '=', $s->id)
             ->orderBy('created_at')
             ->get();
           }

         }

         return \App\Lib\Dto\Admin\PaymentDto::Collection($record);
       }


    }

    public static function Paging($data){
        foreach ($data as $key ) {
          $limits =  $key['limits'];
          $sort =  $key['sort'];
          $direction =  $key['direction'];
          $keyword =  $key['keyword'];
       }

       $record =  DB::table('payment')
       ->when($keyword != "", function ($q)  use ($keyword){
         return $q->whereRaw('LOWER(payment.customer_code) LIKE ?', $keyword)
           ->orWhereRaw('LOWER(payment.customer_name) LIKE ?', $keyword);
       })
        ->paginate($limits)
        ->appends(['limits' => $limits, 'sort' => $sort, 'direction' => $direction, 'keyword' => $keyword]);

       if(!empty($record)){
         return $record;
       }
    }


}
