<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetReportBillingCompany extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $date_from;
    public $date_to;
    public $customer_id;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $billing_company_id =  $key['billing_company_id'];
      }

       $record =  DB::table('booking')
       ->leftjoin('users', 'booking.agent_id', '=', 'users.id')
       ->select('booking.*','users.name as agent_name')
       ->where('deleted', '=', 0)
       // ->where('declaration', '=', '1')
       ->whereDate('loading_date', '>=', $date_from)
       ->whereDate('loading_date', '<=', $date_to)
       // ->where('billing_company_id', '=', 4)
       // ->whereBetween('declaration_date',[$date_from, $date_to])
       // ->when($billing_company_id != "", function ($q)  use ($billing_company_id){
       //   return $q->whereRaw('concat(booking_no) LIKE ?', $billing_company_id);
       //   // return $q->where('billing_company_id', $billing_company_id);
       // })
       // ->when(!empty($billing_company_id), function ($q)  use ($billing_company_id){
       ->where(function ($q) use ($billing_company_id){
      // ->when(!empty($billing_company_id), function ($q) use($billing_company_id){
         foreach($billing_company_id as $bc){
           // $q->whereRaw('concat(booking_no) LIKE ?', '%CLM%');
           $q->orwhereRaw('concat(booking_no) LIKE ?', '%' . $bc . '%');

           // return $q->orwhereRaw('concat(booking_no) LIKE ?', '%' . $bc . '%');
         }


         // return $q;
         // return $q->whereRaw('concat(booking_no) LIKE ?', $billing_company_id);
         // return $q->whereIn('booking.billing_company_id', $billing_company_id);

       })
       ->orderBy($sort, $direction)
       ->get();
       if(!empty($record)){
            return \App\Lib\Dto\Admin\BillingCompanyDto::Collection($record);
       }
    }

    public static function GetCompanyInvAmount($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $billing_company_id =  $key['billing_company_id'];

      }
       $record =  DB::table('booking')
       ->where('deleted', '=', 0)
       // ->where('declaration', '=', '1')
       ->whereDate('loading_date', '>=', $date_from)
       ->whereDate('loading_date', '<=', $date_to)
       // ->whereBetween('declaration_date',[$date_from, $date_to])
       // ->when($billing_company_id != "", function ($q)  use ($billing_company_id){
       //   return $q->whereIn('billing_company_id', $billing_company_id);
       // })
       ->where(function ($q) use ($billing_company_id){
         foreach($billing_company_id as $bc){
           $q->orwhereRaw('concat(booking_no) LIKE ?', '%' . $bc . '%');
         }
       })
       ->sum('billing_goods_amount');

       return $record;

    }

    public static function GetSupplierInvAmount($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $billing_company_id =  $key['billing_company_id'];

      }
       $record =  DB::table('booking')
       ->where('deleted', '=', 0)
       // ->where('declaration', '=', '1')
       ->whereDate('loading_date', '>=', $date_from)
       ->whereDate('loading_date', '<=', $date_to)
       // ->whereBetween('declaration_date',[$date_from, $date_to])
       // ->when($billing_company_id != "", function ($q)  use ($billing_company_id){
       //   // return $q->whereRaw('concat(booking_no) LIKE ?', $billing_company_id);
       //   // return $q->where('billing_company_id', $billing_company_id);
       //   return $q->whereIn('billing_company_id', $billing_company_id);
       //
       // })
       ->where(function ($q) use ($billing_company_id){
         foreach($billing_company_id as $bc){
           $q->orwhereRaw('concat(booking_no) LIKE ?', '%' . $bc . '%');
         }
       })
       ->sum('custom_goods_amount');

       return $record;

    }

}
