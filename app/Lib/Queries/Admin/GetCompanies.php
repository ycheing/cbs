<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetCompanies extends QueryBase {
    public $limits;
    public $sort;
    public $direction;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
      }

       $record =  DB::table('company')
       ->orderBy($sort, $direction)
       ->paginate($limits);

       return \App\Lib\Dto\Admin\CompanyDto::Collection($record);
    }

    public static function Paging($data){
        foreach ($data as $key ) {
          $limits =  $key['limits'];
          $sort =  $key['sort'];
          $direction =  $key['direction'];
       }
       $record =  DB::table('company')
       ->paginate($limits)
       ->appends(['limits' => $limits, 'sort' => $sort, 'direction' => $direction]);


       if(!empty($record)){
         return $record;
       }
    }

    public static function All(){
      $record =  DB::table('company')->orderBy('name')->get();
      if(!empty($record)){
        return $record;
      }
    }

    public static function Active(){
      $record =  DB::table('company')->orderBy('name')->where('status',1)->get();
      if(!empty($record)){
        return $record;
      }
    }
}
