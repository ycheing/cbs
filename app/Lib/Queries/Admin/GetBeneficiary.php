<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetBeneficiary extends QueryBase {
    public static function Result($id){
       $record = DB::table('beneficiary')
              ->where('id', $id)
              ->first();
              if(!empty($record))
       return new \App\Lib\Dto\Admin\BeneficiaryDto($record);
    }

    //Validated result
    public static function ValidateDelete($id){
      $result = true;
      //payment_transfer
       $payment_transfer =  DB::table('payment_transfer')->where('beneficiary_id', '=', $id)
          ->count();

        if($payment_transfer > 0){
          $result = false;
        }

       return $result;
    }
}
