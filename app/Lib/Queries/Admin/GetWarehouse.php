<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetWarehouse extends QueryBase {
    public static function Result($id){
       $record = DB::table('warehouse')
              ->where('id', $id)
              ->first();
        if(!empty($record))
       return new \App\Lib\Dto\Admin\WarehouseDto($record);
    }

    //Validated result
    public static function ValidateDelete($id){
      $result = true;
      //Cargo
       $cargo =  DB::table('cargo')->where('warehouse_id', '=', $id)
          ->count();

        if($cargo > 0){
          $result = false;
        }

       return $result;
    }
}
