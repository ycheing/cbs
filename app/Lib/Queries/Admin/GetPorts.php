<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetPorts extends QueryBase {
    public $limits;
    public $sort;
    public $direction;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
      }

       $record =  DB::table('port')
       ->leftjoin('country', 'port.country_id', '=', 'country.id')
       ->select('port.*', 'country.name as country')
       ->orderBy($sort, $direction)
       ->paginate($limits);

       return \App\Lib\Dto\Admin\PortDto::Collection($record);
    }

    public static function Paging($data){
        foreach ($data as $key ) {
          $limits =  $key['limits'];
          $sort =  $key['sort'];
          $direction =  $key['direction'];
       }

       $record =  DB::table('port')
       ->paginate($limits)
        ->appends(['limits' => $limits, 'sort' => $sort, 'direction' => $direction]);

       if(!empty($record)){
         return $record;
       }
    }

    public static function All(){
      $record =  DB::table('port')->orderBy('name')->get();
      if(!empty($record)){
        return $record;
      }
    }

    public static function ByCountryID($country_id){
      $record =  DB::table('port')->leftjoin('country', 'port.country_id', '=', 'country.id')
       ->select('port.*', 'country.name as country')->where('port.country_id', '=', $country_id)->orderBy('name')->get();
      if(!empty($record)){
        return $record;
      }
    }

    public static function GetDestinationPorts($country_id){
      $record =  DB::table('port')
      ->leftjoin('country', 'port.country_id', '=', 'country.id')
       ->select('port.*', 'country.name as country')
       ->where('port.country_id', '!=', $country_id)->orderBy('country_id')->get();


      if(!empty($record)){
        return $record;
      }
    }
}
