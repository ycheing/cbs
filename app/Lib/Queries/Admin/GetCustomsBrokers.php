<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetCustomsBrokers extends QueryBase {
    public $limits;
    public $sort;
    public $direction;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
      }

       $record =  DB::table('customsbroker')
       ->orderBy($sort, $direction)
       ->paginate($limits);

       return \App\Lib\Dto\Admin\CustomsBrokerDto::Collection($record);
    }

    public static function Paging($data){
        foreach ($data as $key ) {
          $limits =  $key['limits'];
          $sort =  $key['sort'];
          $direction =  $key['direction'];
       }

       $record =  DB::table('customsbroker')
       ->paginate($limits)
       ->appends(['limits' => $limits, 'sort' => $sort, 'direction' => $direction]);

       if(!empty($record)){
         return $record;
       }
    }

    public static function All(){
      $record =  DB::table('customsbroker')->orderBy('name')->get();
      if(!empty($record)){
        return $record;
      }
    }


    public static function Active(){
      $record =  DB::table('customsbroker')->where('status',1)->orderBy('name')->get();
      if(!empty($record)){
        return $record;
      }
    }
}
