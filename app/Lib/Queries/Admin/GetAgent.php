<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetAgent extends QueryBase {
    public static function Result($id){
       $record = DB::table('agent')
              ->where('id', $id)
              ->first();
              if(!empty($record))
       return new \App\Lib\Dto\Admin\AgentDto($record);
    }

    //Validated result
    public static function ValidateDelete($id){
      $result = true;
      //Booking
       $booking =  DB::table('booking')->where('agent_id', '=', $id)
          ->count();

        if($booking > 0){
          $result = false;
        }

       return $result;
    }
}
