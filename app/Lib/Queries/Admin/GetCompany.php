<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetCompany extends QueryBase {
    public static function Result($id){
       $record = DB::table('company')
              ->where('id', $id)
              ->first();
              if(!empty($record))
       return new \App\Lib\Dto\Admin\CompanyDto($record);
    }

    public static function ResultbyCode($code){
       $record = DB::table('company')
              ->where('code', $code)
              ->first();
              if(!empty($record))
       return new \App\Lib\Dto\Admin\CompanyDto($record);
    }

    //Validated result
    public static function ValidateDelete($id){
      $result = true;
      //Booking
       $booking =  DB::table('booking')->where('billing_company_id', '=', $id)
          ->count();

        if($booking > 0){
          $result = false;
        }

       return $result;
    }
}
