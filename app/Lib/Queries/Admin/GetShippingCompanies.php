<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetShippingCompanies extends QueryBase {
    public $limits;
    public $sort;
    public $direction;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
      }

       $record =  DB::table('shipping_company')
       ->orderBy($sort, $direction)
       ->paginate($limits);

       return \App\Lib\Dto\Admin\ShippingCompanyDto::Collection($record);
    }

    public static function Paging($data){
        foreach ($data as $key ) {
          $limits =  $key['limits'];
          $sort =  $key['sort'];
          $direction =  $key['direction'];
       }

       $record =  DB::table('shipping_company')
       ->paginate($limits)
        ->appends(['limits' => $limits, 'sort' => $sort, 'direction' => $direction]);

       if(!empty($record)){
         return $record;
       }
    }

    public static function All(){
      $record =  DB::table('shipping_company')->orderBy('code')->get();
      if(!empty($record)){
        return $record;
      }
    }


    public static function Active(){
      $record =  DB::table('shipping_company')->where('status', 1)->orderBy('code')->get();
      if(!empty($record)){
        return $record;
      }
    }
}
