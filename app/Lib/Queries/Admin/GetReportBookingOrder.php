<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetReportBookingOrder extends QueryBase {
    public $limits;
    public $sort;
    public $direction;
    public $date_from;
    public $date_to;
    public $customer_id;

    public static function Result($data){
      foreach ($data as $key ) {
         $limits =  $key['limits'];
         $sort =  $key['sort'];
         $direction =  $key['direction'];
         $date_from =  $key['date_from'];
         $date_to =  $key['date_to'];
         $customer_id =  $key['customer_id'];

      }
       $record =  DB::table('booking')
       ->leftjoin('agent', 'booking.agent_id', '=', 'agent.id')
       ->select('booking.*','agent.name as agent_name')
       ->where('deleted', '=', 0)
       ->where('booking', '=', '1')
       // ->whereBetween('booking_date',[$date_from, $date_to])
       ->whereDate('booking_date', '>=', $date_from)
       ->whereDate('booking_date', '<=', $date_to)
       ->when($customer_id != "", function ($q)  use ($customer_id){
         return $q->where('customer_id', $customer_id);
       })
       ->orderBy($sort, $direction)
       ->get();

       if(!empty($record)){
            return \App\Lib\Dto\Admin\BookingOrderDto::Collection($record);
       }

    }


}
