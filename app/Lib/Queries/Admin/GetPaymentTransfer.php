<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetPaymentTransfer extends QueryBase {
    public static function Result($id){
       $record = DB::table('payment_transfer')
              ->where('id', $id)
              ->first();
              if(!empty($record))
       return new \App\Lib\Dto\Admin\PaymentTransferDto($record);
    }
}
