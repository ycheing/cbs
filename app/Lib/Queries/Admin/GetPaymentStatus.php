<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetPaymentStatus extends QueryBase {
    public static function Result($id){
       $record = DB::table('payment_status')
              ->where('payment_status.id', $id)
              ->first();
              if(!empty($record))
       return new \App\Lib\Dto\Admin\PaymentStatusDto($record);
    }

    //Validated result
    public static function ValidateDelete($id){
      $result = true;
      //Booking
      //  $booking =  DB::table('booking')->where('loading_payment_status_id', '=', $id)
      //  ->orWhere('destination_payment_status_id', '=', $id)
      // ->count();
      //
      //   if($booking > 0){
      //     $result = false;
      //   }

       return $result;
    }
}
