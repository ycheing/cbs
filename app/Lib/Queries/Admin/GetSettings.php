<?php

namespace App\Lib\Queries\Admin;

use \App\Lib\Queries\QueryBase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetSettings extends QueryBase {

  public static function Result(){
     $record =  DB::table('setting')->get();
     return \App\Lib\Dto\Admin\SettingDto::Collection($record);
  }

}
