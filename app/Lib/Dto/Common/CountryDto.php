<?php

namespace App\Lib\Dto\Common;

use Illuminate\Support\Facades\Log;

use App\Lib\Dto\DtoBase;

class CountryDto extends DtoBase {
	public $id = '';
	public $name = '';
	public $status_id = '';
	public $status = '';
	public $states = '';

	public function __construct($record) {
			$this->id = $record->id;
			$this->name = $record->name;
			$this->status_id = $record->status;
			if($record->status == 0){
				$this->status = "<span class='badge badge-danger'>Disable</label>";
			}else if($record->status == 1){
				$this->status = "<span class='badge badge-success'>Enable</label>";
			}
			if(isset($record->states)){
				$this->states = $record->states;
			}else{
				$this->states = array();
			}
	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new CountryDto(
				$record
			);
		}
		return $col;
	}

}
