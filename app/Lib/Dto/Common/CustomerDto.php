<?php

namespace App\Lib\Dto\Common;

use Illuminate\Support\Facades\Log;

use App\Lib\Dto\DtoBase;

class CustomerDto extends DtoBase {
	public $id = '';
	public $code = '';
	public $name = '';
	public $status_id = '';
	public $status = '';
	public $id_number = '';
	public $vat_number = '';
	public $website = '';
	public $phone = '';
	public $attn = '';
	public $email = '';
	public $mobile = '';
	public $billing_address_1 = '';
	public $billing_address_2 = '';
	public $billing_city = '';
	public $billing_postcode = '';
	public $billing_country_id = 0;
	public $billing_country = '';
	public $billing_state_id = 0;
	public $billing_state = '';
	public $shipping_address_1 = '';
	public $shipping_address_2 = '';
	public $shipping_city = '';
	public $shipping_postcode = '';
	public $shipping_country_id = 0;
	public $shipping_country = '';
	public $shipping_state_id = 0;
	public $shipping_state = '';

	public function __construct($record) {
				$this->id = $record->id;
				$this->code = $record->code;
				$this->name = $record->name;
				$this->status_id = $record->status;
				if($record->status == 0){
					$this->status = "<span class='badge badge-danger'>Disable</label>";
				}else if($record->status == 1){
					$this->status = "<span class='badge badge-success'>Enable</label>";
				}
				$this->id_number = $record->id_number;
				$this->vat_number = $record->vat_number;
				$this->website = $record->website;
				$this->phone = $record->phone;
				$this->attn = $record->attn;
				$this->email = $record->email;
				$this->mobile = $record->mobile;
				if(!empty($record->billing_address) || isset($record->billing_address)){
					$this->billing_address_1 = $record->billing_address->address_1;
					$this->billing_address_2 = $record->billing_address->address_2;
					$this->billing_city = $record->billing_address->city;
					$this->billing_postcode = $record->billing_address->postcode;
					$this->billing_country_id = $record->billing_address->country_id;
					$this->billing_country = $record->billing_address->country;
					if($record->billing_address->country_id != '' || $record->billing_address->country_id != 0){
						$country = \App\Lib\Queries\Common\GetCountry::Result($record->billing_address->country_id);
						if(!empty($country)){
								$this->billing_states = $country->states;
						}
					}
					$this->billing_state_id = $record->billing_address->state_id;
					$this->billing_state = $record->billing_address->state_id;
				}
				if(!empty($record->shipping_address) || isset($record->shipping_address)){
					$this->shipping_address_1 = $record->shipping_address->address_1;
					$this->shipping_address_2 = $record->shipping_address->address_2;
					$this->shipping_city = $record->shipping_address->city;
					$this->shipping_postcode = $record->shipping_address->postcode;
					$this->shipping_country_id = $record->shipping_address->country_id;
					$this->shipping_country = $record->shipping_address->country;

					if($record->shipping_address->country_id != '' || $record->shipping_address->country_id != 0){
						$country = \App\Lib\Queries\Common\GetCountry::Result($record->shipping_address->country_id);
						if(!empty($country)){
								$this->shipping_states = $country->states;
						}
					}

					$this->billing_states = $record->billing_address->country;
					$this->shipping_state_id = $record->shipping_address->state_id;
					$this->shipping_state = $record->shipping_address->state;
				}
	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new CustomerDto(
				$record, ''
			);
		}
		return $col;
	}

}
