<?php

namespace App\Lib\Dto\Agent;

use Illuminate\Support\Facades\Log;

use App\Lib\Dto\DtoBase;

class StaffCommissionDto extends DtoBase {
	public $id = '';
	public $order_no = '';
	public $booking_no = '';
	public $customer_name = '';
	public $container_no = '';
	public $jobs = '';

	public function __construct($record) {
				$this->id = $record->id;
				if(!empty($record->loading_date)){
					$this->loading_date = date('d/m/Y', strtotime($record->loading_date));
				}
				$this->order_no = $record->order_no;
				$this->booking_no = $record->booking_no;
				$this->customer_name = $record->customer_name;
				$this->container_no = $record->container_no;

				$jobs = '';
				if(!empty($record->employees)){
					$this->employees = $record->employees;
					foreach($record->employees as $ep){
  					$jobs .= $ep->job . " + ";
					}
			   	$jobs = rtrim($jobs, "+ ");
				}
				$this->jobs = $jobs;

	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new StaffCommissionDto(
				$record, ''
			);
		}
		return $col;
	}

}
