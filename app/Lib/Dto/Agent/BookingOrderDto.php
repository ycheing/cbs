<?php

namespace App\Lib\Dto\Agent;

use Illuminate\Support\Facades\Log;

use App\Lib\Dto\DtoBase;

class BookingOrderDto extends DtoBase {
	public $id = '';
	public $booking = '';
	public $loading = '';
	public $declaration = '';
	public $order_no = '';
	public $order_num = '';
	public $booking_no = '';
	public $deleted = '';
	public $status = '';
	public $agent_id = '';
	public $customer_id = '';
	public $customer_code = '';
	public $customer_name = '';
	public $delivery_address_1 = '';
	public $delivery_address_2 = '';
	public $delivery_city = '';
	public $delivery_postcode = '';
	public $delivery_country_id = '';
	public $delivery_country = '';
	public $delivery_state_id = '';
	public $delivery_state = '';
	public $shipping_company_id = '';
	public $loading_port_id = '';
	public $loading_port = '';
	public $destination_port_id = '';
	public $destination_port = '';
	public $closing_date = '';
	public $sailing_date = '';
	public $sailing_day = '';
	public $container_no = '';
	public $seal_no = '';
	public $loading_photo = '';
	public $loading_place = '';
	public $forme_apply = '';
	public $inv_plist = '';
	public $etd_date = '';
	public $eta_date = '';
	public $goods_value = '';
	public $freight_charges = '';
	public $handling_charges = '';
	public $apply_charges = '';
	public $commission = '';
	public $other_fee = '';
	public $remote_fee = '';
	public $charges = '';
	public $container_loading_charges = '';
	public $total_charges = '';
	public $custom_borker_id = '';
	public $custom_goods_inv_date = '';
	public $custom_goods_inv_no = '';
	public $custom_goods_amount = '';
	public $custom_handling_inv_date = '';
	public $custom_handling_inv_no = '';
	public $custom_handling_prices = '';
	public $custom_other_prices = '';
	public $billing_company_id = '';
	public $billing_goods_inv_date = '';
	public $billing_goods_inv_no = '';
	public $billing_goods_amount = '';
	public $billing_handling_inv_date = '';
	public $billing_handling_inv_no = '';
	public $billing_handling_prices = '';
	public $billing_other_prices = '';
	public $sst = '';
	public $rate = '';
	public $owner_id = 0;
	public $created_at = '';
	public $updated_at = '';
	public $created_by = '';
	public $updated_by = '';

	public function __construct($record) {
				$this->id = $record->id;
				$this->owner_id = $record->owner_id;
				$this->booking = $record->booking;
				$this->loading = $record->loading;
				$this->declaration = $record->declaration;
				$this->currency_rate = $record->currency_rate;
				$this->order_no = $record->order_no;
				$this->order_num = $record->order_num;
				$this->booking_no = $record->booking_no;
				$this->deleted = $record->deleted;
				$this->status_id = $record->status;
				if($record->status == 0){
					$this->status = "<span class='badge badge-warning'>遗失</label>";
				}else if($record->status == 1){
					$this->status = "<span class='badge badge-primary'>预订</label>";
				}else if($record->status == 2){
					$this->status = "<span class='badge badge-info'>装柜</label>";
				}else if($record->status == 3){
					$this->status = "<span class='badge badge-info'>装柜完成</label>";
				}else if($record->status == 4){
					$this->status = "<span class='badge badge-dark'>清关</label>";
				}else if($record->status == 5){
					$this->status = "<span class='badge badge-success'>完成</label>";
				}
				$this->agent_id = $record->agent_id;
				$this->agent_name = $record->agent_name;
				$this->customer_id = $record->customer_id;
				$this->customer_code = $record->customer_code;
				// $this->customer_name = $record->customer_name;
				if(!empty($record->customer_id) ||  $record->customer_id != 0){
					$customer = \App\Lib\Queries\Admin\GetCustomer::Result($record->customer_id);
					$this->customer_name = $customer->nickname;
				}else{
					$this->customer_name = '';
				}
				$this->delivery_address_1 = $record->delivery_address_1;
				$this->delivery_address_2 = $record->delivery_address_2;
				$this->delivery_city = $record->delivery_city;
				$this->delivery_postcode = $record->delivery_postcode;
				$this->delivery_country_id = $record->delivery_country_id;
				$this->delivery_country = $record->delivery_country;
				$this->delivery_state_id = $record->delivery_state_id;
				$this->delivery_state = $record->delivery_state;
				$this->shipping_company_id = $record->shipping_company_id;
				if(!empty($record->shipping_company_id) ||  $record->shipping_company_id != 0){
					$shipping_company = \App\Lib\Queries\Admin\GetShippingCompany::Result($record->shipping_company_id);
					$this->shipping_company = $shipping_company->code;
				}else{
					$this->shipping_company = '';
				}
				$this->loading_port_id = $record->loading_port_id;
				if(!empty($record->loading_port_id) ||  $record->loading_port_id != 0){
					$loading_port = \App\Lib\Queries\Admin\GetPort::Result($record->loading_port_id);
					$this->loading_port = $loading_port->name;
				}else{
					$this->loading_port = '';
				}
				$this->destination_port_id = $record->destination_port_id;
				if(!empty($record->destination_port_id) ||  $record->destination_port_id != 0){
					$destination_port = \App\Lib\Queries\Admin\GetPort::Result($record->destination_port_id);
					$this->destination_port = $destination_port->name;
				}else{
					$this->destination_port = '';
				}
				if(!empty($record->closing_date)){
					$this->closing_date = date("d/m/Y", strtotime($record->closing_date));
				}else{
					$this->closing_date = $record->closing_date;
				}
				if(!empty($record->sailing_date)){
					$this->sailing_date = date("d/m/Y", strtotime($record->sailing_date));
				}else{
					$this->sailing_date = $record->sailing_date;
				}
				$this->sailing_day = $record->sailing_day;
				$this->container_no = $record->container_no;
				$this->seal_no = $record->seal_no;
				$this->loading_photo = $record->loading_photo;
				$this->loading_place = $record->loading_place;
				$this->sst = $record->sst;
				$this->forme_apply = $record->forme_apply;
				$this->inv_plist = $record->inv_plist;
				if(!empty($record->etd_date)){
					$this->etd_date = date("d/m/Y", strtotime($record->etd_date));
				}else{
					$this->etd_date = $record->etd_date;
				}
				if(!empty($record->eta_date)){
					$this->eta_date = date("d/m/Y", strtotime($record->eta_date));
				}else{
					$this->eta_date = $record->eta_date;
				}
				if(!empty($record->goods_value)){
					$this->goods_value = $record->goods_value;
				}else{
					$this->goods_value =  number_format(0,2,'.','');
				}
				if(!empty($record->freight_charges)){
					$this->freight_charges = $record->freight_charges;
				}else{
					$this->freight_charges =  number_format(0,2,'.','');
				}
				if(!empty($record->handling_charges)){
					$this->handling_charges = $record->handling_charges;
				}else{
					$this->handling_charges = number_format(0,2,'.','');
				}
				if(!empty($record->apply_charges)){
					$this->apply_charges = $record->apply_charges;
				}else{
					$this->apply_charges = number_format(0,2,'.','');
				}
				if(!empty($record->commission)){
					$this->commission = $record->commission;
				}else{
					$this->commission =  number_format(0,2,'.','');
				}
				if(!empty($record->other_fee)){
					$this->other_fee = $record->other_fee;
				}else{
					$this->other_fee =  number_format(0,2,'.','');
				}
				if(!empty($record->remote_fee)){
					$this->remote_fee = $record->remote_fee;
				}else{
					$this->remote_fee =  number_format(0,2,'.','');
				}
				if(!empty($record->charges)){
					$this->charges = $record->charges;
				}else{
					$this->charges =  number_format(0,2,'.','');
				}
				if(!empty($record->container_loading_charges)){
					$this->container_loading_charges = $record->container_loading_charges;
				}else{
					$this->container_loading_charges = number_format(0,2,'.','');
				}
				if(!empty($record->total_charges)){
					$this->total_charges = $record->total_charges;
				}else{
					$this->total_charges = number_format(0,2,'.','');
				}
				// $this->goods_value = $record->goods_value;
				// $this->freight_charges = $record->freight_charges;
				// $this->handling_charges = $record->handling_charges;
				// $this->apply_charges = $record->apply_charges;
				// $this->commission = $record->commission;
				// $this->other_fee = $record->other_fee;
				// $this->remote_fee = $record->remote_fee;
				// $this->charges = $record->charges;
				// $this->container_loading_charges = $record->container_loading_charges;
				// $this->total_charges = $record->total_charges;
				$this->custom_borker_id = $record->custom_borker_id;
				if(!empty($record->custom_borker_id) ||  $record->custom_borker_id != 0){
					$custom_borker = \App\Lib\Queries\Admin\GetCustomsBroker::Result($record->custom_borker_id);
					$this->custom_borker = $custom_borker->name;
				}else{
					$this->custom_borker = '';
				}
				// $this->custom_goods_inv_date = $record->custom_goods_inv_date;
				if(!empty($record->custom_goods_inv_date)){
					$this->custom_goods_inv_date = date("d/m/Y", strtotime($record->custom_goods_inv_date));
				}else{
					$this->custom_goods_inv_date = $record->custom_goods_inv_date;
				}
				$this->custom_goods_inv_no = $record->custom_goods_inv_no;
				$this->custom_goods_amount = $record->custom_goods_amount;
				// $this->custom_handling_inv_date = $record->custom_handling_inv_date;
				if(!empty($record->custom_handling_inv_date)){
					$this->custom_handling_inv_date = date("d/m/Y", strtotime($record->custom_handling_inv_date));
				}else{
					$this->custom_handling_inv_date = $record->custom_handling_inv_date;
				}
				$this->custom_handling_inv_no = $record->custom_handling_inv_no;
				$this->custom_handling_prices = $record->custom_handling_prices;
				$this->custom_other_prices = $record->custom_other_prices;
				$this->billing_company_id = $record->billing_company_id;

				// $this->billing_goods_inv_date = $record->billing_goods_inv_date;
				if(!empty($record->billing_goods_inv_date)){
					$this->billing_goods_inv_date = date("d/m/Y", strtotime($record->billing_goods_inv_date));
				}else{
					$this->billing_goods_inv_date = $record->billing_goods_inv_date;
				}
				$this->billing_goods_inv_no = $record->billing_goods_inv_no;
				$this->billing_goods_amount = $record->billing_goods_amount;
				// $this->billing_handling_inv_date = $record->billing_handling_inv_date;
				if(!empty($record->billing_handling_inv_date)){
					$this->billing_handling_inv_date = date("d/m/Y", strtotime($record->billing_handling_inv_date));
				}else{
					$this->billing_handling_inv_date = $record->billing_handling_inv_date;
				}
				$this->billing_handling_inv_no = $record->billing_handling_inv_no;
				$this->billing_handling_prices = $record->billing_handling_prices;
				$this->billing_other_prices = $record->billing_other_prices;
				if(!empty($record->created_at)){
					$this->created_at = date("d/m/Y", strtotime($record->created_at));
				}else{
					$this->created_at = $record->created_at;
				}
				if(!empty($record->updated_at)){
					$this->updated_at = date("d/m/Y", strtotime($record->updated_at));
				}else{
					$this->updated_at = $record->updated_at;
				}
				$this->created_by = $record->created_by;
				$this->updated_by = $record->updated_by;
				if(!empty($record->booking_date)){
					$this->booking_date = date("d/m/Y", strtotime($record->booking_date));
				}else{
					$this->booking_date = $record->booking_date;
				}
				if(!empty($record->loading_date)){
					$this->loading_date = date("d/m/Y", strtotime($record->loading_date));
				}else{
					$this->loading_date = $record->loading_date;
				}
				if(!empty($record->declaration_date)){
					$this->declaration_date = date("d/m/Y", strtotime($record->declaration_date));
				}else{
					$this->declaration_date = $record->declaration_date;
				}

				if(!empty($booking->incharge)){
					$this->incharge = $booking->incharge;
				}else{
					$this->incharge = array();
				}
	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new BookingOrderDto(
				$record, ''
			);
		}
		return $col;
	}

}
