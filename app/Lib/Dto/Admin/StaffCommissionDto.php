<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;

use App\Lib\Dto\DtoBase;

class StaffCommissionDto extends DtoBase {
	public $id = '';
	public $booking = '';
	public $loading = '';
	public $declaration = '';
	public $order_no = '';
	public $order_num = '';
	public $booking_no = '';
	public $deleted = '';
	public $status = '';
	public $agent_id = '';
	public $customer_id = '';
	public $customer_code = '';
	public $customer_name = '';
	public $container_no = '';
	public $seal_no = '';
	public $created_at = '';
	public $updated_at = '';
	public $created_by = '';
	public $updated_by = '';

	public function __construct($record) {
				$this->id = $record->id;
				$this->loading = $record->loading;
				// $this->loading_date = $record->loading_date;
				if(!empty($record->loading_date)){
					$this->loading_date = date('d/m/Y', strtotime($record->loading_date));
				}
				$this->currency_rate = $record->currency_rate;
				$this->order_no = $record->order_no;
				$this->order_num = $record->order_num;
				$this->booking_no = $record->booking_no;
				$this->deleted = $record->deleted;
				$this->status_id = $record->status;
				if($record->status == 0){
					$this->status = "<span class='badge badge-warning'>遗失</label>";
				}else if($record->status == 1){
					$this->status = "<span class='badge badge-primary'>预订</label>";
				}else if($record->status == 2){
					$this->status = "<span class='badge badge-info'>装柜</label>";
				}else if($record->status == 3){
					$this->status = "<span class='badge badge-info'>装柜完成</label>";
				}else if($record->status == 4){
					$this->status = "<span class='badge badge-dark'>清关</label>";
				}else if($record->status == 5){
					$this->status = "<span class='badge badge-success'>完成</label>";
				}
				$this->agent_id = $record->agent_id;
				$this->agent_name = $record->agent_name;
				$this->customer_id = $record->customer_id;
				$this->customer_code = $record->customer_code;
				// $this->customer_name = $record->customer_name;
				if(!empty($record->customer_id) ||  $record->customer_id != 0){
					$customer = \App\Lib\Queries\Admin\GetCustomer::Result($record->customer_id);
					$this->customer_name = $customer->nickname;
				}else{
					$this->customer_name = '';
				}
				$this->container_no = $record->container_no;
				$this->seal_no = $record->seal_no;
				if(!empty($record->employees)){
					$this->employees = $record->employees;
				}


	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new StaffCommissionDto(
				$record, ''
			);
		}
		return $col;
	}

}
