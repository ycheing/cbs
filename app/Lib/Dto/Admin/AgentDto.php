<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;

use App\Lib\Dto\DtoBase;

class AgentDto extends DtoBase {
	public $id = '';
	public $name = '';
	public $email = '';
	public $status_id = '';
	public $status = '';

	public function __construct($record) {
				$this->id = $record->id;
				$this->name = $record->name;
				$this->email = $record->email;
				$this->status_id = $record->status;
				if($record->status == 0){
					$this->status = "<span class='badge badge-danger'>冻结</label>";
				}else if($record->status == 1){
					$this->status = "<span class='badge badge-success'>启用</label>";
				}
	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new AgentDto(
				$record, ''
			);
		}
		return $col;
	}

}
