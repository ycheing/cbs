<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;

use App\Lib\Dto\DtoBase;

class BookingFeeDto extends DtoBase {
	public $booking_fee_id = '';
	public $booking_id = '';
	public $description = '';
	public $amount = '';

	public function __construct($record) {
				$this->booking_fee_id = $record->booking_fee_id;
				$this->booking_id = $record->booking_id;
				$this->description = $record->description;
				$this->amount = number_format($record->amount,2);
				if(!empty($record->created_at)){
					$this->created_at = date("Y-m-d", strtotime($record->created_at));
				}else{
					$this->created_at = $record->created_at;
				}
				if(!empty($record->updated_at)){
					$this->updated_at = date("Y-m-d", strtotime($record->updated_at));
				}else{
					$this->updated_at = $record->updated_at;
				}
	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new BookingFeeDto(
				$record, ''
			);
		}
		return $col;
	}

}
