<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Lib\Dto\DtoBase;

class BookingAttachmentDto extends DtoBase {
	public $booking_attachment_id = '';
	public $booking_id = '';
	public $type_id = '';
	public $attachment = '';
	public $path = '';
	public $created_at = '';
	public $updated_at = '';

	public function __construct($record) {
				$this->booking_attachment_id = $record->booking_attachment_id;
				$this->booking_id = $record->booking_id;
				$this->type_id = $record->type_id;
				if($record->type_id == 1){
					$this->description = '封柜图';
				}elseif($record->type_id == 2){
					$this->description = '装车单';
				}elseif($record->type_id == 3){
					$this->description = '装柜付款单';
				}elseif($record->type_id == 4){
					$this->description = '清单和装箱单';
				}elseif($record->type_id == 5){
					$this->description = 'Form E 申请单';
				}elseif($record->type_id == 6){
					$this->description = 'SST (K1 Form)';
				}
				$this->name = str_replace($record->attachment, 'upload/', '');
				$this->attachment = $record->attachment;
				if(!empty($record->attachment)){
					$this->path =  url(Storage::url('app/' . $record->attachment));
				}
				if(!empty($record->created_at)){
					$this->created_at = date("Y-m-d", strtotime($record->created_at));
				}else{
					$this->created_at = $record->created_at;
				}
				if(!empty($record->updated_at)){
					$this->updated_at = date("Y-m-d", strtotime($record->updated_at));
				}else{
					$this->updated_at = $record->updated_at;
				}
	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new BookingAttachmentDto(
				$record, ''
			);
		}
		return $col;
	}

}
