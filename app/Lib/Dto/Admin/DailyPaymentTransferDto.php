<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Lib\Dto\DtoBase;

class DailyPaymentTransferDto extends DtoBase {
	public $id = '';
	public $deleted = '';
	public $status = '';
	public $status_id = '';
	public $owner_id = '';
	public $date = '';
	public $customer_id = '';
	public $customer_name = '';

	public $created_at = '';
	public $updated_at = '';
	public $created_by = '';
	public $updated_by = '';
	public $slip = '';

	public function __construct($record) {
				$this->id = $record->id;
				$this->deleted = $record->deleted;
				if(!empty($record->payment_date)){
					$this->payment_date = date("d/m/Y", strtotime($record->payment_date));
				}else{
					$this->payment_date = $record->payment_date;
				}
				$this->status_id = $record->status;
				if($record->status == 0){
					$this->status = "<span class='badge badge-danger'>冻结</label>";
				}else if($record->status == 1){
					$this->status = "<span class='badge badge-success'>启用</label>";
				}
				$this->owner_id = $record->owner_id;
				$this->customer_id = $record->customer_id;
				$this->customer_code = $record->customer_code;
				$this->customer_name = $record->customer_name;
				$this->customer_amount = $record->customer_amount;
				$this->customer_exchange_rate = $record->customer_exchange_rate;
				$this->customer_total = $record->customer_total;
				$this->customer_attachment = $record->customer_attachment;
				if(!empty($record->customer_attachment)){
					$this->customer_attachment_path =  url(Storage::url('app/' . $record->customer_attachment));
				}else{
					$this->customer_attachment_path =  '';
				}

				$this->purpose_payment = $record->purpose_payment;
				$this->deposits = $record->deposits;
				$this->beneficiary_id = $record->beneficiary_id;
				if(!empty($record->beneficiary_id) ||  $record->beneficiary_id != 0){
					$beneficiary = \App\Lib\Queries\Admin\GetBeneficiary::Result($record->beneficiary_id);
					$this->beneficiary = $beneficiary->name;
				}else{
					$this->beneficiary = '';
				}
				$this->supplier_id = $record->supplier_id;
				if(!empty($record->supplier_id) ||  $record->supplier_id != 0){
					$supplier = \App\Lib\Queries\Admin\GetSupplier::Result($record->supplier_id);
					$this->supplier_code = $supplier->code;
					$this->supplier_name = $supplier->name;
				}else{
					$this->supplier_code = '';
					$this->supplier_name = '';
				}
				$this->supplier_amount = $record->supplier_amount;
				$this->supplier_exchange_rate = $record->supplier_exchange_rate;
				$this->supplier_total = $record->supplier_total;
				$this->supplier_bank_charges = $record->supplier_bank_charges;
				$this->supplier_net_amount = $record->supplier_net_amount;
				$this->supplier_attachment = $record->supplier_attachment;
				if(!empty($record->supplier_attachment)){
					$this->supplier_attachment_path =  url(Storage::url('app/' . $record->supplier_attachment));
					$this->slip = 'OK';
				}else{
					$this->supplier_attachment_path =  '';
					$this->slip = '';
				}
				if(!empty($record->date_received)){
					$this->date_received = date("d/m/Y", strtotime($record->date_received));
				}else{
					$this->date_received = $record->date_received;
				}

				if(!empty($record->created_at)){
					$this->created_at = date("d/m/Y", strtotime($record->created_at));
				}else{
					$this->created_at = $record->created_at;
				}
				if(!empty($record->updated_at)){
					$this->updated_at = date("d/m/Y", strtotime($record->updated_at));
				}else{
					$this->updated_at = $record->updated_at;
				}
				$this->created_by = $record->created_by;
				$this->updated_by = $record->updated_by;
	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new DailyPaymentTransferDto(
				$record, ''
			);
		}
		return $col;
	}

}
