<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Lib\Dto\DtoBase;

class PaymentDto extends DtoBase {
	public $id = '';
	public $deleted = '';
	public $status = '';
	public $status_id = '';
	public $owner_id = '';
	public $date = '';
	public $customer_id = '';
	public $customer_name = '';
	public $created_at = '';
	public $updated_at = '';
	public $created_by = '';
	public $updated_by = '';

	public function __construct($record) {
				$this->id = $record->id;
				$this->deleted = $record->deleted;
				if(!empty($record->payment_date)){
					$this->payment_date = date("d/m/Y", strtotime($record->payment_date));
				}else{
					$this->payment_date = $record->payment_date;
				}
				if(!empty($record->remittance_date)){
					$this->remittance_date = date("d/m/Y", strtotime($record->remittance_date));
				}else{
					$this->remittance_date = $record->remittance_date;
				}

				$this->status_id = $record->status_id;
				$this->status =  $record->status;
				$this->owner_id = $record->owner_id;
				$this->customer_id = $record->customer_id;
				$this->customer_code = $record->customer_code;
				$this->customer_name = $record->customer_name;
				$this->customer_amount = $record->customer_amount;
				$this->customer_exchange_rate = $record->customer_exchange_rate;
				$this->customer_total = $record->customer_total;
				// $this->customer_attachment = $record->customer_attachment;
				// if(!empty($record->customer_attachment)){
				// 	$this->customer_attachment_path =  url(Storage::url('app/' . $record->customer_attachment));
				// }else{
				// 	$this->customer_attachment_path =  '';
				// }

				$this->purpose_payment = $record->purpose_payment;
				// $this->deposits = $record->deposits;
				if(isset($record->suppliers)){

					foreach($record->suppliers as $s) {
						if(!empty($s->supplier_id) ||  $s->supplier_id != 0){
							$supplier = \App\Lib\Queries\Admin\GetCustomsBroker::Result($s->supplier_id);
							$s->supplier_code = $supplier->code;
							$s->supplier_name = $supplier->name;
							foreach($s->beneficiaries as $b){
								$received_date = date("d/m/Y", strtotime($b->received_date));
								if($received_date == '01/01/1970'){
									$b->received_date = '';
								}else{
									$b->received_date = $received_date;
								}
							}
						}else{
							$s->supplier_code = '';
							$s->supplier_name = '';
						}
						// $received_date = date("d/m/Y", strtotime($s->received_date));
						// if($received_date == '01/01/1970'){
						// 	$s->received_date = '';
						// }else{
						// 	$s->received_date = $received_date;
						// }
						// $s->received_date = date("d/m/Y", strtotime($s->received_date));
					}
					$this->suppliers = $record->suppliers;

				}else{
					$this->suppliers = array();
				}


				if(!empty($record->created_at)){
					$this->created_at = date("d/m/Y", strtotime($record->created_at));
				}else{
					$this->created_at = $record->created_at;
				}
				if(!empty($record->updated_at)){
					$this->updated_at = date("d/m/Y", strtotime($record->updated_at));
				}else{
					$this->updated_at = $record->updated_at;
				}
				$this->created_by = $record->created_by;
				$this->updated_by = $record->updated_by;



	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new PaymentDto(
				$record, ''
			);
		}
		return $col;
	}

}
