<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;

use App\Lib\Dto\DtoBase;

class BookingPersonDto extends DtoBase {
	public $booking_person_id = '';
	public $booking_id = '';
	public $type_id = '';
	public $job_id = '';
	public $job = '';
	public $employee_id = '';
	public $employee = '';

	public function __construct($record) {
				$this->booking_person_id = $record->booking_person_id;
				$this->booking_id = $record->booking_id;
				$this->type_id = $record->type_id;
				$this->job_id = $record->job_id;
				$this->job = $record->job;
				$this->employee_id = $record->employee_id;
				$this->employee = $record->employee;
				if(!empty($record->created_at)){
					$this->created_at = date("Y-m-d", strtotime($record->created_at));
				}else{
					$this->created_at = $record->created_at;
				}
				if(!empty($record->updated_at)){
					$this->updated_at = date("Y-m-d", strtotime($record->updated_at));
				}else{
					$this->updated_at = $record->updated_at;
				}
	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new BookingPersonDto(
				$record, ''
			);
		}
		return $col;
	}

}
