<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;

use App\Lib\Dto\DtoBase;

class CustomsBrokerDto extends DtoBase {
	public $id = '';
	public $code = '';
	public $name = '';
	public $status_id = '';
	public $status = '';
	public $initial_balance = '0';

	public function __construct($record) {
				$this->id = $record->id;
				$this->code = $record->code;
				$this->name = $record->name;

				// $balance = array();
				if(!empty($record->initial_balance)){
					$this->balance =  json_decode($record->initial_balance);
				}else{
					$this->balance =  array();
				}

				$this->status_id = $record->status;

				if($record->status == 0){
					$this->status = "<span class='badge badge-danger'>冻结</label>";
				}else if($record->status == 1){
					$this->status = "<span class='badge badge-success'>启用</label>";
				}
				// $this->initial_balance = $record->initial_balance;
	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new CustomsBrokerDto(
				$record, ''
			);
		}
		return $col;
	}

}
