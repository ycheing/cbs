<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;

use App\Lib\Dto\DtoBase;

class SettingDto extends DtoBase {
	public $id = '';
	public $setting_key = '';
	public $setting_value = '';

	public function __construct($record) {
				$this->id = $record->id;
				$this->setting_key = $record->setting_key;
				$this->setting_value = $record->setting_value;
	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new SettingDto(
				$record, ''
			);
		}
		return $col;
	}

}
