<?php

namespace App\Lib\Dto\Admin;

use App\Lib\Dto\DtoBase;

class CustomerDto extends DtoBase
{
    public $id = '';
    public $code = '';
    public $name = '';
    public $nickname = '';
    public $status_id = '';
    public $status = '';
    public $id_number = '';
    public $vat_number = '';
    public $website = '';
    public $phone = '';
    public $phone2 = '';
    public $initial_balance = '0';
    public $company_id = '';
    public $attn = '';
    public $email = '';
    public $mobile = '';
    public $billing_company_name = '';
    public $billing_address_1 = '';
    public $billing_address_2 = '';
    public $billing_city = '';
    public $billing_postcode = '';
    public $billing_country_id = 0;
    public $billing_country = '';
    public $billing_state_id = 0;
    public $billing_state = '';
    public $shipping_name = '';
    public $shipping_phone_1 = '';
    public $shipping_phone_2 = '';
    public $shipping_company_name = '';
    public $shipping_address_1 = '';
    public $shipping_address_2 = '';
    public $shipping_city = '';
    public $shipping_postcode = '';
    public $shipping_country_id = 0;
    public $shipping_country = '';
    public $shipping_state_id = 0;
    public $shipping_state = '';
    public $created_by = '';
    public $updated_by = '';

    public function __construct($record)
    {
        $this->id = $record->id;
        $this->code = $record->code;
        $this->name = $record->name;
        $this->nickname = $record->nickname;
        $this->status_id = $record->status;
        if ($record->status == 0) {
            $this->status = "<span class='badge badge-danger'>冻结</label>";
        } elseif ($record->status == 1) {
            $this->status = "<span class='badge badge-success'>启用</label>";
        }
        $this->id_number = $record->id_number;
        $this->vat_number = $record->vat_number;
        $this->website = $record->website;
        $this->phone = $record->phone;
        $this->phone2 = $record->phone2;
        $this->attn = $record->attn;
        $this->email = $record->email;
        $this->mobile = $record->mobile;
        $this->initial_balance = $record->initial_balance;
        $this->company_id = $record->company_id;
        if (!empty($record->billing_address) || isset($record->billing_address)) {
            $this->billing_company_name = $record->billing_address->company_name;
            $this->billing_address_1 = $record->billing_address->address_1;
            $this->billing_address_2 = $record->billing_address->address_2;
            $this->billing_city = $record->billing_address->city;
            $this->billing_postcode = $record->billing_address->postcode;
            $this->billing_country_id = $record->billing_address->country_id;
            $this->billing_country = $record->billing_address->country;
            $this->billing_state_id = $record->billing_address->state_id;
            $this->billing_state = $record->billing_address->state_id;
        }
        $this->shipping_name = $record->shipping_name;
        $this->shipping_phone_1 = $record->shipping_phone_1;
        $this->shipping_phone_2 = $record->shipping_phone_2;
        if (!empty($record->shipping_address) || isset($record->shipping_address)) {
            $this->shipping_company_name = $record->shipping_address->company_name;
            $this->shipping_address_1 = $record->shipping_address->address_1;
            $this->shipping_address_2 = $record->shipping_address->address_2;
            $this->shipping_city = $record->shipping_address->city;
            $this->shipping_postcode = $record->shipping_address->postcode;
            $this->shipping_country_id = $record->shipping_address->country_id;
            $this->shipping_country = $record->shipping_address->country;
            $this->shipping_state_id = $record->shipping_address->state_id;
            $this->shipping_state = $record->shipping_address->state;
        }
        $this->created_by = $record->created_by;
        $this->updated_by = $record->updated_by;
    }

    public static function Collection($records)
    {
        $col = [];
        foreach ($records as $record) {
            $col[] = new CustomerDto(
                $record, ''
            );
        }

        return $col;
    }
}
