<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;

use App\Lib\Dto\DtoBase;

class StateDto extends DtoBase {
	public $id = '';
	public $country_id = '';
	public $name = '';
	public $status_id = '';
	public $status = '';

	public function __construct($record) {
				$this->id = $record->id;
				$this->country_id = $record->country_id;
				$this->name = $record->name;
				$this->status_id = $record->status;
				if($record->status == 0){
					$this->status = "<span class='badge badge-danger'>冻结</label>";
				}else if($record->status == 1){
					$this->status = "<span class='badge badge-success'>启用</label>";
				}
	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new StateDto(
				$record, ''
			);
		}
		return $col;
	}

}
