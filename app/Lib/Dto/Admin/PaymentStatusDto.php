<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;

use App\Lib\Dto\DtoBase;

class PaymentStatusDto extends DtoBase {
	public $id = '';
	public $name = '';
	public $status_id = '';
	public $status = '';
	public $sort_order = '0';

	public function __construct($record) {
				$this->id = $record->id;
				$this->name = $record->name;
				$this->status_id = $record->status;
				if($record->status == 0){
					$this->status = "<span class='badge badge-danger'>冻结</label>";
				}else if($record->status == 1){
					$this->status = "<span class='badge badge-success'>启用</label>";
				}
				$this->sort_order = $record->sort_order;
	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new PaymentStatusDto(
				$record, ''
			);
		}
		return $col;
	}

}
