<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;

use App\Lib\Dto\DtoBase;

class BalanceSheetSupplierDto extends DtoBase {
	public $date = '';
	public $description = '';
	public $in = '';
	public $out = '';

	public function __construct($record) {
		// echo "<pre>";
		// print_r($record);
		// echo "</pre>";
			$this->date = $record->date;
			$description = "";

			if(!empty($record->customer_id) ||  $record->customer_id != 0){
				$customer = \App\Lib\Queries\Admin\GetCustomer::Result($record->customer_id);
				$customer_code = $customer->code;
			}else{
				$customer_code = '';
			}

			if(!empty($record->customsbroker_id) ||  $record->customsbroker_id != 0){
				$customsbroker = \App\Lib\Queries\Admin\GetCustomsBroker::Result($record->customsbroker_id);
				$customsbroker_code = $customsbroker->code;
			}else{
				$customsbroker_code = '';
			}

			if(isset($record->container_no)){
				$container_no = $record->container_no;
			}else{
				$container_no = '';
			}

			if(isset($record->booking_no)){
				// $booking_no = str_replace($customer_code . '-', '', $record->booking_no);
				$booking_no = $record->booking_no;
			}else{
				$booking_no = '';
			}

			if(isset($record->custom_goods_inv_no)){
				$custom_goods_inv_no = $record->custom_goods_inv_no;
			}else{
				$custom_goods_inv_no = '';
			}

			if(isset($record->booking_no)){
				$description = '(' . $container_no . ') (' . $booking_no . ') (' .$custom_goods_inv_no . ')';
			}else{
				$description =  $customer_code . ' ' . $record->description;
			}
			// if(isset($record->description)){
			// 	$description =  $record->description;
			// }else{
			// 	$description = '(' . $container_no . ') (' . $booking_no . ') (' .$custom_goods_inv_no . ')';
			// }

			$this->description = $description;
			if(isset($record->in)){
					$this->in = $record->in;
			}else{
					$this->in = 0;
			}
			if(isset($record->out)){
					$this->out = $record->out;
			}else{
					$this->out = 0;
			}
			$this->balance = 0;
	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new BalanceSheetSupplierDto(
				$record, ''
			);
		}
		return $col;
	}

}
