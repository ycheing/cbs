<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Lib\Dto\DtoBase;

class CargoRecordDto extends DtoBase {
	public $id = '';
	public $deleted = '';
	public $status = '';
	public $status_id = '';
	public $owner_id = '';
	public $owner = '';
	public $date = '';
	public $customer_id = '';
	public $customer_code = '';
	public $customer_name = '';
	public $created_at = '';
	public $updated_at = '';
	public $created_by = '';
	public $updated_by = '';

	public function __construct($record) {
				$this->id = $record->id;
				$this->deleted = $record->deleted;
				if(!empty($record->date)){
					$this->date = date("d/m/Y", strtotime($record->date));
				}else{
					$this->date = $record->date;
				}
				$this->status_id = $record->status_id;
				$this->owner_id = $record->owner_id;
				$this->owner = $record->owner;
				$this->customer_id = $record->customer_id;
				$this->customer = $record->customer;
				$this->customs_broker_id = $record->customs_broker_id;
				$this->customs_broker = $record->customs_broker;
				$this->warehouse_id = $record->warehouse_id;
				$this->warehouse = $record->warehouse;
				$this->tracking_no = $record->tracking_no;
				$this->supplier_inv_no = $record->supplier_inv_no;
				$this->cbm = $record->cbm;
				$this->supplier_inv_amount = $record->supplier_inv_amount;
				$this->pack = $record->pack;
				$this->supplier_goods_inv = $record->supplier_goods_inv;
				$this->supplier_goods_amount = $record->supplier_goods_amount;
				$this->customer_goods_inv = $record->customer_goods_inv;
				$this->customer_goods_amount = $record->customer_goods_amount;
				$this->transport_inv = $record->transport_inv;
				$this->transport_inv_amount = $record->transport_inv_amount;

				if(!empty($record->created_at)){
					$this->created_at = date("d/m/Y", strtotime($record->created_at));
				}else{
					$this->created_at = $record->created_at;
				}
				if(!empty($record->updated_at)){
					$this->updated_at = date("d/m/Y", strtotime($record->updated_at));
				}else{
					$this->updated_at = $record->updated_at;
				}
				if(!empty($record->deleted_at)){
					$this->deleted_at = date("d/m/Y", strtotime($record->deleted_at));
				}else{
					$this->deleted_at = $record->deleted_at;
				}
				$this->created_by = $record->created_by;
				$this->updated_by = $record->updated_by;
				$this->deleted_by = $record->deleted_by;

	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new CargoDto(
				$record, ''
			);
		}
		return $col;
	}

}
