<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Lib\Dto\DtoBase;

class PaymentSupplierDto extends DtoBase {
	public $id = '';
	public $created_at = '';
	public $updated_at = '';
	public $created_by = '';
	public $updated_by = '';

	public function __construct($record) {
				$this->id = $record->id;
				$this->payment_id = $record->payment_id;
				$this->supplier_id = $record->supplier_id;
				if(!empty($record->supplier_id) ||  $record->supplier_id != 0){
					$supplier = \App\Lib\Queries\Admin\GetCustomsBroker::Result($record->supplier_id);
					$this->supplier_code = $supplier->code;
					$this->supplier_name = $supplier->name;
				}else{
					$this->supplier_code = '';
					$this->supplier_name = '';
				}
				$this->supplier_amount = $record->supplier_amount;
				$this->supplier_exchange_rate = $record->supplier_exchange_rate;
				$this->supplier_total = $record->supplier_total;
				// $this->supplier_bank_charges = $record->supplier_bank_charges;
				// $this->supplier_net_amount = $record->supplier_net_amount;
				// $this->supplier_attachment = $record->supplier_attachment;
				$balance = 0;
				$b_total = 0;
				if(isset($record->beneficiaries)){

					foreach($record->beneficiaries as $beneficiary){
						$b_total += $beneficiary->amount;
						if(!empty($beneficiary->attachment)){
							$beneficiary->attachment_path =  url(Storage::url('app/' . $beneficiary->attachment));
						}else{
							$beneficiary->attachment_path =  '';
						}
						if(!empty($beneficiary->received_date)){
							$beneficiary->received_date = date("d/m/Y", strtotime($beneficiary->received_date));
							if($beneficiary->received_date == '01/01/1970'){
									$beneficiary->received_date = '';
							}
						}else{
							$beneficiary->received_date = '';
						}
						// $total_amt = $beneficiary->amount + $beneficiary->bank_charges;
						// $beneficiary->total_amount = $total_amt;
					}
					$this->beneficiaries = $record->beneficiaries;
					$this->total_beneficiary = count($record->beneficiaries);
				}else{
					$this->beneficiaries = '';
					$this->total_beneficiary = 0;
				}
				$balance = $record->supplier_total - $b_total;
				$this->balance = number_format($balance, '2', '.', '');

				if(!empty($record->created_at)){
					$this->created_at = date("d/m/Y", strtotime($record->created_at));
				}else{
					$this->created_at = $record->created_at;
				}
				if(!empty($record->updated_at)){
					$this->updated_at = date("d/m/Y", strtotime($record->updated_at));
				}else{
					$this->updated_at = $record->updated_at;
				}
				$this->created_by = $record->created_by;
				$this->updated_by = $record->updated_by;
	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new PaymentSupplierDto(
				$record, ''
			);
		}
		return $col;
	}

}
