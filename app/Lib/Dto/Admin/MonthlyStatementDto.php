<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;

use App\Lib\Dto\DtoBase;

class MonthlyStatementDto extends DtoBase {
	public $id = '';
	public $order_no = '';
	public $booking_no = '';
	public $customer_name = '';
	public $container_no = '';
	public $seal_no = '';
	public $goods_value = 0;
	public $freight_charges = 0;
	public $document_fee = 0;
	public $net_freight_charges = 0;
	public $handling_charges = 0;
	public $apply_charges =0;
	public $commission = 0;
	public $other_fee = 0;
	public $remote_fee = 0;
	public $charges = 0;
	public $container_loading_charges = 0;
	public $total_charges = 0;
	public $custom_goods_amount = 0;
	public $custom_handling_prices = 0;
	public $custom_other_prices = 0;
	public $billing_goods_amount = 0;
	public $billing_handling_prices = 0;
	public $billing_other_prices = 0;
	public $created_at = '';
	public $updated_at = '';
	public $created_by = '';
	public $updated_by = '';

	public function __construct($record) {
				$this->id = $record->id;
				if(!empty($record->loading_date)){
					$this->loading_date = date("d/m/Y", strtotime($record->loading_date));
				}else{
					$this->loading_date = $record->loading_date;
				}
				if(!empty($record->declaration_date)){
					$this->declaration_date = date("d/m/Y", strtotime($record->declaration_date));
				}else{
					$this->declaration_date = $record->declaration_date;
				}
				$this->order_no = $record->order_no;
				$this->booking_no = $record->booking_no;
				// $this->customer_name = $record->customer_name;
				if(!empty($record->customer_id) ||  $record->customer_id != 0){
					$customer = \App\Lib\Queries\Admin\GetCustomer::Result($record->customer_id);
					$this->customer_name = $customer->nickname;
				}else{
					$this->customer_name = '';
				}
				$this->container_no = $record->container_no;
				$this->seal_no = $record->seal_no;

				// if(!empty($record->handling_charges)) {
				// 	$this->handling_charges = $record->handling_charges;
				// }else{
				// 	$this->handling_charges = 0;
				// }

				//Calculate handling fee = handling fee + charges + other fee
				if(!empty($record->handling_charges)) {
					$handling_charges = $record->handling_charges;
				}else{
					$handling_charges = 0;
				}

				if(!empty($record->apply_charges)) {
					$apply_charges = $record->apply_charges;
				}else{
					$apply_charges = 0;
				}

				if(!empty($record->other_fee_1)) {
					$other_fee_1 = $record->other_fee_1;
				}else{
					$other_fee_1 = 0;
				}

				if(!empty($record->offsite_fee)) {
					$offsite_fee = $record->offsite_fee;
				}else{
					$offsite_fee = 0;
				}

				$this->handling_charges = $handling_charges + $apply_charges + $other_fee_1 + $offsite_fee;

				if(!empty($record->commission)) {
					$this->commission = $record->commission;
				}else{
					$this->commission =  0;
				}

				if(!empty($record->freight_charges)){
					$this->freight_charges  = $record->freight_charges;
				}else{
					$this->freight_charges  = 0;
				}

				if(!empty($record->document_fee)){
					$this->document_fee  = $record->document_fee;
				}else{
					$this->document_fee  = 0;
				}

				$this->net_freight_charges = 	$this->freight_charges - $this->document_fee;

				if(!empty($record->container_loading_charges)) {
					$container_loading_charges = $record->container_loading_charges;
				}else{
					$container_loading_charges = 0;
				}

				if(!empty($record->remote_fee)) {
					$remote_fee = $record->remote_fee;
				}else{
					$remote_fee = 0;
				}

				if(!empty($record->charges)) {
					$charges = $record->charges;
				}else{
					$charges = 0;
				}

				if(!empty($record->other_fee_2)) {
					$other_fee_2 = $record->other_fee_2;
				}else{
					$other_fee_2 = 0;
				}

				if(!empty($record->entry_fee)) {
					$entry_fee = $record->entry_fee;
				}else{
					$entry_fee = 0;
				}

				$this->container_loading_charges = $container_loading_charges + $remote_fee + $charges + $other_fee_2 + $entry_fee;

				if(!empty($record->custom_handling_prices)){
					$this->custom_handling_prices = $record->custom_handling_prices;
				}else{
					$this->custom_handling_prices = 0 ;
				}

				if(!empty($record->custom_other_prices)){
					$this->custom_other_prices = $record->custom_other_prices;
				}else{
					$this->custom_other_prices = 0 ;
				}

				if(!empty($record->billing_handling_prices)) {
					$this->billing_handling_prices = $record->billing_handling_prices ;
				}else{
					$this->billing_handling_prices = 0 ;
				}

				if(!empty($record->billing_other_prices)) {
					$this->billing_other_prices = $record->billing_other_prices ;
				}else{
					$this->billing_other_prices = 0 ;
				}

				$this->total_charges = $this->handling_charges+$this->commission-$this->container_loading_charges-$this->freight_charges;

				$this->total = ($this->billing_handling_prices + $this->billing_other_prices) - ($this->custom_handling_prices + $this->custom_other_prices);

				if(!empty($record->created_at)){
					$this->created_at = date("d/m/Y", strtotime($record->created_at));
				}else{
					$this->created_at = $record->created_at;
				}

				if(!empty($record->updated_at)){
					$this->updated_at = date("d/m/Y", strtotime($record->updated_at));
				}else{
					$this->updated_at = $record->updated_at;
				}
				$this->created_by = $record->created_by;
				$this->updated_by = $record->updated_by;

	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new MonthlyStatementDto(
				$record, ''
			);
		}
		return $col;
	}

}
