<?php

namespace App\Lib\Dto\Admin;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Lib\Dto\DtoBase;

class PaymentBeneficiaryDto extends DtoBase {
	public $id = '';
	public $created_at = '';
	public $updated_at = '';
	public $created_by = '';
	public $updated_by = '';

	public function __construct($record) {
				$this->id = $record->id;
				$this->payment_id = $record->payment_id;
				$this->payment_supplier_id = $record->payment_supplier_id;
				$this->beneficiary_id = $record->beneficiary_id;
				$this->amount = $record->amount;
				$this->bank_charges = $record->bank_charges;
				$this->net_amount = $record->net_amount;
				$this->attachment = $record->attachment;
				if(!empty($record->attachment)){
					$this->attachment_path =  url(Storage::url('app/' . $record->attachment));
				}else{
					$this->attachment_path =  '';
				}

				$this->received_date = date("d/m/Y", strtotime($record->received_date));
				if($this->received_date == '01/01/1970'){
						$this->received_date = '';
				}

				if(!empty($record->created_at)){
					$this->created_at = date("d/m/Y", strtotime($record->created_at));
				}else{
					$this->created_at = $record->created_at;
				}
	}


	public static function Collection($records) {

		$col = [];
		foreach ($records as $record) {
			$col[] = new PaymentBeneficiaryDto(
				$record, ''
			);
		}
		return $col;
	}

}
