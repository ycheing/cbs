<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/', 'Auth\LoginController@login');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', 'AccountController@SignOut')->name('logout');
Route::get('/password', 'AccountController@ChangePassword');
Route::post('/password', 'AccountController@ChangePasswordPost');
Route::get('/profile', 'AccountController@Profile');
Route::post('/profile', 'AccountController@ProfilePost');

Route::get('/admin', 'AdminController@index');
Route::get('/admin/dashboard', 'AdminController@index');
Route::get('/{locale}/admin/dashboard', 'AdminController@index');

Route::get('/agent', 'AgentController@index');
Route::get('/agent/dashboard', 'AgentController@index');

/* setting */
Route::get('/setting', 'AdminController@Setting');
Route::post('/setting', 'AdminController@SettingPost');

/* user */
Route::get('/users', 'AdminController@Users');
Route::get('/user/add', 'AdminController@AddUser');
Route::post('/user/add', 'AdminController@AddUserPost');
Route::get('/user/edit/{id}', 'AdminController@EditUser');
Route::post('/user/edit/', 'AdminController@EditUserPost');
Route::post('/user/delete', 'AdminController@DeleteUserPost');
Route::post('/user/reset', 'AdminController@ResetUserPasswordPost');

/* employee */
Route::get('/employees', 'AdminController@Employees');
Route::get('/employee/add', 'AdminController@AddEmployee');
Route::post('/employee/add', 'AdminController@AddEmployeePost');
Route::get('/employee/edit/{id}', 'AdminController@EditEmployee');
Route::post('/employee/edit/', 'AdminController@EditEmployeePost');
Route::post('/employee/delete', 'AdminController@DeleteEmployeePost');

/* agent */
Route::get('/agents', 'AdminController@Agents');
Route::get('/agent/add', 'AdminController@AddAgent');
Route::post('/agent/add', 'AdminController@AddAgentPost');
Route::get('/agent/edit/{id}', 'AdminController@EditAgent');
Route::post('/agent/edit/', 'AdminController@EditAgentPost');
Route::post('/agent/delete', 'AdminController@DeleteAgentPost');

/* country */
Route::get('/countries', 'AdminController@Countries');
Route::get('/country/add', 'AdminController@AddCountry');
Route::post('/country/add', 'AdminController@AddCountryPost');
Route::get('/country/edit/{id}', 'AdminController@EditCountry');
Route::post('/country/edit/', 'AdminController@EditCountryPost');
Route::post('/country/delete', 'AdminController@DeleteCountryPost');
Route::post('/country/reset', 'AdminController@ResetCountryPasswordPost');

/* state */
Route::get('/states', 'AdminController@States');
Route::get('/state/add', 'AdminController@AddState');
Route::post('/state/add', 'AdminController@AddStatePost');
Route::get('/state/edit/{id}', 'AdminController@EditState');
Route::post('/state/edit/', 'AdminController@EditStatePost');
Route::post('/state/delete', 'AdminController@DeleteStatePost');

/* port */
Route::get('/ports', 'AdminController@Ports');
Route::get('/port/add', 'AdminController@AddPort');
Route::post('/port/add', 'AdminController@AddPortPost');
Route::get('/port/edit/{id}', 'AdminController@EditPort');
Route::post('/port/edit/', 'AdminController@EditPortPost');
Route::post('/port/delete', 'AdminController@DeletePortPost');

/* Company */
Route::get('/companies', 'AdminController@Companies');
Route::get('/company/add', 'AdminController@AddCompany');
Route::post('/company/add', 'AdminController@AddCompanyPost');
Route::get('/company/edit/{id}', 'AdminController@EditCompany');
Route::post('/company/edit/', 'AdminController@EditCompanyPost');
Route::post('/company/delete', 'AdminController@DeleteCompanyPost');

/* shipping company */
Route::get('/shipping_companies', 'AdminController@ShippingCompanies');
Route::get('/shipping_company/add', 'AdminController@AddShippingCompany');
Route::post('/shipping_company/add', 'AdminController@AddShippingCompanyPost');
Route::get('/shipping_company/edit/{id}', 'AdminController@EditShippingCompany');
Route::post('/shipping_company/edit/', 'AdminController@EditShippingCompanyPost');
Route::post('/shipping_company/delete', 'AdminController@DeleteShippingCompanyPost');

/* customsbroker */
Route::get('/customsbrokers', 'AdminController@CustomsBrokers');
Route::get('/customsbroker/add', 'AdminController@AddCustomsBroker');
Route::post('/customsbroker/add', 'AdminController@AddCustomsBrokerPost');
Route::get('/customsbroker/edit/{id}', 'AdminController@EditCustomsBroker');
Route::post('/customsbroker/edit/', 'AdminController@EditCustomsBrokerPost');
Route::post('/customsbroker/delete', 'AdminController@DeleteCustomsBrokerPost');

/* job */
Route::get('/jobs', 'AdminController@Jobs');
Route::get('/job/add', 'AdminController@AddJob');
Route::post('/job/add', 'AdminController@AddJobPost');
Route::get('/job/edit/{id}', 'AdminController@EditJob');
Route::post('/job/edit/', 'AdminController@EditJobPost');
Route::post('/job/delete', 'AdminController@DeleteJobPost');

/* warehouse */
Route::get('/warehouses', 'AdminController@Warehouses');
Route::get('/warehouse/add', 'AdminController@AddWarehouse');
Route::post('/warehouse/add', 'AdminController@AddWarehousePost');
Route::get('/warehouse/edit/{id}', 'AdminController@EditWarehouse');
Route::post('/warehouse/edit/', 'AdminController@EditWarehousePost');
Route::post('/warehouse/delete', 'AdminController@DeleteWarehousePost');

/* customer */
Route::get('/customers', 'AdminController@Customers');
Route::get('/customer/add', 'AdminController@AddCustomer');
Route::post('/customer/add', 'AdminController@AddCustomerPost');
Route::get('/customer/edit/{id}', 'AdminController@EditCustomer');
Route::post('/customer/edit/', 'AdminController@EditCustomerPost');
Route::post('/customer/delete', 'AdminController@DeleteCustomerPost');
Route::get('/customer/copy/{id}', 'AdminController@CopyCustomer');

/* booking */
Route::get('/bookings', 'AdminController@Bookings');
Route::get('/booking/add', 'AdminController@AddBooking');
Route::post('/booking/add', 'AdminController@AddBookingPost');
Route::get('/booking/add/booking', 'AdminController@AddBookingBooking');
Route::post('/booking/add/booking', 'AdminController@AddBookingBookingPost');
Route::get('/booking/add/declaration', 'AdminController@AddBookingDeclaration');
Route::post('/booking/add/declaration', 'AdminController@AddBookingDeclarationPost');
Route::post('/booking/add/loading', 'AdminController@AddBookingLoadingPost');
Route::post('/booking/add/person', 'AdminController@AddBookingPersonPost');
Route::post('/booking/add/fee', 'AdminController@AddBookingFeePost');
Route::get('/booking/edit/{id}', 'AdminController@Booking');
Route::get('/booking/edit/booking/{id}', 'AdminController@EditBooking');
Route::post('/booking/edit/booking', 'AdminController@EditBookingPost');
Route::get('/booking/edit/loading/{id}', 'AdminController@EditLoading');
Route::post('/booking/edit/loading', 'AdminController@EditLoadingPost');
Route::get('/booking/edit/declaration/{id}', 'AdminController@EditDeclaration');
Route::post('/booking/edit/declaration', 'AdminController@EditDeclarationPost');
Route::post('/booking/update/declaration', 'AdminController@UpdateDeclarationPost');
Route::post('/booking/update/status', 'AdminController@UpdateStatusPost');

Route::post('/booking/delete', 'AdminController@DeleteBookingPost');
Route::post('/booking/person/delete', 'AdminController@DeleteBookingPersonPost');
Route::post('/booking/attachment/delete', 'AdminController@DeleteBookingAttachmentPost');
Route::post('/booking/fee/delete', 'AdminController@DeleteBookingFeePost');

Route::get('/booking/getpersons/{id}', 'AdminController@GetPersons');
Route::get('/booking/getinchargepersons/{id}', 'AdminController@GetInchargePersons');
Route::get('/booking/getinspectpersons/{id}', 'AdminController@GetInspectPersons');
Route::get('/booking/getphotoseals/{id}', 'AdminController@GetPhotoSeals');
Route::get('/booking/getloadingattachments/{id}', 'AdminController@GetLoadingAttachments');
Route::get('/booking/getdeclarationattachments/{id}', 'AdminController@GetDeclarationAttachments');
Route::get('/booking/getattachments/{id}/{type}', 'AdminController@GetTypeAttachments');
Route::get('/booking/getfees/{id}/{type}', 'AdminController@GetFees');

Route::post('/booking/upload', 'AdminController@UplaodFilePost');

/* supplier */
Route::get('/suppliers', 'AdminController@Suppliers');
Route::get('/supplier/add', 'AdminController@AddSupplier');
Route::post('/supplier/add', 'AdminController@AddSupplierPost');
Route::get('/supplier/edit/{id}', 'AdminController@EditSupplier');
Route::post('/supplier/edit/', 'AdminController@EditSupplierPost');
Route::post('/supplier/delete', 'AdminController@DeleteSupplierPost');

/* beneficiary */
Route::get('/beneficiaries', 'AdminController@Beneficiaries');
Route::get('/beneficiary/add', 'AdminController@AddBeneficiary');
Route::post('/beneficiary/add', 'AdminController@AddBeneficiaryPost');
Route::get('/beneficiary/edit/{id}', 'AdminController@EditBeneficiary');
Route::post('/beneficiary/edit/', 'AdminController@EditBeneficiaryPost');
Route::post('/beneficiary/delete', 'AdminController@DeleteBeneficiaryPost');

/* payment transfer */
Route::get('/payment_transfers', 'AdminController@PaymentTransfers');
Route::get('/payment_transfer/add', 'AdminController@AddPaymentTransfer');
Route::post('/payment_transfer/add', 'AdminController@AddPaymentTransferPost');
Route::get('/payment_transfer/edit/{id}', 'AdminController@EditPaymentTransfer');
Route::post('/payment_transfer/edit/', 'AdminController@EditPaymentTransferPost');
Route::post('/payment_transfer/delete', 'AdminController@DeletePaymentTransferPost');
Route::post('/payment_transfer/remove/attachment', 'AdminController@DeletePaymentTransferAttachmentPost');

/* payment_status */
Route::get('/payment_statuses', 'AdminController@PaymentStatuses');
Route::get('/payment_status/add', 'AdminController@AddPaymentStatus');
Route::post('/payment_status/add', 'AdminController@AddPaymentStatusPost');
Route::get('/payment_status/edit/{id}', 'AdminController@EditPaymentStatus');
Route::post('/payment_status/edit/', 'AdminController@EditPaymentStatusPost');
Route::post('/payment_status/delete', 'AdminController@DeletePaymentStatusPost');

/* payment */
Route::get('/payments', 'AdminController@Payments');
Route::get('/payment/add', 'AdminController@AddPayment');
Route::post('/payment/add', 'AdminController@AddPaymentPost');
Route::get('/payment/edit/{id}', 'AdminController@EditPayment');
Route::post('/payment/edit/', 'AdminController@EditPaymentPost');
Route::post('/payment/delete', 'AdminController@DeletePaymentPost');
Route::post('/payment/remove/attachment', 'AdminController@DeletePaymentAttachmentPost');

Route::get('/payment/suppliers/{id}', 'AdminController@GetPaymentSuppliers');
Route::get('/payment/supplier/{id}', 'AdminController@GetPaymentSupplier');
Route::post('/payment/supplier/add', 'AdminController@AddPaymentSupplierPost');
Route::post('/payment/supplier/edit/', 'AdminController@EditPaymentSupplierPost');
Route::post('/payment/supplier/delete', 'AdminController@DeletePaymentSupplierPost');
Route::post('/payment/supplier/attachment/remove', 'AdminController@DeletePaymentSupplierAttachmentPost');

Route::get('/payment/beneficiary/{id}', 'AdminController@GetPaymentBeneficiary');
Route::post('/payment/beneficiary/add', 'AdminController@AddPaymentBeneficiaryPost');
Route::post('/payment/beneficiary/edit/', 'AdminController@EditPaymentBeneficiaryPost');
Route::post('/payment/beneficiary/delete', 'AdminController@DeletePaymentBeneficiaryPost');
Route::post('/payment/beneficiary/attachment/remove', 'AdminController@DeletePaymentBeneficiaryAttachmentPost');

/* cargo */
Route::get('/cargos', 'AdminController@Cargos');
Route::get('/cargo/add', 'AdminController@AddCargo');
Route::post('/cargo/add', 'AdminController@AddCargoPost');
Route::get('/cargo/edit/{id}', 'AdminController@EditCargo');
Route::post('/cargo/edit/', 'AdminController@EditCargoPost');
Route::post('/cargo/delete', 'AdminController@DeleteCargoPost');
Route::get('/cargo/copy/{id}', 'AdminController@CopyCargo');

/* report */
Route::get('/report/overview', 'AdminController@ReportOverview');
Route::get('/report/staff_commission', 'AdminController@ReportStaffCommission');
Route::get('/report/booking_order', 'AdminController@ReportBookingOrder');
Route::get('/report/container_loading', 'AdminController@ReportContainerLoading');
Route::get('/report/billing_company', 'AdminController@ReportBillingCompany');
Route::get('/report/monthly_statement', 'AdminController@ReportMonthlyStatement');
Route::get('/report/yearly_statement', 'AdminController@ReportYearlyStatement');
Route::get('/report/daily_payment_transfer', 'AdminController@ReportDailyPaymentTransfer');
Route::get('/report/daily_payment', 'AdminController@ReportDailyPayment');
Route::get('/report/cargo_record', 'AdminController@ReportCargoRecord');
Route::get('/report/balance_sheet_customer', 'AdminController@ReportBalanceSheetCustomer');
Route::get('/report/balance_sheet_supplier', 'AdminController@ReportBalanceSheetSupplier');

/* export */
Route::get('/report/export/staff_commission', 'AdminController@ReportExportStaffCommission');
Route::get('/report/export/booking_order', 'AdminController@ReportExportBookingOrder');
Route::get('/report/export/container_loading', 'AdminController@ReportExportContainerLoading');
Route::get('/report/export/billing_company', 'AdminController@ReportExportBillingCompany');
Route::get('/report/export/monthly_statement', 'AdminController@ReportExportMonthlyStatement');
Route::get('/report/export/yearly_statement', 'AdminController@ReportExportYearlyStatement');
Route::get('/report/export/daily_payment_transfer', 'AdminController@ReportExportDailyPaymentTransfer');
Route::get('/report/export/daily_payment', 'AdminController@ReportExportDailyPayment');
Route::get('/report/export/cargo_record', 'AdminController@ReportExportCargoRecord');
Route::get('/report/export/balance_sheet_customer', 'AdminController@ReportExportBalanceSheetCustomer');
Route::get('/report/export/balance_sheet_supplier', 'AdminController@ReportExportBalanceSheetSupplier');

/* print */
Route::get('/report/print/staff_commission', 'AdminController@ReportPrintStaffCommission');
Route::get('/report/print/booking_order', 'AdminController@ReportPrintBookingOrder');
Route::get('/report/print/container_loading', 'AdminController@ReportPrintContainerLoading');
Route::get('/report/print/billing_company', 'AdminController@ReportPrintBillingCompany');
Route::get('/report/print/monthly_statement', 'AdminController@ReportPrintMonthlyStatement');
Route::get('/report/print/yearly_statement', 'AdminController@ReportPrintYearlyStatement');
Route::get('/report/print/daily_payment_transfer', 'AdminController@ReportPrintDailyPaymentTransfer');
Route::get('/report/print/daily_payment', 'AdminController@ReportPrintDailyPayment');
Route::get('/report/print/cargo_record', 'AdminController@ReportPrintCargoRecord');
Route::get('/report/print/balance_sheet_customer', 'AdminController@ReportPrintBalanceSheetCustomer');
Route::get('/report/print/balance_sheet_supplier', 'AdminController@ReportPrintBalanceSheetSupplier');

// Ajax
Route::get('getcountry', 'CommonController@GetCountry');
Route::get('getcustomer', 'CommonController@GetCustomer');
Route::post('upload', 'CommonController@UplaodFile');
Route::get('/upload/{file}/{name}', 'CommonController@DownloadFile');

/* ======================= agent ====================== */

/* customer */
Route::get('/agent/customers', 'AgentController@Customers');
Route::get('/agent/customer/add', 'AgentController@AddCustomer');
Route::post('/agent/customer/add', 'AgentController@AddCustomerPost');
Route::get('/agent/customer/edit/{id}', 'AgentController@EditCustomer');
Route::post('/agent/customer/edit/', 'AgentController@EditCustomerPost');
Route::post('/agent/customer/delete', 'AgentController@DeleteCustomerPost');
Route::get('/agent/customer/copy/{id}', 'AgentController@CopyCustomer');

/* booking */
Route::get('/agent/bookings', 'AgentController@Bookings');
Route::get('/agent/booking/type', 'AgentController@BookingType');
Route::post('/agent/booking/type', 'AgentController@BookingTypePost');
Route::get('/agent/booking/add', 'AgentController@AddBooking');
Route::post('/agent/booking/add', 'AgentController@AddBookingPost');
Route::get('/agent/booking/edit/{id}', 'AgentController@Booking');
Route::get('/agent/booking/edit/booking/{id}', 'AgentController@EditBooking');
Route::post('/agent/booking/edit', 'AgentController@EditBookingPost');
Route::post('/agent/booking/add/loading', 'AgentController@AddBookingLoadingPost');
Route::get('/agent/booking/loading/{id}', 'AgentController@EditLoading');
Route::post('/agent/booking/loading', 'AgentController@EditLoadingPost');
Route::post('/agent/booking/add/person', 'AgentController@AddBookingPersonPost');
Route::post('/agent/booking/delete', 'AgentController@DeleteBookingPost');
Route::post('/agent/booking/person/delete', 'AgentController@DeleteBookingPersonPost');
Route::post('/agent/booking/attachment/delete', 'AgentController@DeleteBookingAttachmentPost');
Route::post('/agent/booking/complete/loading', 'AgentController@CompleteBookingLoadingPost');

Route::get('/agent/booking/declaration', 'AgentController@AddDeclaration');
Route::post('/agent/booking/declaration', 'AgentController@AddDeclarationPost');
Route::get('/agent/booking/edit/declaration/{id}', 'AgentController@EditDeclaration');
Route::post('/agent/booking/edit/declaration', 'AgentController@EditDeclarationPost');
Route::post('/agent/booking/update/declaration', 'AgentController@UpdateDeclarationPost');

Route::get('/agent/booking/getpersons/{id}', 'AgentController@GetPersons');
Route::get('/agent/booking/getinchargepersons/{id}', 'AgentController@GetInchargePersons');
Route::get('/agent/booking/getinspectpersons/{id}', 'AgentController@GetInspectPersons');
Route::get('/agent/booking/getphotoseals/{id}', 'AgentController@GetPhotoSeals');
Route::get('/agent/booking/getdeclarationattachments/{id}', 'AgentController@GetDeclarationAttachments');
Route::post('/agent/booking/upload', 'AgentController@UplaodFilePost');
Route::post('/agent/booking/update/status', 'AgentController@UpdateStatusPost');
Route::get('/agent/booking/getattachments/{id}/{type}', 'AgentController@GetTypeAttachments');
Route::get('/agent/booking/getfees/{id}/{type}', 'AgentController@GetFees');
Route::post('/agent/booking/fee/delete', 'AgentController@DeleteBookingFeePost');
Route::post('/agent/booking/add/fee', 'AgentController@AddBookingFeePost');

/* cargo */
Route::get('/agent/cargos', 'AgentController@Cargos');
Route::get('/agent/cargo/add', 'AgentController@AddCargo');
Route::post('/agent/cargo/add', 'AgentController@AddCargoPost');
Route::get('/agent/cargo/edit/{id}', 'AgentController@EditCargo');
Route::post('/agent/cargo/edit/', 'AgentController@EditCargoPost');
Route::post('/agent/cargo/delete', 'AgentController@DeleteCargoPost');

/* report */
Route::get('/agent/report/overview', 'AgentController@ReportOverview');
Route::get('/agent/report/full_staff_commission', 'AgentController@ReportFullStaffCommission');
Route::get('/agent/report/staff_commission', 'AgentController@ReportStaffCommission');
Route::get('/agent/report/booking_order', 'AgentController@ReportBookingOrder');
Route::get('/agent/report/container_loading', 'AgentController@ReportContainerLoading');
Route::get('/agent/report/cargo_record', 'AgentController@ReportCargoRecord');

Route::get('/agent/report/export/full_staff_commission', 'AgentController@ReportExportFullStaffCommission');
Route::get('/agent/report/export/staff_commission', 'AgentController@ReportExportStaffCommission');
Route::get('/agent/report/export/booking_order', 'AgentController@ReportExportBookingOrder');
Route::get('/agent/report/export/container_loading', 'AgentController@ReportExportContainerLoading');
Route::get('/agent/report/export/cargo_record', 'AgentController@ReportExportCargoRecord');

Route::get('/agent/report/print/full_staff_commission', 'AgentController@ReportPrintFullStaffCommission');
Route::get('/agent/report/print/staff_commission', 'AgentController@ReportPrintStaffCommission');
Route::get('/agent/report/print/booking_order', 'AgentController@ReportPrintBookingOrder');
Route::get('/agent/report/print/container_loading', 'AgentController@ReportPrintContainerLoading');
Route::get('/agent/report/print/cargo_record', 'AgentController@ReportPrintCargoRecord');
