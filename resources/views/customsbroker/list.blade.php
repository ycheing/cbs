@extends('layouts.app')
@section('title', '清关行清单')
@section('content')
<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center media-info">
            <h2 class="card-title">清关行清单</h2>
          </div>
          <div class="wrapper ml-auto action-bar">
            <a href="{{url('/customsbroker/add')}}" class="btn btn-primary btn-block"><i class="fa fa-plus "></i>添加清关行</a>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th style="width:10%"></th>
                <th style="width:10%" class="text-center">@sortablelink('status', '状态')</th>
                <th>@sortablelink('code', '清关行编号')</th>
                <th>@sortablelink('name', '清关行名称')</th>
              </tr>
            </thead>
            <tbody>
              @foreach($vm->dto as $customsbroker)
              <tr>
                <td>
                  <a href="{{url('/customsbroker/edit/'.$customsbroker->id.'')}}" class="btn btn-icons btn-success btn-action "><i class="fa fa-pencil"></i></a>
                  <button type="button" class="btn btn-icons btn-danger  btn-delete" data-id="{{$customsbroker->id}}"><i class="fa fa-trash"></i></button>
                </td>
                <td class="text-center">{!! $customsbroker->status !!}</td>
                <td>{{$customsbroker->code}} </td>
                <td>{{$customsbroker->name}} </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div><!--table-responsive-->
        <div class="mt-5 ">
          {{$vm->paging->links()}}
        </div>
      </div><!--card-body-->
    </div>
  </div>
</div>

<form method="POST" id="form-delete" action="{{url('/customsbroker/delete')}}">
  @csrf
  <input type="hidden" id="customsbroker_id" name="id" value="" />
</form>
<!-- // form-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除清关行</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>请确认是否要删除此清关行?</p>
            </div>
            <div class="modal-footer">
                <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
                <button data-dismiss="modal"  class="btn btn-secondary">取消</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('footer')
<script>
    $('.btn-delete').on('click', function() {
       $('#modalConfirmDelete').modal();
       $('#customsbroker_id').val($(this).attr('data-id'));
   });


   $('#btnModalConfirmDeleteOK').on('click', function() {
       if (!$('#form-delete').valid()) return false;

       $.ajax({
           url: '{{url('/customsbroker/delete')}}',
           type: 'POST',
           data: $('#form-delete').serialize(),
       }).fail(function(xhr, text, err) {
          notifySystemError(err);
       }).done(function(data) {
         $('#modalConfirmDelete').modal('hide');
         if(data['error']){
           notifySystemError(data['error']);
         }else{
           notifySuccess('清关行已删除。');
           setTimeout(function(){ location.href='{{url('/customsbrokers')}}'; }, 2000);
         }
       }).always(function() {
       });
   });
</script>

@endsection
