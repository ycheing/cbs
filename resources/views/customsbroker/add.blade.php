@extends('layouts.app')
@section('title', '添加清关行')
@section('content')

<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <!-- form -->
      <form method="post" id="form-customsbroker" action="{{url('customsbroker/add')}}" >
        @csrf
        <div class="card-header header-sm ">
          <div class="d-flex ">
              <div class="wrapper d-flex align-items-center">
                <h2 class="card-title mb4">添加清关行</h2>
              </div>
              <div class="wrapper ml-auto action-bar">
                <button type="submit" data-toggle="tooltip" data-placement="top" data-original-title="储存" class="btn btn-icons btn-success btn-sm"><i class="fa fa-save"></i></button>
                <a class="btn btn-icons btn-outline-primary btn-sm"  data-toggle="tooltip" data-placement="top" data-original-title="返回"  href="{{url('/customsbrokers')}}"><i class="fa fa-close"></i></a>
              </div>
          </div>
        </div><!--//card-header-->

        <div class="card-body">

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="code">清关行编号</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="code" placeholder="" required oninput="this.value = this.value.toUpperCase()"/>
                </div><!--//col-md-9-->
              </div><!--//form-group-->
            </div><!--//col-->

            <div class="col-md-6">
             <div class="form-group row">
               <label class="col-md-3 col-form-label"  for="name">清关行名称</label>
               <div class="col-md-9">
                 <input type="text" class="form-control" name="name" placeholder="" required oninput="this.value = this.value.toUpperCase()"/>
               </div><!--//col-md-9-->
             </div><!--//form-group-->
            </div><!--//col-->

            <div class="col-md-6">
             <div class="form-group row">
                <label class="col-md-3 col-form-label" for="status">状态</label>
                <div class="col-md-9">
                  <select name="status" class="form-control ">
                    @foreach($vm->GetStatuses() as $k=>$v)
                       <option value="{{$k}}">{{$v}}</option>
                    @endforeach
                  </select>
                </div><!--//col-md-9-->
             </div><!--//form-group-->
            </div><!--//col-->



          </div>

          <div class="row mt-5">
            <div class="col-md-12">
              <div class="d-flex pb-2 mb-4 border-bottom  align-items-center">
                  <i class="fa fa-money mr-3 icon-md text-primary"></i> 初始余额
              </div>
            </div>
          </div>

          <div class="row">
            @foreach($vm->GetBillingCompanies() as $bc)
            <div class="col-md-6">
             <div class="form-group row">
               <label class="col-md-3 col-form-label" for="balance[{{$bc->id}}][balance']">{{$bc->code}}</label>
                <div class="col-md-9">
                 <input type="hidden" class="form-control"  name="balance[{{$bc->id}}][company_id]" value="{{$bc->id}}" />
                 <input type="text" class="form-control"  name="balance[{{$bc->id}}][balance]" />
              </div><!--//col-md-9-->
            </div><!--//form-group-->
           </div><!--//col-->
          @endforeach

        </div><!--//row-->

        </div><!--card body-->
      </form><!-- // form-->
    </div><!--card -->
  </div>
</div>
@endsection

@section('footer')

{!! JsValidator::formRequest('App\Http\Requests\Admin\CustomsBrokerRequest', '#form-customsbroker'); !!}
<script>
  $('#form-customsbroker').submit(function (e) {
      e.preventDefault();
      if (!$(this).valid()) return false;
      var _btn = $('button[type=submit]', this);
      startSpin(_btn);
      $.ajax({
          url: this.action,
          type: this.method,
          data: $(this).serialize(),
      }).fail(function(xhr, text, err) {
         notifySystemError(err);
      }).done(function(data) {
        notifySuccess('清关行已添加。');
          setTimeout(function(){ location.href='{{url('/customsbrokers')}}'; }, 2000);
      }).always(function() {
          stopSpin(_btn);
      });
  });
</script>
@endsection
