@extends('layouts.app')
@section('title', '修改清关行')
@section('content')

<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <form method="post" id="form-customsbroker" action="{{url('customsbroker/edit')}}" >
          @csrf
          <input type="hidden" name="id" value="{{$vm->dto->id}}" />
          <div class="card-header header-sm ">
            <div class="d-flex ">
                <div class="wrapper d-flex align-items-center">
                  <h2 class="card-title mb4">修改清关行</h2>
                </div>
                <div class="wrapper ml-auto action-bar">
                  <button type="submit" class="btn btn-icons btn-success btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="储存"><i class="fa fa-save"></i></button>
                  <button  type="button" class="btn btn-icons btn-danger btn-sm" id="btnDelete"  data-toggle="modal" data-target="#modalConfirmDelete">
                    <i class="fa fa-trash"></i>
                  </button>
                  <a class="btn btn-icons btn-outline-primary btn-sm"  data-toggle="tooltip" data-placement="top" data-original-title="返回"  href="{{url('/customsbrokers')}}"><i class="fa fa-close"></i></a>


                </div>
            </div>
          </div><!--//card-header-->

          <div class="card-body">
              <div class="form-group row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="code">清关行编号</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" value="{{$vm->dto->code}}" name="code" placeholder="" required oninput="this.value = this.value.toUpperCase()"/>
                    </div><!--//col-md-9-->
                  </div><!--//form-group-->
                </div><!--//col-->

                <div class="col-md-6">
                 <div class="form-group row">
                   <label class="col-md-3 col-form-label"  for="name">清关行名称</label>
                   <div class="col-md-9">
                     <input type="text" class="form-control" value="{{$vm->dto->name}}" name="name" placeholder="" required oninput="this.value = this.value.toUpperCase()"/>
                   </div><!--//col-md-9-->
                 </div><!--//form-group-->
                </div><!--//col-->

                <div class="col-md-6">
                 <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="status">状态</label>
                    <div class="col-md-9">
                      <select name="status" class="form-control ">
                        @foreach($vm->GetStatuses() as $k=>$v)
                          @if($k == $vm->dto->status_id)
                           <option value="{{$k}}" selected>{{$v}}</option>
                          @else
                           <option value="{{$k}}">{{$v}}</option>
                          @endif
                        @endforeach
                      </select>
                    </div><!--//col-md-9-->
                 </div><!--//form-group-->
                </div><!--//col-->


              </div>
              <div class="row mt-5">
                <div class="col-md-12">
                  <div class="d-flex pb-2 mb-4 border-bottom  align-items-center">
                      <i class="fa fa-money mr-3 icon-md text-primary"></i> 初始余额
                  </div>
                </div>
              </div>

             <div class="row">
               @foreach($vm->GetBillingCompanies() as $bc)
               <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="balance[{{$bc->id}}][balance']">{{$bc->code}}</label>
                   <div class="col-md-9">
                    <input type="hidden" class="form-control"  name="balance[{{$bc->id}}][company_id]" value="{{$bc->id}}"/>
                    <input type="text" class="form-control"  name="balance[{{$bc->id}}][balance]" value="{{ $vm->GetBalance($bc->id) }}"/>
                 </div><!--//col-md-9-->
               </div><!--//form-group-->
              </div><!--//col-->
             @endforeach

           </div><!--//row-->
          </div>
      </form><!-- // form-->
    </div>
  </div>
</div>

<form method="POST" id="form-delete" action="{{url('/customsbroker/delete')}}">
  @csrf
  <input type="hidden" name="id" value="{{$vm->dto->id}}" />
</form>
<!-- // form-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
       <div class="modal-content">
           <div class="modal-header">
               <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除清关行</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
           </div>
           <div class="modal-body">
               <p>请确认是否要删除此清关行?</p>
           </div>
           <div class="modal-footer">
               <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
               <button data-dismiss="modal"  class="btn btn-secondary">取消</button>
           </div>
       </div>
   </div>
</div>
@endsection
@section('footer')
{!! JsValidator::formRequest('App\Http\Requests\Admin\CustomsBrokerInfoRequest', '#form-customsbroker'); !!}
<script>
$('#form-customsbroker').submit(function (e) {
    e.preventDefault();
    if (!$(this).valid()) return false;
    var _btn = $('button[type=submit]', this);
    startSpin(_btn);
    $.ajax({
        url: this.action,
        type: this.method,
        data: $(this).serialize(),
    }).fail(function(xhr, text, err) {
       notifySystemError(err);
    }).done(function(data) {
      notifySuccess('清关行已修改。');
      setTimeout(function(){ location.href='{{url('/customsbrokers')}}'; }, 2000);
    }).always(function() {
        stopSpin(_btn);
    });
});

$('#btnModalConfirmDeleteOK').on('click', function() {
   if (!$('#form-delete').valid()) return false;
   startSpin($('#btnDelete'));
   $.ajax({
       url: '{{url('/customsbroker/delete')}}',
       type: 'POST',
       data: $('#form-delete').serialize(),
   }).fail(function(xhr, text, err) {
      notifySystemError(err);
   }).done(function(data) {
     $('#modalConfirmDelete').modal('hide');
     if(data['error']){
       notifySystemError(data['error']);
     }else{
       notifySuccess('清关行已删除。');
       setTimeout(function(){ location.href='{{url('/customsbrokers')}}'; }, 2000);
     }

   }).always(function() {
     stopSpin($('#btnDelete'));
   });
});
</script>
@endsection
