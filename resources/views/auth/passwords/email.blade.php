@extends('layouts.logout')

@section('content')
<div class="nav-get-started">
  <a class="btn get-started-btn" href="{{url('login')}}">LOGIN</a>
</div>
<form method="POST" action="{{ route('password.email') }}">
    @csrf

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <h3 class="mr-auto">  {{ __('Reset Password') }}</h3>
    <div class="form-group">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="mdi mdi-account-outline"></i>
              </span>
            </div>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email"/>
            </div>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>

          <div class="form-group">
             <button  type="submit" class="btn btn-primary submit-btn">{{ __('Send Password Reset Link') }}</button>
           </div>
           <div class="wrapper mt-5 text-gray">
              <p class="footer-text">Copyright © 2019. All rights reserved.</p>
          </div>

</form>
@endsection
