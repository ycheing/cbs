@extends('layouts.logout')
@section('title', 'Reset password')
@section('content')


  <form method="POST" action="{{ route('password.update') }}">
      @csrf
      <h3 class="mr-auto">  {{ __('Reset Password') }}</h3>
      <input type="hidden" name="token" value="{{ $token }}">


        <div class="form-group">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="mdi mdi-account-outline"></i>
                </span>
              </div>
              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('E-Mail Address') }}"/>
              </div>
              @error('email')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
            </div>

            <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="mdi mdi-account-outline"></i>
                      </span>
                    </div>
  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="{{ __('Password') }}"/>
</div>

              @error('password')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
        </div>


          <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="mdi mdi-account-outline"></i>
                    </span>
                  </div>
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password"placeholder="{{ __('Confirm Password') }}"/>
          </div>

        </div>

        <div class="form-group">
           <button  type="submit" class="btn btn-primary submit-btn">{{ __('Reset Password') }}</button>
         </div>
         <div class="wrapper mt-5 text-gray">
            <p class="footer-text">Copyright © 2019. All rights reserved.</p>
        </div>
  </form>

@endsection
