@extends('layouts.app')
@section('title', '添加代理')
@section('content')
    <div class="row">
      <div class="col-lg-12 col-sm-12 grid-margin">
        <!-- form -->
        <form method="post" id="form-agent" action="{{url('/agent/add')}}" class="forms-sample">
            @csrf
        <div class="card">
          <div class="card-header header-sm ">
            <div class="d-flex ">
                <div class="wrapper d-flex align-items-center">
                  <h2 class="card-title mb4">添加代理</h2>
                </div>
                <div class="wrapper ml-auto action-bar">
                  <button type="submit" data-toggle="tooltip" data-placement="top" data-original-title="储存" class="btn btn-icons btn-success btn-sm"><i class="fa fa-save"></i></button>
                  <a class="btn btn-icons btn-outline-primary btn-sm"  data-toggle="tooltip" data-placement="top" data-original-title="返回"  href="{{url('/agents')}}"><i class="fa fa-close"></i></a>
                </div>
            </div>
          </div><!--//card-header-->
          <div class="card-body">
            <div class="form-group row">
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="name">代理姓名</label>
                  <div class="col-md-9">
                  <input type="text" class="form-control"  name="name" placeholder="" required/>
                  </div><!--//col-md-9-->
                </div><!--//form-group-->
              </div><!--//col-->

              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="email">电邮</label>
                  <div class="col-md-9">
                    <input type="email" class="form-control" name="email" placeholder=""  />
                  </div><!--//col-md-9-->
                </div><!--//form-group-->
              </div><!--//col-->

              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="status">状态</label>
                  <div class="col-md-9">
                    <select name="status" class="form-control ">
                    @foreach($vm->GetStatuses() as $k=>$v)
                      <option value="{{$k}}">{{$v}}</option>
                    @endforeach
                    </select>
                  </div><!--//col-md-9-->
                </div><!--//form-group-->
              </div><!--//col-->

            </div><!--form-group-->

          </div>
        </div>

      </form>
      <!-- // form-->
      </div>
    </div>
@endsection
@section('footer')
{!! JsValidator::formRequest('App\Http\Requests\Admin\AgentRequest', '#form-agent'); !!}
<script>
  $('#form-agent').submit(function (e) {
      e.preventDefault();
      if (!$(this).valid()) return false;
      var _btn = $('button[type=submit]', this);
      startSpin(_btn);
      $.ajax({
          url: this.action,
          type: this.method,
          data: $(this).serialize(),
      }).fail(function(xhr, text, err) {
         notifySystemError(err);
      }).done(function(data) {
        notifySuccess('代理已添加。');
        setTimeout(function(){ location.href='{{url('/agents')}}'; }, 2000);
      }).always(function() {
          stopSpin(_btn);
      });
  });
</script>
@endsection
