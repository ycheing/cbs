<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title') | {{ config('app.name') }}</title>

  <!-- Scripts -->
  <!-- Styles -->
  <link rel="stylesheet" href="{{ asset('vendor/mdi/css/materialdesignicons.min.css')}}" />
  <link rel="stylesheet" href="{{ asset('vendor/simple-line-icon/css/simple-line-icons.css')}}" />
  <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css')}}" />
  <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css')}}" />
  <link rel="stylesheet" href="{{ asset('css/vendor.bundle.base.css')}}" />
  <link rel="stylesheet" href="{{ asset('css/vendor.bundle.addons.css')}}" />
  <link rel="stylesheet" href="{{ asset('vendor/jquery-confirm-master/css/jquery-confirm.css') }}" />
  <link rel="stylesheet" href="{{ asset('vendor/bootstrap4-datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ asset('css/shared/style.css')}}" />
  <!-- endinject -->
  <!-- Layout styles -->
  <link rel="stylesheet" href="{{asset('css/style.css')}}" />
  <link rel="stylesheet" href="{{asset('css/custom.css')}}" />
  <!-- End Layout styles -->
  <link rel="shortcut icon" href="{{ asset('/images/favicon.png') }}" />
  @yield('header')
</head>

<body class="sidebar-icon-only sidebar-fixed">
  <div class="container-scroller">
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="{{url('agent/dashboard')}}">
          <img src="{{asset('images/logo.png')}}" alt="T & O GLOBAL RESOURCES" /> </a>
        <a class="navbar-brand brand-logo-mini" href="{{url('agent/dashboard')}}">
          <img src="{{asset('images/logo-mini.png')}}" alt="T & O GLOBAL RESOURCES" /> </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="mdi mdi-menu"></span>
        </button>
        <ul class="navbar-nav navbar-nav-left header-links">
          <li class="nav-item active d-none d-md-flex">
            <a href="{{url('agent/report/overview')}}" class="nav-link">
              <i class="mdi mdi-elevation-rise"></i>报表</a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item dropdown  d-xl-inline-block user-dropdown">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <img class="img-xs rounded-circle" src="{{asset('images/avatar.png')}}" alt="Profile image"> </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <div class="dropdown-header text-center">
                <p class="mb-1 mt-3 font-weight-semibold">{{Auth::user()->name}}</p>
                <p class="font-weight-light text-muted mb-0">{{Auth::user()->email}}</p>
              </div>
              <a class="dropdown-item" href="{{url('profile')}}">个人资料</a>
              <a class="dropdown-item" href="{{url('password')}}">更改密码</a>
              <a class="dropdown-item" href="{{ url('logout') }}">登出</a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
          data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">

      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas dynamic-active-class-disabled" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile not-navigation-link">
            <div class="nav-link">

              @if(auth()->user()->can('add declaration'))

              <a class="btn btn-success btn-block btn-lg" href="{{url('agent/booking/type')}}">添加订单<i
                  class="mdi mdi-plus"></i>
              </a>
              @else
              <a class="btn btn-success btn-block btn-lg" href="{{url('agent/booking/add')}}">添加预订<i
                  class="mdi mdi-plus"></i>
              </a>
              @endif
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('agent/dashboard')}}">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">面板</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('agent/bookings')}}">
              <i class="menu-icon  icon-book-open "></i>
              <span class="menu-title">整柜</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('agent/cargos')}}">
              <i class="menu-icon  fa fa-cube "></i>
              <span class="menu-title">散货</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('agent/customers')}}">
              <i class="menu-icon icon-mustache "></i>
              <span class="menu-title">客户</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{url('agent/report/overview')}}">
              <i class="menu-icon mdi mdi-elevation-rise "></i>
              <span class="menu-title">报表</span>
            </a>
          </li>

        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          @yield('content')
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright &copy; 2020 -
              {{date('Y')}} by T&O GLOBAL RESOURCES.</strong> All rights reserved.
            </span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Made with <i
                class="mdi mdi-heart text-danger"></i>
            </span>
          </div>
        </footer>
        <!-- partial -->

      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>

  <script src="{{asset('js/vendor.bundle.base.js')}}"></script>
  <script src="{{asset('js/vendor.bundle.addons.js')}}"></script>
  <script src="{{asset('js/off-canvas.js')}}"></script>
  <script src="{{asset('js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('js/misc.js')}}"></script>
  <script src="{{asset('vendor/jsvalidation/js/jsvalidation.js') }}"></script>
  <script src="{{asset('vendor/bootstrap4-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
  <script src="{{asset('vendor/jquery-confirm-master/js/jquery-confirm.js') }}"></script>
  <script src="{{asset('vendor/select2/js/select2.min.js') }}"></script>
  <script src="{{asset('js/custom.js')}}"></script>
  @yield('footer')

</body>

</html>