@extends('layouts.app')
@section('title', '修改货款')
@section('content')

<div class="row">

<div class="col-lg-12 col-sm-12 grid-margin">
  <div class="card">
    <!-- form -->
    <form method="post" id="form-payment-transfer" action="{{url('payment_transfer/edit')}}" >
      @csrf
      <input type="hidden" name="id" value="{{$vm->dto->id}}" />
      <div class="card-header header-sm ">
        <div class="d-flex ">
            <div class="wrapper d-flex align-items-center">
              <h2 class="card-title mb4">修改货款</h2>
            </div>
            <div class="wrapper ml-auto action-bar">
              <button type="submit" class="btn btn-icons btn-success btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="储存"><i class="fa fa-save"></i></button>
              <button  type="button" class="btn btn-icons btn-danger btn-sm" id="btnDelete"  data-toggle="modal" data-target="#modalConfirmDelete">
                <i class="fa fa-trash"></i>
              </button>
              <a class="btn btn-icons btn-outline-primary btn-sm"  data-toggle="tooltip" data-placement="top" data-original-title="返回"  href="{{url('/payment_transfers')}}"><i class="fa fa-close"></i></a>

            </div>
        </div>
      </div><!--//card-header-->

      <div class="card-body">
        <div class="d-flex pb-2 mb-4 border-bottom  align-items-center">
          <i class="mdi mdi-bank-transfer mr-3 icon-md text-primary"></i>客户资料
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="customer_id">客户代码</label>
              <div class="col-md-9">
                <select name="customer_id" class="select2 form-control">
                  <option value=""></option>
                  @foreach($vm->GetCustomers() as $customer)
                   @if($vm->dto->customer_id == $customer->id)
                     <option value="{{$customer->id}}" selected>{{$customer->code}}</option>
                    @else
                      <option value="{{$customer->id}}">{{$customer->code}}</option>
                    @endif
                  @endforeach
                </select>
              </div><!--//col-md-9-->
            </div><!--//form-group-->
          </div><!--//col-->
          <div class="col-md-4">
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="customer_name">客户名称</label>
              <div class="col-md-9">
                <input name="customer_name" id="input-customer-name" class="form-control"   value="{{$vm->dto->customer_name}}"/>
                <input type="hidden" name="customer_code" value="{{$vm->dto->customer_code}}"/>
              </div><!--//col-md-9-->
            </div><!--//form-group-->
          </div><!--//col-->
          <div class="col-md-4">
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="status">状态</label>
              <div class="col-md-9">
               <select name="status" class="form-control ">
                 @foreach($vm->GetStatuses() as $k=>$v)
                  @if($k == $vm->dto->status_id)
                    <option value="{{$k}}" selected>{{$v}}</option>
                  @else
                    <option value="{{$k}}">{{$v}}</option>
                  @endif
                 @endforeach
               </select>
              </div><!--//col-md-9-->
           </div><!--//form-group-->
         </div><!--//col-->
        </div><!--row-->


        <div class="row">
          <div class="col-md-6">

            <div class="form-group row">
                <label class="col-md-3 col-form-label"  for="customer_amount">金额 (RM)</label>
                <div class="col-md-9">
                  <input type="text" id="customer_amount" class=" form-control"  name="customer_amount" placeholder="" value="{{$vm->dto->customer_amount}}" />
                </div><!--//col-->
            </div>

            <div class="form-group row">
              <label class="col-md-3 col-form-label"  for="customer_exchange_rate">汇率</label>
              <div class="col-md-9">
                <input type="text" id="customer_exchange_rate" class=" form-control"  name="customer_exchange_rate" placeholder="" value="{{$vm->dto->customer_exchange_rate}}"/>
              </div><!--//col-->
            </div>

            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="customer_total">金额 (RMB)</label>
              <div class="col-md-9">
                <input type="text" id="customer_total" class=" form-control"  name="customer_total" placeholder=""  value="{{$vm->dto->customer_total}}"/>
              </div><!--//col-->
            </div>

            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="purpose_payment">付款目的</label>
              <div class="col-md-9">
                <input type="text" class=" form-control"  name="purpose_payment" placeholder=""  value="{{$vm->dto->purpose_payment}}"/>
              </div><!--//col-md-9-->
            </div><!--//form-group-->

            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="deposits">定金</label>
              <div class="col-md-9">
                <input type="text" class=" form-control"  name="deposits" placeholder="" value="{{$vm->dto->deposits}}"/>
              </div><!--//col-md-9-->
            </div><!--//form-group-->

            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="beneficiary_id">收款人</label>
                <div class="col-md-9">
                  <select name="beneficiary_id" class="form-control">
                    <option value=""></option>
                    @foreach($vm->GetBeneficiaries() as $cb)
                      @if($vm->dto->beneficiary_id == $cb->id)
                      <option value="{{$cb->id}}" selected>{{$cb->name}}</option>
                      @else
                      <option value="{{$cb->id}}">{{$cb->name}}</option>
                      @endif
                    @endforeach
                  </select>
                </div><!--//col-md-9-->
            </div><!--//form-group-->
          </div>

          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-md-3 col-form-label"  for="payment_date">汇款日期</label>
              <div class="col-md-9">
               <div class="input-group ">
                 <input id="payment_date" type="text" class="form-control date" name="payment_date"  data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->payment_date}}"/>
                 <div class="input-group-append">
                      <span> <i class="fa fa-calendar"></i></span>
                 </div>
               </div>
              </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label"  for="file_customer_attachment">附件</label>
                <div class="col-md-9">
                  @if($vm->dto->customer_attachment_path != '')
                  <div id="customer_attachment">
                    <a href="{{$vm->dto->customer_attachment_path}}" target="_blank" class="btn btn-primary mb-1">下载</a>
                    <button type="button" class="btn-delete-attachment btn btn-danger mb-1" data-type="1"><i class="fa fa-trash"></i></button>
                  </div>
                  @endif
                <input type="file" class="dropify form-control"  name="file_customer_attachment"  data-default-file="{{$vm->dto->customer_attachment_path}}"/>
              </div><!--//col-->
            </div>
          </div>
        </div>

        <div class="d-flex pb-2 mb-4 border-bottom  align-items-center">
          <i class="mdi mdi-bank-transfer mr-3 icon-md text-primary"></i>供应商资料
        </div>


        <div class="row">
          <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label"  for="supplier_id">供应商</label>
                <div class="col-md-9">
                <select name="supplier_id" class="form-control">
                  <option value=""></option>
                  @foreach($vm->GetSuppliers() as $s)
                    @if($vm->dto->supplier_id == $s->id)
                      <option value="{{$s->id}}" selected>{{$s->name}}</option>
                    @else
                      <option value="{{$s->id}}">{{$s->name}}</option>
                    @endif
                  @endforeach
                </select>
              </div><!--//col-->
            </div>

            <div class="form-group row">
                <label class="col-md-3 col-form-label"  for="supplier_amount">金额 (RM)</label>
                <div class="col-md-9">
                  <input type="text" id="supplier_amount" class=" form-control"  name="supplier_amount" placeholder="" value="{{$vm->dto->supplier_amount}}"/>
                </div><!--//col-->
            </div>

            <div class="form-group row">
              <label class="col-md-3 col-form-label"   for="supplier_exchange_rate">汇率</label>
              <div class="col-md-9">
                <input type="text" id="supplier_exchange_rate" class=" form-control"  name="supplier_exchange_rate" placeholder="" value="{{$vm->dto->supplier_exchange_rate}}"/>
              </div><!--//col-->
            </div>

            <div class="form-group row">
              <label class="col-md-3 col-form-label"   for="supplier_total">金额 (RMB)</label>
              <div class="col-md-9">
                <input type="text" id="supplier_total" class=" form-control"  name="supplier_total" placeholder="" value="{{$vm->dto->supplier_total}}" />
              </div><!--//col-->
            </div>

            <div class="form-group row">
              <label class="col-md-3 col-form-label"  for="supplier_bank_charges">银行收费</label>
              <div class="col-md-9">
                <input type="text" id="supplier_bank_charges" class=" form-control"  name="supplier_bank_charges" placeholder="" value="{{$vm->dto->supplier_bank_charges}}"/>
              </div><!--//col-->
            </div>

            <div class="form-group row">
              <label class="col-md-3 col-form-label"  for="supplier_net_amount">净额  (RMB)</label>
              <div class="col-md-9">
                <input type="text" id="supplier_net_amount" class=" form-control"  name="supplier_net_amount" placeholder=""  value="{{$vm->dto->supplier_net_amount}}"/>
              </div><!--//col-->
            </div>

          </div>

          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-md-3 col-form-label"  for="date_received">接收日期</label>
              <div class="col-md-9">
               <div class="input-group ">
                 <input id="date_received" type="text" class="form-control date" name="date_received"  data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->date_received}}"/>
                 <div class="input-group-append">
                      <span> <i class="fa fa-calendar"></i></span>
                 </div>
               </div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-3 col-form-label"  for="supplier_attachment">附件</label>
              <div class="col-md-9">
                @if($vm->dto->supplier_attachment != '')
                <div id="supplier_attachment">
                  <a href="{{$vm->dto->supplier_attachment_path}}" target="_blank" class="btn btn-primary mb-1">下载</a>
                  <button type="button" class="btn-delete-attachment btn btn-danger mb-1" data-type="2"><i class="fa fa-trash"></i></button>
                </div>
                @endif
                <input type="file" class="dropify form-control"  name="file_supplier_attachment"   data-default-file="{{$vm->dto->supplier_attachment_path}}"/>
              </div><!--//col-->
            </div>
          </div>
        </div>

      </div> <!--card body-->
    </div><!--card -->

    </form>
  <!-- // form-->
  </div>
</div>


<form method="POST" id="form-delete" action="{{url('/payment_transfer/delete')}}">
  @csrf
  <input type="hidden" name="id" value="{{$vm->dto->id}}" />
</form>
<!-- // form-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除货款</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>请确认是否要删除此货款?</p>
            </div>
            <div class="modal-footer">
                <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
                <button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">取消</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')

{!! JsValidator::formRequest('App\Http\Requests\Admin\PaymentTransferInfoRequest', '#form-payment-transfer'); !!}
<script>
$('#form-payment-transfer').submit(function (e) {
    e.preventDefault();
    if (!$(this).valid()) return false;
    var _btn = $('button[type=submit]', this);
    startSpin(_btn);
    var formData = new FormData($(this)[0]);
    // var _data = formToJSON($(this)[0]);
    $.ajax({
        url: this.action,
        type: this.method,
        data: formData,
        enctype: 'multipart/form-data',
        contentType: false,
        processData: false,
    }).fail(function(xhr, text, err) {
       notifySystemError(err);
    }).done(function(data) {
      notifySuccess('货款已修改。');
        setTimeout(function(){ location.href='{{url('/payment_transfers')}}'; }, 2000);
    }).always(function() {
        stopSpin(_btn);
    });
});

$('#btnModalConfirmDeleteOK').on('click', function() {
   if (!$('#form-delete').valid()) return false;
   startSpin($('#btnDelete'));
   $.ajax({
       url: '{{url('/payment_transfer/delete')}}',
       type: 'POST',
       data: $('#form-delete').serialize(),
   }).fail(function(xhr, text, err) {
      notifySystemError(err);
   }).done(function(data) {
     $('#modalConfirmDelete').modal('hide');
     if(data['error']){
       notifySystemError(data['error']);
     }else{
       notifySuccess('货款已删除。');
       setTimeout(function(){ location.href='{{url('/payment_transfers')}}'; }, 2000);
     }

   }).always(function() {
       stopSpin($('#btnDelete'));
   });
});
//customer change
$('select[name=\'customer_id\']').on('change', function() {
  $.ajax({
    type:"GET",
    url: '{{url('getcustomer?customer_id=')}}' + this.value,
    dataType: 'json',
    beforeSend: function() {
      $('select[name=\'customer_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      if(json.hasOwnProperty('code')){
        if (json['code'] && json['code'] != '') {
          $('input[name=\'customer_code\']').val(json['code']);
        }
      }
      if(json.hasOwnProperty('name')){
        if (json['name'] && json['name'] != '') {
          $('input[name=\'customer_name\']').val(json['name']);
        }
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('select[name=\'customer_id\']').trigger('change');

$("#customer_amount").change(function() {
  calCustomerTotal();
});
$("#customer_exchange_rate").change(function() {
  calCustomerTotal();
});
$("#supplier_amount").change(function() {
  calSupplierTotal();
});
$("#supplier_exchange_rate").change(function() {
  calSupplierTotal();
});
$("#supplier_bank_charges").change(function() {
  calSupplierNetTotal();
});
function calCustomerTotal(){
  var _total = $('#customer_amount').val() * $('#customer_exchange_rate').val();
  $('#customer_total').val(parseFloat(_total).toFixed(2));
}
function calSupplierTotal(){
  var _total = $('#supplier_amount').val() * $('#supplier_exchange_rate').val();
  $('#supplier_total').val(parseFloat(_total).toFixed(2));
}
function calSupplierNetTotal(){
  var _total = parseFloat($('#supplier_total').val()) - parseFloat($('#supplier_bank_charges').val());
  $('#supplier_net_amount').val(parseFloat(_total).toFixed(2));
}

$(document).on('click', '.btn-delete-attachment', function(e) {
  e.preventDefault();
  e.stopImmediatePropagation();
  if (!confirm('是否删除此附件？')) return;
  var _formData = new FormData();
  var _type = $(this).attr('data-type');
  _formData.append('payment_transfer_id', '{{$vm->dto->id}}');
  _formData.append('type', _type);
  $.ajax({
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '{{url('/payment_transfer/remove/attachment')}}',
      type: 'POST',
      contentType: false,
      processData: false,
      data: _formData,
  }).fail(function(xhr, text, err) {
     notifySystemError(err);
  }).done(function(data){
    if(data['result']){
      notifySuccess('附件已删除。');
      if(_type==1){
        $('#customer_attachment').empty();
      }else if(_type==2){
        $('#supplier_attachment').empty();
      }
    }else{
      notifySystemError(data['error']);
    }

  }).always(function(){
  });
});
</script>

@endsection
