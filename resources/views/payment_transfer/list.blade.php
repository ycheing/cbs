@extends('layouts.app')
@section('title', '货款清单')
@section('content')
<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center media-info">
            <h2 class="card-title">货款清单</h2>
          </div>
          <div class="wrapper ml-auto action-bar">
            <a href="{{url('/payment_transfer/add')}}" class="btn btn-primary btn-block"><i class="fa fa-plus "></i>添加货款</a>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="page-header-toolbar">
          <form method="GET" action="{{url('/payment_transfers')}}" style="width:100%">
            <div class="form-group row">
              <div class="col-md-6">
                <input name="keyword" id="keyword" placeholder="关键词" class="form-control"/>
              </div>
              <div class="col-md-1">
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </form>
        </div>
        <div class="table-responsive">
          <table class="table table-striped" >
            <thead>
              <tr>
                <th></th>
                <!-- <th style="width:10%" class="text-center">@sortablelink('status', '状态')</th> -->
                <th>@sortablelink('payment_date', ' 汇款日期')</th>
                <th>@sortablelink('customer_code', ' 客户代码')</th>
                <th class="text-right">@sortablelink('customer_amount',' 金额 (RM)')</th>
                <th class="text-right">@sortablelink('customer_exchange_rate', ' 汇率')</th>
                <th class="text-right">@sortablelink('customer_total', ' 金额 (RMB)')</th>
                <th>@sortablelink('supplier_id', ' 供应商')</th>
                <th class="text-right">@sortablelink('supplier_amount', ' 金额 (RM)')</th>
                <th class="text-right">@sortablelink('supplier_exchange_rate', ' 汇率')</th>
                <th class="text-right">@sortablelink('supplier_total', ' 金额 (RMB)')</th>
                <th class="text-right">@sortablelink('supplier_bank_charges', ' 银行收费')</th>
                <th class="text-right">@sortablelink('supplier_net_amount', ' 净额 (RMB)')</th>
                <th>@sortablelink('date_received', ' 接收日期')</th>
              </tr>
            </thead>
            <tbody>
              @foreach($vm->dto as $payment_transfer)
              <tr>
                <td>
                  <a href="{{url('/payment_transfer/edit/'.$payment_transfer->id.'')}}" class="btn btn-icons btn-success btn-action "><i class="fa fa-pencil"></i></a>
                  <button type="button" class="btn btn-icons btn-danger  btn-delete" data-id="{{$payment_transfer->id}}"><i class="fa fa-trash"></i></button>
                </td>
                <!-- <td class="text-center">{!! $payment_transfer->status !!}</td> -->
                <td class="text-center">{{$payment_transfer->payment_date}} </td>
                <td>{{$payment_transfer->customer_code}} </td>
                <td class="text-right">{{$payment_transfer->customer_amount}} </td>
                <td class="text-right">{{$payment_transfer->customer_exchange_rate}} </td>
                <td class="text-right">{{$payment_transfer->customer_total}} </td>
                <td>{{$payment_transfer->supplier_code}} </td>
                <td class="text-right">{{$payment_transfer->supplier_amount}} </td>
                <td class="text-right">{{$payment_transfer->supplier_exchange_rate}} </td>
                <td class="text-right">{{$payment_transfer->supplier_total}} </td>
                <td class="text-right">{{$payment_transfer->supplier_bank_charges}} </td>
                <td class="text-right">{{$payment_transfer->supplier_net_amount}} </td>
                <!-- <td>{{$payment_transfer->beneficiary}} </td> -->
                <td class="text-center">{{$payment_transfer->date_received}} </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!--table-responsive-->
        <div class="mt-5 ">
          {{$vm->paging->links()}}
        </div>
      </div><!--card-body-->
    </div>
  </div>
</div>

<form method="POST" id="form-delete" action="{{url('/payment_transfer/delete')}}">
  @csrf
  <input type="hidden" id="payment_transfer_id" name="id" value="" />
</form>
<!-- // form-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除货款</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <p>请确认是否要删除此货款?</p>
          </div>
          <div class="modal-footer">
            <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
            <button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">取消</button>
          </div>
      </div>
  </div>
</div>

@endsection
@section('footer')
<script>
    $('.btn-delete').on('click', function() {
       $('#modalConfirmDelete').modal();
       $('#payment_transfer_id').val($(this).attr('data-id'));
   });

   $('#btnModalConfirmDeleteOK').on('click', function() {
       if (!$('#form-delete').valid()) return false;

       $.ajax({
           url: '{{url('/payment_transfer/delete')}}',
           type: 'POST',
           data: $('#form-delete').serialize(),
       }).fail(function(xhr, text, err) {
          notifySystemError(err);
       }).done(function(data) {
         $('#modalConfirmDelete').modal('hide');
         if(data['error']){
           notifySystemError(data['error']);
         }else{
           notifySuccess('货款已删除。');
           setTimeout(function(){ location.href='{{url('/payment_transfers')}}'; }, 2000);
         }
       }).always(function() {
       });
   });
</script>

@endsection
