@extends('layouts.app')
@section('title', '修改客户')
@section('content')

<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <!-- form -->
      <form method="post" id="form-customer" action="{{url('customer/edit')}}">
        @csrf
        <input type="hidden" name="id" value="{{$vm->dto->id}}" />
        <div class="card-header header-sm ">
          <div class="d-flex ">
            <div class="wrapper d-flex align-items-center">
              <h2 class="card-title mb4">修改客户</h2>
            </div>
            <div class="wrapper ml-auto action-bar">
              <a href="{{url('/customer/copy/'. $vm->dto->id.'')}}" class="btn btn-icons btn-info  btn-sm"
                data-toggle="tooltip" data-placement="top" data-original-title="复制"><i class="fa fa-copy"></i></a>
              <button type="submit" class="btn btn-icons btn-success btn-sm" data-toggle="tooltip" data-placement="top"
                data-original-title="储存"><i class="fa fa-save"></i></button>
              <button type="button" class="btn btn-icons btn-danger btn-sm" id="btnDelete" data-toggle="modal"
                data-target="#modalConfirmDelete">
                <i class="fa fa-trash"></i>
              </button>
              <a class="btn btn-icons btn-outline-primary btn-sm" data-toggle="tooltip" data-placement="top"
                data-original-title="返回" href="{{url('/customers')}}"><i class="fa fa-close"></i></a>

            </div>
          </div>
        </div>
        <!--//card-header-->

        <div class="card-body">

          <div class="d-flex pb-2 mb-4 border-bottom  align-items-center">
            <i class="fa fa-building-o mr-3 icon-md text-primary"></i>信息
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="code">客户编号</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="code" placeholder="" required
                    oninput="this.value = this.value.toUpperCase()" value="{{$vm->dto->code}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="name">客户名称</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="name" placeholder="" required
                    oninput="this.value = this.value.toUpperCase()" value="{{$vm->dto->name}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->

          </div>
          <!--//row-->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="nickname">客户昵称</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="nickname" placeholder="" required
                    oninput="this.value = this.value.toUpperCase()" value="{{$vm->dto->nickname}}" maxlength="12" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="status">状态</label>
                <div class="col-md-9">
                  <select name="status" class="form-control ">
                    @foreach($vm->GetStatuses() as $k=>$v)
                    @if($vm->dto->status_id == $k)
                    <option value="{{$k}}" selected>{{$v}}</option>
                    @else
                    <option value="{{$k}}">{{$v}}</option>
                    @endif
                    @endforeach
                  </select>
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->

          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="id_number">注册号码</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="id_number" placeholder=""
                    value="{{$vm->dto->id_number}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="vat_number">税号</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="vat_number" placeholder=""
                    value="{{$vm->dto->vat_number}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="website">网站</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="website" placeholder="" value="{{$vm->dto->website}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="phone">电话号码</label>
                <div class="col-md-9">
                  <div class="row">
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="phone" placeholder="" value="{{$vm->dto->phone}}" />
                    </div>
                    <!--//col-md-9-->
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="phone2" placeholder=""
                        value="{{$vm->dto->phone2}}" />
                    </div>
                    <!--//col-md-9-->
                  </div>
                </div>

              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="initial_balance">初始余额</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="initial_balance" placeholder=""
                    value="{{$vm->dto->initial_balance}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="company_id">公司</label>
                <div class="col-md-9">
                  <select name="company_id" class="form-control">
                    <option value="0"></option>
                    @foreach($vm->GetBillingCompanies() as $company)
                    @if($vm->dto->company_id == $company->id)
                    <option value="{{$company->id}}" selected>{{$company->code}}</option>
                    @else
                    <option value="{{$company->id}}">{{$company->code}}</option>
                    @endif
                    @endforeach
                  </select>
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
          </div>

          <div class="d-flex pb-2 mt-5 mb-4 border-bottom  align-items-center">
            <i class="fa fa-vcard-o mr-3 icon-md text-primary"></i>联系人
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="attn">联系人姓名</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="attn" placeholder="" value="{{$vm->dto->attn}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="email">电邮</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="email" placeholder="" value="{{$vm->dto->email}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="mobile">行动电话</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="mobile" placeholder="" value="{{$vm->dto->mobile}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
          </div>



          <div class="d-flex pb-2 mt-5 mb-4 border-bottom  align-items-center">
            <i class="fa fa-address-book-o mr-3 icon-md text-primary"></i>帐单地址
          </div>


          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_company_name">公司名字</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="billing_company_name" placeholder=""
                    id="billing_company_name" value="{{$vm->dto->billing_company_name}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
            <div class="col-md-6">
            </div>
            <!--//col-->
          </div>


          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_address_1">地址第一行</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="billing_address_1" id="billing_address_1"
                    placeholder="" value="{{$vm->dto->billing_address_1}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_address_2">地址第二行</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="billing_address_2" id="billing_address_2"
                    placeholder="" value="{{$vm->dto->billing_address_2}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
          </div>

          <div class=" row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_city">城市</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="billing_city" id="billing_city" placeholder=""
                    value="{{$vm->dto->billing_city}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_postcode">邮编</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="billing_postcode" id="billing_postcode" placeholder=""
                    value="{{$vm->dto->billing_postcode}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_country_id">国家</label>
                <div class="col-md-9">
                  <select name="billing_country_id" id="billing_country_id" class="select2 form-control">
                    <option value=""></option>
                    @foreach($vm->GetCountries() as $country)
                    @if($vm->dto->billing_country_id == $country->id)
                    <option value="{{$country->id}}" selected>{{$country->name}}</option>
                    @else
                    <option value="{{$country->id}}">{{$country->name}}</option>
                    @endif
                    @endforeach
                  </select>
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_state_id">州</label>
                <div class="col-md-9">
                  <select name="billing_state_id" id="billing_state_id" class="select2 form-control">
                  </select>
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
          </div>

          <div class="d-flex pb-2 mt-5 mb-4 border-bottom  align-items-center">
            <i class="fa fa-truck mr-3 icon-md text-primary"></i>送货地址

            <div class="form-check ml-auto">
              <label class="form-check-label">
                <input type="checkbox" id="cbxSame" onclick="sameAddress()">
                与账单地址相同 </label>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="shipping_company_name">公司名字</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="shipping_company_name" placeholder=""
                    id="shipping_company_name" value="{{$vm->dto->shipping_company_name}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
            <div class="col-md-6">
            </div>
            <!--//col-->
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="shipping_address_1">地址第一行</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="shipping_address_1" id="shipping_address_1"
                    placeholder="" value="{{$vm->dto->shipping_address_1}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="shipping_address_2">地址第二行</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="shipping_address_2" id="shipping_address_2"
                    placeholder="" value="{{$vm->dto->shipping_address_2}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="shipping_city">城市</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="shipping_city" id="shipping_city" placeholder=""
                    value="{{$vm->dto->shipping_city}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="shipping_postcode">邮编</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="shipping_postcode" id="shipping_postcode"
                    placeholder="" value="{{$vm->dto->shipping_postcode}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="shipping_country_id">国家</label>
                <div class="col-md-9">
                  <select name="shipping_country_id" id="shipping_country_id" class="select2 form-control">
                    <option value=""></option>
                    @foreach($vm->GetCountries() as $country)
                    @if($vm->dto->shipping_country_id == $country->id)
                    <option value="{{$country->id}}" selected>{{$country->name}}</option>
                    @else
                    <option value="{{$country->id}}">{{$country->name}}</option>
                    @endif
                    @endforeach
                  </select>
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="shipping_state_id">州</label>
                <div class="col-md-9">
                  <select name="shipping_state_id" id="shipping_state_id" class="select2 form-control">
                  </select>
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="shipping_name">收货联系人</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="shipping_name" id="shipping_name" placeholder=""
                    value="{{$vm->dto->shipping_name}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="phone">电话号码</label>
                <div class="col-md-9">
                  <div class=" row">
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="shipping_phone_1" placeholder="电话号码 1"
                        value="{{$vm->dto->shipping_phone_1}}" />
                    </div>
                    <!--//col-md-9-->
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="shipping_phone_2" placeholder="电话号码 2"
                        value="{{$vm->dto->shipping_phone_2}}" />
                    </div>
                    <!--//col-md-9-->
                  </div>

                </div>

              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
          </div>

        </div>
        <!--card body-->


      </form> <!-- // form-->
    </div>
    <!--card -->
  </div>
</div>

<form method="POST" id="form-delete" action="{{url('/customer/delete')}}">
  @csrf
  <input type="hidden" name="id" value="{{$vm->dto->id}}" />
</form>
<!-- // form-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除客户</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>请确认是否要删除此客户?</p>
      </div>
      <div class="modal-footer">
        <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
        <button data-dismiss="modal" class="btn btn-secondary">取消</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('footer')
{!! JsValidator::formRequest('App\Http\Requests\Admin\CustomerInfoRequest', '#form-customer'); !!}
<script>
  $('#form-customer').submit(function (e) {
    e.preventDefault();
    if (!$(this).valid()) return false;
    var _btn = $('button[type=submit]', this);
    startSpin(_btn);
    $.ajax({
        url: this.action,
        type: this.method,
        data: $(this).serialize(),
    }).fail(function(xhr, text, err) {
       notifySystemError(err);
    }).done(function(data) {
      notifySuccess('客户已修改。');
        setTimeout(function(){ location.href='{{url('/customers')}}'; }, 2000);
    }).always(function() {
        stopSpin(_btn);
    });
});

$('#btnModalConfirmDeleteOK').on('click', function() {
   if (!$('#form-delete').valid()) return false;
   startSpin($('#btnDelete'));
   $.ajax({
       url: '{{url('/customer/delete')}}',
       type: 'POST',
       data: $('#form-delete').serialize(),
   }).fail(function(xhr, text, err) {
      notifySystemError(err);
   }).done(function(data) {
     $('#modalConfirmDelete').modal('hide');
     if(data['error']){
       notifySystemError(data['error']);
     }else{
       notifySuccess('客户已删除。');
       setTimeout(function(){ location.href='{{url('/customers')}}'; }, 2000);
     }

   }).always(function() {
       stopSpin($('#btnDelete'));
   });
});

$('select[name=\'billing_country_id\']').on('change', function() {

	$.ajax({
		type:"GET",
		url: '{{url('getcountry?country_id=')}}' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'billing_country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			html = '<option value="">{{__('Please select')}}</option>';
			if (json['json']['states'] && json['json']['states'] != '') {
				for (i = 0; i < json['json']['states'].length; i++) {
          if(json['json']['states'][i]['id'] == '{{$vm->dto->billing_state_id}}'){
            html += '<option value="' + json['json']['states'][i]['id'] + '"';
  					html += ' selected>' + json['json']['states'][i]['name'] + '</option>';
          }else{
            html += '<option value="' + json['json']['states'][i]['id'] + '"';
  					html += '>' + json['json']['states'][i]['name'] + '</option>';
          }

				}
			} else {
				html += '<option value="0" selected="selected">{{__('None')}}</option>';
			}
			$('select[name=\'billing_state_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'billing_country_id\']').trigger('change');

$('select[name=\'shipping_country_id\']').on('change', function() {

	$.ajax({
		type:"GET",
		url: '{{url('getcountry?country_id=')}}' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'shipping_country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			html = '<option value="">{{__('Please select')}}</option>';

			if (json['json']['states'] && json['json']['states'] != '') {

				for (i = 0; i < json['json']['states'].length; i++) {
          if(json['json']['states'][i]['id'] == '{{$vm->dto->shipping_state_id}}'){
            html += '<option value="' + json['json']['states'][i]['id'] + '"';
  					html += ' selected>' + json['json']['states'][i]['name'] + '</option>';
          }else{
            html += '<option value="' + json['json']['states'][i]['id'] + '"';
  					html += '>' + json['json']['states'][i]['name'] + '</option>';
          }
				}
			} else {
				html += '<option value="0" selected="selected">{{__('None')}}</option>';
			}

			$('select[name=\'shipping_state_id\']').html(html);

      if ($('#cbxSame').is(':checked')) {
        $('#shipping_state_id').val($('#billing_state_id').val());
      }
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'shipping_country_id\']').trigger('change');



function sameAddress() {
    if ($('#cbxSame').is(':checked')) {
        $('#shipping_company_name').val($('#billing_company_name').val());
        $('#shipping_address_1').val($('#billing_address_1').val());
        $('#shipping_address_2').val($('#billing_address_2').val());
        $('#shipping_city').val($('#billing_city').val());
        $('#shipping_postcode').val($('#billing_postcode').val());
        $('#shipping_country_id').val($('#billing_country_id').val());
        $('select[name=\'shipping_country_id\']').trigger('change');
        // $('select[name=\'shipping_state_id\']').val($('#billing_state_id').val());
    } 
  }
//-->
</script>

@endsection