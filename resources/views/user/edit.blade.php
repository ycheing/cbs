@extends('layouts.app')
@section('title', '修改用户')
@section('content')
<div class="row">
  <div class="col-lg-8 col-sm-8 grid-margin">
    <div class="card">
      <form method="post" id="form-user" action="{{url('/user/edit')}}">
          @csrf
        <input type="hidden" name="id" value="{{$vm->dto->id}}" />
      <div class="card-header header-sm ">
        <div class="d-flex ">
            <div class="wrapper d-flex align-items-center">
              <h2 class="card-title mb4  ">修改用户</h2>
            </div>
            <div class="wrapper ml-auto action-bar">
              <button type="submit" class="btn btn-icons btn-success btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="储存"><i class="fa fa-save"></i></button>
              <button  type="button" class="btn btn-icons btn-danger btn-sm" id="btnDelete"  data-toggle="modal" data-target="#modalConfirmDelete">
                <i class="fa fa-trash"></i>
              </button>
              <a class="btn btn-icons btn-outline-primary btn-sm"  data-toggle="tooltip" data-placement="top" data-original-title="返回"  href="{{url('/users')}}"><i class="fa fa-close"></i></a>
            </div>
        </div>
      </div><!--//card-header-->
      <div class="card-body">
        <!-- form -->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="username">用户名 </label>
                  <div class="col-md-9">
                    <input type="text" class="form-control"  name="name"  value="{{$vm->dto->username}}" disabled/>
                 </div><!--//col-->
              </div>
            </div><!--//col-->

          </div>

          <div class="row">
             <div class="col-md-6">
               <div class="form-group row">
                 <label class="col-md-3 col-form-label" for="role">权限</label>
                 <div class="col-md-9">
                   <select name="role" class="form-control ">
                     @foreach($vm->GetRoles() as $k=>$v)
                       @if($k == $vm->dto->status_id)
                        <option value="{{$k}}" selected>{{$v}}</option>
                       @else
                        <option value="{{$k}}">{{$v}}</option>
                       @endif
                     @endforeach
                   </select>
                 </div>
               </div><!--//form-group-->
             </div>

             <div class="col-md-6">
               <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="status">状态</label>
                  <div class="col-md-9">
                    <select name="status" class="form-control ">
                      @foreach($vm->GetStatuses() as $k=>$v)
                        @if($k == $vm->dto->status_id)
                         <option value="{{$k}}" selected>{{$v}}</option>
                        @else
                         <option value="{{$k}}">{{$v}}</option>
                        @endif
                      @endforeach
                    </select>
                  </div><!--//col-->
               </div><!--//form-group-->
             </div>

          </div><!--row-->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="name">姓名</label>
                <div class="col-md-9">
                  <input type="text" class="form-control"  name="name" placeholder="" required value="{{$vm->dto->name}}" />
                </div><!--//col-->
              </div><!--//form-group-->
            </div>

            <div class="col-md-6">
               <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="email">电邮</label>
                  <div class="col-md-9">
                     <input type="email" class="form-control" name="email" placeholder="" required value="{{$vm->dto->email}}"  />
                  </div><!--//col-->
                </div><!--//form-group-->
            </div>
          </div><!--row-->

        </form>
        <!-- // form-->
      </div>
    </div>
  </div>

  <div class="col-lg-4 col-sm-4 grid-margin">
      <div class="card">
        <div class="card-header header-sm">
          <div class="wrapper d-flex align-items-center">
            <h2 class="card-title">更改密码</h2>
          </div>
        </div>
        <div class="card-body">
          <!-- form -->
          <form method="POST" id="form-password" action="{{url('/user/reset')}}"  class="forms-sample">
            @csrf
           <input type="hidden" name="id" value="{{$vm->dto->id}}" />

           <div class="form-group row">
             <label class="col-md-3 col-form-label" for="password">密码</label>
             <div class="col-md-9">
               <input type="password" class="form-control"  name="password" placeholder="" required maxlength="50"/>
             </div>
           </div><!-- form-group-->
          <div class="form-group row">
             <label class="col-md-3 col-form-label" for="password2">确认密码</label>
             <div class="col-md-9">
               <input type="password" class="form-control" name="password2" placeholder="" required maxlength="50" />
             </div>
          </div><!-- form-group-->
          <div class="row justify-content-end">
             <div class="col-md-9">
               <button class="btn btn-primary btn-block btn-fw" id="btnReset" type="button">
                 <i class="fa fa-lock"></i>更改密码
               </button>
             </div>
           </div>
         </form>
         <!-- // form-->
        </div><!--card-body-->
      </div><!--card-->
    </div>
</div><!-- row -->

<form method="POST" id="form-delete" action="{{url('/user/delete')}}">
  @csrf
  <input type="hidden" name="id" value="{{$vm->dto->id}}" />
</form>
<!-- // form-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
       <div class="modal-content">
           <div class="modal-header">
               <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除用户</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
           </div>
           <div class="modal-body">
               <p>请确认是否要删除此用户?</p>
           </div>
           <div class="modal-footer">
               <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
               <button data-dismiss="modal" class="btn btn-secondary">取消</button>
           </div>
       </div>
   </div>
</div>
@endsection
@section('footer')
{!! JsValidator::formRequest('App\Http\Requests\Admin\UserInfoRequest', '#form-user'); !!}
{!! JsValidator::formRequest('App\Http\Requests\Admin\ResetUserPasswordRequest', '#form-password'); !!}
<script>
$('#form-user').submit(function (e) {
    e.preventDefault();
    if (!$(this).valid()) return false;
    var _btn = $('button[type=submit]', this);
    startSpin(_btn);
    $.ajax({
        url: this.action,
        type: this.method,
        data: $(this).serialize(),
    }).fail(function(xhr, text, err) {
       notifySystemError(err);
    }).done(function(data) {
      notifySuccess('用户已修改。');
    setTimeout(function(){ location.href='{{url('/users')}}'; }, 2000);
    }).always(function() {
        stopSpin(_btn);
    });
});

$('#btnModalConfirmDeleteOK').on('click', function() {
   //$('#form-delete').submit();
//  e.preventDefault();
   if (!$('#form-delete').valid()) return false;

   startSpin($('#btnDelete'));
   $.ajax({
       url: '{{url('/user/delete')}}',
       type: 'POST',
       data: $('#form-delete').serialize(),
   }).fail(function(xhr, text, err) {
      notifySystemError(err);
   }).done(function(data) {
     $('#modalConfirmDelete').modal('hide');
     if(data['error']){
       notifySystemError(data['error']);
     }else{
       notifySuccess('用户已删除。');
       setTimeout(function(){ location.href='{{url('/users')}}'; }, 2000);
     }


   }).always(function() {
       stopSpin($('#btnDelete'));
   });
});

$('#btnReset').on('click', function() {
    if (!$('#form-password').valid()) return false;
     startSpin($('#btnReset'));
    $.ajax({
        url: '{{url('/user/reset')}}',
        type: 'POST',
        data: $('#form-password').serialize(),
    }).fail(function(xhr, text, err) {
       notifySystemError(err);
    }).done(function(data) {
      notifySuccess('密码已更改。');
      setTimeout(function(){ location.href='{{url('/users')}}'; }, 2000);
    }).always(function() {
        stopSpin(_btn);
    });
});
</script>
@endsection
