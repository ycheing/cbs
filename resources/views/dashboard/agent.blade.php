@extends('layouts.agent')
@section('title', 'Dashboard')
@section('content')

<div class="row ">

  <div class="col-md-12 text-center">
    <img src="{{asset('images/logo-lg.png')}}" style="width:200px" />
    <h2>T & O GLOBAL RESOURCES <small>(PG0225049-A)</small></h2>
    <p><strong>Address</strong>: No. 1, Jalan Nilam, Bagan Ajam,<br/>
       13000 Butterworth,<br/>
      Penang, Malaysia.<br/>
      <strong>Tel</strong>: (6)012-401 6680
    </p>
    <p>
      <strong class="text-uppercase">We are providing the following services:</strong><br/>
      FCL services from China to all the international major ports.<br/>
      LCL services from China to all the major ports in Malaysia.<br/>
      Place the orders, translation, order supervisor, container loading etc.
    </p>
    <p>
      <strong>我们为您提供以下服务：</strong><br/>
      从中国至世界各地各港口国际海运。<br/>
      从中国至马来西亚各主要港口FCL或LCL散货拼柜。<br/>
      采购，翻译，跟单，监装服务等。
    </p>

  </div>
</div>
@endsection
