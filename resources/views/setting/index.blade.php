@extends('layouts.app')
@section('title', '系统设置')
@section('content')

<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <!-- form -->
    <form method="post" id="form-setting" action="{{url('/setting')}}" >
        @csrf
    <div class="card">
      <div class="card-header header-sm">
        <div class="d-flex ">
            <div class="wrapper d-flex align-items-center media-info">
              <h2 class="card-title"><i class="fa fa-cog mr-2"></i>系统设置</h2>
            </div>
            <div class="wrapper ml-auto action-bar">
              <button type="submit" data-toggle="tooltip" data-placement="top" data-original-title="储存" class="btn btn-icons btn-success btn-sm"><i class="fa fa-save"></i></button>
            </div>
        </div>
      </div><!--//card-header-->
      <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label"  for="setting[0]['value']">汇率</label>
                <div class="col-md-9">
                  <input type="hidden" name="setting[0][setting_key]" value="exchange_rate" />
                  <input type="text" class="form-control"  name="setting[0][setting_value]" value="{{$vm->GetValue('exchange_rate')}}" />
                </div>
              </div>
             </div><!--//col-->


          </div>
      </div>
    </div>
  </form>
  <!-- // form-->
  </div>
</div>
@endsection
@section('footer')
{!! JsValidator::formRequest('App\Http\Requests\Admin\SettingRequest', '#form-setting'); !!}
<script>
  $('#form-setting').submit(function (e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      if (!$(this).valid()) return false;
      var _btn = $('button[type=submit]', this);
      startSpin(_btn);
      $.ajax({
          url: this.action,
          type: this.method,
          data: $(this).serialize(),
      }).fail(function(xhr, text, err) {
         notifySystemError(err);
      }).done(function(data) {
        notifySuccess('系统设置已修改。');
      }).always(function() {
          stopSpin(_btn);
      });
  });
</script>
@endsection
