@extends('layouts.app')
@section('title', '公司清单')
@section('content')
<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center media-info">
            <h2 class="card-title">公司清单</h2>
          </div>
          <div class="wrapper ml-auto action-bar">
            <a href="{{url('/company/add')}}" class="btn btn-primary btn-block"><i class="fa fa-plus "></i>添加公司</a>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                  <tr>
                    <th style="width:10%"></th>
                    <th style="width:10%" class="text-center">@sortablelink('status', '状态')</th>
                    <th>@sortablelink('code', '公司编号')</th>
                    <th>@sortablelink('name', '公司名称')</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($vm->dto as $company)
                  <tr>
                    <td>
                      <a href="{{url('/company/edit/'.$company->id.'')}}" class="btn btn-icons btn-success btn-action "><i class="fa fa-pencil"></i></a>
                      <button type="button" class="btn btn-icons btn-danger btn-delete" data-id="{{$company->id}}"><i class="fa fa-trash"></i></button>
                    </td>
                    <td class="text-center">{!! $company->status !!}</td>
                     <td>{{$company->code}}</td>
                     <td>{{$company->name}} </td>
                  </tr>
                 @endforeach
                </tbody>

            </table>
        </div><!--table-responsive-->
        <div class="mt-5 ">
          {{$vm->paging->links()}}
        </div>
      </div>
    </div>
  </div>
</div>

<form method="POST" id="form-delete" action="{{url('/company/delete')}}">
@csrf
<input type="hidden" id="company_id" name="id" value="" />
</form>
<!-- // form-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除公司</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <p>请确认是否要删除此公司?</p>
          </div>
          <div class="modal-footer">
            <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
            <button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">取消</button>
          </div>
      </div>
  </div>
</div>

@endsection
@section('footer')
<script>
    $('.btn-delete').on('click', function() {
       $('#modalConfirmDelete').modal();
       $('#company_id').val($(this).attr('data-id'));
   });

   $('#btnModalConfirmDeleteOK').on('click', function() {
       if (!$('#form-delete').valid()) return false;

       $.ajax({
           url: '{{url('/company/delete')}}',
           type: 'POST',
           data: $('#form-delete').serialize(),
       }).fail(function(xhr, text, err) {
          notifySystemError(err);
       }).done(function(data) {
         $('#modalConfirmDelete').modal('hide');
         if(data['error']){
           notifySystemError(data['error']);
         }else{
           notifySuccess('公司已删除。');
           setTimeout(function(){ location.href='{{url('/companies')}}'; }, 2000);
         }
       }).always(function() {
       });
   });
</script>

@endsection
