@extends('layouts.agent')
@section('title', '客户名单')
@section('content')
<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center media-info">
            <h2 class="card-title">客户名单</h2>
          </div>
          <div class="wrapper ml-auto action-bar">
            <a href="{{url('/agent/customer/add')}}" class="btn btn-primary btn-block"><i class="fa fa-plus "></i>添加新客户</a>
          </div>
        </div>
      </div>


      <div class="card-body">
        <div class="page-header-toolbar">
          <form method="GET" action="{{url('/agent/customers')}}" style="width:100%">
            <div class="form-group row">
              <div class="col-md-6">
                <input name="keyword" id="keyword" placeholder="关键词" class="form-control"/>
              </div>
              <div class="col-md-1">
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </form>
        </div>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th style="width:10%"></th>
                <th style="width:10%" class="text-center">@sortablelink('status', '状态')</th>
                <th>@sortablelink('name', '客户编号')</th>
                <th>@sortablelink('name', '客户名称')</th>
                <th>@sortablelink('attn', '联系人')</th>
                <th>@sortablelink('phone', '电话号码')</th>
                <th>@sortablelink('created_by', '创建人')</th>
              </tr>
            </thead>
            <tbody>
              @foreach($vm->dto as $customer)
              <tr>
                <td>
                  <a href="{{url('/agent/customer/edit/'.$customer->id.'')}}" class="btn btn-icons btn-success btn-action "><i class="fa fa-pencil"></i></a>
                  <button type="button" class="btn btn-icons btn-danger  btn-delete" data-id="{{$customer->id}}"><i class="fa fa-trash"></i></button>
                </td>
                <td class="text-center">{!! $customer->status !!}</td>
                <td>{{$customer->code}} </td>
                <td>{{$customer->name}} </td>
                <td>{{$customer->attn}} </td>
                <td>{{$customer->phone}} </td>
                <td>{{$customer->created_by}} </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div><!--table-responsive-->
        <div class="mt-5 ">
          {{$vm->paging->links()}}
        </div>
      </div><!--card-body-->
    </div>
  </div>
</div>

<form method="POST" id="form-delete" action="{{url('/agent/customer/delete')}}">
  @csrf
  <input type="hidden" id="customer_id" name="id" value="" />
</form>
<!-- // form-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除客户</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>请确认您要删除此客户？</p>
            </div>
            <div class="modal-footer">
                <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
                <button id="btnModalConfirmDeleteCancel" class="btn btn-secondary">取消</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('footer')
<script>
    $('.btn-delete').on('click', function() {
       $('#modalConfirmDelete').modal();
       $('#customer_id').val($(this).attr('data-id'));
   });

   $('#btnModalConfirmDeleteCancel').on('click', function() {
       $('#modalConfirmDelete').modal('hide');
   });

   $('#btnModalConfirmDeleteOK').on('click', function() {
       if (!$('#form-delete').valid()) return false;

       $.ajax({
           url: '{{url('/agent/customer/delete')}}',
           type: 'POST',
           data: $('#form-delete').serialize(),
       }).fail(function(xhr, text, err) {
          notifySystemError(err);
       }).done(function(data) {
         $('#modalConfirmDelete').modal('hide');
         if(data['error']){
           notifySystemError(data['error']);
         }else{
           notifySuccess('客户已删除');
           setTimeout(function(){ location.href='{{url('/agent/customers')}}'; }, 2000);
         }
       }).always(function() {
       });
   });
</script>

@endsection
