@extends('layouts.agent')
@section('title', '个人资料')
@section('content')

    <div class="row">
      <div class="col-lg-6 col-sm-12 grid-margin">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">个人资料</h4>
            <form method="POST" action="{{ url('/profile') }}" id="form-profile">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}" />
                <div class="form-group row">
                  <div class="col">
                      <label for="username">用户名</label>
                      <label for="username">{{$data->username}}</label>
                   </div><!--//col-->
                </div>
                <div class="form-group row">
                <div class="col">
                    <label for="name">姓名</label>
                    <input type="text" class="form-control" value="{{$data->name}}" name="name" placeholder="Name" required/>
                 </div><!--//col-->
                   <div class="col">
                     <label for="email">电邮</label>
                     <input type="email" class="form-control" value="{{$data->email}}" name="email" placeholder="Email" required />
                 </div><!--//col-->

                </div>

                <div class="form-group ">
                    <button type="submit" class="btn btn-primary submit-btn btn-block">
                      更改
                    </button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('footer')

    {!! JsValidator::formRequest('App\Http\Requests\Account\ProfileRequest', '#form-profile'); !!}
    <script>
        $('#form-profile').submit(function (e) {
            e.preventDefault();
            if (!$(this).valid()) return false;
            var _btn = $('button[type=submit]', this);
            startSpin(_btn);
            $.ajax({
                url: this.action,
                type: this.method,
                data: $(this).serialize(),
            }).fail(function(xhr, text, err) {
               notifySystemError(err);
            }).done(function(data) {
              notifySuccess('个人资料已更改。');
              setTimeout(function(){ location.href='{{url('/home')}}'; }, 2000);
            }).always(function() {
                stopSpin(_btn);
            });
        });
    </script>

@endsection
