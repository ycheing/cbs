@extends('layouts.agent')
@section('title', '散货清单')
@section('content')
<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center media-info">
            <h2 class="card-title">散货清单</h2>
          </div>
          <div class="wrapper ml-auto action-bar">
            <a href="{{url('/agent/cargo/add')}}" class="btn btn-primary btn-block"><i class="fa fa-plus "></i>添加订单</a>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="page-header-toolbar">
          <form method="GET" action="{{url('/agent/cargos')}}" style="width:100%">
            <div class="form-group row">
              <div class="col-md-6">
                <input name="keyword" id="keyword" placeholder="关键词" class="form-control"/>
              </div>
              <div class="col-md-1">
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </form>
        </div>
        <div class="table-responsive">
          <table class="table table-striped" >
            <thead>
              <tr>
                <th></th>
                <th>@sortablelink('date', ' 日期')</th>
                <th>@sortablelink('arrival_date', ' 预计到港日期')</th>
                <th>@sortablelink('customer_code', ' 客户代码')</th>
                <th>@sortablelink('customs_broker_id', ' 清关行')</th>
                <th>@sortablelink('warehouse_id', ' 收货仓库')</th>
                <th>@sortablelink('owner_id', ' 负责人')</th>
                <th>@sortablelink('tracking_no', ' 追踪编号')</th>
                <th>@sortablelink('cbm', ' 立方数')</th>
                <th>@sortablelink('pack', ' 箱数')</th>
              </tr>
            </thead>
            <tbody>
              @foreach($vm->dto as $cargo)
              <tr>
                <td>
                  <a href="{{url('agent/cargo/edit/'.$cargo->id.'')}}" class="btn btn-icons btn-success btn-action "><i class="fa fa-pencil"></i></a>
                  <button type="button" class="btn btn-icons btn-danger btn-delete" data-id="{{$cargo->id}}" data-owner-id="{{$cargo->owner_id}}"><i class="fa fa-trash"></i></button>
                </td>
                <td>{{$cargo->date}}</td>
                <td>{{$cargo->arrival_date}}</td>
                <td>{{$cargo->customer}}</td>
                <td>{{$cargo->customs_broker}}</td>
                <td>{{$cargo->warehouse}}</td>
                <td>{{$cargo->owner}}</td>
                <td>{{$cargo->tracking_no}}</td>
                <td class="text-center">{{$cargo->cbm}}</td>
                <td class="text-center">{{$cargo->pack}}</td>
              </tr>
              @endforeach

            </tbody>
          </table>
        </div>
        <!--table-responsive-->
        <div class="mt-5 ">
          {{$vm->paging->links()}}
        </div>
      </div><!--card-body-->
    </div>
  </div>
</div>

<form method="POST" id="form-delete" action="{{url('/agent/cargo/delete')}}">
  @csrf
  <input type="hidden" id="cargo_id" name="id" value="" />
</form>
<!-- // form-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除散货</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <p>请确认是否要删除此散货?</p>
          </div>
          <div class="modal-footer">
            <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
            <button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">取消</button>
          </div>
      </div>
  </div>
</div>
<!-- // cannot delete-->
<div class="modal fade" id="modalRejectDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="modalConfirmDeleteLabel">无法删除此散货。</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <p>对不起，你无法删除此散货。</p>
          </div>
      </div>
  </div>
</div>
@endsection
@section('footer')
<script>
  $('.btn-delete').on('click', function() {
      var owner_id = $(this).attr('data-owner-id');
      if(owner_id == {{Auth::user()->id}}){
        $('#modalConfirmDelete').modal();
        $('#cargo_id').val($(this).attr('data-id'));
      }else{
        $('#modalRejectDelete').modal();
      }
  });

   $('#btnModalConfirmDeleteOK').on('click', function() {
       if (!$('#form-delete').valid()) return false;

       $.ajax({
           url: '{{url('/agent/cargo/delete')}}',
           type: 'POST',
           data: $('#form-delete').serialize(),
       }).fail(function(xhr, text, err) {
          notifySystemError(err);
       }).done(function(data) {
         $('#modalConfirmDelete').modal('hide');
         if(data['error']){
           notifySystemError(data['error']);
         }else{
           notifySuccess('散货已删除。');
           setTimeout(function(){ location.href='{{url('/agent/cargos')}}'; }, 2000);
         }
       }).always(function() {
       });
   });
</script>

@endsection
