@extends('layouts.agent')
@section('title', '修改散货')
@section('content')

<div class="row">

  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <!-- form -->
      <form method="post" id="form-cargo" action="{{url('agent/cargo/edit')}}" >
        @csrf
        <input type="hidden" name="id" value="{{$vm->dto->id}}" />
        <div class="card-header header-sm ">
          <div class="d-flex ">
              <div class="wrapper d-flex align-items-center">
                <h2 class="card-title mb4">修改散货</h2>
              </div>
              <div class="wrapper ml-auto action-bar">
                 @if($vm->dto->owner_id == Auth::user()->id)
                <button type="submit" class="btn btn-icons btn-success btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="储存"><i class="fa fa-save"></i></button>
                <button  type="button" class="btn btn-icons btn-danger btn-sm" id="btnDelete"  data-toggle="modal" data-target="#modalConfirmDelete">
                  <i class="fa fa-trash"></i>
                </button>
                @endif
                <a class="btn btn-icons btn-outline-primary btn-sm"  data-toggle="tooltip" data-placement="top" data-original-title="返回"  href="{{url('agent/cargos')}}"><i class="fa fa-close"></i></a>

              </div>
          </div>
        </div><!--//card-header-->

        <div class="card-body">

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label"  for="date">日期</label>
                <div class="col-md-9">
                 <div class="input-group ">
                   <input id="date" type="text" class="form-control date" name="date"  data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->date}}"/>
                   <div class="input-group-append">
                        <span> <i class="fa fa-calendar"></i></span>
                   </div>
                 </div>
                </div>
              </div>
            </div><!-- col-md-6-->


            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="arrival_date">预计到港日期</label>
                <div class="col-md-9">
                 <div class="input-group ">
                   <input id="arrival_date" type="text" class="form-control date" name="arrival_date" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->arrival_date}}"/>
                   <div class="input-group-append">
                      <span><i class="fa fa-calendar"></i></span>
                   </div>
                 </div>
                </div>
              </div>
            </div><!-- col-md-6-->

        </div>

        <div class="row">
            

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="loading_date">装柜日期</label>
                <div class="col-md-9">
                 <div class="input-group ">
                   <input id="loading_date" type="text" class="form-control date" name="loading_date" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->loading_date}}"/>
                   <div class="input-group-append">
                      <span><i class="fa fa-calendar"></i></span>
                   </div>
                 </div>
                </div>
              </div>
            </div><!-- col-md-6-->

            <div class="col-md-6">
              
            </div><!-- col-md-6-->

          </div><!--//row-->

        <div class="row">

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="status_id">状态</label>
                <div class="col-md-9">
                 <select name="status_id" class="form-control ">
                   @foreach($vm->GetStatuses() as $k => $v)
                     @if($k == $vm->dto->status_id)
                      <option value="{{$k}}" selected>{{$v}}</option>
                     @else
                      <option value="{{$k}}">{{$v}}</option>
                     @endif
                   @endforeach
                 </select>
               </div><!--//col-md-9-->
             </div><!--//form-group-->
            </div><!-- col-md-6-->
          </div><!--//row-->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_id">客户代码</label>
                <div class="col-md-9">
                  <select name="customer_id" class="select2 form-control">
                    <option value=""></option>
                    @foreach($vm->GetCustomers() as $customer)
                     @if($vm->dto->customer_id == $customer->id)
                       <option value="{{$customer->id}}" selected>{{$customer->code}}</option>
                      @else
                        <option value="{{$customer->id}}">{{$customer->code}}</option>
                      @endif
                    @endforeach
                  </select>
                </div><!--//col-md-9-->
              </div><!--//form-group-->
            </div><!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customs_broker_id">清关行</label>
                <div class="col-md-9">
                  <select name="customs_broker_id" class="select2 form-control">
                    <option value=""></option>
                    @foreach($vm->GetCustomsBrokers() as $cb)
                     @if($vm->dto->customs_broker_id == $cb->id)
                       <option value="{{$cb->id}}" selected>{{$cb->code}}</option>
                      @else
                        <option value="{{$cb->id}}">{{$cb->code}}</option>
                      @endif
                    @endforeach
                  </select>
                </div><!--//col-md-9-->
              </div><!--//form-group-->
            </div>
          </div><!--row-->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="warehouse_id">收货仓库</label>
                <div class="col-md-9">
                  <select name="warehouse_id" class="select2 form-control">
                    <option value=""></option>
                    @foreach($vm->GetWarehouses() as $wareshouse)
                      @if($vm->dto->warehouse_id == $wareshouse->id)
                      <option value="{{$wareshouse->id}}" selected>{{$wareshouse->code}}</option>
                      @else
                      <option value="{{$wareshouse->id}}">{{$wareshouse->code}}</option>
                      @endif
                    @endforeach
                  </select>
                </div><!--//col-md-9-->
              </div><!--//form-group-->
            </div><!--//col-->

            <div class="col-md-6">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="tracking_no">追踪编号</label>
                  <div class="col-md-9">
                    <input type="text" id="tracking_no" class=" form-control"  name="tracking_no" placeholder=""  value="{{$vm->dto->tracking_no}}" />
                  </div><!--//col-->
              </div>
            </div><!--//col-->
          </div><!--row-->


          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="cbm">立方数</label>
                  <div class="col-md-9">
                    <input type="text" id="cbm" class=" form-control"  name="cbm" placeholder=""  value="{{$vm->dto->cbm}}"/>
                  </div><!--//col-->
              </div>

            </div><!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="pack">箱数</label>
                  <div class="col-md-9">
                    <input type="text" id="pack" class=" form-control"  name="pack" placeholder=""  value="{{$vm->dto->pack}}"/>
                  </div><!--//col-->
              </div>
            </div><!--//col-->
          </div><!--row-->



        </div> <!--card body-->

      </form>
      <!-- // form-->
    </div><!--card -->
  </div>
</div>

<form method="POST" id="form-delete" action="{{url('/agent/cargo/delete')}}">
  @csrf
  <input type="hidden" name="id" value="{{$vm->dto->id}}" />
</form>
<!-- // form-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除散货</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>请确认是否要删除此散货?</p>
            </div>
            <div class="modal-footer">
                <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
                <button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">取消</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')

{!! JsValidator::formRequest('App\Http\Requests\Agent\CargoInfoRequest', '#form-cargo'); !!}
<script>

$('#form-cargo').submit(function (e) {
    e.preventDefault();
    if (!$(this).valid()) return false;
    var _btn = $('button[type=submit]', this);
    startSpin(_btn);
    var formData = new FormData($(this)[0]);
    // var _data = formToJSON($(this)[0]);
    $.ajax({
        url: this.action,
        type: this.method,
        data: formData,
        enctype: 'multipart/form-data',
        contentType: false,
        processData: false,
    }).fail(function(xhr, text, err) {
       notifySystemError(err);
    }).done(function(data) {
      notifySuccess('散货已修改。');
      setTimeout(function(){ location.href='{{url('/agent/cargos')}}'; }, 2000);
    }).always(function() {
        stopSpin(_btn);
    });
});

$('#btnModalConfirmDeleteOK').on('click', function() {
   if (!$('#form-delete').valid()) return false;
   startSpin($('#btnDelete'));
   $.ajax({
       url: '{{url('/agent/cargo/delete')}}',
       type: 'POST',
       data: $('#form-delete').serialize(),
   }).fail(function(xhr, text, err) {
      notifySystemError(err);
   }).done(function(data) {
     $('#modalConfirmDelete').modal('hide');
     if(data['error']){
       notifySystemError(data['error']);
     }else{
       notifySuccess('散货已删除。');
       setTimeout(function(){ location.href='{{url('/agent/cargos')}}'; }, 2000);
     }

   }).always(function() {
       stopSpin($('#btnDelete'));
   });
});

</script>

@endsection
