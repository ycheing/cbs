<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
  <table>
    <thead>
      <tr >
        <th align="center"><b>No.</b></th>
        <th align="center"><b>装柜日期</b></th>
        <th align="center"><b>客户名称</b></th>
        <th align="center"><b>货柜号码</b></th>
        <th align="center"><b>预定号</b></th>
        <th align="center"><b>{{$data['name']}}</b></th>
      </tr>
    </thead>
    <tbody>
      @foreach($data['results'] as $r)
      <tr>
        <td align="center">{{$r['no']}}</td>
        <td align="center">{{$r['loading_date']}}</td>
        <td>{{$r['customer_name']}}</td>
        <td align="center">{{$r['container_no']}}</td>
        <td align="center">{{$r['booking_no']}}</td>
        <td>{{$r['jobs']}}</td>
      </tr>
      @endforeach
    </tbody>
    <tfoot>
        <tr>
      <td colspan="5">
      @foreach($data['footer'] as $ft)
      <td>
        {{$ft['name']}}
        @foreach($ft['job'] as $jk => $jv)
        {{$jk}} : {{$jv}} <br/>
        @endforeach
      </td>
      @endforeach
        </tr>
    </tfoot>
  </table>
</body>
</html>
