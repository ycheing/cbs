<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>散货记录表</title>
<style>
  body{
    font-family: sans-serif;
    font-size: 12px;
  }
  table{
    font-size: 12px;
    width: 100%;
    border-collapse: collapse;
    padding: 0;
    border: solid 1px #000;
  }
  table th{
    border: solid 1px #000;
    padding: 4px;
  }
  table td{
    border: solid 1px #000;
    padding: 4px;
  }
</style>
</head>
<body>
<table>
  <thead>
    <tr>
      <td style="text-align: center" colspan="17" ><b>{{$data['title']}}</b></td>
    </tr>
    <tr>
      <th align="center">No.</th>
      <th align="center">日期</th>
      <th align="center">装柜日期</th>
      <th align="center">到港日期</th>
      <th align="center">客户代码</th>
      <th align="center">清关行</th>
      <th align="center">收货仓库</th>
      <th align="center">负责人</th>
      <th align="center">追踪编号</th>
      <th align="center">立方数</th>
      <th align="center">箱数</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data['results'] as $v)
    <tr>
      <td align="center">{{$v['no']}}</td>
      <td>{{$v['date']}}</td>
      <td>{{$v['loading_date']}}</td>
      <td>{{$v['arrival_date']}}</td>
      <td>{{$v['customer']}}</td>
      <td>{{$v['customs_broker']}}</td>
      <td>{{$v['warehouse']}}</td>
      <td>{{$v['owner']}}</td>
      <td>{{$v['tracking_no']}}</td>
      <td align="center">{{$v['cbm']}}</td>
      <td align="center">{{$v['pack']}}</td>
    </tr>
    @endforeach
  </tbody>

</table>
