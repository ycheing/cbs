@extends('layouts.agent')
@section('title', '装柜报表')
@section('content')

<div class="row page-title-header">
  <div class="col-12">
    <div class="page-header">
      <h4 class="page-title">货柜监装报表</h4>
    </div>
  </div>
  <div class="col-md-12">
    <div class="page-header-toolbar">
      <form method="GET" action="{{url('/agent/report/container_loading')}}" style="width:100%">
        <div class="row">
          <div class="col-md-2">
            <div class="form-group row">
              <label class="col-4 col-form-label " for="year" >年</label>
              <div class="col-8">
                <select class="form-control " name="year" id="year">
                  @foreach($vm->GetYears() as $k => $v)
                    @if($v == $vm->inputs['year'])
                    <option value="{{$v}}" selected>{{$v}}</option>
                    @else
                    <option value="{{$v}}" >{{$v}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="month" >月</label>
              <div class="col-8">
                <select class="form-control" name="month" id="month">
                  @foreach($vm->GetMonths() as $k => $v)
                    @if($k== $vm->inputs['month'])
                      <option value="{{$k}}" selected>{{$v}}</option>
                    @else
                      <option value="{{$k}}">{{$v}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="customer_id" >客户</label>
              <div class="col-8">
                <select name="customer_id[]" id="customer_id" class="select2 form-control" multiple>
                  @foreach($vm->GetCustomers() as $customer)
                    @if(in_array($customer->id, $vm->inputs['customer_id']))
                      <option value="{{$customer->id}}" selected>{{$customer->code}}</option>
                    @else
                      <option value="{{$customer->id}}">{{$customer->code}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="agent_id" >代理</label>
              <div class="col-8">
                <select name="agent_id" id="agent_id" class="select2 form-control">
                  <option value="">全部</option>
                  @foreach($vm->GetAgents() as $agent)
                    @if($agent->id == $vm->inputs['agent_id'])
                      <option value="{{$agent->id}}" selected>{{$agent->name}}</option>
                    @else
                      <option value="{{$agent->id}}">{{$agent->name}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-1">
            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm ">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center media-info">
          </div>
          <div class="wrapper ml-auto action-bar">
            <button type="button" id="btnExport" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o mr-2"></i>输出 Excel</button>
            <button type="button" id="btnPrint" class="btn btn-info btn-sm"><i class="fa fa-print mr-2"></i>打印</button>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead class=" text-center">
                <tr>
                <th style="width:5%" class="text-center">No.</th>
                <th class="text-center">装柜日期</th>
                <th class="text-center">到港日期</th>
                <th>货柜费用<br/>(RMB)</th>
                <th>客户</th>
                <th>货柜号码</th>
                <th>预订号</th>
                <th>货值</th>
                <th class="text-center">Form E</th>
                <th>代理</th>
                <th>异地费</th>
                <th>船公司</th>
              </tr>
            </thead>
            <tbody>
              @foreach($vm->dto as $k => $v)
              <tr>
                <td class="text-center">{{$k+1}}</td>
                <td class="text-center">{{$v->loading_date}}</td>
                <td class="text-center">{{$v->eta_date}}</td>
                <td class="text-right">{{$v->handling_charges}}</td>
                <td>{{$v->customer_name}}</td>
                <td class="text-center">{{$v->container_no}}</td>
                <td class="text-center"><a href="{{url('/agent/booking/loading/'.$v->id.'')}}">{{$v->booking_no}}</a></td>
                <td class="text-right">{{$v->goods_value}}</td>
                <td class="text-center">
                  @if($v->forme_apply == 1)
                    <label class="badge badge-success">YES</span>
                  @else
                    <label class="badge badge-danger">NO</span>
                  @endif
                </td>
                <td class="text-center">{{$v->agent_name}}</td>
                <td class="text-right">{{$v->remote_fee}}</td>
                <td class="text-center">{{$v->shipping_company}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  <div>
</div>
@endsection
@section('footer')
<script type="text/javascript">

  $('#btnExport').on('click', function() {
    var _val = '?year=' +  $('#year').val();
    _val += '&month=' +  $('#month').val();
    _val += '&customer_id=' +  $('#customer_id').val();
    _val += '&agent_id=' +  $('#agent_id').val();
    window.location.href = '{{url('/agent/report/export/container_loading')}}' + _val;

  });

  $('#btnPrint').on('click', function() {
    var _val = '?year=' +  $('#year').val();
    _val += '&month=' +  $('#month').val();
    _val += '&customer_id=' +  $('#customer_id').val();
    _val += '&agent_id=' +  $('#agent_id').val();
    window.open('{{url('/agent/report/print/container_loading')}}' + _val);
  });
</script>
@endsection
