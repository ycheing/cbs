@extends('layouts.agent')
@section('title', '散货记录表')
@section('content')
<div class="row page-title-header">
  <div class="col-12">
    <div class="page-header">
      <h4 class="page-title">散货记录表</h4>
    </div>
  </div>
  <div class="col-12">
    <div class="page-header-toolbar" >
      <form id="form-search" method="GET" action="{{url('/agent/report/cargo_record')}}" style="width:100%">
        <div class="row">
          <div class="col-md-3 ">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="date_from" >开始日期</label>
              <div class="col-8">
                <div class="input-group ">
                  <input id="date_from" type="text" class="date form-control " name="date_from" value="{{$vm->inputs['date_from']}}" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                  <div class="input-group-append">
                     <span> <i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="date_to" >结束日期</label>
              <div class="col-8">
                <div class="input-group">
                  <input id="date_to" type="text" class="date form-control " name="date_to" value="{{$vm->inputs['date_to']}}" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                  <div class="input-group-append">
                     <span> <i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="customer_id" >客户</label>
              <div class="col-8">
                <select name="customer_id" id="customer_id" class="select2 form-control">
                  <option value="">全部</option>
                  @foreach($vm->GetCustomers() as $customer)
                    @if($customer->id == $vm->inputs['customer_id'])
                      <option value="{{$customer->id}}" selected>{{$customer->code}}</option>
                    @else
                      <option value="{{$customer->id}}">{{$customer->code}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="customs_broker_id" >清关行</label>
              <div class="col-8">
                <select name="customs_broker_id" id="customs_broker_id" class="select2 form-control">
                  <option value="">全部</option>
                  @foreach($vm->GetCustomsBrokers() as $customs)
                    @if($customs->id == $vm->inputs['customs_broker_id'])
                      <option value="{{$customs->id}}" selected>{{$customs->code}}</option>
                    @else
                      <option value="{{$customs->id}}">{{$customs->code}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>

        </div>
        <div class="row">
          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="tracking_no" >追踪编号</label>
              <div class="col-8">
                  <input id="tracking_no" type="text" class=" form-control " name="tracking_no" value="{{$vm->inputs['tracking_no']}}"/>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="owner_id" >负责人</label>
              <div class="col-8">
                <select name="owner_id" id="owner_id" class="select2 form-control">
                  <option value="">全部</option>
                  @foreach($vm->GetEmployees() as $employee)
                    @if($employee->user_id == $vm->inputs['owner_id'])
                    <option value="{{$employee->user_id}}" selected>{{$employee->name}}</option>
                    @else
                    <option value="{{$employee->user_id}}">{{$employee->name}}</option>
                    @endif
                   @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="warehouse_id" >收货仓库</label>
              <div class="col-8">
                <select name="warehouse_id" id="warehouse_id" class="select2 form-control">
                  <option value="">全部</option>
                  @foreach($vm->GetWarehouses() as $wareshouse)
                      @if($vm->inputs['warehouse_id']== $wareshouse->id)
                      <option value="{{$wareshouse->id}}" selected>{{$wareshouse->code}}</option>
                      @else
                      <option value="{{$wareshouse->id}}">{{$wareshouse->code}}</option>
                      @endif
                    @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-1">
            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm ">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center media-info">
          </div>
          <div class="wrapper ml-auto action-bar">
            <button type="button" id="btnExport" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o mr-2"></i>输出 Excel</button>
            <button type="button" id="btnPrint" class="btn btn-info btn-sm"><i class="fa fa-print mr-2"></i>打印</button>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered"  style="border:solid 3px #000">
            <thead class="text-center">
              <tr>
                <th style="width:5%">No.</th>
                <th>日期</th>
                <th>装柜日期</th>
                <th>到港日期</th>
                <th>客户代码</th>
                <th>清关行</th>
                <th>收货仓库</th>
                <th>负责人</th>
                <th>追踪编号</th>
                <th>立方数</th>
                <th>箱数</th>
              </tr>
            </thead>
            <tbody>
              @foreach($vm->dto as $k => $v)
              <tr>
                <td class="text-center">{{$k+1}}</td>
                <td>{{$v->date}}</td>
                <td>{{$v->loading_date}}</td>
                <td>{{$v->arrival_date}}</td>
                <td><a href="{{url('agent/cargo/edit/'.$v->id.'')}}">{{$v->customer}}</a></td>
                <td>{{$v->customs_broker}}</td>
                <td>{{$v->warehouse}}</td>
                <td>{{$v->owner}}</td>
                <td>{{$v->tracking_no}}</td>
                <td class="text-center">{{$v->cbm}}</td>
                <td class="text-center">{{$v->pack}}</td>
              </tr>
              @endforeach
            </tbody>
           
          </table>
        </div>
      </div>
    </div>
  <div>
</div>
@endsection
@section('footer')
<script type="text/javascript">

  $('#btnExport').on('click', function() {
    var _val = '?date_from=' +  $('#date_from').val();
    _val += '&date_to=' +  $('#date_to').val();
    _val += '&customer_id=' +  $('#customer_id').val();
    _val += '&customs_broker_id=' +  $('#customs_broker_id').val();
    _val += '&owner_id=' +  $('#owner_id').val();
    _val += '&warehouse_id=' +  $('#warehouse_id').val();
    _val += '&tracking_no=' +  $('#tracking_no').val();
    window.location.href = '{{url('/agent/report/export/cargo_record')}}' + _val;

  });

  $('#btnPrint').on('click', function() {
    var _val = '?date_from=' +  $('#date_from').val();
    _val += '&date_to=' +  $('#date_to').val();
    _val += '&customer_id=' +  $('#customer_id').val();
    _val += '&customs_broker_id=' +  $('#customs_broker_id').val();
    _val += '&owner_id=' +  $('#owner_id').val();
    _val += '&warehouse_id=' +  $('#warehouse_id').val();
    _val += '&tracking_no=' +  $('#tracking_no').val();
    window.open('{{url('/agent/report/print/cargo_record')}}' + _val);
  });
</script>
@endsection
