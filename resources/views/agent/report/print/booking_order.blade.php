<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>货柜预订报表</title>
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+SC:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
<style>
@media all {
  body{
    font-size: 12pt;
    font-family: 'Noto Sans SC', sans-serif;
    font-weight: 100;
  }
  table{
    width: 100%;
    border-collapse: collapse;
    border:solid 1px #000;
  }
  table th{
    border:solid 1px #000;
    font-weight:700;
    padding: 4px;
    font-size: 12px;
  }
  table td{
    padding: 4px;
    border:solid 1px #000;
    font-size: 12px;
  }

  .text-center{
    text-align: center;
  }
  .text-right{
    text-align: right;
  }
}
@media print {
  table th{
    font-size: 10px;
  }
  table td{
    font-size: 10px;
  }

}
</style>
</head>
<body onload="window.print()">
<!-- <body> -->
  <div class="text-center"><strong>货柜预订报表</strong></div>
  <table class="table table-bordered">
    <thead class="  text-center">
      <tr>
        <th style="width:5%" class="text-center">No.</th>
        <th class="text-center">预订装柜日期</th>
        <th class="text-center">到港日期</th>
        <th>客户</th>
        <th class="text-right">货柜费用（RMB）</th>
        <th>预订号</th>
        <th style="width:10%" >货值</th>
        <th>代理</th>
        <th>船公司</th>
        <th>起运港</th>
        <th>目的港</th>
      </tr>
    </thead>
    <tbody>
      @foreach($vm->dto as $k => $v)
      <tr>
        <td class="text-center">{{$k+1}}</td>
        <td class="text-center">{{$v->booking_date}}</td>
        <td class="text-center">{{$v->eta_date}}</td>
        <td>{{$v->customer_name}}</td>
        <td class="text-right" style="font-size:14px">{{$v->handling_charges}}</td>
        <td class="text-center">{{$v->booking_no}}</td>
        <td class="text-right" style="font-size:14px">{{$v->goods_value}}</td>
        <td class="text-center">{{$v->agent_name}}</td>
        <td class="text-center">{{$v->shipping_company}}</td>
        <td>{{$v->loading_port}}</td>
        <td>{{$v->destination_port}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</body>
</html>
