@extends('layouts.agent')
@section('title', '报表')
@section('content')
<h2 class="card-title">报表</h2>
<div class="row reports">

  @if(auth()->user()->can('view full staff commission report'))
  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
    <div class="card">
      <a href="{{url('/agent/report/full_staff_commission')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center">
            <i class="fa fa-money icon-lg text-primary d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4  col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">完整员工提成</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>
  @endif

  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
    <div class="card">
      <a href="{{url('/agent/report/staff_commission')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center">
            <i class="fa fa-money icon-lg text-primary d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4  col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">员工提成</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>

  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
    <div class="card">
      <a href="{{url('agent/report/booking_order')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center ">
            <i class="fa fa-bar-chart-o icon-lg text-primary  d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4  col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">货柜预订</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>

  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
    <div class="card">
      <a href="{{url('agent/report/container_loading')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center">
            <i class="fa fa-cube  icon-lg text-primary d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4 col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">货柜监装</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>

  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
    <div class="card">
      <a href="{{url('agent/report/cargo_record')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center">
            <i class="fa fa-cubes  icon-lg text-primary d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4  col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">散货记录表</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>

</div>
@endsection
@section('footer')
@endsection