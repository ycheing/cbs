@extends('layouts.agent')
@section('title', '添加新订单')
@section('header')
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <!-- form -->
      <form method="post" id="form-booking" action="{{url('agent/booking/add')}}">
        @csrf

        <div class="card-header header-sm ">
          <div class="d-flex ">
            <div class="wrapper d-flex align-items-center">
              <h2 class="card-title mb4"><i class="fa fa-plus text-primary mr-2"></i>添加新订单</h2>
            </div>
            <div class="wrapper ml-auto action-bar">
              <button type="submit" data-toggle="tooltip" data-placement="top" data-original-title="储存"
                class="btn btn-icons btn-success btn-sm"><i class="fa fa-save"></i></button>

              <button type="button" id="btnBack" class="btn btn-icons btn-outline-primary btn-sm" title="返回"
                data-toggle="modal" data-target="#modalConfirmBack"><i class="fa fa-close"></i></button>
              <!-- <a class="btn btn-icons btn-outline-primary btn-sm"  data-toggle="tooltip" data-placement="top" data-original-title="返回"  href="{{url('/agent/bookings')}}"><i class="fa fa-close"></i></a> -->
            </div>
          </div>
        </div>
        <!--//card-header-->

        <div class="card-body">

          <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="form-group row">
                <label class="col-md-3 col-sm-12  col-xs-12 col-form-label" for="booking_no">预定号码</label>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <input type="text" name="booking_no_1" class="form-control" placeholder="123" maxlength="3" />
                </div>
                <div class="col-md-5 col-sm-5 col-xs-5">
                  <input type="text" name="booking_no_2" class="form-control" placeholder="AB12" maxlength="5" />
                </div>
              </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="booking_date">预定装柜日期</label>
                <div class="col-md-9">
                  <div class="input-group ">
                    <input id="booking_date" type="text" class="form-control date" name="booking_date"
                      data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="" />
                    <div class="input-group-append">
                      <span> <i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>
              <!--form-group-->
            </div>

          </div>
          <!--row-->

          <div class="row">
            <div class="col-md-6">

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_id">客户编号</label>
                <div class="col-md-9">
                  <select name="customer_id" class="select2 form-control">
                    <option value=""></option>
                    @foreach($vm->GetCustomers() as $customer)
                    <option value="{{$customer->id}}">{{$customer->code}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <!--form-group-->
            </div>
            <!--//col-->

            <div class="col-md-6">
              <div class="form-group row">
                <label for="customer_name" class="col-md-3 col-form-label">客户名字</label>
                <div class="col-md-9">
                  <input name="customer_name" id="input-customer-name" class="form-control" />
                  <input type="hidden" name="customer_code" />
                </div>
              </div>
              <!--form-group-->
            </div>
            <!--//col-->
          </div>
          <!--//row-->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="delivery_address_1">配送地址</label>
                <div class="col-md-9">
                  <input type="text" name="delivery_address_1" class="form-control" placeholder="地址第一行" />
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="delivery_address_2"></label>
                <div class="col-md-9">
                  <input type="text" name="delivery_address_2" class="form-control" placeholder="地址第二行" />
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="delivery_address_1">城市</label>
                <div class="col-md-9">
                  <input type="text" name="delivery_city" class="form-control" placeholder="" />
                </div>
              </div>
            </div>
            <!--//col-->
            <div class="col-md-6">

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="delivery_country_id">国家</label>
                <div class="col-md-9">
                  <select name="delivery_country_id" class="form-control">
                    <option value=""></option>
                    @foreach($vm->GetCountries() as $country)
                    <option value="{{$country->id}}">{{$country->name}}</option>
                    @endforeach
                  </select>
                  <input type="hidden" name="delivery_country" />
                </div>
              </div>
              <!--//form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="delivery_state_id">州属</label>
                <div class="col-md-9">
                  <select name="delivery_state_id" class="form-control">
                  </select>
                  <input type="hidden" name="delivery_state" />
                </div>
              </div>
              <!--//form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="delivery_postcode">邮编</label>
                <div class="col-md-9">
                  <input type="text" name="delivery_postcode" class="form-control" placeholder="" />
                </div>
              </div>
              <!--//form-group-->

            </div>
            <!--//col-->
          </div>
          <!--//row-->

          <div class="row">
            <div class="col-md-6">

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="handling_charges">货柜费用</label>
                <div class="col-md-9">
                  <input name="handling_charges" type="text" class="form-control" />
                </div>
              </div>
              <!--//form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="goods_value">货值</label>
                <div class="col-md-9">
                  <input type="text" name="goods_value" class="form-control" placeholder="" />
                </div>
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
          </div>
          <!--//row-->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="agent_id">代理</label>
                <div class="col-md-9">
                  <select name="agent_id" class="form-control select2">
                    <option value=""></option>
                    @foreach($vm->GetAgents() as $agent)
                    <option value="{{$agent->id}}">{{$agent->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <!--//col-->

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="shipping_company_id">船公司</label>
                <div class="col-md-9">
                  <select name="shipping_company_id" class="form-control select2">
                    <option value=""></option>
                    @foreach($vm->GetShippingCompanies() as $ShippingCompany)
                    <option value="{{$ShippingCompany->id}}">{{$ShippingCompany->code}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <!--//col-->
          </div>
          <!--//row-->



          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="loading_port_id">起运港</label>
                <div class="col-md-9">
                  <select name="loading_port_id" class="form-control select2">
                    <option value=""></option>
                    @foreach($vm->GetLoadingPorts() as $loading)
                    <option value="{{$loading->id}}">{{$loading->name}} ({{$loading->country}})</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <!--//col-->

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="destination_port_id">目的港</label>
                <div class="col-md-9">
                  <select name="destination_port_id" class="form-control select2">
                    <option value=""></option>
                    @foreach($vm->GetDestinationPorts() as $destination)
                    <option value="{{$destination->id}}">{{$destination->name}} ({{$destination->country}})</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <!--//col-->

          </div>
          <!--//row-->

          <div class="row">
            <div class="col-md-6">

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="closing_date">结关日期</label>
                <div class="col-md-9">
                  <div class="input-group ">
                    <input id="closing_date" type="text" class="form-control date" name="closing_date"
                      data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                    <div class="input-group-append">
                      <span> <i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>
              <!--//form-group-->
            </div>

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="sailing_date">开船日期</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="sailing_date" type="text" class="date form-control " name="sailing_date"
                      data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                    <div class="input-group-append ">
                      <span><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>
              <!--//form-group-->
            </div>

          </div>
          <!--row-->

          <div class="row">

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="sailing_day">航行时间</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="sailing_day" type="text" class="form-control" name="sailing_day" />
                    <div class="input-group-append ">
                      <span>天</span>
                    </div>
                  </div>
                </div>
              </div>
              <!--//form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="rental_period">免柜租期</label>
                <div class="col-md-9">
                  <input name="rental_period" type="text" class="form-control" />
                </div>
              </div>

            </div>
          </div>
          <!--row-->

        </div>
        <!--card body-->
    </div>
    <!--card -->
    </form>
    <!-- // form-->
  </div>
</div>


<div class="modal fade" id="modalConfirmBack" tabindex="-1" role="dialog" aria-labelledby="modalConfirmBackLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalConfirmBackLabel">是否储存资料</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>是否储存资料,并返回清单列表？</p>
      </div>
      <div class="modal-footer">
        <button id="btnModalConfirmSubmitBack" class="btn btn-primary">确认</button>
        <a href="{{url('/agent/bookings')}}" class="btn btn-secondary">取消</a>
      </div>
    </div>
  </div>
</div>
@endsection

@section('footer')
{!! JsValidator::formRequest('App\Http\Requests\Agent\BookingRequest', '#form-booking'); !!}
<script>
  $('#btnModalConfirmSubmitBack').on('click', function() {
    $('#form-booking').submit();
});
  $('#form-booking').submit(function (e) {
      $('#modalConfirmBack').modal('hide');
      e.preventDefault();
      if (!$(this).valid()) return false;
      var _btn = $('button[type=submit]', this);
      startSpin(_btn);
      $.ajax({
          url: this.action,
          type: this.method,
          data: $(this).serialize(),
      }).fail(function(xhr, text, err) {
         notifySystemError(err);
      }).done(function(data) {
          notifySuccess('预定已添加！');
          setTimeout(function(){ location.href='{{url('/agent/bookings')}}'; }, 2000);
      }).always(function() {
          stopSpin(_btn);
      });
  });

  //customer change
  $('select[name=\'customer_id\']').on('change', function() {
    //reset all value
    $('input[name=\'customer_code\']').val('');
    $('input[name=\'customer_name\']').val('');
    $('input[name=\'delivery_address_1\']').val('');
    $('input[name=\'delivery_address_2\']').val('');
    $('input[name=\'delivery_city\']').val('');
    $('input[name=\'delivery_postcode\']').val('');
    $('select[name=\'delivery_country_id\']').val('');
    $('input[name=\'delivery_country\']').val('');
    html = '<option value="">{{__('请选择')}}</option>';
    html += '<option value="0" selected="selected">{{__('无')}}</option>';
    $('select[name=\'delivery_state_id\']').html(html);
    $('input[name=\'delivery_state\']').val('');

  	$.ajax({
  		type:"GET",
  		url: '{{url('getcustomer?customer_id=')}}' + this.value,
  		dataType: 'json',
  		beforeSend: function() {
  			$('select[name=\'customer_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
  		},
  		complete: function() {
  			$('.fa-spin').remove();
  		},
  		success: function(json) {


        if(json.hasOwnProperty('code')){
          if (json['code'] && json['code'] != '') {
            $('input[name=\'customer_code\']').val(json['code']);
          }
        }
        if(json.hasOwnProperty('name')){
          if (json['name'] && json['name'] != '') {
            $('input[name=\'customer_name\']').val(json['name']);
          }
        }
        if(json.hasOwnProperty('shipping_address_1')){
          if (json['shipping_address_1'] && json['shipping_address_1'] != '') {
            $('input[name=\'delivery_address_1\']').val(json['shipping_address_1']);
          }
        }
        if(json.hasOwnProperty('shipping_address_2')){
          if (json['shipping_address_2'] && json['shipping_address_2'] != '') {
            $('input[name=\'delivery_address_2\']').val(json['shipping_address_2']);
          }
        }
        if(json.hasOwnProperty('shipping_city')){
          if (json['shipping_city'] && json['shipping_city'] != '') {
            $('input[name=\'delivery_city\']').val(json['shipping_city']);
          }
        }
        if(json.hasOwnProperty('shipping_postcode')){
          if (json['shipping_postcode'] && json['shipping_postcode'] != '') {
            $('input[name=\'delivery_postcode\']').val(json['shipping_postcode']);
          }
        }
        if(json.hasOwnProperty('shipping_country_id')){
          if (json['shipping_country_id'] && json['shipping_country_id'] != '') {
            $('select[name=\'delivery_country_id\']').val(json['shipping_country_id']);
          }
        }
        if(json.hasOwnProperty('shipping_country')){
          if (json['shipping_country'] && json['shipping_country'] != '') {
            $('input[name=\'delivery_country\']').val(json['shipping_country']);
          }
        }

        if(json.hasOwnProperty('shipping_states')){
          html = '<option value="">{{__('请选择')}}</option>';

          if (json['shipping_states'] && json['shipping_states'] != '') {

            for (i = 0; i < json['shipping_states'].length; i++) {
              html += '<option value="' + json['shipping_states'][i]['id'] + '"';
              html += '>' + json['shipping_states'][i]['name'] + '</option>';
            }
          } else {
            html += '<option value="0" selected="selected">{{__('无')}}</option>';
          }

          $('select[name=\'delivery_state_id\']').html(html);
        }

        if(json.hasOwnProperty('shipping_state_id')){
          if (json['shipping_state_id'] && json['shipping_state_id'] != '') {
            $('select[name=\'delivery_state_id\']').val(json['shipping_state_id']);
          }
        }
        if(json.hasOwnProperty('shipping_state')){
          if (json['shipping_state'] && json['shipping_state'] != '') {
            $('input[name=\'delivery_state\']').val(json['shipping_state']);
          }
        }
  		},
  		error: function(xhr, ajaxOptions, thrownError) {
  			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
  		}
  	});
  });

  $('select[name=\'customer_id\']').trigger('change');

  $('select[name=\'delivery_country_id\']').on('change', function() {
  	$.ajax({
  		type:"GET",
  		url: '{{url('getcountry?country_id=')}}' + this.value,
  		dataType: 'json',
  		beforeSend: function() {
  			$('select[name=\'delivery_country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
  		},
  		complete: function() {
  			$('.fa-spin').remove();
  		},
  		success: function(json) {
  			html = '<option value="">{{__('Please select')}}</option>';

  			if (json['json']['states'] && json['json']['states'] != '') {

  				for (i = 0; i < json['json']['states'].length; i++) {
  					html += '<option value="' + json['json']['states'][i]['id'] + '"';
  					html += '>' + json['json']['states'][i]['name'] + '</option>';
  				}
  			} else {
  				html += '<option value="0" selected="selected">{{__('无')}}</option>';
  			}

  			$('select[name=\'delivery_state_id\']').html(html);
  		},
  		error: function(xhr, ajaxOptions, thrownError) {
  			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
  		}
  	});
  });

  $('select[name=\'delivery_country_id\']').trigger('change');

</script>
@endsection