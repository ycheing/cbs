@extends('layouts.agent')
@section('title', '装柜资料')
@section('header')
  <link rel="stylesheet" href="{{ asset('vendor/lightbox2-master/css/lightbox.min.css') }}">
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <!-- form -->
      <form method="post" id="form-loading" action="{{url('agent/booking/loading')}}" >
        @csrf
        <input type="hidden" id="booking-id" name="id" value="{{$vm->dto->id}}" />
        <div class="card-header header-sm ">
          <div class="d-flex ">
              <div class="wrapper d-flex align-items-center">
                <h2 class="card-title mb4"><i class="fa fa-plus text-primary mr-2"></i>修改装柜资料</h2>
              </div>
              <div class="wrapper ml-auto action-bar">
                  @if($vm->dto->owner_id == Auth::user()->id)
                    <div class="d-none d-sm-block">
                    @if($vm->dto->status_id == 1 || $vm->dto->status_id == 2)
                      <button type="button" class="btn btn-icons  btn-success btn-sm" id="btnLoadingComplete" data-toggle="modal" data-target="#modalLoadingComplete">装柜完毕</button>
                    @endif

                    @if($vm->dto->booking == 1)
                      <a href="{{url('/agent/booking/edit/booking/' . $vm->dto->id)}}" class="btn btn-icons  btn-primary btn-sm"  data-toggle="tooltip" data-original-title="预定记录">预定记录</a>
                    @endif

                    @if($vm->dto->declaration == 0)
                      <button type="button" class="btn btn-icons btn-dark btn-sm" data-toggle="modal" data-target="#modalConfirmDeclartion"  id="btnDeclaration">添加清关记录</button>
                    @elseif($vm->dto->declaration == 1)
                      <a href="{{url('/agent/booking/edit/declaration/' . $vm->dto->id)}}" class="btn btn-icons btn-dark btn-sm"  data-toggle="tooltip" data-original-title="清关记录">清关记录</a>
                    @endif
                    <button type="submit" class="btn btn-icons btn-success btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="储存"><i class="fa fa-save"></i></button>
                    <button type="button" id="btnBack" class="btn btn-icons btn-outline-primary btn-sm" title="返回"  data-toggle="modal" data-target="#modalConfirmBack"><i class="fa fa-close"></i></button>

                    </div>
                    <div class="dropdown d-block d-sm-none float-right">
                      <button class="btn btn-secondary  btn-sm dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 管理订单 </button>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton2">



                        @if($vm->dto->booking == 1)
                          <a href="{{url('/agent/booking/edit/booking/' . $vm->dto->id)}}" class="dropdown-item" >预定记录</a>
                        @endif

                        @if($vm->dto->declaration == 1)
                          <a href="{{url('/agent/booking/edit/declaration/' . $vm->dto->id)}}" class="dropdown-item" >清关记录</a>
                        @endif
                        <div class="dropdown-divider"></div>
                        @if($vm->dto->status_id == 1 || $vm->dto->status_id == 2)
                          <button type="button" class="dropdown-item"  id="btnLoadingComplete" data-toggle="modal" data-target="#modalLoadingComplete">装柜完毕</button>
                        @endif

                        @if($vm->dto->declaration == 0)
                          <button type="button" class="dropdown-item"  data-toggle="modal" data-target="#modalConfirmDeclartion"  id="btnDeclaration">添加清关记录</button>
                        @endif
                        <div class="dropdown-divider"></div>
                        <button type="submit" class="dropdown-item" ><i class="fa fa-save"></i> 储存</button>
                        <button type="button" id="btnBack" class="dropdown-item" data-toggle="modal" data-target="#modalConfirmBack"><i class="fa fa-close"></i> 返回</button>
                      </div>
                    </div>
                  @else
                    <div class="d-none d-sm-block">
                    @if($vm->dto->booking == 1)
                      <a href="{{url('/agent/booking/edit/booking/' . $vm->dto->id)}}" class="btn btn-icons  btn-primary btn-sm"  data-toggle="tooltip" data-original-title="预定记录">预定记录</a>
                    @endif
                    @if($vm->dto->declaration == 1)
                      <a href="{{url('/agent/booking/edit/declaration/' . $vm->dto->id)}}" class="btn btn-icons btn-dark btn-sm"  data-toggle="tooltip" data-original-title="清关记录">清关记录</a>
                    @endif
                    <a class="btn btn-icons btn-outline-primary btn-sm"  href="{{url('/agent/bookings')}}"  data-toggle="tooltip" data-original-title="返回"><i class="fa fa-close"></i></a>
                    </div>

                    <div class="dropdown d-block d-sm-none float-right">
                      <button class="btn btn-secondary  btn-sm dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 管理订单 </button>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton2">
                        @if($vm->dto->booking == 1)
                          <a href="{{url('/agent/booking/edit/booking/' . $vm->dto->id)}}" class="dropdown-item" >预定记录</a>
                        @endif
                        @if($vm->dto->declaration == 1)
                          <a href="{{url('/agent/booking/edit/declaration/' . $vm->dto->id)}}" class="dropdown-item" >清关记录</a>
                        @endif
                        <div class="dropdown-divider"></div>
                        <a  class="dropdown-item"  href="{{url('/agent/bookings')}}" ><i class="fa fa-close"></i> 返回</a>
                      </div>
                    </div>

                  @endif

              </div>
          </div>
        </div><!--//card-header-->

        <div class="card-body">
          <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="booking_no">预定号 </label>
                <div class="col-md-9">
                  <span class="badge badge-primary">{{$vm->dto->booking_no}} </span>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="form-group row">
                <label class="col-md-3 col-form-label"  for="order_no" >订单号</label>
                <div class="col-md-9">
                  <span class="badge badge-primary">{{$vm->dto->order_no}}</span>
                </div>
              </div>
            </div>
          </div><!--//row-->

          <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="loading_date" >装柜日期</label>
                  <div class="col-md-9">
                     <div class="input-group ">
                       <input id="loading_date" type="text" class="form-control date" name="loading_date"  data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->loading_date}}"/>
                       <div class="input-group-append">
                            <span> <i class="fa fa-calendar"></i></span>
                       </div>
                     </div>
                   </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="status" >状态</label>
                  <div class="col-md-9">
                  {!! $vm->dto->status !!}
                  </div>
                </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="customer_id">客户编号</label>
                  <div class="col-md-9">
                    <select name="customer_id" class="select2 form-control" disabled>
                      <option value=""></option>
                      @foreach($vm->GetCustomers() as $customer)
                        @if($vm->dto->customer_id == $customer->id)
                          <option value="{{$customer->id}}" selected>{{$customer->code}}</option>
                        @else
                          <option value="{{$customer->id}}">{{$customer->code}}</option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                </div><!--form-group-->
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">

              <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="customer_name" >客户名字</label>
                  <div class="col-md-9">
                    <input name="customer_name" id="input-customer-name" class="form-control" value="{{$vm->dto->customer_name}}"  disabled/>
                    <input type="hidden" name="customer_code" value="{{$vm->dto->customer_code}}"/>
                  </div>
              </div><!--form-group-->
            </div>
          </div><!--//row-->

          <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">

                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="container_no">货柜号码</label>
                  <div class="col-md-9">
                    <input name="container_no" type="text"  class="form-control" placeholder=""  value="{{$vm->dto->container_no}}"/>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="seal_no">封条号</label>
                  <div class="col-md-9">
                    <input name="seal_no" type="text"  class="form-control" placeholder="" value="{{$vm->dto->seal_no}}"/>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="photo_seal "> 封柜图</label>
                  <div class="col-md-9">
                    <ul id="list-photo-seal" class="lightgallery">
                    </ul>
                    <div class="clearfix mb-2"></div>
                    @if($vm->dto->owner_id == Auth::user()->id)
                      <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalImage"><i class="fa fa-plus"></i></button>
                    @endif
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="loading_place">装柜区域</label>
                  <div class="col-md-9">
                    <textarea name="loading_place" class="form-control" placeholder="" >{{$vm->dto->loading_place}}</textarea>
                  </div>
                </div><!--form-group-->

                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="agent_id">代理</label>
                  <div class="col-md-9">
                    <select name="agent_id" class="form-control" disabled>
                      <option value=""></option>
                      @foreach($vm->GetAgents() as $agent)
                        @if($vm->dto->agent_id == $agent->id)
                          <option value="{{$agent->id}}" selected>{{$agent->name}}</option>
                        @else
                          <option value="{{$agent->id}}">{{$agent->name}}</option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="shipping_company_id">船公司</label>
                <div class="col-md-9">
                  <select name="shipping_company_id" class="form-control" disabled>
                    <option value=""></option>
                    @foreach($vm->GetShippingCompanies() as $ShippingCompany)
                      @if($vm->dto->shipping_company_id == $ShippingCompany->id)
                        <option value="{{$ShippingCompany->id}}" selected>{{$ShippingCompany->code}}</option>
                      @else
                        <option value="{{$ShippingCompany->id}}">{{$ShippingCompany->code}}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="loading_port_id">起运港</label>
                <div class="col-md-9">
                  <select name="loading_port_id" class="form-control" disabled>
                    <option value=""></option>
                    @foreach($vm->GetLoadingPorts() as $loading)
                      @if($vm->dto->loading_port_id == $loading->id)
                        <option value="{{$loading->id}}" selected>{{$loading->name}} ({{$loading->country}})</option>
                      @else
                        <option value="{{$loading->id}}">{{$loading->name}} ({{$loading->country}})</option>
                      @endif
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label"  for="destination_port_id">目的港</label>
                <div class="col-md-9">
                  <select name="destination_port_id" class="form-control" disabled>
                    <option value=""></option>
                    @foreach($vm->GetDestinationPorts() as $destination)
                      @if($vm->dto->destination_port_id == $destination->id)
                        <option value="{{$destination->id}}" selected>{{$destination->name}} ({{$destination->country}})</option>
                      @else
                        <option value="{{$destination->id}}">{{$destination->name}} ({{$destination->country}})</option>
                      @endif

                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="forme_apply">Form E 申请</label>
                <div class="col-md-9">
                  <div class="form-group">
                     <div class="form-check form-check-flat mt-1 ">
                       <label class="form-check-label ">
                         @if($vm->dto->forme_apply == 1)
                         <input type="checkbox" name="forme_apply" value="1" class="form-check-input" checked />
                         @else
                          <input type="checkbox" name="forme_apply" value="1" class="form-check-input"  />
                         @endif
                          Yes </label>
                     </div>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label"  for="custom_borker_id">清关行</label>
                <div class="col-md-9">
                  <select name="custom_borker_id" class="form-control select2">
                    <option value=""></option>
                    @foreach($vm->GetCustomsBrokers() as $cb)
                      @if($vm->dto->custom_borker_id ==  $cb->id)
                        <option value="{{$cb->id}}" selected>{{$cb->code}}</option>
                      @else
                        <option value="{{$cb->id}}">{{$cb->code}}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label"   for="etd_date">离港日期</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="etd_date" type="text" class="date form-control " name="etd_date" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->etd_date}}"/>
                    <div class="input-group-append ">
                        <span><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div><!--//form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label"   for="eta_date">到港日期</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="eta_date" type="text" class="date form-control " name="eta_date" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->eta_date}}"/>
                    <div class="input-group-append ">
                        <span><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div><!--//form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" >工作人员 (任务记录)</label>
                <div class="col-md-9">
                  <table class="table table-border">
                    <thead>
                      <tr class="bg-dark text-white">
                        <th></th>
                        <th>工作</th>
                        <th>员工</th>
                      </tr>
                    </thead>
                    <tbody id="table-person">
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="3" class="text-right">
                          @if($vm->dto->owner_id == Auth::user()->id)
                          <button class="btn btn-success" type="button" data-toggle="modal" data-target="#ModalPerson" id="btnPerson"><i class="fa fa-plus"></i></button>
                          @endif
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>

            </div><!--//col-->

            <div class="col-md-6 col-sm-12 col-xs-12">

                <input type="hidden" name="total_charges" id="total_charges" class="form-control border-primary"  value="{{$vm->dto->total_charges}}" />

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="handling_charges">货柜费用</label>
                <div class="col-md-9">
                  <input name="handling_charges" type="text" class="form-control"  value="{{$vm->dto->handling_charges}}"/>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="apply_charges">Form E 申请费用</label>
                <div class="col-md-9">
                  <input name="apply_charges" type="text"  class="form-control"  value="{{$vm->dto->apply_charges}}"/>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="commission">佣金</label>
                <div class="col-md-9">
                <input name="commission"  type="text" class="form-control "  value="{{$vm->dto->commission}}"/>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="offsite_fee">异地费用</label>
                <div class="col-md-9">
                  <input name="offsite_fee" type="text"  class="form-control"  value="{{$vm->dto->offsite_fee}}"/>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="other_fee">其他费用</label>

                  <!-- <input name="other_fee" type="text" class="form-control"  value="{{$vm->dto->other_fee}}"/> -->
                <div class="col-md-9">
                  <table class="table table-border">
                    <thead>
                      <tr class="bg-dark text-white">
                        <th></th>
                        <th>说明</th>
                        <th>金额</th>
                      </tr>
                    </thead>
                    <tbody id="table-fees-1">
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="3" class="text-right">
                          <button class="btn btn-success" type="button" data-toggle="modal" data-target="#ModalFee" id="btnFee1"><i class="fa fa-plus"></i></button>
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>

              </div><!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label">附件</label>
                <div class="col-md-9">
                  <table class="table table-border">
                    <tbody id="table-attachment2">
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="2" class="text-right">
                            @if($vm->dto->owner_id == Auth::user()->id)
                            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#ModalFiles" id="btnAttachment2"><i class="fa fa-plus"></i></button>
                            @endif
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>

              <!-- <div class="form-group row mt-5">
                <label class="col-md-3 col-form-label" for="freight_charges">全包价</label>
                <div class="col-md-9"> -->
                    <input name="freight_charges" id="freight_charges" class="form-control"  value="{{$vm->dto->freight_charges}}" type="hidden" />
                <!-- </div>
              </div> -->
              <!--row-->

              <div class="form-group row mt-5">
                <label class="col-md-3 col-form-label" for="document_fee">文件费</label>
                <div class="col-md-9">
                    <input name="document_fee" id="document_fee" class="form-control"  value="{{$vm->dto->document_fee}}"/>
                </div>
              </div><!--row-->


              <input  type="hidden" name="net_freight_charges" id="net_freight_charges" class="form-control"  readonly value="{{number_format($vm->dto->net_freight_charges,2, '.', '')}}"/>


              <div class="form-group row">
                <label class="col-md-3 col-form-label"for="remote_fee">异地费</label>
                <div class="col-md-9">
                  <input name="remote_fee" id="remote_fee" class="form-control "  value="{{$vm->dto->remote_fee}}"/>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="charges">Form E 费用</label>
                <div class="col-md-9">
                  <input name="charges" id="charges" class="form-control" value="{{$vm->dto->charges}}" />
                </div>
              </div><!--row-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="container_loading_charges">装柜费</label>
                <div class="col-md-9">
                  <input name="container_loading_charges" id="container_loading_charges" class="form-control"   value="{{$vm->dto->container_loading_charges}}" />
                </div>
              </div><!--row-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="entry_fee">进场费</label>
                <div class="col-md-9">
                  <input name="entry_fee" id="entry_fee" class="form-control"   value="{{$vm->dto->entry_fee}}" />
                </div>
              </div><!--row-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label">附件</label>
                <div class="col-md-9">
                  <table class="table table-border">
                    <tbody id="table-attachment3">
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="2" class="text-right">
                            @if($vm->dto->owner_id == Auth::user()->id)
                            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#ModalFiles" id="btnAttachment3"><i class="fa fa-plus"></i></button>
                            @endif
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>

              <div class="form-group row">

                <label class="col-md-3 col-form-label">其他费用</label>
                <div class="col-md-9">
                  <table class="table table-border">
                    <thead>
                      <tr class="bg-dark text-white">
                        <th></th>
                        <th>说明</th>
                        <th>金额</th>
                      </tr>
                    </thead>
                    <tbody id="table-fees">
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="3" class="text-right">

                            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#ModalFee" id="btnFee2"><i class="fa fa-plus"></i></button>
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
             </div><!--row-->

            </div><!--//col-->
          </div><!--//row-->


        </div> <!--card body-->
      </form>  <!-- // form-->
    </div><!--card -->
  </div>
</div>

<!-- booking person -->
<div class="modal fade" id="ModalPerson" tabindex="-1" role="dialog" aria-labelledby="ModalPersonLabel" aria-hidden="true">
    <form method="POST" id="form-person" action="{{url('agent/booking/add/person')}}">
      <input type="hidden" name="booking_id" value="{{$vm->dto->id}}"/>
      <input type="hidden" name="type_id" id="type_id" value="1"/>
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalImageLabel">添加工作人员</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                  <div class="form-group row">
                    <div class="col">
                      <label for="job_id">工作</label>
                      <select name="job_id" class="form-control" >
                        <option value=""></option>
                        @foreach($vm->GetJobs() as $job)
                          <option value="{{$job->id}}">{{$job->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col">
                      <label for="employee_id">员工</label>
                      <select name="employee_id" class="form-control">
                        <option value=""></option>
                        @foreach($vm->GetEmployees() as $employee)
                        <option value="{{$employee->user_id}}">{{$employee->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div><!--row-->
                </div>
                <div class="modal-footer">
                  <button type="button" id="btnPersonAdd" class="btn btn-success"><i class="fa fa-plus"></i>添加</button>
                  <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-light"><i class="fa fa-close"></i>关闭</button>
                </div>
            </div>
        </div>
    </form>
</div>


<!-- upload image -->
<div class="modal fade" id="modalImage" tabindex="-1" role="dialog" aria-labelledby="modalImageLabel" aria-hidden="true">
  <form method="POST" id="form-image" action="{{url('agent/booking/upload')}}">
      <div class="modal-dialog modal-dialog-centered " role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="modalImageLabel">上载图片</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                <input name="booking_id" type="hidden" value="{{$vm->dto->id}}"/>
                <input name="type_id" type="hidden"  id="type_id" value="1"/>
                <div id="dropFileState" ondragover="return false">
                    <div id="dragUploadFile">
                        <p>档案类型：PNG，JPG，GIF，TIFF<br/>
                           档案大小上限：2MB
                        </p>
                        <input type="file" id="file_file" name="file_file" class="dropify" data-max-file-size="2M" data-allowed-file-extensions="jpg png gif tiff"  />
                    </div>
                </div>
                <div class="text-center mt-2">
                  <button type="button" class="btn btn-success mb-3" id="btnUploadImage"><i class="fa fa-excel"></i> 上载</button>
                </div>
              </div>
              <!-- <div class="modal-footer">
                  <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-secondary"><i class="fa fa-close"></i>Close</button>
              </div> -->
          </div>
      </div>
  </form>
</div>


<!-- upload files -->
<div class="modal fade" id="ModalFiles" tabindex="-1" role="dialog" aria-labelledby="modalImageLabel" aria-hidden="true">
  <form method="POST" id="form-files" action="{{url('booking/upload')}}">
      <div class="modal-dialog modal-dialog-centered " role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="modalImageLabel">上载附件</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                <input name="booking_id" type="hidden" value="{{$vm->dto->id}}"/>
                <input name="type_id" id="attachment_type_id" type="hidden" value=""/>
                <!-- <div class="form-group  row">
                  <div class="col-md-6 col-sm-12 col-xs-12">
                    <label>说明</label>
                    <select name="type_id" class="form-control" >
                      <option value=""></option>
                      <option value="2">装车单</option>
                      <option value="3">付款单</option>
                      <option value="6">SST (K1 Form)</option>
                    </select>
                  </div>
                </div> -->

                <div id="dropFileState" ondragover="return false">
                    <div id="dragUploadFile">
                      <p>档案类型：Excel, PDF, JPG, PNG, GIF, TIFF<br/>
                       <!-- 档案大小上限：2MB -->
                     </p>
                    <input type="file" id="file_attachment" name="file_file" class="dropify"  data-allowed-file-extensions="csv xls xlsx pdf jpg png gif tiff txt"  />
                    </div>
                </div>
                <div class="text-center mt-2">
                  <button type="button" class="btn btn-success mb-3" id="btnUploadFile"><i class="fa fa-excel"></i> 上载</button>
                </div>
              </div>
          </div>
      </div>
  </form>
</div>

<!-- Permission -->
<div class="modal fade" id="modalPermission" tabindex="-1" role="dialog" aria-labelledby="modalPermissionLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered " role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="modalImageLabel">没有权限</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                <p>对不起，你没有此预定删除或更改权限</p>
              </div>
              <!-- <div class="modal-footer">
                  <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-secondary"><i class="fa fa-close"></i>Close</button>
              </div> -->
          </div>
      </div>
</div>

<div class="modal fade" id="modalLoadingComplete" tabindex="-1" role="dialog" aria-labelledby="modalLoadingCompleteLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalConfirmDeleteLabel">确认装柜完成</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- <p>是否确认装柜完成？确认后无法更改此订单的装柜资料。</p> -->
                <p>是否确认装柜完成？</p>
            </div>
            <div class="modal-footer">
                <button id="btnModalConfirmLoadingComplete" class="btn btn-primary">确定</button>
                <button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">取消</button>
            </div>
        </div>
    </div>
</div>


<!-- other fee -->
<div class="modal fade" id="ModalFee" tabindex="-1" role="dialog" aria-labelledby="ModalFeeLabel" aria-hidden="true">
    <form method="POST" id="form-fee" action="{{url('booking/add/fee')}}">
      <input type="hidden" name="booking_id" value="{{$vm->dto->id}}"/>
      <input type="hidden" name="fee_type" id="fee_type" />
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalImageLabel">添加其他费用</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                  <div class="form-group row">
                    <div class="col">
                      <label for="fee_description">说明	</label>
                      <input type="text" name="fee_description" class="form-control" value="">
                    </div>
                    <div class="col">
                      <label for="fee_amount">金额</label>
                      <input type="text" name="fee_amount" class="form-control" value="">
                    </div>
                  </div><!--row-->
                </div>
                <div class="modal-footer">
                  <button type="button" id="btnFeeAdd" class="btn btn-success"><i class="fa fa-plus"></i>添加</button>
                  <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-light"><i class="fa fa-close"></i>取消</button>
                </div>
            </div>
        </div>
    </form>
</div>


<!-- // confirm declartion-->
<div class="modal fade" id="modalConfirmDeclartion" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeclartionLabel" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
       <div class="modal-content">
           <div class="modal-header">
               <h5 class="modal-title" id="modalConfirmDeleteLabel">清关确认</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
           </div>
           <div class="modal-body">
               <p>是否添加清关资料?</p>
           </div>
           <div class="modal-footer">
               <button id="btnModalConfirmDeclarationOK" class="btn btn-primary">确认</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">取消</button>
           </div>
       </div>
   </div>
</div>


<div class="modal fade" id="modalConfirmBack" tabindex="-1" role="dialog" aria-labelledby="modalConfirmBackLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalConfirmBackLabel">是否储存资料</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>是否储存资料,并返回清单列表？</p>
            </div>
            <div class="modal-footer">
              <button id="btnModalConfirmSubmitBack" class="btn btn-primary">确认</button>
              <a href="{{url('/agent/bookings')}}" class="btn btn-secondary">取消</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
{!! JsValidator::formRequest('App\Http\Requests\Admin\BookingLoadingRequest', '#form-loading'); !!}
{!! JsValidator::formRequest('App\Http\Requests\Admin\BookingPersonRequest', '#form-person'); !!}
{!! JsValidator::formRequest('App\Http\Requests\Admin\BookingAttachmentRequest', '#form-image'); !!}
{!! JsValidator::formRequest('App\Http\Requests\Admin\BookingFeeRequest', '#form-fee'); !!}
<script>
//load preferred list
$('#table-person').load('{{url('/agent/booking/getpersons/' . $vm->dto->id . '')}}');
// $('#table-inspect').load('{{url('/agent/booking/getinspectpersons/' . $vm->dto->id . '')}}');
$('#list-photo-seal').load('{{url('/agent/booking/getphotoseals/' . $vm->dto->id . '')}}');
$('#table-fees-1').load('{{url('/agent/booking/getfees/' . $vm->dto->id . '/1')}}');
$('#table-fees').load('{{url('/agent/booking/getfees/' . $vm->dto->id . '/2')}}');
$('#table-attachment2').load('{{url('/agent/booking/getattachments/' . $vm->dto->id . '/2')}}');
$('#table-attachment3').load('{{url('/agent/booking/getattachments/' . $vm->dto->id . '/3')}}');

$('#btnModalConfirmSubmitBack').on('click', function() {
    $('#form-loading').submit();
});
$('#form-loading').submit(function (e) {
    $('#modalConfirmBack').modal('hide');
    e.preventDefault();
    if (!$(this).valid()) return false;
    var _btn = $('button[type=submit]', this);
    startSpin(_btn);
    $.ajax({
        url: this.action,
        type: this.method,
        data: $(this).serialize(),
    }).fail(function(xhr, text, err) {
      var json = $.parseJSON(xhr.responseText);
      if (json['errors'] && json['errors'] != '') {
          var msg ='';
          $.each(json['errors'], function (key, value) {
             msg +=  value + '<br/>';
          });
          notifyError(msg);
      }
    }).done(function(data) {
        notifySuccess('装柜资料已成功更改！');
        setTimeout(function(){ location.href='{{url('/agent/bookings')}}'; }, 2000);
    }).always(function() {
        stopSpin(_btn);
    });
});

// $('#btnInCharge').on('click', function() {
//   $('#type_id').val('1');
// });
//
// $('#btnInspect').on('click', function() {
//   $('#type_id').val('2');
// });

//add person
$('#btnPersonAdd').on('click', function(){
  if (!$('#form-person').valid()) return false;
  var formData = new FormData($('#form-person')[0]);
  startSpin($('#btnPersonAdd'));
  $.ajax({
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '{{url('agent/booking/add/person')}}',
      type: 'POST',
      data:formData,
      enctype: 'multipart/form-data',
      contentType: false,
      processData: false,
  }).fail(function(xhr, text, err) {
     //notifySystemError(err);
     var json = $.parseJSON(xhr.responseText);

     if (json['errors'] && json['errors'] != '') {
         var msg ='';
         $.each(json['errors'], function (key, value) {
            msg +=  value + '<br/>';
         });
         notifySystemError(msg);
    }
  }).done(function(data) {
      $("#form-person")[0].reset();
      $('#table-person').load('{{url('/agent/booking/getpersons/' . $vm->dto->id . '')}}');
      // $('#table-inspect').load('{{url('/agent/booking/getinspectpersons/' . $vm->dto->id . '')}}');
      $('#ModalPerson').modal('hide');
      notifySuccess('工作人员已成功添加!');

  }).always(function() {
      stopSpin($('#btnPersonAdd'));
  });

});

$(document).on('click', '.btn-delete-person', function(e) {
  @if($vm->dto->owner_id == Auth::user()->id)

   e.preventDefault();
   e.stopImmediatePropagation();
   if (!confirm('是否删除这个工作人员?')) return;
   var _id = $(this).attr('data-id');
   var _formData = new FormData();
   _formData.append('booking_person_id', _id);
   $.ajax({
       headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       },
       url: '{{url('/agent/booking/person/delete')}}',
       type: 'POST',
       contentType: false,
       processData: false,
       data: _formData,
   }).fail(function(xhr, text, err) {
      notifySystemError(err);
   }).done(function(data){
     if(data['result']){
       notifySuccess('工作人员已删除！');
     }else{
       notifySystemError(data['error']);
     }
     $('#table-person').load('{{url('/agent/booking/getpersons/' . $vm->dto->id . '')}}');
     // $('#table-inspect').load('{{url('/agent/booking/getinspectpersons/' . $vm->dto->id . '')}}');
   }).always(function(){
   });
   @else
   $('#modalPermission').modal('show');
   @endif
});

$('#btnUploadImage').on('click', function() {
 if (!$('#form-image').valid()) return false;
 var formData = new FormData($('#form-image')[0]);
 var _btn = $('#btnUploadImage', this);
 startSpin(_btn);
 $.ajax({
     headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     },
     url: '{{url('agent/booking/upload')}}',
     type: 'POST',
     data:formData,
     enctype: 'multipart/form-data',
     contentType: false,
     processData: false,
 }).fail(function(xhr, text, err) {
    notifySystemError(err);
 }).done(function(data) {
   if(data['result']){
     $('#list-photo-seal').load('{{url('/agent/booking/getphotoseals/' . $vm->dto->id . '')}}');
     notifySuccess('图片添加成功!');
     $('#modalImage').modal('hide');
    var drEvent = $('#file_file').dropify();
    drEvent = drEvent.data('dropify');
    drEvent.resetPreview();
    drEvent.clearElement();
   }else{
     notifySystemError(data['error']);
   }
 }).always(function() {
     stopSpin(_btn);
 });
});

$(document).on('click', '.btn-delete-image', function(e) {
  @if($vm->dto->owner_id == Auth::user()->id)
    e.preventDefault();
    e.stopImmediatePropagation();
    if (!confirm('请确认是否要删除这图片?')) return;
    var _id = $(this).attr('data-id');
    var _formData = new FormData();
    _formData.append('booking_attachment_id', _id);
    $.ajax({
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '{{url('/agent/booking/attachment/delete')}}',
        type: 'POST',
        contentType: false,
        processData: false,
        data: _formData,
    }).fail(function(xhr, text, err) {
       notifySystemError(err);
    }).done(function(data){
      if(data['result']){
        notifySuccess('图片已删除！');
      }else{
        notifySystemError(data['error']);
      }
      $('#list-photo-seal').load('{{url('/agent/booking/getphotoseals/' . $vm->dto->id . '')}}');
    }).always(function(){
    });
  @else
   $('#modalPermission').modal('show');
  @endif
});

//add attachment
$('#btnAttachment2').on('click', function() {
  $('#attachment_type_id').val('2');
});

$('#btnAttachment3').on('click', function() {
  $('#attachment_type_id').val('3');
});

$('#btnUploadFile').on('click', function() {
  var _type = $('#attachment_type_id').val();
  if (!$('#form-files').valid()) return false;
  var formData = new FormData($('#form-files')[0]);
  var _btn = $('#btnUploadFile', this);
  startSpin(_btn);
  $.ajax({
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '{{url('/agent/booking/upload')}}',
      type: 'POST',
      data:formData,
      enctype: 'multipart/form-data',
      contentType: false,
      processData: false,
  }).fail(function(xhr, text, err) {
     notifySystemError(err);
  }).done(function(data) {
    if(data['result']){
      if(_type == 2){
          $('#table-attachment2').load('{{url('/agent/booking/getattachments/' . $vm->dto->id . '/2')}}');
      }else if(_type == 3){
          $('#table-attachment3').load('{{url('/agent/booking/getattachments/' . $vm->dto->id . '/3')}}');
      }

      notifySuccess('附件已上载。');
      $('#ModalFiles').modal('hide');
     var drEvent = $('#file_file').dropify();
     drEvent = drEvent.data('dropify');
     drEvent.resetPreview();
     drEvent.clearElement();
    }else{
      notifySystemError(data['error']);
    }
  }).always(function() {
      stopSpin(_btn);
  });
});

$(document).on('click', '.btn-delete-attachment', function(e) {
   e.preventDefault();
   e.stopImmediatePropagation();
   @if($vm->dto->owner_id == Auth::user()->id)
     if (!confirm('是否删除此附件？')) return;
     var _id = $(this).attr('data-id');
     var _type = $(this).attr('data-type');
     var _formData = new FormData();
     _formData.append('booking_attachment_id', _id);
     $.ajax({
         headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         url: '{{url('/agent/booking/attachment/delete')}}',
         type: 'POST',
         contentType: false,
         processData: false,
         data: _formData,
     }).fail(function(xhr, text, err) {
        notifySystemError(err);
     }).done(function(data){
       if(data['result']){
         notifySuccess('附件已删除。');
         if(_type == 2){
             $('#table-attachment2').load('{{url('/agent/booking/getattachments/' . $vm->dto->id . '/2')}}');
         }else if(_type == 3){
             $('#table-attachment3').load('{{url('/agent/booking/getattachments/' . $vm->dto->id . '/3')}}');
         }
       }else{
         notifySystemError(data['error']);
       }

     }).always(function(){
     });
  @else
     notifySystemError('对不起，你没有删除权限。');
  @endif
});

$('#btnModalConfirmLoadingComplete').on('click', function(e) {
    e.preventDefault();
    if (!$('#form-loading').valid()) return false;

    startSpin($('#btnLoadingComplete'));
    var _formData = new FormData();
    _formData.append('id', '{{$vm->dto->id}}');
    $.ajax({
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '{{url('/agent/booking/complete/loading')}}',
        type: 'POST',
        contentType: false,
        processData: false,
        data: _formData,
    }).fail(function(xhr, text, err) {
      notifySystemError(err);
    }).done(function(data) {
       $('#modalLoadingComplete').modal('hide');
       notifySuccess('订单状态已更新!');
       setTimeout(function(){ location.href='{{url('/agent/bookings')}}'; }, 2000);
    }).always(function() {
       stopSpin($('#btnLoadingComplete'));
    });

 });

 //Declaration
 $('#btnModalConfirmDeclarationOK').on('click', function() {
   startSpin($('#btnDeclaration'));
   var _formData = new FormData();
   _formData.append('id', '{{$vm->dto->id}}');
   $.ajax({
       headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       },
       url: '{{url('/agent/booking/update/declaration')}}',
       type: 'POST',
       contentType: false,
       processData: false,
       data: _formData,
   }).fail(function(xhr, text, err) {
      notifySystemError(err);
   }).done(function(data){
     $('#modalConfirmDeclartion').modal('hide');
     notifySuccess('前往清关资料。');
     location.href= '{{url('agent/booking/edit/declaration/' . $vm->dto->id . '')}}';
   }).always(function() {
       stopSpin($('#btnDeclaration'));
   });

 });

//add fee
$('#btnFee1').on('click', function() {
  $('#fee_type').val('1');
});
$('#btnFee2').on('click', function() {
  $('#fee_type').val('2');
});
$('#btnFeeAdd').on('click', function() {
 if (!$('#form-fee').valid()) return false;
 var formData = new FormData($('#form-fee')[0]);
 startSpin($('#btnFeeAdd'));
 var _type = $('#fee_type').val();
 $.ajax({
     headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     },
     url: '{{url('agent/booking/add/fee')}}',
     type: 'POST',
     data:formData,
     enctype: 'multipart/form-data',
     contentType: false,
     processData: false,
 }).fail(function(xhr, text, err) {
    //notifySystemError(err);
    var json = $.parseJSON(xhr.responseText);

    if (json['errors'] && json['errors'] != '') {
        var msg ='';
        $.each(json['errors'], function (key, value) {
           msg +=  value + '<br/>';
        });
        notifySystemError(msg);
   }
 }).done(function(data) {
     $("#form-fee")[0].reset();
     if(_type == '1'){
       $('#table-fees-1').load('{{url('/agent/booking/getfees/' . $vm->dto->id . '/1')}}');
     }else if(_type == '2'){
       $('#table-fees').load('{{url('/agent/booking/getfees/' . $vm->dto->id . '/2')}}', function(){
         calTotal();
       });
     }
     $('#ModalFee').modal('hide');
     notifySuccess('费用已添加。');
 }).always(function() {
     stopSpin($('#btnFeeAdd'));
 });

});

$(document).on('click', '.btn-delete-fee', function(e) {
  e.preventDefault();
  e.stopImmediatePropagation();
  if (!confirm('是否删除此费用?')) return;
  var _id = $(this).attr('data-id');
  var _formData = new FormData();
  _formData.append('booking_fee_id', _id);
  $.ajax({
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '{{url('/agent/booking/fee/delete')}}',
      type: 'POST',
      contentType: false,
      processData: false,
      data: _formData,
  }).fail(function(xhr, text, err) {
     notifySystemError(err);
  }).done(function(data){
    if(data['result']){
      notifySuccess('费用已删除。');
      $('#table-fees-1').load('{{url('/agent/booking/getfees/' . $vm->dto->id . '/1')}}');
      $('#table-fees').load('{{url('/agent/booking/getfees/' . $vm->dto->id . '/2')}}', function(){
        calTotal();
      });
    }else{
      notifySystemError(data['error']);
    }

  }).always(function(){
  });
});

$("#freight_charges").change(function() {
  calTotal();
  calNetFreightCharges();
});

$("#document_fee").change(function() {
  calTotal();
  calNetFreightCharges();
});

$("#remote_fee").change(function() {
 calTotal();
});

$("#charges").change(function() {
 calTotal();
});

$("#container_loading_charges").change(function() {
 calTotal();
});

$("#entry_fee").change(function() {
 calTotal();
});

function calTotal(){
 var _otherFee = 0;
 $('#table-fees tr').each(function(){
    $(this).find('input').each(function(){
      _otherFee += parseFloat($(this).val());
    });
 });

 var _total = parseFloat($('#freight_charges').val()) - parseFloat($('#document_fee').val()) + parseFloat($('#remote_fee').val()) + parseFloat($('#charges').val()) + parseFloat($('#container_loading_charges').val())  + parseFloat($('#entry_fee').val()) + parseFloat(_otherFee);

 $('#total_charges').val(parseFloat(_total).toFixed(2));
}

function calNetFreightCharges(){
  var _total = parseFloat($('#freight_charges').val()) - parseFloat($('#document_fee').val());
  $('#net_freight_charges').val(parseFloat(_total).toFixed(2));
}
</script>
@endsection
