@foreach($data as $d)
<tr>
  <td>
    <button type="button" class="btn btn-icons btn-danger btn-delete-person" data-id="{{$d->booking_person_id}}"><i class="fa fa-trash"></i></button>
  </td>
  <td>{{$d->job}}</td>
  <td>{{$d->employee}}</td>
</tr>
@endforeach
