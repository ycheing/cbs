@foreach($data as $d)
<li>
  <button type="button" class="btn btn-icons btn-danger btn-delete-image" data-id="{{$d->booking_attachment_id}}"><i class="fa fa-close"></i></button>
  <!-- <a  class="btn btn-icons btn-success btn-download" href="{{url('upload/'. $d->attachment . '/' . $d->name . '')}}"><i class="fa fa-download"/></i></a> -->
  <a class="lightbox-image" data-lightbox="gallery" href="{{ url($d->path) }}" target="_blank"><img src="{{$d->path}}" /></a>
</li>
@endforeach
