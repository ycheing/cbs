@extends('layouts.agent')
@section('title', '添加新订单')
@section('header')
<style>
  .record_type div {
    cursor: pointer;
  }

  .record_type input[type="radio"] {
    display: none;
  }

  .record_type div.selected .card {
    background: linear-gradient(to right, #24e8a6, #09cdd1);
    color: #fff;
  }
</style>
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <!-- form -->
      <form method="post" id="form-booking" action="{{url('/agent/booking/type')}}">
        @csrf
        <div class="card-header header-sm ">
          <div class="d-flex ">
            <div class="wrapper d-flex align-items-center">
              <h2 class="card-title mb4"><i class="fa fa-plus text-primary mr-2"></i>添加新订单</h2>
            </div>
            <div class="wrapper ml-auto action-bar">
              <a class="btn btn-icons btn-outline-primary btn-sm" href="{{url('/agent/bookings')}}"><i
                  class="fa fa-close"></i></a>
            </div>
          </div>
        </div>
        <!--//card-header-->

        <div class="card-body">


          <div class="form-group row ">
            <div class="col">
              <label for="booking_type" class="mb-4 font-weight-bold">请选择需要添加的货柜记录</label>

              <div class="row record_type">
                <div
                  class="col-12 col-sm-6 col-md-6 grid-margin stretch-card justify-content-center align-items-center">
                  <input type="radio" name="booking_type" value="booking" />
                  <div class="card">
                    <div class="card-body">
                      <div class="d-flex justify-content-center align-items-center">
                        <h4 class="font-weight-bold text-center">预定记录</h4>
                      </div>
                    </div>
                  </div>
                </div>
                <!--// stretch-card-->

                @if(auth()->user()->can('add declaration'))

                <div
                  class="col-12 col-sm-6 col-md-6 grid-margin stretch-card justify-content-center align-items-center">
                  <input type="radio" name="booking_type" value="declaration" />
                  <div class="card">
                    <div class="card-body">
                      <div class="d-flex justify-content-center align-items-center">
                        <h4 class="font-weight-bold text-center">清关记录</h4>
                      </div>
                    </div>
                  </div>
                </div>

                @endif

              </div>
            </div>
            <!--//col-->
          </div>
          <!--//row-->

          <div class="form-group row mt-5">
            <div class="col text-right">
              <button class="btn btn-success" type="submit">下一步</button>
            </div>
            <!--//col-->
          </div>
          <!--//row-->

        </div>
        <!--card body-->
    </div>
    <!--card -->
    </form>
    <!-- // form-->
  </div>
</div>
@endsection

@section('footer')
{!! JsValidator::formRequest('App\Http\Requests\Agent\BookingTypeRequest', '#form-booking'); !!}
<script>
  $(".record_type :radio").hide().click(function(e){
      e.stopPropagation();
  });
  $(".record_type div").click(function(e){
      $(this).closest(".record_type").find("div").removeClass("selected");
      // $(this).addClass("selected").find("input:radio").attr('checked', true);
      $(this).addClass("selected").find("input:radio").click();
  });


  $('#form-booking').submit(function (e) {
      e.preventDefault();
      if (!$(this).valid()) return false;
      var _btn = $('button[type=submit]', this);
      startSpin(_btn);
      $.ajax({
          url: this.action,
          type: this.method,
          data: $(this).serialize(),
      }).fail(function(xhr, text, err) {
         notifySystemError(err);
      }).done(function(data) {
        if(data['location'] != ''){
          location.href= data['location'];
        }
      }).always(function() {
          stopSpin(_btn);
      });
  });
</script>
@endsection