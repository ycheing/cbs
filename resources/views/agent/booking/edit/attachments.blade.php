@foreach($data as $d)
<tr>
  <td style="width:10%">
    <button type="button" class="btn btn-icons btn-danger btn-delete-attachment" data-id="{{$d->booking_attachment_id}}" data-type="{{$d->type_id}}"><i class="fa fa-trash"></i></button>
  </td>
  <td><a href="{{$d->path}}" target="_blank" >{{$d->description}}</a></td>
</tr>
@endforeach
