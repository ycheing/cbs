@extends('layouts.agent')
@section('title', '更改密码')
@section('content')
    <div class="row">
      <div class="col-lg-6 col-sm-12 grid-margin">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">更改密码</h4>
            <form method="POST" action="{{ url('/password') }}" id="form-password">
                @csrf
                <div class="form-group ">
                    <label for="password" class="label">密码</label>
                    <div class="input-group">
                      <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="*********">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-key"></i>
                        </span>
                      </div>
                      </div>

                </div>

                <div class="form-group ">
                    <label for="password2" class="label">确认密码</label>
                    <div class="input-group">
                      <input id="password2" type="password" class="form-control" name="password2" required placeholder="*********" />
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-key"></i>
                        </span>
                      </div>
                    </div>

                </div>

                <div class="form-group ">
                    <button type="submit" class="btn btn-primary submit-btn btn-block">更改密码</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('footer')

    {!! JsValidator::formRequest('App\Http\Requests\Account\PasswordRequest', '#form-password'); !!}
    <script>
        $('#form-password').submit(function (e) {
            e.preventDefault();
            if (!$(this).valid()) return false;
            var _btn = $('button[type=submit]', this);
            startSpin(_btn);
            $.ajax({
                url: this.action,
                type: this.method,
                data: $(this).serialize(),
            }).fail(function(xhr, text, err) {
               notifySystemError(err);
            }).done(function(data) {
              notifySuccess('密码已更改。');
              setTimeout(function(){ location.href='{{url('/home')}}'; }, 2000);
            }).always(function() {
                stopSpin(_btn);
            });
        });
    </script>

@endsection
