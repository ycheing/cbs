@extends('layouts.app')
@section('title', '添加散货')
@section('content')

<div class="row">

  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">

      <!-- form -->
      <form method="post" id="form-cargo" action="{{url('cargo/add')}}" >
          @csrf
        <div class="card-header header-sm ">
          <div class="d-flex ">
              <div class="wrapper d-flex align-items-center">
                <h2 class="card-title mb4">添加散货</h2>
              </div>
              <div class="wrapper ml-auto action-bar">
                <button type="submit" data-toggle="tooltip" data-placement="top" data-original-title="储存" class="btn btn-icons btn-success btn-sm"><i class="fa fa-save"></i></button>
                <a class="btn btn-icons btn-outline-primary btn-sm"  data-toggle="tooltip" data-placement="top" data-original-title="返回"  href="{{url('/cargos')}}"><i class="fa fa-close"></i></a>

              </div>
          </div>
        </div><!--//card-header-->

        <div class="card-body">

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label"  for="date">日期</label>
                <div class="col-md-9">
                 <div class="input-group ">
                   <input id="date" type="text" class="form-control date" name="date"  data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                   <div class="input-group-append">
                        <span> <i class="fa fa-calendar"></i></span>
                   </div>
                 </div>
                </div>
              </div>
            </div><!-- col-md-6-->

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="arrival_date">预计到港日期</label>
                <div class="col-md-9">
                 <div class="input-group ">
                   <input id="arrival_date" type="text" class="form-control date" name="arrival_date" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                   <div class="input-group-append">
                      <span><i class="fa fa-calendar"></i></span>
                   </div>
                 </div>
                </div>
              </div>
            </div><!-- col-md-6-->

          </div><!--//row-->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label"  for="loading_date">装柜日期</label>
                <div class="col-md-9">
                 <div class="input-group ">
                   <input id="loading_date" type="text" class="form-control date" name="loading_date"  data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                   <div class="input-group-append">
                        <span> <i class="fa fa-calendar"></i></span>
                   </div>
                 </div>
                </div>
              </div>
            </div><!-- col-md-6-->

            <div class="col-md-6">
            
            </div><!-- col-md-6-->

          </div><!--//row-->

          <div class="row">

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="owner_id">负责人</label>
                <div class="col-md-9">
                 <select name="owner_id" class="form-control ">
                   <option value=""></option>
                   @foreach($vm->GetEmployees() as $employee)
                     <option value="{{$employee->user_id}}">{{$employee->name}}</option>
                   @endforeach
                 </select>
               </div><!--//col-md-9-->
             </div><!--//form-group-->
            </div><!-- col-md-6-->

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="status_id">状态</label>
                <div class="col-md-9">
                 <select name="status_id" class="form-control ">
                   @foreach($vm->GetStatuses() as $k => $v)
                      <option value="{{$k}}">{{$v}}</option>
                   @endforeach
                 </select>
               </div><!--//col-md-9-->
             </div><!--//form-group-->
            </div><!-- col-md-6-->
          </div><!--//row-->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_id">客户代码</label>
                <div class="col-md-9">
                  <select name="customer_id" class="select2 form-control">
                    <option value=""></option>
                    @foreach($vm->GetCustomers() as $customer)
                      <option value="{{$customer->id}}">{{$customer->code}}</option>
                    @endforeach
                  </select>
                </div><!--//col-md-9-->
              </div><!--//form-group-->
            </div><!--//col-->

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customs_broker_id">清关行</label>
                <div class="col-md-9">
                  <select name="customs_broker_id" class="select2 form-control">
                    <option value=""></option>
                    @foreach($vm->GetCustomsBrokers() as $cb)
                        <option value="{{$cb->id}}">{{$cb->code}}</option>
                    @endforeach
                  </select>
                </div><!--//col-md-9-->
              </div><!--//form-group-->
            </div><!--//col-->
          </div><!--row-->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="warehouse_id">收货仓库</label>
                <div class="col-md-9">
                  <select name="warehouse_id" class="select2 form-control">
                    <option value=""></option>
                    @foreach($vm->GetWarehouses() as $wareshouse)
                      <option value="{{$wareshouse->id}}">{{$wareshouse->code}}</option>
                    @endforeach
                  </select>
                </div><!--//col-md-9-->
              </div><!--//form-group-->
            </div><!--//col-->

            <div class="col-md-6">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="tracking_no">追踪编号</label>
                  <div class="col-md-9">
                    <input type="text" id="tracking_no" class=" form-control"  name="tracking_no" placeholder=""  />
                  </div><!--//col-->
              </div>
            </div><!--//col-->
          </div><!--row-->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="supplier_inv_no">供应商运费单</label>
                  <div class="col-md-9">
                    <input type="text" id="supplier_inv_no" class=" form-control"  name="supplier_inv_no" placeholder=""  />
                  </div><!--//col-->
              </div>
            </div><!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="supplier_inv_amount">供应商运费单 金额 (RM)</label>
                  <div class="col-md-9">
                    <input type="text" id="supplier_inv_amount" class=" form-control"  name="supplier_inv_amount" placeholder=""  />
                  </div><!--//col-->
              </div>
            </div><!--//col-->
          </div><!--row-->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="cbm">立方数</label>
                  <div class="col-md-9">
                    <input type="text" id="cbm" class=" form-control"  name="cbm" placeholder=""  />
                  </div><!--//col-->
              </div>

            </div><!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="pack">箱数</label>
                  <div class="col-md-9">
                    <input type="text" id="pack" class=" form-control"  name="pack" placeholder=""  />
                  </div><!--//col-->
              </div>
            </div><!--//col-->
          </div><!--row-->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="supplier_goods_inv">供应商货单</label>
                  <div class="col-md-9">
                    <input type="text" id="supplier_goods_inv" class=" form-control"  name="supplier_goods_inv" placeholder=""  />
                  </div><!--//col-->
              </div>
            </div><!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="supplier_goods_amount">供应商货单 金额 (RM)</label>
                  <div class="col-md-9">
                    <input type="text" id="supplier_goods_amount" class=" form-control"  name="supplier_goods_amount" placeholder=""  />
                  </div><!--//col-->
              </div>
            </div><!--//col-->
          </div><!--row-->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="customer_goods_inv">客户货单</label>
                  <div class="col-md-9">
                    <input type="text" id="customer_goods_inv" class=" form-control"  name="customer_goods_inv" placeholder=""  />
                  </div><!--//col-->
              </div>
            </div><!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="customer_goods_amount">客户货单 金额 (RM)</label>
                  <div class="col-md-9">
                    <input type="text" id="customer_goods_amount" class=" form-control"  name="customer_goods_amount" placeholder=""  />
                  </div><!--//col-->
              </div>
            </div><!--//col-->
          </div><!--row-->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="transport_inv">运费单</label>
                  <div class="col-md-9">
                    <input type="text" id="transport_inv" class=" form-control"  name="transport_inv" placeholder=""  />
                  </div><!--//col-->
              </div>
            </div><!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="transport_inv_amount">运费单 金额 (RM)</label>
                  <div class="col-md-9">
                    <input type="text" id="transport_inv_amount" class=" form-control"  name="transport_inv_amount" placeholder=""  />
                  </div><!--//col-->
              </div>
            </div><!--//col-->
          </div><!--row-->

        </div> <!--card body-->
      </form>
      <!-- // form-->
    </div><!--card -->


  </div>
</div>
@endsection

@section('footer')

{!! JsValidator::formRequest('App\Http\Requests\Admin\CargoRequest', '#form-cargo'); !!}
<script>
$('#form-cargo').submit(function (e) {
    e.preventDefault();
    if (!$(this).valid()) return false;
    var _btn = $('button[type=submit]', this);
    startSpin(_btn);
      var formData = new FormData($(this)[0]);
    $.ajax({
        url: this.action,
        type: this.method,
        data: formData,
        enctype: 'multipart/form-data',
        contentType: false,
        processData: false,
    }).fail(function(xhr, text, err) {
       notifySystemError(err);
    }).done(function(data) {
      notifySuccess('散货已添加。');
        setTimeout(function(){ location.href='{{url('/cargo/edit/')}}' + '/' + data['id']; }, 2000);
    }).always(function() {
        stopSpin(_btn);
    });
});
</script>

@endsection
