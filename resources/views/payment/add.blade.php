@extends('layouts.app')
@section('title', '添加货款')
@section('content')

<div class="row">

  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">

      <!-- form -->

      <form method="post" id="form-payment-transfer" action="{{url('payment/add')}}">
        @csrf
        <div class="card-header header-sm ">
          <div class="d-flex ">
            <div class="wrapper d-flex align-items-center">
              <h2 class="card-title mb4">添加货款</h2>
            </div>
            <div class="wrapper ml-auto action-bar">
              <button type="submit" data-toggle="tooltip" data-placement="top" data-original-title="储存"
                class="btn btn-icons btn-success btn-sm"><i class="fa fa-save"></i></button>
              <a class="btn btn-icons btn-outline-primary btn-sm" data-toggle="tooltip" data-placement="top"
                data-original-title="返回" href="{{url('/payments')}}"><i class="fa fa-close"></i></a>

            </div>
          </div>
        </div>
        <!--//card-header-->

        <div class="card-body">
          <div class="d-flex pb-2 mb-4 border-bottom  align-items-center">
            <i class="mdi mdi-bank-transfer mr-3 icon-md text-primary"></i>客户资料
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_id">客户代码</label>
                <div class="col-md-9">
                  <select name="customer_id" class="select2 form-control">
                    <option value=""></option>
                    @foreach($vm->GetCustomers() as $customer)
                    <option value="{{$customer->id}}">{{$customer->code}}</option>
                    @endforeach
                  </select>
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_name">客户名称</label>
                <div class="col-md-9">
                  <input name="customer_name" id="input-customer-name" class="form-control" />
                  <input type="hidden" name="customer_code" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
          </div>
          <!--row-->

          <div class="row">
            <div class="col-md-6">

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="status">状态</label>
                <div class="col-md-9">
                  <select name="status" class="form-control ">
                    @foreach($vm->GetStatuses() as $status)
                    <option value="{{$status->id}}">{{$status->name}}</option>
                    @endforeach
                  </select>
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_amount">金额 (RM)</label>
                <div class="col-md-9">
                  <input type="text" id="customer_amount" class=" form-control" name="customer_amount" placeholder="" />
                </div>
                <!--//col-->
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_exchange_rate">汇率</label>
                <div class="col-md-9">
                  <input type="text" id="customer_exchange_rate" class=" form-control" name="customer_exchange_rate"
                    placeholder="" />
                </div>
                <!--//col-->
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_total">金额 (RMB)</label>
                <div class="col-md-9">
                  <input type="text" id="customer_total" class=" form-control" name="customer_total" placeholder="" />
                </div>
                <!--//col-->
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="purpose_payment">付款目的</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="purpose_payment" placeholder="" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->

            </div>

            <div class="col-md-6">


              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="payment_date">收款日期</label>
                <div class="col-md-9">
                  <div class="input-group ">
                    <input id="payment_date" type="text" class="form-control date" name="payment_date"
                      data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                    <div class="input-group-append">
                      <span> <i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="remittance_date">汇款日期</label>
                <div class="col-md-9">
                  <div class="input-group ">
                    <input id="remittance_date" type="text" class="form-control date" name="remittance_date"
                      data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                    <div class="input-group-append">
                      <span> <i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>
              <!--//form-group-->
            </div>
          </div>

        </div>
        <!--card body-->
    </div>
    <!--card -->

    </form>
    <!-- // form-->
  </div>
</div>
@endsection

@section('footer')

{!! JsValidator::formRequest('App\Http\Requests\Admin\PaymentTransferRequest', '#form-payment-transfer'); !!}
<script>
  $('#form-payment-transfer').submit(function (e) {
    e.preventDefault();
    if (!$(this).valid()) return false;
    var _btn = $('button[type=submit]', this);
    startSpin(_btn);
      var formData = new FormData($(this)[0]);
    $.ajax({
        url: this.action,
        type: this.method,
        data: formData,
        enctype: 'multipart/form-data',
        contentType: false,
        processData: false,
    }).fail(function(xhr, text, err) {
       notifySystemError(err);
    }).done(function(data) {
      notifySuccess('货款已添加。');
        setTimeout(function(){ location.href='{{url('/payment/edit/')}}' + '/' + data['id']; }, 2000);
    }).always(function() {
        stopSpin(_btn);
    });
});

//customer change
$('select[name=\'customer_id\']').on('change', function() {

  $.ajax({
    type:"GET",
    url: '{{url('getcustomer?customer_id=')}}' + this.value,
    dataType: 'json',
    beforeSend: function() {
      $('select[name=\'customer_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      if(json.hasOwnProperty('code')){
        if (json['code'] && json['code'] != '') {
          $('input[name=\'customer_code\']').val(json['code']);
        }
      }
      if(json.hasOwnProperty('name')){
        if (json['name'] && json['name'] != '') {
          $('input[name=\'customer_name\']').val(json['name']);
        }
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('select[name=\'customer_id\']').trigger('change');

$("#customer_amount").change(function() {
  calCustomerTotal();
});
$("#customer_exchange_rate").change(function() {
  calCustomerTotal();
});
// $("#supplier_amount").change(function() {
//   calSupplierTotal();
// });
// $("#supplier_exchange_rate").change(function() {
//   calSupplierTotal();
// });
// $("#supplier_bank_charges").change(function() {
//   calSupplierNetTotal();
// });
function calCustomerTotal(){
  var _total = $('#customer_amount').val() * $('#customer_exchange_rate').val();
  $('#customer_total').val(parseFloat(_total).toFixed(2));
}
// function calSupplierTotal(){
//   var _total = $('#supplier_amount').val() * $('#supplier_exchange_rate').val();
//   $('#supplier_total').val(parseFloat(_total).toFixed(2));
// }
// function calSupplierNetTotal(){
//   var _total = parseFloat($('#supplier_total').val()) - parseFloat($('#supplier_bank_charges').val());
//   $('#supplier_net_amount').val(parseFloat(_total).toFixed(2));
// }
</script>

@endsection