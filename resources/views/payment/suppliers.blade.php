@foreach($data as $k => $d)
<tr style='border-top:solid 2px #000'>
  <td>
    <button type="button" class="btn btn-icons btn-success btn-edit-supplier" data-id="{{$d->id}}"><i class="fa fa-pencil"></i></button>
    <button type="button" class="btn btn-icons btn-danger btn-delete-supplier" data-id="{{$d->id}}"><i class="fa fa-trash"></i></button>
  </td>
  <td>{{$d->supplier_name}}</td>
  <td class="text-right">{{$d->supplier_amount}}</td>
  <td class="text-right">{{$d->supplier_exchange_rate}}</td>
  <td class="text-right">{{$d->supplier_total}}
    <input type="hidden" name="sp_total[]" class="sp_total" value="{{$d->supplier_total}}" />
  </td>
  @if($d->total_beneficiary > 0)
    <td>{{$d->beneficiaries[0]->received_date}}</td>
    <td>{{$d->beneficiaries[0]->beneficiary}}</td>
    <td>
      @if($d->beneficiaries[0]->attachment_path != '')
        <a href="{{$d->beneficiaries[0]->attachment_path}}" target="_blank" class="btn btn-primary">下载</a>
      @endif
    </td>
    <td class="text-right">{{$d->beneficiaries[0]->amount}}</td>
    <td class="text-right">{{$d->beneficiaries[0]->bank_charges}}</td>
    <td class="text-right">{{$d->beneficiaries[0]->net_amount}}  </td>
    <td>
      <button type="button" class="btn btn-edit-beneficiary text-success" data-id="{{$d->beneficiaries[0]->id}}"><i class="fa fa-pencil"></i></button>
      <button type="button" class="btn btn-delete-beneficiary text-danger" data-id="{{$d->beneficiaries[0]->id}}"><i class="fa fa-close"></i></button>
    </td>
  @else
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  @endif
</tr>
@foreach($d->beneficiaries as $k => $b)
  @if($k != 0)
  <tr>
    <td colspan="5"></td>
    <td>{{$b->received_date}}</td>
    <td>{{$b->beneficiary}}</td>
    <td>
      @if($b->attachment_path != '')
        <a href="{{$b->attachment_path}}" target="_blank" class="btn btn-primary">下载</a>
      @endif
    </td>
    <td class="text-right">{{$b->amount}}</td>
    <td class="text-right">{{$b->bank_charges}}</td>
    <td class="text-right">{{$b->net_amount}}</td>
    <td>
      <button type="button" class="btn btn-edit-beneficiary text-success" data-id="{{$b->id}}"><i class="fa fa-pencil"></i></button>
      <button type="button" class="btn btn-delete-beneficiary text-danger" data-id="{{$b->id}}"><i class="fa fa-close"></i></button></td>
  </tr>
  @endif
@endforeach
<tr style='border-bottom:solid 2px #000'>
  <td colspan="8" class="text-right">
    <b>结余</b>
  </td>
  <td class="text-right">{{$d->balance}}</td>
  <td colspan="3" class="text-right">
    <button type="button" data-supplier-id="{{$d->id}}" class="btn btn-success  btn-xs btn-add-beneficiary"><i class="fa fa-plus"></i>添加收款人</button>
  </td>
</tr>
@endforeach
