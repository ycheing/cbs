<div class="modal fade" id="modalBeneficiary" role="dialog" aria-labelledby="modalBeneficiaryLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalBeneficiaryLabel">添加收款人</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" id="form-payment-beneficiary" action="{{url('payment/beneficiary/add')}}">
          @csrf
          <input type="hidden" id="type" name="type" value="" />
          <input type="hidden" id="payment_id" name="payment_id" value="{{$vm->dto->id}}" />
          <input type="hidden" id="payment_supplier_id" name="payment_supplier_id" value="" />
          <input type="hidden" id="payment_beneficiary_id" name="payment_beneficiary_id" value="" />
          <div class="row">
            <div class="col-md-12">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="beneficiary_id">收款人</label>
                <div class="col-md-9">
                  <select name="beneficiary_id" id="beneficiary_id" class="select22 form-control">
                    <option value=""></option>
                    @foreach($vm->GetBeneficiaries() as $cb)
                    <option value="{{$cb->id}}">{{$cb->name}}</option>
                    @endforeach
                  </select>
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="amount">金额 (RMB)</label>
                <div class="col-md-9">
                  <input type="text" id="amount" class=" form-control" name="amount" placeholder="" />
                </div>
                <!--//col-->
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="bank_charges">银行收费</label>
                <div class="col-md-9">
                  <input type="text" id="bank_charges" class=" form-control" name="bank_charges" placeholder="" />
                </div>
                <!--//col-->
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="net_amount">净额 (RMB)</label>
                <div class="col-md-9">
                  <input type="text" id="net_amount" class=" form-control" name="net_amount" placeholder="" />
                </div>
                <!--//col-->
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="received_date">到账日期</label>
                <div class="col-md-9">
                  <div class="input-group ">
                    <input id="received_date" type="text" class="form-control date" name="received_date"
                      data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                    <div class="input-group-append">
                      <span><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="file_attachment">附件</label>
                <div class="col-md-9">
                  <div id="bnf_attachment"></div>
                  <input type="file" class="dropify form-control" name="file_attachment" />
                </div>
                <!--//col-->
              </div>

            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button id="btnBeneficiaryOK" type="button" class="btn btn-primary">确认</button>
        <button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">取消</button>
      </div>
    </div>
  </div>
</div>