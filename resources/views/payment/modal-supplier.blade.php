<div class="modal fade" id="modalSupplier" tabindex="-1" role="dialog" aria-labelledby="modalSupplierLabel" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalSupplierLabel">供应商</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <form method="post" id="form-payment-supplier">
                @csrf
                <input type="hidden" id="type" name="type" value="" />
                <input type="hidden" id="payment_id" name="payment_id" value="{{$vm->dto->id}}" />
                <input type="hidden" id="payment_supplier_id" name="payment_supplier_id" value="" />
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"  for="supplier_id">供应商</label>
                        <div class="col-md-9">
                        <select name="supplier_id" id="supplier_id" class="form-control">
                          <option value=""></option>
                          @foreach($vm->GetSuppliers() as $s)
                            <option value="{{$s->id}}">{{$s->name}}</option>
                          @endforeach
                        </select>
                      </div><!--//col-->
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"  for="supplier_amount">金额 (RM)</label>
                        <div class="col-md-9">
                          <input type="text" id="supplier_amount" class=" form-control"  name="supplier_amount" placeholder="" />
                        </div><!--//col-->
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 col-form-label" for="supplier_exchange_rate">汇率</label>
                      <div class="col-md-9">
                        <input type="text" id="supplier_exchange_rate" class=" form-control"  name="supplier_exchange_rate" placeholder="" />
                      </div><!--//col-->
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 col-form-label"   for="supplier_total">金额 (RMB)</label>
                      <div class="col-md-9">
                        <input type="text" id="supplier_total" class=" form-control"  name="supplier_total" placeholder="" />
                      </div><!--//col-->
                    </div>



                  </div>

                </div>
              </form>
            </div>
            <div class="modal-footer">
                <button id="btnSupplierOK" type="button" class="btn btn-primary">确认</button>
                <button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">取消</button>
            </div>
        </div>
    </div>
</div>
