@extends('layouts.app')
@section('title', '修改货款')
@section('content')

<div class="row">

  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <!-- form -->
      <form method="post" id="form-payment" action="{{url('payment/edit')}}">
        @csrf
        <input type="hidden" name="id" value="{{$vm->dto->id}}" />
        <div class="card-header header-sm ">
          <div class="d-flex ">
            <div class="wrapper d-flex align-items-center">
              <h2 class="card-title mb4">修改货款</h2>
            </div>
            <div class="wrapper ml-auto action-bar">
              <button type="submit" class="btn btn-icons btn-success btn-sm" data-toggle="tooltip" data-placement="top"
                data-original-title="储存"><i class="fa fa-save"></i></button>
              <button type="button" class="btn btn-icons btn-danger btn-sm" id="btnDelete" data-toggle="modal"
                data-target="#modalConfirmDelete">
                <i class="fa fa-trash"></i>
              </button>
              <a class="btn btn-icons btn-outline-primary btn-sm" data-toggle="tooltip" data-placement="top"
                data-original-title="返回" href="{{url('/payments')}}"><i class="fa fa-close"></i></a>

            </div>
          </div>
        </div>
        <!--//card-header-->

        <div class="card-body">
          <div class="d-flex pb-2 mb-4 border-bottom  align-items-center">
            <i class="mdi mdi-bank-transfer mr-3 icon-md text-primary"></i>客户资料
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_id">客户代码</label>
                <div class="col-md-9">
                  <select name="customer_id" class="select2 form-control">
                    <option value=""></option>
                    @foreach($vm->GetCustomers() as $customer)
                    @if($vm->dto->customer_id == $customer->id)
                    <option value="{{$customer->id}}" selected>{{$customer->code}}</option>
                    @else
                    <option value="{{$customer->id}}">{{$customer->code}}</option>
                    @endif
                    @endforeach
                  </select>
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_name">客户名称</label>
                <div class="col-md-9">
                  <input name="customer_name" id="input-customer-name" class="form-control"
                    value="{{$vm->dto->customer_name}}" />
                  <input type="hidden" name="customer_code" value="{{$vm->dto->customer_code}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->
            </div>
            <!--//col-->
            <div class="col-md-4">

            </div>
            <!--//col-->
          </div>
          <!--row-->


          <div class="row">
            <div class="col-md-6">

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_amount">金额 (RM)</label>
                <div class="col-md-9">
                  <input type="text" id="customer_amount" class=" form-control" name="customer_amount" placeholder=""
                    value="{{$vm->dto->customer_amount}}" />
                </div>
                <!--//col-->
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_exchange_rate">汇率</label>
                <div class="col-md-9">
                  <input type="text" id="customer_exchange_rate" class=" form-control" name="customer_exchange_rate"
                    placeholder="" value="{{$vm->dto->customer_exchange_rate}}" />
                </div>
                <!--//col-->
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_total">金额 (RMB)</label>
                <div class="col-md-9">
                  <input type="text" id="customer_total" class=" form-control" name="customer_total" placeholder=""
                    value="{{$vm->dto->customer_total}}" />
                </div>
                <!--//col-->
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="purpose_payment">付款目的</label>
                <div class="col-md-9">
                  <input type="text" class=" form-control" name="purpose_payment" placeholder=""
                    value="{{$vm->dto->purpose_payment}}" />
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->

            </div>

            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="status_id">状态</label>
                <div class="col-md-9">
                  <select name="status_id" class="form-control ">
                    @foreach($vm->GetStatuses() as $status)
                    @if($status->id == $vm->dto->status_id)
                    <option value="{{$status->id}}" selected>{{$status->name}}</option>
                    @else
                    <option value="{{$status->id}}">{{$status->name}}</option>
                    @endif
                    @endforeach
                  </select>
                </div>
                <!--//col-md-9-->
              </div>
              <!--//form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="payment_date">收款日期</label>
                <div class="col-md-9">
                  <div class="input-group ">
                    <input id="payment_date" type="text" class="form-control date" name="payment_date"
                      data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->payment_date}}" />
                    <div class="input-group-append">
                      <span> <i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="remittance_date">汇款日期</label>
                <div class="col-md-9">
                  <div class="input-group ">
                    <input id="remittance_date" type="text" class="form-control date" name="remittance_date"
                      data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->remittance_date}}" />
                    <div class="input-group-append">
                      <span> <i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>
              <!--//form-group-->

            </div>
          </div>

          <div class="d-flex pb-2 mb-4 border-bottom  align-items-center">
            <i class="mdi mdi-bank-transfer mr-3 icon-md text-primary"></i>供应商资料
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table  mb-2">
                  <thead>
                    <tr class="bg-dark text-white">
                      <th></th>
                      <th>供应商</th>
                      <th>金额 (RM)</th>
                      <th>汇率</th>
                      <th>金额 (RMB)</th>
                      <th>到账日期</th>
                      <th>收款人</th>
                      <th>附件</th>
                      <th>金额 (RMB)</th>
                      <th>银行收费</th>
                      <th>净额 (RMB)</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody id="suppliers">
                  </tbody>
                  <tfoot>
                    <tr style="border-bottom:double 3px #000">
                      <td colspan="4" class="text-right"><b>结余</b></td>
                      <td class="text-right"><span id="lblBalance"></span></td>
                      <td colspan="5"></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-md-12 text-right">
              <button type="button" id="btnAddSupplier" class="btn btn-success"><i class="fa fa-plus"></i>添加供应商</button>
            </div>
          </div>
          <!--//row-->



        </div>
        <!--card body-->
    </div>
    <!--card -->

    </form>
    <!-- // form-->
  </div>
</div>

<form method="POST" id="form-delete" action="{{url('/payment/delete')}}">
  @csrf
  <input type="hidden" name="id" value="{{$vm->dto->id}}" />
</form>
<!-- // form-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除货款</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>请确认是否要删除此货款?</p>
      </div>
      <div class="modal-footer">
        <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
        <button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">取消</button>
      </div>
    </div>
  </div>
</div>

<!-- add supplier -->
@include('payment.modal-supplier')
<!-- add beneficiary -->
@include('payment.modal-beneficiary')

@endsection

@section('footer')

{!! JsValidator::formRequest('App\Http\Requests\Admin\PaymentInfoRequest', '#form-payment'); !!}
{!! JsValidator::formRequest('App\Http\Requests\Admin\PaymentSupplierRequest', '#form-payment-supplier'); !!}
{!! JsValidator::formRequest('App\Http\Requests\Admin\PaymentBeneficiaryRequest', '#form-payment-beneficiary'); !!}
<script>
  $('#suppliers').load('{{url('/payment/suppliers/' . $vm->dto->id . '')}}', function() {
  /* When load is done */
   calBalance();
});
// $('#suppliers').load('{{url('/payment/suppliers/' . $vm->dto->id . '')}}');

$('#form-payment').submit(function (e) {
    e.preventDefault();
    if (!$(this).valid()) return false;
    var _btn = $('button[type=submit]', this);
    startSpin(_btn);
    var formData = new FormData($(this)[0]);
    // var _data = formToJSON($(this)[0]);
    $.ajax({
        url: this.action,
        type: this.method,
        data: formData,
        enctype: 'multipart/form-data',
        contentType: false,
        processData: false,
    }).fail(function(xhr, text, err) {
       notifySystemError(err);
    }).done(function(data) {
      notifySuccess('货款已修改。');
      setTimeout(function(){ location.href='{{url('/payments')}}'; }, 2000);
    }).always(function() {
        stopSpin(_btn);
    });
});

$('#btnModalConfirmDeleteOK').on('click', function() {
   if (!$('#form-delete').valid()) return false;
   startSpin($('#btnDelete'));
   $.ajax({
       url: '{{url('/payment/delete')}}',
       type: 'POST',
       data: $('#form-delete').serialize(),
   }).fail(function(xhr, text, err) {
      notifySystemError(err);
   }).done(function(data) {
     $('#modalConfirmDelete').modal('hide');
     if(data['error']){
       notifySystemError(data['error']);
     }else{
       notifySuccess('货款已删除。');
       setTimeout(function(){ location.href='{{url('/payments')}}'; }, 2000);
     }

   }).always(function() {
       stopSpin($('#btnDelete'));
   });
});
//customer change
$('select[name=\'customer_id\']').on('change', function() {
  $.ajax({
    type:"GET",
    url: '{{url('getcustomer?customer_id=')}}' + this.value,
    dataType: 'json',
    beforeSend: function() {
      $('select[name=\'customer_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      if(json.hasOwnProperty('code')){
        if (json['code'] && json['code'] != '') {
          $('input[name=\'customer_code\']').val(json['code']);
        }
      }
      if(json.hasOwnProperty('name')){
        if (json['name'] && json['name'] != '') {
          $('input[name=\'customer_name\']').val(json['name']);
        }
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('select[name=\'customer_id\']').trigger('change');

$("#customer_total").change(function() {
  calBalance();
});
$("#customer_amount").change(function() {
  calCustomerTotal();
  calBalance();
});
$("#customer_exchange_rate").change(function() {
  calCustomerTotal();
  calBalance();
});
$("#supplier_amount").change(function() {
  calSupplierTotal();
});
$("#supplier_exchange_rate").change(function() {
  calSupplierTotal();
});
$("#amount").change(function() {
  calSupplierNetTotal();
});
$("#bank_charges").change(function() {
  calSupplierNetTotal();
});
function calCustomerTotal(){
  var _total = $('#customer_amount').val() * $('#customer_exchange_rate').val();
  $('#customer_total').val(parseFloat(_total).toFixed(2));
}
function calSupplierTotal(){
  var _samt = $('#supplier_amount').val();
  var _exchange_rate = $('#supplier_exchange_rate').val();

  if( _samt || _samt === false || _samt === 'undefined' ){
    _samt = $('#supplier_amount').val();
  }else{
    _samt = 0;
  }

  if( _exchange_rate || _exchange_rate === false || _exchange_rate === 'undefined' ){
    _exchange_rate = $('#supplier_exchange_rate').val();
  }else{
    _exchange_rate = 0;
  }
  var _total = _samt * _exchange_rate;
  $('#supplier_total').val(parseFloat(_total).toFixed(2));
}
function calSupplierNetTotal(){
  var _amt = $('#amount').val();
  var _bank_charges = $('#bank_charges').val();

  if( _amt || _amt === false || _amt === 'undefined' ){
    _amt = $('#amount').val();
  }else{
    _amt = 0;
  }
  if( _bank_charges || _bank_charges === false || _bank_charges === 'undefined' ){
    _bank_charges = $('#bank_charges').val();
  }else{
    _bank_charges = 0;
  }
  var _total = parseFloat(_amt) - parseFloat(_bank_charges);
  $('#net_amount').val(parseFloat(_total).toFixed(2));
}

$(document).on('click', '.btn-delete-attachment', function(e) {
  e.preventDefault();
  e.stopImmediatePropagation();
  if (!confirm('是否删除此附件？')) return;
  var _formData = new FormData();
  var _type = $(this).attr('data-type');
  _formData.append('payment_id', '{{$vm->dto->id}}');
  _formData.append('type', _type);
  $.ajax({
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '{{url('/payment/remove/attachment')}}',
      type: 'POST',
      contentType: false,
      processData: false,
      data: _formData,
  }).fail(function(xhr, text, err) {
     notifySystemError(err);
  }).done(function(data){
    if(data['result']){
      notifySuccess('附件已删除。');
      if(_type==1){
        $('#customer_attachment').empty();
      }else if(_type==2){
        $('#supplier_attachment').empty();
      }
    }else{
      notifySystemError(data['error']);
    }

  }).always(function(){
  });
});

$('#btnAddSupplier').on('click', function() {
  $('#modalSupplierLabel').html('添加供应商');
  $("#form-payment-supplier")[0].reset();
  $('#sp_attachment').html('');
  $('#type').val('add');
  $('#payment_supplier_id').val('0');
  $('#modalSupplier').modal();
});

$('#btnSupplierOK').on('click', function(e) {
    e.preventDefault();
    if($('#type').val() == 'add'){
      supplier_add();
    }else if($('#type').val() == 'edit'){
      supplier_edit();
    }

});

$(document).on('click', '.btn-edit-supplier', function(e) {
  e.preventDefault();
  e.stopImmediatePropagation();
  $('#modalSupplierLabel').html('修改供应商');
  var _id = $(this).attr('data-id');

  $.ajax({
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '{{url('payment/supplier')}}' + '/' + _id,
      type: 'GET',
  }).fail(function(xhr, text, err) {
     notifySystemError(err);
  }).done(function(data){
    if(data['json']){
      // alert(data['json']['supplier_id']);
      $("#form-payment-supplier")[0].reset();
      $('#sp_attachment').html('');
      $('#type').val('edit');
      $('#payment_supplier_id').val(_id);
      if (data['json'].hasOwnProperty('supplier_id')) {
        $('#supplier_id').val(data['json']['supplier_id']);
      }
      if (data['json'].hasOwnProperty('supplier_amount')) {
        $('#supplier_amount').val(data['json']['supplier_amount']);
      }
      if (data['json'].hasOwnProperty('supplier_exchange_rate')) {
        $('#supplier_exchange_rate').val(data['json']['supplier_exchange_rate']);
      }
      if (data['json'].hasOwnProperty('supplier_total')) {
        $('#supplier_total').val(data['json']['supplier_total']);
      }
      if (data['json'].hasOwnProperty('supplier_bank_charges')) {
        $('#supplier_bank_charges').val(data['json']['supplier_bank_charges']);
      }
      if (data['json'].hasOwnProperty('supplier_net_amount')) {
        $('#supplier_net_amount').val(data['json']['supplier_net_amount']);
      }
      if (data['json'].hasOwnProperty('received_date')) {
        $('#received_date').val(data['json']['received_date']);
      }
      if (data['json'].hasOwnProperty('supplier_attachment_path')) {
        if(data['json']['supplier_attachment_path'] != ""){
          _attach = '<a href="' + data['json']['supplier_attachment_path'] +'" target="_blank" class="btn btn-primary mb-1 mr-1">下载</a><button type="button" class="btn btn-danger mb-1 btn-delete-supplier-attachment" data-id="'+ _id +'">删除</button>';
          $('#sp_attachment').html(_attach);
        }
      }
      $('#modalSupplier').modal();
    }
  }).always(function(){
  });

});

function supplier_add(){
  if (!$('#form-payment-supplier').valid()) return false;
  var _btn = $('#btnSupplierOK');
  startSpin(_btn);
  var formData = new FormData($('#form-payment-supplier')[0]);
  $.ajax({
    url:  '{{url('payment/supplier/add')}}',
    type: 'POST',
    data: formData,
    enctype: 'multipart/form-data',
    contentType: false,
    processData: false,
  }).fail(function(xhr, text, err) {
     notifySystemError(err);
  }).done(function(data) {
    notifySuccess('供应商已添加。');
    $('#modalSupplier').modal('hide');
    $('#suppliers').load('{{url('/payment/suppliers/' . $vm->dto->id . '')}}', function() {
       calBalance();
    });
  }).always(function() {
      stopSpin(_btn);
  });
}

function supplier_edit(){
  if (!$('#form-payment-supplier').valid()) return false;
  var _btn = $('#btnSupplierOK');
  startSpin(_btn);
  var formData = new FormData($('#form-payment-supplier')[0]);

  $.ajax({
    url:  '{{url('payment/supplier/edit')}}',
    type: 'POST',
    data: formData,
    enctype: 'multipart/form-data',
    contentType: false,
    processData: false,
  }).fail(function(xhr, text, err) {
     notifySystemError(err);
  }).done(function(data) {
    notifySuccess('供应商已修改。');
    $('#modalSupplier').modal('hide');
    $('#suppliers').load('{{url('/payment/suppliers/' . $vm->dto->id . '')}}', function() {
       calBalance();
    });
  }).always(function() {
      stopSpin(_btn);
  });
}

$(document).on('click', '.btn-delete-supplier', function(e) {
   e.preventDefault();
   e.stopImmediatePropagation();
   if (!confirm('是否删除此供应商?')) return;
   var _id = $(this).attr('data-id');
   var _formData = new FormData();
   _formData.append('id', _id);
   $.ajax({
       headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       },
       url: '{{url('payment/supplier/delete')}}',
       type: 'POST',
       contentType: false,
       processData: false,
       data: _formData,
   }).fail(function(xhr, text, err) {
      notifySystemError(err);
   }).done(function(data){
     if(data['result']){
       notifySuccess('供应商已删除。');
       $('#suppliers').load('{{url('/payment/suppliers/' . $vm->dto->id . '')}}', function() {
          calBalance();
       });
     }else{
       notifySystemError(data['error']);
     }

   }).always(function(){
   });
});


$(document).on('click', '.btn-add-beneficiary', function(e) {
   $('#form-payment-beneficiary')[0].reset();
   $('#form-payment-beneficiary #type').val("add");
   $('#form-payment-beneficiary #payment_supplier_id').val($(this).attr('data-supplier-id'));
   $('#form-payment-beneficiary #payment_beneficiary_id').val(0);
   $('#modalBeneficiary').modal();
});

$('#btnBeneficiaryOK').on('click', function(e) {
  e.preventDefault();
  if (!$('#form-payment-beneficiary').valid()) return false;
  var _btn = $(this);
  startSpin(_btn);
  var formData = new FormData($('#form-payment-beneficiary')[0]);
  if($('#form-payment-beneficiary #type').val() == "add"){
    $.ajax({
        url: '{{url('payment/beneficiary/add')}}',
        type: 'post',
        data: formData,
        enctype: 'multipart/form-data',
        contentType: false,
        processData: false,
    }).fail(function(xhr, text, err) {
       notifySystemError(err);
    }).done(function(data) {
       notifySuccess('收款人已添加。');
       $('#modalBeneficiary').modal('hide');
       $('#suppliers').load('{{url('/payment/suppliers/' . $vm->dto->id . '')}}', function() {
          calBalance();
       });
    }).always(function() {
        stopSpin(_btn);
    });
  }else if($('#form-payment-beneficiary #type').val() == "edit"){
    $.ajax({
        url: '{{url('payment/beneficiary/edit')}}',
        type: 'post',
        data: formData,
        enctype: 'multipart/form-data',
        contentType: false,
        processData: false,
    }).fail(function(xhr, text, err) {
       notifySystemError(err);
    }).done(function(data) {
       notifySuccess('收款人已修改。');
       $('#modalBeneficiary').modal('hide');
       $('#suppliers').load('{{url('/payment/suppliers/' . $vm->dto->id . '')}}', function() {
          calBalance();
       });
    }).always(function() {
        stopSpin(_btn);
    });
  }



});

$(document).on('click', '.btn-delete-beneficiary', function(e) {
   e.preventDefault();
   e.stopImmediatePropagation();
   if (!confirm('是否删除此收款人?')) return;
   var _id = $(this).attr('data-id');
   var _formData = new FormData();
   _formData.append('payment_beneficiary_id', _id);
   $.ajax({
       headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       },
       url: '{{url('payment/beneficiary/delete')}}',
       type: 'POST',
       contentType: false,
       processData: false,
       data: _formData,
   }).fail(function(xhr, text, err) {
      notifySystemError(err);
   }).done(function(data){
     if(data['result']){
       notifySuccess('收款人已删除。');
       $('#suppliers').load('{{url('/payment/suppliers/' . $vm->dto->id . '')}}', function() {
          calBalance();
       });
     }else{
       notifySystemError(data['error']);
     }

   }).always(function(){
   });
});

$(document).on('click', '.btn-edit-beneficiary', function(e) {
   e.preventDefault();
   e.stopImmediatePropagation();

   $('#modalBeneficiaryLabel').html('修改收款人');
   var _id = $(this).attr('data-id');

   $.ajax({
       headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       },
       url: '{{url('payment/beneficiary')}}' + '/' + _id,
       type: 'GET',
   }).fail(function(xhr, text, err) {
      notifySystemError(err);
   }).done(function(data){
     if(data['json']){
       $('#form-payment-beneficiary')[0].reset();
       $('#bnf_attachment').html('');
       $('#form-payment-beneficiary #type').val('edit');
       $('#payment_beneficiary_id').val(_id);
       if (data['json'].hasOwnProperty('payment_supplier_id')) {
         $('#form-payment-beneficiary #payment_supplier_id').val(data['json']['payment_supplier_id']);
       }
       if (data['json'].hasOwnProperty('beneficiary_id')) {
         $('#beneficiary_id').val(data['json']['beneficiary_id']);
       }
       if (data['json'].hasOwnProperty('amount')) {
         $('#amount').val(data['json']['amount']);
       }
       if (data['json'].hasOwnProperty('bank_charges')) {
         $('#bank_charges').val(data['json']['bank_charges']);
       }
       if (data['json'].hasOwnProperty('net_amount')) {
         $('#net_amount').val(data['json']['net_amount']);
       }
       if (data['json'].hasOwnProperty('received_date')) {
         $('#received_date').val(data['json']['received_date']);
       }
       if (data['json'].hasOwnProperty('attachment_path')) {
         if(data['json']['attachment_path'] != ""){
           _attach = '<a href="' + data['json']['attachment_path'] +'" target="_blank" class="btn btn-primary mb-1 mr-1">下载</a><button type="button" class="btn btn-danger mb-1 btn-delete-beneficiary-attachment" data-id="'+ _id +'">删除</button>';
           $('#bnf_attachment').html(_attach);
         }
       }
       $('#modalBeneficiary').modal();
     }
   }).always(function(){
   });

});


$(document).on('click', '.btn-delete-beneficiary-attachment', function(e) {
  e.preventDefault();
  e.stopImmediatePropagation();
  if (!confirm('是否删除此附件？')) return;
  var _formData = new FormData();
  var _id = $(this).attr('data-id');
  _formData.append('payment_beneficiary_id', _id);
  $.ajax({
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '{{url('/payment/beneficiary/attachment/remove')}}',
      type: 'POST',
      contentType: false,
      processData: false,
      data: _formData,
  }).fail(function(xhr, text, err) {
     notifySystemError(err);
  }).done(function(data){
    if(data['result']){
      notifySuccess('附件已删除。');
      $('#bnf_attachment').empty();
      $('#modalBeneficiary').modal('hide');
      $('#suppliers').load('{{url('/payment/suppliers/' . $vm->dto->id . '')}}', function() {
         calBalance();
      });
    }else{
      notifySystemError(data['error']);
    }

  }).always(function(){
  });
});

function calBalance(){
  var ct = $('#customer_total').val();
  var bal = 0;
  var total = 0;
  $("#suppliers input[type=hidden]").each(function() {
      total += parseFloat(this.value);
  });

  bal =  parseFloat(ct).toFixed(2) - parseFloat(total).toFixed(2);
  $('#lblBalance').html(parseFloat(bal).toFixed(2));
}

$('#modalBeneficiary').on('shown.bs.modal', function () {
    $("#beneficiary_id").select2({
        dropdownParent: $("#modalBeneficiary"),
        matcher: function (params, data) {
            // If there are no search terms, return all data
            if ($.trim(params.term) === "") {
                return data;
            }

            // `params.term` should be the search term, `data.text` is the option text
            if (
                data.text.toUpperCase().indexOf(params.term.toUpperCase()) === 0
            ) {
                return data;
            }

            // Return `null` if the term doesn't match from the start
            return null;
        },
    }).select2('open'); // 强制打开 Select2
});
</script>

@endsection