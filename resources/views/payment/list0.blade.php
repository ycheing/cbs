@extends('layouts.app')
@section('title', '货款清单')
@section('content')
<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center media-info">
            <h2 class="card-title">货款清单</h2>
          </div>
          <div class="wrapper ml-auto action-bar">
            <a href="{{url('/payment/add')}}" class="btn btn-primary btn-block"><i class="fa fa-plus "></i>添加货款</a>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="page-header-toolbar">
          <form method="GET" action="{{url('/payments')}}" style="width:100%">
            <div class="form-group row">
              <div class="col-md-6">
                <input name="keyword" id="keyword" placeholder="关键词" class="form-control"/>
              </div>
              <div class="col-md-1">
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </form>
        </div>
        <div class="table-responsive">
          <table class="table table-striped" >
            <thead>
              <tr>
                <th></th>
                <th style="width:10%" class="text-center">@sortablelink('status_id', ' 状态')</th>
                <th>@sortablelink('payment_date', ' 收款日期')</th>
                <th>@sortablelink('remittance_date', ' 汇款日期')</th>
                <th>@sortablelink('customer_code', ' 客户代码')</th>
                <th class="text-right">@sortablelink('customer_amount',' 金额 (RM)')</th>
                <th class="text-right">@sortablelink('customer_exchange_rate', ' 汇率')</th>
                <th class="text-right">@sortablelink('customer_total', ' 金额 (RMB)')</th>
                <th>供应商</th>
                <th class="text-right">金额 (RM)</th>
                <th class="text-right">汇率</th>
                <th class="text-right">金额 (RMB)</th>
                <th class="text-right">银行收费</th>
                <th class="text-right">净额 (RMB)</th>
                <th>到账日期</th>
              </tr>
            </thead>
            <tbody>
              @foreach($vm->dto as $payment)
              <tr>
                <td>
                  <a href="{{url('/payment/edit/'.$payment->id.'')}}" class="btn btn-icons btn-success btn-action "><i class="fa fa-pencil"></i></a>
                  <button type="button" class="btn btn-icons btn-danger  btn-delete" data-id="{{$payment->id}}"><i class="fa fa-trash"></i></button>
                </td>
                <td class="text-center">{!! $payment->status !!}</td>
                <td class="text-center">{{$payment->payment_date}} </td>
                <td class="text-center">{{$payment->remittance_date}} </td>
                <td>{{$payment->customer_code}} </td>
                <td class="text-right">{{$payment->customer_amount}} </td>
                <td class="text-right">{{$payment->customer_exchange_rate}} </td>
                <td class="text-right">{{$payment->customer_total}} </td>
                @if(count($payment->suppliers) > 0)
                <td>{{$payment->suppliers[0]->supplier_code}}</td>
                <td>{{$payment->suppliers[0]->supplier_amount}}</td>
                <td>{{$payment->suppliers[0]->supplier_exchange_rate}}</td>
                <td>{{$payment->suppliers[0]->supplier_total}}</td>
                <td>{{$payment->suppliers[0]->supplier_bank_charges}}</td>
                <td>{{$payment->suppliers[0]->supplier_net_amount}}</td>
                <td>{{$payment->suppliers[0]->received_date}}</td>
                @else
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                @endif
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!--table-responsive-->
        <div class="mt-5 ">
          {{$vm->paging->links()}}
        </div>
      </div><!--card-body-->
    </div>
  </div>
</div>

<form method="POST" id="form-delete" action="{{url('/payment/delete')}}">
  @csrf
  <input type="hidden" id="payment_id" name="id" value="" />
</form>
<!-- // form-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除货款</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <p>请确认是否要删除此货款?</p>
          </div>
          <div class="modal-footer">
            <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
            <button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">取消</button>
          </div>
      </div>
  </div>
</div>

@endsection
@section('footer')
<script>
    $('.btn-delete').on('click', function() {
       $('#modalConfirmDelete').modal();
       $('#payment_id').val($(this).attr('data-id'));
   });

   $('#btnModalConfirmDeleteOK').on('click', function() {
       if (!$('#form-delete').valid()) return false;

       $.ajax({
           url: '{{url('/payment/delete')}}',
           type: 'POST',
           data: $('#form-delete').serialize(),
       }).fail(function(xhr, text, err) {
          notifySystemError(err);
       }).done(function(data) {
         $('#modalConfirmDelete').modal('hide');
         if(data['error']){
           notifySystemError(data['error']);
         }else{
           notifySuccess('货款已删除。');
           setTimeout(function(){ location.href='{{url('/payments')}}'; }, 2000);
         }
       }).always(function() {
       });
   });
</script>

@endsection
