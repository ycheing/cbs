@extends('layouts.app')
@section('title', '月份货柜总报表')
@section('content')

<div class="row page-title-header">
  <div class="col-12">
    <div class="page-header">
      <h4 class="page-title">月份货柜总报表</h4>
    </div>
  </div>
  <div class="col-md-12">
    <div class="page-header-toolbar">
      <form method="GET" action="{{url('/report/monthly_statement')}}" style="width:100%">
        <div class="row">
          <div class="col-md-2">
            <div class="form-group row">
              <label class="col-4 col-form-label " for="year" >年</label>
              <div class="col-8">
                <select class="form-control " name="year" id="year">
                  @foreach($vm->GetYears() as $k => $v)
                    @if($v == $vm->inputs['year'])
                    <option value="{{$v}}" selected>{{$v}}</option>
                    @else
                    <option value="{{$v}}" >{{$v}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="month" >月</label>
              <div class="col-8">
                <select class="form-control" name="month" id="month">
                  @foreach($vm->GetMonths() as $k => $v)
                    @if($k== $vm->inputs['month'])
                      <option value="{{$k}}" selected>{{$v}}</option>
                    @else
                      <option value="{{$k}}">{{$v}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="customer_id" >客户</label>
              <div class="col-8">
                <select name="customer_id" id="customer_id" class="select2 form-control">
                  <option value="">全部</option>
                  @foreach($vm->GetCustomers() as $customer)
                    @if($customer->id == $vm->inputs['customer_id'])
                      <option value="{{$customer->id}}" selected>{{$customer->code}}</option>
                    @else
                      <option value="{{$customer->id}}">{{$customer->code}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="billing_company_id" >开票公司</label>
              <div class="col-8 ">
                <select name="billing_company_id[]" id="billing_company_id" class="select2 form-control" multiple>
                  <option value="">全部</option>
                  @foreach($vm->GetBillingCompanies() as $company)
                    @if(in_array($company->code, $vm->inputs['billing_company_id']))
                      <option value="{{$company->code}}" selected>{{$company->name}}</option>
                    @else
                      <option value="{{$company->code}}">{{$company->name}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="rate" >汇率</label>
              <div class="col-8">
                <input type="text" name="rate" id="rate" class="form-control" value="{{$vm->inputs['rate']}}" />
              </div>
            </div>
          </div>
          <div class="col-md-1">
            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm ">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center media-info">
            <h2 class="card-title" style="text-transform:none">{!! $vm->title !!}<span class="ml-2 badge badge-success ">{{$vm->rate}}</span></h2>
          </div>
          <div class="wrapper ml-auto action-bar">
            <button type="button" id="btnExport" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o mr-2"></i>输出 Excel</button>
            <button type="button" id="btnPrint" class="btn btn-info btn-sm"><i class="fa fa-print mr-2"></i>打印</button>

          </div>
        </div>
      </div>

      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" style="border:double 3px #333;">
            <thead class="text-center">
              <tr class="border-double-bottom">
                <th style="width:5%" class="text-center">No.</th>
                <th>装柜日期</th>
                <th>客户</th>
                <th>货柜号码</th>
                <th>预订号</th>
                <th class="text-center border-double-left bg-primary">客户<br/>清关价格<br/>(MYR)</th>
                <th class="text-center bg-primary">其他费用<br/>(MYR)</th>
                <th class="text-center bg-primary">货柜<br/>清关费用<br/>(MYR)</th>
                <th class="text-center bg-primary">其他费用<br/>(MYR)</th>
                <th class="text-center border-right-bold bg-primary">总额<br/>(MYR)</th>
                <th class="text-center bg-info">货柜费用<br/>(RMB)</th>
                <th class="text-center bg-info">佣金<br/>(RMB)</th>
                <th class="text-center bg-info">全包价<br/>(RMB)</th>
                <th class="text-center bg-info">装柜费<br/>(RMB)</th>
                <th class="text-center bg-info">总额<br/>(RMB)</th>
                <th class="text-center bg-primary">净额<br/>(MYR)</th>
              </tr>
            </thead>
            <tbody>
            @foreach($vm->dto as $k => $v)
              <tr>
                <td class="text-center">{{$k+1}}</td>
                <td class="text-center">{{$v->loading_date}}</td>
                <td>{{$v->customer_name}}</td>
                <td class="text-center">{{$v->container_no}}</td>
                <td class="text-center"><a href="{{url('/booking/edit/'.$v->id.'')}}">{{$v->booking_no}}</a></td>
                <td class="text-right border-double-left bg-primary">{{$v->billing_handling_prices}}</td>
                <td class="text-right bg-primary">{{$v->billing_other_prices}}</td>
                <td class="text-right bg-primary">{{$v->custom_handling_prices}}</td>
                <td class="text-right bg-primary">{{$v->custom_other_prices}}</td>
                <td class="text-right border-right-bold bg-primary">{{$v->total}}</td>
                <td class="text-right bg-info">{{$v->handling_charges}}</td>
                <td class="text-right bg-info">{{$v->commission}}</td>
                <td class="text-right bg-info">{{$v->net_freight_charges}}</td>
                <td class="text-right bg-info">{{$v->container_loading_charges}}</td>
                <td class="text-right bg-info">{{$v->total_charges}}</td>
                <td class="text-right bg-primary">{{$v->net_total}}</td>
              </tr>
            @endforeach
            </tbody>
            <tfoot>
              <tr class="border-double-bottom">
                <td colspan="5" class="text-right "><strong>总额</strong></td>
                <td class="text-right border-double-left bg-primary">{{$vm->total['billing_handling']}}</td>
                <td class="text-right border-double-left bg-primary">{{$vm->total['billing_other']}}</td>
                <td class="text-right bg-primary">{{$vm->total['customs_handling']}}</td>
                <td class="text-right bg-primary">{{$vm->total['customs_other']}}</td>
                <td class="text-right  border-right-bold bg-primary">{{$vm->total['total']}}</td>
                <td class="text-right bg-info">{{$vm->total['handling_charges']}}</td>
                <td class="text-right bg-info">{{$vm->total['commission']}}</td>
                <td class="text-right bg-info">{{$vm->total['net_freight_charges']}}</td>
                <td class="text-right bg-info">{{$vm->total['loadingfee']}}</td>
                <td class="text-right bg-info">{{$vm->total['total_charges']}}</td>
                <td class="text-right bg-primary">{{$vm->total['net_total']}}</td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  <div>
</div>
@endsection
@section('footer')
<script type="text/javascript">

  $('#btnExport').on('click', function() {
    var _val = '?year=' +  $('#year').val();
    _val += '&month=' +  $('#month').val();
    _val += '&customer_id=' +  $('#customer_id').val();
    _val += '&billing_company_id=' +  $('#billing_company_id').val();
    _val += '&rate=' +  $('#rate').val();
    window.location.href = '{{url('/report/export/monthly_statement')}}' + _val;
  });

  $('#btnPrint').on('click', function() {
    var _val = '?year=' +  $('#year').val();
    _val += '&month=' +  $('#month').val();
    _val += '&customer_id=' +  $('#customer_id').val();
    _val += '&billing_company_id=' +  $('#billing_company_id').val();
    _val += '&rate=' +  $('#rate').val();
    window.open('{{url('/report/print/monthly_statement')}}' + _val);
  });

</script>
@endsection
