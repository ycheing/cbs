<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>年份货柜总报表</title>
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+SC:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
<style>
@media all {
  body{
    font-size: 12pt;
    font-family: 'Noto Sans SC', sans-serif;
    font-weight: 100;
  }
  table{
    width: 100%;
    border-collapse: collapse;
    border:solid 1px #000;
  }
  table th{
    border:solid 1px #000;
    font-weight:700;
    padding: 4px;
    font-size: 12px;
  }
  table td{
    padding: 4px;
    border:solid 1px #000;
    font-size: 12px;
  }

  .text-center{
    text-align: center;
  }
  .text-right{
    text-align: right;
  }

  .border-double-left{
    border-left:double 3px #333 !important;
  }
  .border-double-right{
    border-right:double 3px #333 !important;
  }
  .border-double-bottom{
    border-bottom:double 3px #333 !important;
  }
  .border-right-bold{
    border-right:solid 2px #333 !important;
  }
  .text-uppercase{
    text-transform: uppercase;
  }
  .bg-primary {
    background: #2196f3 !important;
    color: #fff !important;
  }
  .bg-info {
    background: #8862e0 !important;
    color: #fff !important;
  }
}
@media print {
  table th{
    font-size: 10px;
  }
  table td{
    font-size: 10px;
  }
  .bg-primary {
    background: #2196f3 !important;
    color: #fff !important;
  }
  .bg-info {
    background: #8862e0 !important;
    color: #fff !important;
  }
}
</style>
</head>
<body onload="window.print()">
<div class="text-center"><strong>年份货柜总报表</strong></div>
  <table class="table table-bordered" style="border:double 3px #333;">
    <thead class="text-center">
      <tr class="border-double-bottom" >
        <td colspan="13" align="center" style="font-size: 12pt;"><b>{!! $vm->title !!}</b></td>
        <td align="right" style="font-size: 12pt;"><b>{{ $vm->rate }}</b></td>
      </tr>
      <tr class="border-double-bottom">
        <th style="width:5%" class="text-center">No.</th>
        <th>装柜日期</th>
        <th>客户</th>
        <th>货柜号码</th>
        <th>预订号</th>
        <th class="bg-primary text-center border-double-left ">客户<br/>清关价格<br/>(MYR)</th>
        <th class="bg-primary text-center ">货柜<br/>清关费用<br/>(MYR)</th>
        <th class="bg-primary text-center border-right-bold ">总额<br/>(MYR)</th>
        <th class="bg-info text-center ">货柜费用<br/>(RMB)</th>
        <th class="bg-info text-center ">佣金<br/>(RMB)</th>
        <th class="bg-info text-center">全包价<br/>(RMB)</th>
        <th class="bg-info text-center ">装柜费<br/>(RMB)</th>
        <th class="bg-info text-center ">总额<br/>(RMB)</th>
        <th class="bg-primary text-center  ">净额<br/>(MYR)</th>
      </tr>
    </thead>
    <tbody>
    @foreach($vm->dto as $k => $v)
      <tr>
        <td class="text-center">{{$k+1}}</td>
        <td class="text-center ">{{$v->loading_date}}</td>
        <td>{{$v->customer_name}}</td>
        <td class="text-center">{{$v->container_no}}</td>
        <td class="text-center ">{{$v->booking_no}}</td>
        <td class="bg-primary text-right border-double-left ">{{$v->billing_handling_prices}}</td>
        <td class="bg-primary text-right ">{{$v->custom_handling_prices}}</td>
        <td class="bg-primary text-right border-right-bold ">{{$v->total}}</td>
        <td class="bg-info text-right ">{{$v->handling_charges}}</td>
        <td class="bg-info text-right ">{{$v->commission}}</td>
        <td class="bg-info text-right ">{{$v->net_freight_charges}}</td>
        <td class="bg-info text-right ">{{$v->container_loading_charges}}</td>
        <td class="bg-info text-right ">{{$v->total_charges}}</td>
        <td class="bg-primary text-right ">{{$v->net_total}}</td>
      </tr>
    @endforeach
    </tbody>
    <tfoot>
      <tr class="border-double-bottom">
        <td colspan="5" class="text-right "><strong>总额</strong></td>
        <td class="bg-primary text-right border-double-left ">{{$vm->total['billing_handling']}}</td>
        <td class="bg-primary text-right ">{{$vm->total['customs_handling']}}</td>
        <td class="bg-primary text-right  border-right-bold ">{{$vm->total['total']}}</td>
        <td class="bg-info text-right ">{{$vm->total['handling_charges']}}</td>
        <td class="bg-info text-right ">{{$vm->total['commission']}}</td>
        <td class="bg-info text-right ">{{$vm->total['net_freight_charges']}}</td>
        <td class="bg-info text-right ">{{$vm->total['loadingfee']}}</td>
        <td class="bg-info text-right ">{{$vm->total['total_charges']}}</td>
        <td class="bg-primary text-right ">{{$vm->total['net_total']}}</td>
      </tr>
    </tfoot>
  </table>
</body>
</html>
