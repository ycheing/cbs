<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>散货记录表</title>
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+SC:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
<style>
@media all {
  body{
    font-size: 12pt;
    font-family: 'Noto Sans SC', sans-serif;
    font-weight: 100;
  }
  table{
    width: 100%;
    border-collapse: collapse;
    border:solid 1px #000;
  }
  table th{
    border:solid 1px #000;
    font-weight:700;
    padding: 4px;
    font-size: 12px;
  }
  table td{
    padding: 4px;
    border:solid 1px #000;
    font-size: 12px;
  }

  .text-center{
    text-align: center;
  }
  .text-right{
    text-align: right;
  }

  .border-double-left{
    border-left:double 3px #333 !important;
  }
  .border-double-right{
    border-right:double 3px #333 !important;
  }
  .border-double-bottom{
    border-bottom:double 3px #333 !important;
  }
  .border-right-bold{
    border-right:solid 2px #333 !important;
  }
  .text-uppercase{
    text-transform: uppercase;
  }
  .bg-primary {
    background: #2196f3 !important;
    color: #fff !important;
  }
  .bg-info {
    background: #8862e0 !important;
    color: #fff !important;
  }
}
@media print {
  table th{
    font-size: 10px;
  }
  table td{
    font-size: 10px;
  }

}
</style>
</head>
<body onload="window.print()">
  <table class="table table-bordered" style="border:solid 3px #000">
  <thead class="text-center">
    <tr><th colspan="17">散货记录表 - {{$vm->title}} </th></tr>
    <tr style="border-top:solid 3px #000">
      <th style="width:5%">No.</th>
      <th>日期</th>
      <th>装柜日期</th>
      <th>到港日期</th>
      <th>客户代码</th>
      <th>清关行</th>
      <th>收货仓库</th>
      <th>负责人</th>
      <th>追踪编号</th>
      <th>立方数</th>
      <th>箱数</th>
      <th style="border-left:solid 3px #000">清关行运费单</th>
      <th class="text-right bg-primary">金额 (RM)</th>
      <th style="border-left:solid 2px #000">运费单</th>
      <th class="text-right bg-primary" >金额 (RMB)</th>
      <th style="border-left:solid 3px #000">清关行货单</th>
      <th class="text-right bg-primary">金额 (RMB)</th>
      <th style="border-left:solid 2px #000">客户货单</th>
      <th class="text-right bg-primary" style="border-right:solid 3px #000">金额 (RMB)</th>
    </tr>
  </thead>
  <tbody>
    @foreach($vm->dto as $k => $v)
    <tr>
      <td class="text-center">{{$k+1}}</td>
      <td>{{$v->date}}</td>
      <td>{{$v->loading_date}}</td>
      <td>{{$v->arrival_date}}</td>
      <td>{{$v->customer}}</td>
      <td>{{$v->customs_broker}}</td>
      <td>{{$v->warehouse}}</td>
      <td>{{$v->owner}}</td>
      <td>{{$v->tracking_no}}</td>
      <td class="text-center">{{$v->cbm}}</td>
      <td class="text-center">{{$v->pack}}</td>
      <td style="border-left:solid 3px #000">{{$v->supplier_inv_no}}</td>
      <td class="text-right bg-primary">{{number_format($v->supplier_inv_amount,2)}}</td>
      <td style="border-left:solid 2px #000">{{$v->transport_inv}}</td>
      <td class="text-right bg-primary">{{number_format($v->transport_inv_amount,2)}}</td>
      <td style="border-left:solid 3px #000">{{$v->supplier_goods_inv}}</td>
      <td class="text-right bg-primary">{{number_format($v->supplier_goods_amount,2)}}</td>
      <td style="border-left:solid 2px #000">{{$v->customer_goods_inv}}</td>
      <td class="text-right bg-primary">{{number_format($v->customer_goods_amount,2)}}</td>
    </tr>
    @endforeach
  </tbody>

  <tfoot>
    <tr class="border-bottom" style="border-top:solid 3px #000">
      <td colspan="9" class="text-right "><strong>总额</strong></td>
      <td class="text-center">{{$vm->total['cbm']}}</td>
      <td></td>
      <td ></td>
      <td class="bg-primary text-right">{{$vm->total['supplier_inv_amount']}}</td>
      <td ></td>
      <td style="border-right:solid 3px #000"  class="bg-primary  text-right">{{$vm->total['transport_inv_amount']}}</td>
      <td ></td>
      <td class="bg-primary text-right">{{$vm->total['supplier_goods_amount']}}</td>
      <td ></td>
      <td class="bg-primary text-right">{{$vm->total['customer_goods_amount']}}</td>

    </tr>
    <tr>
      <td colspan="9" class="text-right "><strong>净额</strong></td>
      <td colspan="5"></td>
      <td style="border-right:solid 3px #000"  class="text-right bg-primary">{{$vm->total['net_total_transport_inv_amount']}}</td>
      <td colspan="3"></td>
      <td class="text-right bg-primary"  >{{$vm->total['net_total_supplier_goods_amount']}}</td>
    </tr>
  </tfoot>
</table>
