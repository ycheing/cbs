<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>佣金报表</title>
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+SC:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
<style>
@media all {
  body{
    font-size: 12pt;
    font-family: 'Noto Sans SC', sans-serif;
    font-weight: 100;
  }
  table{
    width: 100%;
    border-collapse: collapse;
    border:solid 1px #000;
  }
  table th{
    border:solid 1px #000;
    font-weight:700;
    padding: 4px;
      font-size: 12px;
  }
  table td{
    padding: 4px;
    border:solid 1px #000;
      font-size: 12px;
  }

  .text-center{
    text-align: center;
  }
  .text-right{
    text-align: right;
  }
}
@media print {
  table th{
    font-size: 10px;
  }
  table td{
    font-size: 10px;
  }

}
</style>
</head>
<body onload="window.print()">
<div class="text-center"><strong>{{$vm->inputs['year']}}年{{$vm->inputs['month']}}月份 员工提成报表 {{ $vm->inputs['employee']}}</strong></div>
<table>
  <thead>
    <tr>
      @foreach($vm->header as $k => $v)
        <th class="text-center">{{$v}}</th>
      @endforeach
    </tr>
  </thead>
  <tbody>
    @foreach($vm->data as $k => $v)
    <tr>
      <td class="text-center">{{$k+1}}</td>
      <td class="text-center">{{$v['loading_date']}}</td>
      <td>{{$v['customer_name']}}</td>
      <td class="text-center">{{$v['container_no']}}</td>
      <td class="text-center">{{$v['booking_no']}}</td>
      @foreach($v['employees'] as $e => $ev)
          @if(array_key_exists($e, $vm->footer))
            <td> {!! $ev !!}</td>
          @endif
      @endforeach
    </tr>
    @endforeach

  <!-- <tfoot> -->
    <tr>
    <td colspan="5"></td>
    @foreach($vm->footer as $ft)
    <td style="vertical-align:top">
      <p><b> {{$ft['name']}}</b></p>
      @foreach($ft['job'] as $jk => $jv)
       <p style="margin:0 0 10px 0">{{$jk}} : {{$jv}}</p>
      @endforeach
    </td>
    @endforeach
  </tr>
  <!-- </tfoot> -->
  </tbody>
</table>
</body>
</html>
