<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>装柜报表</title>
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+SC:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
<style>
@media all {
  body{
    font-size: 12pt;
    font-family: 'Noto Sans SC', sans-serif;
    font-weight: 100;
  }
  table{
    width: 100%;
    border-collapse: collapse;
    border:solid 1px #000;
  }
  table th{
    border:solid 1px #000;
    font-weight:700;
    padding: 4px;
      font-size: 12px;
  }
  table td{
    padding: 4px;
    border:solid 1px #000;
      font-size: 12px;
  }

  .text-center{
    text-align: center;
  }
  .text-right{
    text-align: right;
  }

.table2{
  width: 100%;
  color:#ff0000;
  border:none 1px #000;
  margin: 20px 0 0 0;
}
.table2 th{
  border:none 1px #000;
}
.table2 tbody td{
  border:none 1px #000;
  height: 100px;
}
.table2 tfoot td{
  border:none 1px #000;
  height: 30px;
}
}
@media print {
  table th{
    font-size: 10px;
  }
  table td{
    font-size: 10px;
  }

}
</style>
</head>
<body onload="window.print()">
  <div class="text-center"><strong>{{ $vm->title }} 货柜监装报表 {{ $vm->inputs['agent'] }}</strong></div>
  <table>
    <thead>
      <tr class="text-center">
        <th style="width:5%" >No.</th>
        <th class="text-center">装柜日期</th>
        <th class="text-center">到港日期</th>
        <th>货柜费用 (RMB)</th>
        <th>其他费用</th>
        <th>客户</th>
        <th>货柜号码</th>
        <th>预订号	</th>
        <th>货值</th>
        <th class="text-center">Form E</th>
        <th>代理</th>
        <th>全包价 (RMB)</th>
        <th>异地费</th>
        <th>船公司</th>
      </tr>
    </thead>
    <tbody>
      @foreach($vm->dto as $k => $v)
      <tr>
        <td class="text-center">{{$k+1}}</td>
        <td class="text-center">{{$v->loading_date}}</td>
        <td class="text-center">{{$v->eta_date}}</td>
        <td class="text-right">{{$v->handling_charges}}</td>
        <td class="text-right">{{number_format($v->total_other_fee,2,'.','')}}</td>
        <td>{{$v->customer_name}}</td>
        <td class="text-center">{{$v->container_no}}</td>
        <td class="text-center">{{$v->booking_no}}</td>
        <td class="text-right">{{$v->goods_value}}</td>
        <td class="text-center">
          @if($v->forme_apply == 1)
            <label class="badge badge-success">YES</span>
          @else
            <label class="badge badge-danger">NO</span>
          @endif
        </td>
        <td class="text-center">{{$v->agent_name}}</td>
        <td class="text-right">{{$v->net_freight_charges}}</td>
        <td class="text-right">{{$v->remote_fee}}</td>
        <td class="text-center">{{$v->shipping_company}}</td>
      </tr>
      @endforeach

    </tbody>
  </table>
  <div style="margin:20px 0">
    <div style="width:80%; float:left; text-align:right"><b>Total</b></div>
    <div style="width:20%; float:right; border-bottom:solid 1px #000">&nbsp;</div>
  </div><br/>
</body>
</html>
