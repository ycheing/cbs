<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>公司总报表</title>
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+SC:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
<style>
@media all {
  body{
    font-size: 12pt;
    font-family: 'Noto Sans SC', sans-serif;
    font-weight: 100;
  }
  table{
    width: 100%;
    border-collapse: collapse;
    border:solid 1px #000;
  }
  table th{
    border:solid 1px #000;
    font-weight:700;
    padding: 4px;
    font-size: 12px;
  }
  table td{
    padding: 4px;
    border:solid 1px #000;
    font-size: 12px;
  }

  .text-center{
    text-align: center;
  }
  .text-right{
    text-align: right;
  }

  .border-double-left{
    border-left:double 3px #333 !important;
  }
  .border-double-right{
    border-right:double 3px #333 !important;
  }
  .border-double-bottom{
    border-bottom:double 3px #333 !important;
  }
  .border-right-bold{
    border-right:solid 2px #333 !important;
  }
  .text-uppercase{
    text-transform: uppercase;
  }
  .bg-primary {
    background: #2196f3 !important;
    color: #fff !important;
  }
}
@media print {
  table th{
    font-size: 10px;
  }
  table td{
    font-size: 10px;
  }
  .bg-primary {
    background: #2196f3 !important;
    color: #fff !important;
  }
}
</style>
</head>
<body onload="window.print()">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th colspan="15" class="text-center" style="font-size: 12pt;"><strong>公司总报表</strong></th>
      </tr>
      <tr>
        <th colspan="15" class="text-center"  style="font-size: 12pt;">{!! $vm->title !!}</th>
      </tr>
      <tr>
        <th style="width:25px"  class="text-right" rowspan="2">No.</th>
        <th rowspan="2" class="text-center">装柜日期</th>
        <th rowspan="2" class="text-center">到港日期</th>
        <th rowspan="2">客户</th>
        <th rowspan="2">预订号</th>
        <th rowspan="2">货柜号码</th>
        <th colspan="3" class="text-center border-double-left">公司货单</th>
        <th colspan="4" class="text-center border-double-right">供应商货单</th>
        <th class="text-center">SST</th>
        <th rowspan="2" style="border-right:solid 1px #333">清关行</th>
      </tr>
     <tr>
        <th class="text-center border-double-left">货单日期</th>
        <th class="text-center" >货单号</th>
        <th class="bg-primary">货单金额</th>
        <th class="text-center">货单日期</th>
        <th class="text-center" >货单号</th>
        <th class="bg-primary"> 货单金额</th>
        <th class="text-center border-double-right">费用单号</th>
        <th class="text-center">付款</th>
      </tr>
    </thead>
    <tbody>
      @foreach($vm->dto as $k => $v)
      <tr>
        <td class="text-right">{{$k+1}}</td>
        <td class="text-center">{{$v->loading_date}}</td>
        <td class="text-center">{{$v->eta_date}}</td>
        <td>{{$v->customer_name}}</td>
        <td class="text-center">{{$v->booking_no}}</td>
        <td class="text-center">{{$v->container_no}}</td>
        <td class="text-center border-double-left">{{$v->billing_goods_inv_date}}</td>
        <td>{{$v->billing_goods_inv_no}}</td>
        <td class="bg-primary text-right">{{number_format($v->billing_goods_amount,2,'.',',')}}</td>
        <td class="text-center">{{$v->custom_goods_inv_date}}</td>
        <td>{{$v->custom_goods_inv_no}}</td>
        <td class="bg-primary text-right">{{number_format($v->custom_goods_amount,2,'.',',')}}</td>
        <td class="border-double-right">{{$v->custom_handling_inv_no}}</td>
        <td class="text-center">
          @if($v->sst == 1)
            <label class="badge badge-success"><i class='fa fa-check'></i></span>
          @else
            <label class="badge badge-danger"><i class='fa fa-close'></i></span>
          @endif
        </td>
        <td>{{$v->custom_borker}}</td>
      </tr>
      @endforeach

      <tr>
        <td colspan="8" class="text-right">总额</td>
        <td  class="bg-primary text-right"><strong>{{$vm->company_inv_amount}}</strong></td>
        <td colspan="2" class="text-right">总额</td>
        <td  class="bg-primary text-right"><strong>{{$vm->supplier_inv_amount}}</strong></td>
        <td colspan="3"></td>
      </tr>
      <tr>
        <td colspan="11" class="text-right">净额</td>
        <td  class="bg-primary text-right"><strong>{{$vm->net_profit}}</strong></td>
        <td colspan="3"></td>
      </tr>
    </tbody>
  </table>
</body>
</html>
