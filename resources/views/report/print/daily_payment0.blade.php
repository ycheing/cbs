<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>货款支付报表</title>
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+SC:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
<style>
@media all {
  body{
    font-size: 12pt;
    font-family: 'Noto Sans SC', sans-serif;
    font-weight: 100;
  }
  table{
    width: 100%;
    border-collapse: collapse;
    border:solid 1px #000;
  }
  table th{
    border:solid 1px #000;
    font-weight:700;
    padding: 4px;
    font-size: 12px;
  }
  table td{
    padding: 4px;
    border:solid 1px #000;
    font-size: 12px;
  }

  .text-center{
    text-align: center;
  }
  .text-right{
    text-align: right;
  }

  .border-double-left{
    border-left:double 3px #333 !important;
  }
  .border-double-right{
    border-right:double 3px #333 !important;
  }
  .border-double-bottom{
    border-bottom:double 3px #333 !important;
  }
  .border-right-bold{
    border-right:solid 2px #333 !important;
  }

}
@media print {
  table th{
    font-size: 10px;
  }
  table td{
    font-size: 10px;
  }

}
</style>
</head>
<body onload="window.print()">
  <div class="text-center"><strong>{{$vm->inputs['date_from']}} - {{$vm->inputs['date_to']}} 货款支付报表 {{$vm->inputs['supplier']}}</strong></div>
    <table class="table table-bordered">
      <thead class="text-uppercase text-center">
        <tr>
          <th>收款日期</th>
          <th>汇款日期</th>
          <th>客户编号</th>
          <th class="bg-primary">金额<br/>(RM)</th>
          <th>汇率</th>
          <th class="bg-info">金额<br/>(RMB)</th>
          <th>供应商</th>
          <th class="bg-primary">金额<br/>(RM)</th>
          <th>汇率</th>
          <th class="bg-info">金额<br/>(RMB)</th>
          <th class="bg-info">银行收费<br/>(RMB)</th>
          <th class="bg-info">净额<br/>(RMB)</th>
          <th>到账日期</th>
          <th>收款人</th>
          <th>金额</th>
          <th style="border-right: solid 1px #000">付款证明</th>
        </tr>
      </thead>
      <tbody>
      @foreach($vm->dto as $v)
      <tr style='border-top:solid 2px #000'>
        <td class="text-center" rowspan="{{$v->rowspan}}">{{$v->payment_date}}</td>
        <td class="text-center" rowspan="{{$v->rowspan}}">{{$v->remittance_date}}</td>
        <td class="text-center" rowspan="{{$v->rowspan}}">{{$v->customer_code}}</td>
        <td class="text-right bg-primary" rowspan="{{$v->rowspan}}">{{$v->customer_amount}}</td>
        <td class="text-right" rowspan="{{$v->rowspan}}">{{$v->customer_exchange_rate}}</td>
        <td class="text-right bg-info" rowspan="{{$v->rowspan}}">{{$v->customer_total}}</td>
        @if(isset($v->suppliers) && count($v->suppliers) > 0)
          @foreach($v->suppliers as $k => $s)
            @if($k == 0)

            <td rowspan="{{$s->rowspan}}">{{$s->supplier_code}}</td>
            <td rowspan="{{$s->rowspan}}" class="bg-primary text-right">{{$s->supplier_amount}}</td>
            <td rowspan="{{$s->rowspan}}">{{$s->supplier_exchange_rate}}</td>
            <td rowspan="{{$s->rowspan}}" class="bg-info text-right">{{$s->supplier_total}}</td>
            <td rowspan="{{$s->rowspan}}" class="bg-info text-right">{{$s->supplier_bank_charges}}</td>
            <td rowspan="{{$s->rowspan}}" class="bg-info text-right">{{$s->supplier_net_amount}}</td>
            <td rowspan="{{$s->rowspan}}">{{$s->received_date}}</td>
            @if(isset($s->beneficiaries) && count($s->beneficiaries) > 0)
              @foreach($s->beneficiaries as $k => $b)
               @if($k != 0)
               <tr>
                 <td>{{$b->beneficiary}}</td>
                 <td>{{$b->amount}}</td>
               </tr>
               @else
                 <td>{{$b->beneficiary}}</td>
                 <td>{{$b->amount}}</td>
                 <td rowspan="{{$s->rowspan}}">{{$s->slip}}</td>
              </tr>
               @endif
              @endforeach
            @else
              <td></td>
              <td></td>
              <td rowspan="{{$s->rowspan}}">{{$s->slip}}</td>
             </tr>
            @endif

          @else
            <tr>
            <td rowspan="{{$s->rowspan}}">{{$s->supplier_code}}</td>
            <td rowspan="{{$s->rowspan}}" class="bg-primary text-right">{{$s->supplier_amount}}</td>
            <td rowspan="{{$s->rowspan}}">{{$s->supplier_exchange_rate}}</td>
            <td rowspan="{{$s->rowspan}}" class="bg-info text-right">{{$s->supplier_total}}</td>
            <td rowspan="{{$s->rowspan}}" class="bg-info text-right">{{$s->supplier_bank_charges}}</td>
            <td rowspan="{{$s->rowspan}}" class="bg-info text-right">{{$s->supplier_net_amount}}</td>
            <td rowspan="{{$s->rowspan}}">{{$s->received_date}}</td>
            @if(isset($s->beneficiaries) && count($s->beneficiaries) > 0)
              @foreach($s->beneficiaries as $k => $b)
               @if($k != 0)
               <tr>
                 <td>{{$b->beneficiary}}</td>
                 <td>{{$b->amount}}</td>
               </tr>
               @else
                 <td>{{$b->beneficiary}}</td>
                 <td>{{$b->amount}}</td>
                 <td rowspan="{{$s->rowspan}}">{{$s->slip}}</td>
              </tr>
               @endif
              @endforeach
            @else
              <td></td>
              <td></td>
              <td rowspan="{{$s->rowspan}}">{{$s->slip}}</td>
             </tr>
            @endif
          @endif

        @endforeach

        @else
         <!-- // 没有supplier -->
          <td></td>
          <td class="bg-primary text-right"></td>
          <td></td>
          <td class="bg-info text-right"></td>
          <td class="bg-info text-right"></td>
          <td class="bg-info text-right"></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        @endif

      @endforeach
      </tbody>
      <tfoot>
        <tr style="border-top:solid 2px #000">
          <td colspan="3" class="text-right">总额	</td>
          <td class="text-right bg-primary">{{$vm->total['total_customer_amount']}}</td>
          <td class="text-right"></td>
          <td class="text-right bg-info">{{$vm->total['total_customer_total']}}</td>
          <td class="text-center"></td>
          <td class="text-right bg-primary">{{$vm->total['total_supplier_amount']}}</td>
          <td class="text-right"></td>
          <td class="text-right bg-info">{{$vm->total['total_supplier_total']}}</td>
          <td class="text-right bg-info">{{$vm->total['total_supplier_bank_charges']}}</td>
          <td class="text-right bg-info">{{$vm->total['total_supplier_net_amount']}}</td>
          <td colspan="4" class="text-center"></td>
        </tr>
      </tfoot>
    </table>
  </body>
  </html>
