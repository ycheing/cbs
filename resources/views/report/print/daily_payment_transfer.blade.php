<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>货款支付报表</title>
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+SC:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
<style>
@media all {
  body{
    font-size: 12pt;
    font-family: 'Noto Sans SC', sans-serif;
    font-weight: 100;
  }
  table{
    width: 100%;
    border-collapse: collapse;
    border:solid 1px #000;
  }
  table th{
    border:solid 1px #000;
    font-weight:700;
    padding: 4px;
    font-size: 12px;
  }
  table td{
    padding: 4px;
    border:solid 1px #000;
    font-size: 12px;
  }

  .text-center{
    text-align: center;
  }
  .text-right{
    text-align: right;
  }

  .border-double-left{
    border-left:double 3px #333 !important;
  }
  .border-double-right{
    border-right:double 3px #333 !important;
  }
  .border-double-bottom{
    border-bottom:double 3px #333 !important;
  }
  .border-right-bold{
    border-right:solid 2px #333 !important;
  }
  .bg-primary {
    background: #2196f3 !important;
    color: #fff !important;
  }
  .bg-info {
    background: #8862e0 !important;
    color: #fff !important;
  }
}
@media print {
  table th{
    font-size: 10px;
  }
  table td{
    font-size: 10px;
  }

}
</style>
</head>
<body onload="window.print()">
  <div class="text-center"><strong>{{$vm->inputs['date_from']}} - {{$vm->inputs['date_to']}} 货款支付报表 {{$vm->inputs['supplier']}}</strong></div>
  <table class="table table-bordered">
    <thead class="text-uppercase text-center">
      <tr>
        <th>汇款日期</th>
        <th>客户编号</th>
        <th class="bg-primary ">金额<br/>(RM)</th>
        <th>汇率</th>
        <th class="bg-info ">金额<br/>(RMB)</th>
        <th>供应商</th>
        <th class="bg-primary ">金额<br/>(RM)</th>
        <th>汇率</th>
        <th class="bg-info ">金额<br/>(RMB)</th>
        <th class="bg-info ">银行收费<br/>(RMB)</th>
        <th class="bg-info ">净额<br/>(RMB)</th>
        <th>接收日期</th>
        <th>收款人</th>
        <th style="border-right: solid 1px #000">付款证明</th>
      </tr>
    </thead>
    <tbody>
      @foreach($vm->dto as $v)
      <tr>
        <td class="text-center">{{$v->payment_date}}</td>
        <td class="text-center">{{$v->customer_code}}</td>
        <td class="bg-primary text-right ">{{$v->customer_amount}}</td>
        <td class="text-right">{{$v->customer_exchange_rate}}</td>
        <td class="bg-info text-right ">{{$v->customer_total}}</td>
        <td class="text-center">{{$v->supplier_code}}</td>
        <td class="bg-primary text-right ">{{$v->supplier_amount}}</td>
        <td class="text-right">{{$v->supplier_exchange_rate}}</td>
        <td class="bg-info text-right ">{{$v->supplier_total}}</td>
        <td class="bg-info text-right ">{{$v->supplier_bank_charges}}</td>
        <td class="bg-info text-right ">{{$v->supplier_net_amount}}</td>
        <td class="text-center">{{$v->date_received}}</td>
        <td class="text-center">{{$v->beneficiary}}</td>
        <td class="text-center">{{$v->slip}}</td>
      </tr>
      @endforeach
    </tbody>
    <tfoot>
      <tr>
        <td colspan="2" class="text-right">总额	</td>
        <td class="text-right bg-primary">{{$vm->total['total_customer_amount']}}</td>
        <td class="text-right"></td>
        <td class="text-right bg-info">{{$vm->total['total_customer_total']}}</td>
        <td class="text-center"></td>
        <td class="text-right bg-primary">{{$vm->total['total_supplier_amount']}}</td>
        <td class="text-right"></td>
        <td class="text-right bg-info">{{$vm->total['total_supplier_total']}}</td>
        <td class="text-right bg-info">{{$vm->total['total_supplier_bank_charges']}}</td>
        <td class="text-right bg-info">{{$vm->total['total_supplier_net_amount']}}</td>
        <td colspan="3" class="text-center"></td>
      </tr>
    </tfoot>
  </table>
</body>
</html>
