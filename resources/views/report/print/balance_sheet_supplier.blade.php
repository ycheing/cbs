<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>供应商月份结单</title>
<link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+SC:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
<style>
@media all {
  body{
    font-size: 12pt;
    font-family: 'Noto Sans SC', sans-serif;
    font-weight: 100;
  }
  table{
    width: 100%;
    border-collapse: collapse;
    border:solid 1px #000;
  }
  table th{
    border:solid 1px #000;
    font-weight:700;
    padding: 4px;
    font-size: 12px;
  }
  table td{
    padding: 4px;
    border:solid 1px #000;
    font-size: 12px;
  }

  .text-center{
    text-align: center;
  }
  .text-right{
    text-align: right;
  }

  .border-double-left{
    border-left:double 3px #333 !important;
  }
  .border-double-right{
    border-right:double 3px #333 !important;
  }
  .border-double-bottom{
    border-bottom:double 3px #333 !important;
  }
  .border-right-bold{
    border-right:solid 2px #333 !important;
  }
  .text-uppercase{
    text-transform: uppercase;
  }
}
@media print {
  table th{
    font-size: 10px;
  }
  table td{
    font-size: 10px;
  }

}
</style>
</head>
<body onload="window.print()">
<table class="table table-bordered">
    <thead class=" text-center">
      <tr>
        <th colspan="5">供应商月份结单 - {{$vm->title}}</th>
      </tr>
      <tr>
        <!-- <th></th> -->
        <th class="text-center">日期</th>
        <th>说明</th>
        <th>进账</th>
        <th>出账</th>
        <th>余额</th>
      </tr>
    </thead>
    <tbody>
      @if(!empty($vm->initial_balance))
      <tr>
        <!-- <td></td> -->
        <td class="text-center"></td>
        <td style="font-size:13px">Balance b/f</td>
        <td></td>
        <td></td>
        <td class="text-right">{{$vm->initial_balance}}</td>
      </tr>
      @endif
      @foreach($vm->dto as $d)
      <tr>
        <!-- <td>{{$d->type}}</td> -->
        <td class="text-center">{{$d->date}}</td>
        <td style="font-size:13px">{{$d->description}}</td>
        <td class="text-right">{{$d->in}}</td>
        <td class="text-right">{{$d->out}}</td>
        <td class="text-right">{{$d->balance}}</td>
      </tr>
      @endforeach
    </tbody>
    <tfoot>

    </tfoot>
  </table>
</body>
</html>
