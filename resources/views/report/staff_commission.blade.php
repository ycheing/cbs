@extends('layouts.app')
@section('title', '佣金报表')
@section('content')
<div class="row page-title-header">
  <div class="col-12">
    <div class="page-header">
      <h4 class="page-title">员工提成报表</h4>
    </div>
  </div>
  <div class="col-md-12">
    <div class="page-header-toolbar">
      <form id="form-search" method="GET" action="{{url('/report/staff_commission')}}" style="width:100%">
        <div class="row">
          <div class="col-md-2 col-sm-12">
            <div class="form-group row">
              <label class="col-4 col-form-label " for="year" >年</label>
              <div class="col-8">
                <select class="form-control " name="year" id="year">
                  @foreach($vm->GetYears() as $k => $v)
                    @if($v == $vm->inputs['year'])
                    <option value="{{$v}}" selected>{{$v}}</option>
                    @else
                    <option value="{{$v}}" >{{$v}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-3 col-sm-12">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="month" >月</label>
              <div class="col-8">
                <select class="form-control" name="month" id="month">
                  @foreach($vm->GetMonths() as $k => $v)
                    @if($k== $vm->inputs['month'])
                      <option value="{{$k}}" selected>{{$v}}</option>
                    @else
                      <option value="{{$k}}">{{$v}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-3 col-sm-12">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="employee_id" >员工</label>
              <div class="col-8">
                <select name="employee_id" id="employee_id" class="select2 form-control">
                  <option value="">全部</option>
                  @foreach($vm->GetEmployees() as $employee)
                    @if($employee->user_id == $vm->inputs['employee_id'])
                      <option value="{{$employee->user_id}}" selected>{{$employee->name}}</option>
                    @else
                      <option value="{{$employee->user_id}}">{{$employee->name}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-1 col-sm-12 col-xs-12">
            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm ">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center media-info">
            <!-- <h2 class="card-title">STATEMENT FOR THE MONTH OF OCTOBER 2020 <span class="ml-2 badge badge-success ">1.67</span></h2> -->
          </div>
          <div class="wrapper ml-auto action-bar">
            <button type="button" id="btnExport" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o mr-2"></i>输出 Excel</button>
            <button type="button" id="btnPrint" class="btn btn-info btn-sm"><i class="fa fa-print mr-2"></i>打印</button>
            <!-- <div class="btn-group">
              <button type="button" class="btn btn-secondary   btn-sm dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Export </button>
              <div class="dropdown-menu ">
                <a class="dropdown-item" href="#"><i class="fa fa-file-excel-o mr-2"></i>Excel</a>
              </div>
            </div> -->
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                @foreach($vm->header as $k => $v)
                  <th class="text-center">{{$v}}</th>
                @endforeach
              </tr>
            </thead>
            <tbody>
              @foreach($vm->data as $k => $v)
              <tr>
                <td class="text-center">{{$k+1}}</td>
                <td class="text-center">{{$v['loading_date']}}</td>
                <td>{{$v['customer_name']}}</td>
                <td class="text-center">{{$v['container_no']}}</td>
                <td class="text-center">{{$v['booking_no']}}</td>
                @foreach($v['employees'] as $e => $ev)
                    @if(array_key_exists($e, $vm->footer))
                      <td> {!! $ev !!}</td>
                    @endif
                @endforeach
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <td colspan="5"></td>
              @foreach($vm->footer as $ft)
              <td style="vertical-align:top">
                <p><b> {{$ft['name']}}</b></p>
                @foreach($ft['job'] as $jk => $jv)
                 <p style="margin:0 0 10px 0">{{$jk}} : {{$jv}}</p>
                @endforeach
              </td>
              @endforeach
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  <div>
</div>
@endsection
@section('footer')
<script type="text/javascript">
  $('#btnExport').on('click', function() {
    var _val = '?year=' +  $('#year').val();
    _val += '&month=' +  $('#month').val();
    _val += '&employee_id=' +  $('#employee_id').val();
    window.location.href = '{{url('/report/export/staff_commission')}}' + _val;
  });

  $('#btnPrint').on('click', function() {
    var _val = '?year=' +  $('#year').val();
    _val += '&month=' +  $('#month').val();
    _val += '&employee_id=' +  $('#employee_id').val();
    window.open('{{url('/report/print/staff_commission')}}' + _val);
  });
</script>
@endsection
