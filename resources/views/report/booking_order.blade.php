@extends('layouts.app')
@section('title', '货柜预订报表')
@section('content')
<div class="row page-title-header">
  <div class="col-12">
    <div class="page-header">
      <h4 class="page-title">货柜预订报表</h4>
    </div>
  </div>
  <div class="col-12">
    <div class="page-header-toolbar" >
      <form id="form-search" method="GET" action="{{url('/report/booking_order')}}" style="width:100%">
        <div class="row">
          <div class="col-md-3 ">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="date_from" >开始日期</label>
              <div class="col-8">
                <div class="input-group ">
                  <input id="date_from" type="text" class="date form-control " name="date_from" value="{{$vm->inputs['date_from']}}" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                  <div class="input-group-append">
                     <span> <i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="date_to" >结束日期</label>
              <div class="col-8">
                <div class="input-group">
                  <input id="date_to" type="text" class="date form-control " name="date_to" value="{{$vm->inputs['date_to']}}" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                  <div class="input-group-append">
                     <span> <i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="customer_id" >客户</label>
              <div class="col-8">
                <select name="customer_id" id="customer_id" class="select2 form-control">
                  <option value="">全部</option>
                  @foreach($vm->GetCustomers() as $customer)
                    @if($customer->id == $vm->inputs['customer_id'])
                      <option value="{{$customer->id}}" selected>{{$customer->code}}</option>
                    @else
                      <option value="{{$customer->id}}">{{$customer->code}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-1">
            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i></button>
          </div>
        </div>

      </form>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm ">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center media-info">
            <!-- <h2 class="card-title">STATEMENT FOR THE MONTH OF OCTOBER 2020 <span class="ml-2 badge badge-success ">1.67</span></h2> -->
          </div>
          <div class="wrapper ml-auto action-bar">
            <button type="button" id="btnExport" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o mr-2"></i>输出 Excel</button>
            <button type="button" id="btnPrint" class="btn btn-info btn-sm"><i class="fa fa-print mr-2"></i>打印</button>
            <!-- <div class="btn-group">
              <button type="button" class="btn btn-secondary   btn-sm dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Export </button>
              <div class="dropdown-menu ">
                <a class="dropdown-item" href="#"><i class="fa fa-file-excel-o mr-2"></i>Excel</a>
              </div>
            </div> -->
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead class="text-center">
              <tr >
                <th style="width:5%" class="text-center">No.</th>
                <th class="text-center">预订装柜日期</th>
                <th class="text-center">到港日期</th>
                <th>客户</th>
                <th class="text-right">货柜费用（RMB）</th>
                <th style="width:10%">预订号</th>
                <th style="width:10%" >货值</th>
                <th>代理</th>
                <th class="text-right">全包价（RMB）</th>
                <th>船公司</th>
                <th>起运港</th>
                <th style="border-right:solid 1px #000">目的港</th>
              </tr>
            </thead>
            <tbody>
              @foreach($vm->dto as $k => $v)
              <tr>
                <td class="text-center">{{$k+1}}</td>
                <td class="text-center">{{$v->booking_date}}</td>
                <td class="text-center">{{$v->eta_date}}</td>
                <td>{{$v->customer_name}}</td>
                <td class="text-right">{{$v->handling_charges}}</td>
                <td class="text-center"><a href="{{url('/booking/edit/booking/'.$v->id.'')}}">{{$v->booking_no}}</a></td>
                <td class="text-right">{{$v->goods_value}}</td>
                <td class="text-center">{{$v->agent_name}}</td>
                <td class="text-right">{{$v->freight_charges}}</td>
                <td class="text-center">{{$v->shipping_company}}</td>
                <td>{{$v->loading_port}}</td>
                <td>{{$v->destination_port}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  <div>
</div>
@endsection
@section('footer')
<script type="text/javascript">

  $('#btnExport').on('click', function() {
    var _val = '?date_from=' +  $('#date_from').val();
    _val += '&date_to=' +  $('#date_to').val();
    _val += '&customer_id=' +  $('#customer_id').val();
    window.location.href = '{{url('/report/export/booking_order')}}' + _val;

  });

  $('#btnPrint').on('click', function() {
    var _val = '?date_from=' +  $('#date_from').val();
    _val += '&date_to=' +  $('#date_to').val();
    // _val += '&customer_id=' +  $('#customer_id').val();
    window.open('{{url('/report/print/booking_order')}}' + _val);
  });
</script>
@endsection
