@extends('layouts.app')
@section('title', '货款支付报表')
@section('content')
<div class="row page-title-header">
  <div class="col-12">
    <div class="page-header">
      <h4 class="page-title">货款支付报表</h4>
    </div>
  </div>
  <div class="col-12">
    <div class="page-header-toolbar" >
      <form method="GET" action="{{url('/report/daily_payment')}}" style="width:100%">
        <div class="row">

          <div class="col-md-3 ">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="date_from" >开始日期</label>
              <div class="col-8">
                <div class="input-group ">
                  <input id="date_from" type="text" class="date form-control " name="date_from" value="{{$vm->inputs['date_from']}}" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                  <div class="input-group-append">
                     <span> <i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="date_to" >结束日期</label>
              <div class="col-8">
                <div class="input-group">
                  <input id="date_to" type="text" class="date form-control " name="date_to" value="{{$vm->inputs['date_to']}}" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                  <div class="input-group-append">
                     <span> <i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="supplier_id" >供应商</label>
              <div class="col-8">
                <select name="supplier_id" id="supplier_id" class="select2 form-control">
                  <option value="">全部</option>
                  @foreach($vm->GetSuppliers() as $s)
                    @if($s->id == $vm->inputs['supplier_id'])
                      <option value="{{$s->id}}" selected>{{$s->code}}</option>
                    @else
                      <option value="{{$s->id}}">{{$s->code}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="customer_id" >客户</label>
              <div class="col-8">
                <select name="customer_id" id="customer_id" class="select2 form-control">
                  <option value="">全部</option>
                  @foreach($vm->GetCustomers() as $c)
                    @if($c->id == $vm->inputs['customer_id'])
                      <option value="{{$c->id}}" selected>{{$c->code}}</option>
                    @else
                      <option value="{{$c->id}}">{{$c->code}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>
        </div><!--//row-->
        <div class="row">
          <div class="col-md-10"></div>
          <div class="col-md-2">
            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm ">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center media-info">
            <h2 class="card-title"></h2>
          </div>
          <div class="wrapper ml-auto action-bar">
            <button type="button" id="btnExport" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o mr-2"></i>输出 Excel</button>
            <button type="button" id="btnPrint" class="btn btn-info btn-sm"><i class="fa fa-print mr-2"></i>打印</button>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead class="text-uppercase text-center">
              <tr>
                <th>收款日期</th>
                <th>汇款日期</th>
                <th>客户编号</th>
                <th class="bg-primary">金额<br/>(RM)</th>
                <th>汇率</th>
                <th class="bg-primary">金额<br/>(RMB)</th>
                <th>供应商</th>
                <th class="bg-info">金额<br/>(RM)</th>
                <th>汇率</th>
                <th>金额<br/>(RMB)</th>
                <th>到账日期</th>
                <th>收款人</th>
                <th>金额</th>
                <th>银行收费<br/>(RMB)</th>
                <th class="bg-info">净额<br/>(RMB)</th>
                <th style="border-right: solid 1px #000">付款证明</th>
              </tr>
            </thead>
            <tbody>
            @foreach($vm->dto as $v)
            <tr style='border-top:solid 2px #000'>
              <td class="text-center" rowspan="{{$v->rowspan}}">{{$v->payment_date}}</td>
              <td class="text-center" rowspan="{{$v->rowspan}}">{{$v->remittance_date}}</td>
              <td class="text-center" rowspan="{{$v->rowspan}}"><a href="{{url('/payment/edit/'.$v->id.'')}}">{{$v->customer_code}}</a></td>
              <td class="text-right bg-primary" rowspan="{{$v->rowspan}}">{{number_format($v->customer_amount,2,'.',',')}}</td>
              <td class="text-right" rowspan="{{$v->rowspan}}">{{$v->customer_exchange_rate}}</td>
              <td class="text-right bg-primary" rowspan="{{$v->rowspan}}">{{number_format($v->customer_total,2,'.',',')}}</td>

              @if(isset($v->suppliers) && count($v->suppliers) > 0)
                @foreach($v->suppliers as $k => $s)
                  @if($k == 0)
                  <td rowspan="{{$s->rowspan}}">{{$s->supplier_code}}</td>
                  <td rowspan="{{$s->rowspan}}" class="bg-info text-right">{{number_format($s->supplier_amount,2,'.',',')}}</td>
                  <td rowspan="{{$s->rowspan}}">{{$s->supplier_exchange_rate}}</td>
                  <td rowspan="{{$s->rowspan}}" class="text-right">{{number_format($s->supplier_total,2,'.',',')}}</td>

                  @if(isset($s->beneficiaries) && count($s->beneficiaries) > 0)
                    @foreach($s->beneficiaries as $k => $b)
                       @if($k != 0)
                       <tr>
                       @endif
                         <td>{{$b->received_date}}</td>
                         <td>{{$b->beneficiary}}</td>
                         <td class="text-right">{{number_format($b->amount,2,'.',',')}} </td>
                         <td class="text-right">{{number_format($b->bank_charges,2,'.',',')}} </td>
                         <td class="bg-info text-right">{{number_format($b->net_amount,2,'.',',')}}</td>
                         <td class="text-center">{{$b->slip}}</td>
                       </tr>
                    @endforeach
                  @else
                    <td colspan="6"></td>
                  @endif

                @else
                <tr>
                  <td rowspan="{{$s->rowspan}}">{{$s->supplier_code}}</td>
                  <td rowspan="{{$s->rowspan}}" class="bg-info text-right">{{number_format($s->supplier_amount,2,'.',',')}}</td>
                  <td rowspan="{{$s->rowspan}}" >{{$s->supplier_exchange_rate}}</td>
                  <td rowspan="{{$s->rowspan}}" class="text-right"> {{number_format($s->supplier_total,2,'.',',')}} </td>

                  @if(isset($s->beneficiaries) && count($s->beneficiaries) > 0)
                    @foreach($s->beneficiaries as $k => $b)
                     @if($k != 0)
                     <tr>
                     @endif
                       <td>{{$b->received_date}}</td>
                       <td>{{$b->beneficiary}}</td>
                       <td class="text-right">{{number_format($b->amount,2,'.',',')}}</td>
                       <td class="text-right">{{number_format($b->bank_charges,2,'.',',')}} </td>
                       <td class="bg-info text-right">{{number_format($b->net_amount,2,'.',',')}}</td>
                       <td class="text-center">{{$b->slip}}</td>
                     </tr>
                    @endforeach
                  @else
                    <td colspan="6"></td>
                  </tr>
                  @endif
                @endif
                @endforeach

              @else
               <!-- // 没有supplier -->
                <td colspan="10"></td>
                <!-- <td class="bg-primary text-right"></td>
                <td></td>
                <td class="bg-info text-right"></td>
                <td></td>
                <td></td>
                <td class="bg-info text-right"></td>
                <td class="bg-info text-right"></td>
                <td class="bg-info text-right"></td>
                <td></td> -->
              </tr>
              @endif

            @endforeach
            </tbody>
            <tfoot>
              <tr style="border-top:solid 2px #000">
                <td colspan="3" class="text-right">总额	</td>
                <td class="text-right bg-primary">{{$vm->total['total_customer_amount']}}</td>
                <td class="text-right"></td>
                <td class="text-right bg-primary">{{$vm->total['total_customer_total']}}</td>
                <td class="text-center"></td>
                <td class="text-right bg-info">{{$vm->total['total_supplier_amount']}}</td>
                <td class="text-right"></td>
                <td class="text-right ">{{$vm->total['total_supplier_total']}}</td>
                <td colspan="3"></td>
                <td class="text-right ">{{$vm->total['total_supplier_bank_charges']}}</td>
                <td class="text-right bg-info">{{$vm->total['total_supplier_net_amount']}}</td>
                <td  class="text-center"></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footer')
<script type="text/javascript">

$('#btnExport').on('click', function() {
  var _val = '?date_from=' +  $('#date_from').val();
  _val += '&date_to=' +  $('#date_to').val();
  _val += '&supplier_id=' +  $('#supplier_id').val();
  _val += '&customer_id=' +  $('#customer_id').val();
  window.location.href = '{{url('/report/export/daily_payment')}}' + _val;
});

$('#btnPrint').on('click', function() {
  var _val = '?date_from=' +  $('#date_from').val();
  _val += '&date_to=' +  $('#date_to').val();
  _val += '&supplier_id=' +  $('#supplier_id').val();
  _val += '&customer_id=' +  $('#customer_id').val();
  window.open('{{url('/report/print/daily_payment')}}' + _val);
  });


</script>
@endsection
