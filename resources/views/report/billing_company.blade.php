@extends('layouts.app')
@section('title', '公司总报表')
@section('content')
<div class="row page-title-header">
  <div class="col-12">
    <div class="page-header">
      <h4 class="page-title">公司总报表</h4>
    </div>
  </div>
  <div class="col-md-12">
    <div class="page-header-toolbar">
      <form method="GET" action="{{url('/report/billing_company')}}" style="width:100%">
        <div class="row">
          <div class="col-md-2 col-xs-6">
            <div class="form-group row">
              <label class="col-4 col-form-label " for="year">年</label>
              <div class="col-8">
                <select class="form-control " name="year" id="year">
                  @foreach($vm->GetYears() as $k => $v)
                    @if($v == $vm->inputs['year'])
                    <option value="{{$v}}" selected>{{$v}}</option>
                    @else
                    <option value="{{$v}}">{{$v}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-xs-6">
            <div class="form-group row">
              <label class="col-4 col-form-label " for="month">月</label>
              <div class="col-8">
                <select class="form-control" name="month" id="month">
                  @foreach($vm->GetMonths() as $k => $v)
                    @if($k== $vm->inputs['month'])
                      <option value="{{$k}}" selected>{{$v}}</option>
                    @else
                      <option value="{{$k}}">{{$v}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="billing_company_id">开票公司</label>
              <div class="col-8">
                <select name="billing_company_id[]" id="billing_company_id" class="select2 form-control" multiple>
                  <option value="">全部</option>
                  @foreach($vm->GetBillingCompanies() as $company)
                    @if(in_array($company->code, $vm->inputs['billing_company_id']))
                      <option value="{{$company->code}}" selected>{{$company->name}}</option>
                    @else
                      <option value="{{$company->code}}">{{$company->name}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-1">
            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm ">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center">
            <h2 class="card-title" style="text-transform:none">{!! $vm->title !!}</h2>
          </div>
          <div class="wrapper ml-auto action-bar">
            <button type="button" id="btnExport" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o mr-2"></i>输出 Excel</button>
            <button type="button" id="btnPrint" class="btn btn-info btn-sm"><i class="fa fa-print mr-2"></i>打印</button>
            <!-- <div class="btn-group">
              <button type="button" class="btn btn-secondary   btn-sm dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Export </button>
              <div class="dropdown-menu ">
                <a class="dropdown-item" href="#"><i class="fa fa-file-excel-o mr-2"></i>Excel</a>
              </div>
            </div> -->
          </div>
        </div>
      </div>


      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead class=" text-center">
              <tr>
                <th style="width:5%"  rowspan="2">No.</th>
                <th rowspan="2" class="text-center">装柜日期</th>
                <th rowspan="2" class="text-center">到港日期</th>
                <th rowspan="2">客户</th>
                <th rowspan="2">预订号</th>
                <th rowspan="2">货柜号码</th>
                <th colspan="3" class="text-center border-double-left">公司货单</th>
                <th colspan="4" class="text-center border-double-right">供应商货单</th>
                <th class="text-center">SST</th>
                <th rowspan="2" style="border-right:solid 1px #333">清关行</th>
              </tr>
             <tr>
                <th class="text-center border-double-left">货单日期</th>
                <th class="text-center">货单号</th>
                <th class="bg-primary">货单金额</th>
                <th class="text-center">货单日期</th>
                <th class="text-center">货单号</th>
                <th class="bg-primary">货单金额</th>
                <th class="text-center border-double-right">费用单号</th>
                <th class="text-center">付款</th>
              </tr>
            </thead>
            <tbody>
              @foreach($vm->dto as $k => $v)
              <tr>
                <td>{{$k+1}}</td>
                <td class="text-center">{{$v->loading_date}}</td>
                <td class="text-center">{{$v->eta_date}}</td>
                <td>{{$v->customer_name}}</td>
                <td class="text-center"><a href="{{url('/booking/edit/'.$v->id.'')}}">{{$v->booking_no}}</a></td>
                <td class="text-center">{{$v->container_no}}</td>
                <td class="text-center border-double-left">{{$v->billing_goods_inv_date}}</td>
                <td>{{$v->billing_goods_inv_no}}</td>
                <td class="bg-primary text-right">{{number_format($v->billing_goods_amount,2,'.',',')}}</td>
                <td class="text-center">{{$v->custom_goods_inv_date}}</td>
                <td>{{$v->custom_goods_inv_no}}</td>
                <td class="bg-primary text-right">{{number_format($v->custom_goods_amount,2,'.',',')}}</td>
                <td class="border-double-right">{{$v->custom_handling_inv_no}}</td>
                <td class="text-center">
                  @if($v->sst == 1)
                    <label class="badge badge-success"><i class='fa fa-check'></i></span>
                  @else
                    <label class="badge badge-danger"><i class='fa fa-close'></i></span>
                  @endif
                </td>
                <td>{{$v->custom_borker}}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <td colspan="8" class="text-right">总额</td>
                <td  class="bg-primary text-right"><strong>{{$vm->company_inv_amount}}</strong></td>
                <td colspan="2" class="text-right">总额</td>
                <td  class="bg-primary text-right"><strong>{{$vm->supplier_inv_amount}}</strong></td>
                <td colspan="3"></td>
              </tr>
              <tr>
                <td colspan="11" class="text-right">净额</td>
                <td  class="bg-primary  text-right"><strong>{{$vm->net_profit}}</strong></td>
                <td colspan="3"></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  <div>
</div>
@endsection
@section('footer')
<script type="text/javascript">

  $('#btnExport').on('click', function() {
    var _val = '?year=' +  $('#year').val();
    _val += '&month=' +  $('#month').val();
    _val += '&billing_company_id=' +  $('#billing_company_id').val();
    window.location.href = '{{url('/report/export/billing_company')}}' + _val;
  });

  $('#btnPrint').on('click', function() {
    var _val = '?year=' +  $('#year').val();
    _val += '&month=' +  $('#month').val();
    _val += '&billing_company_id=' +  $('#billing_company_id').val();
    window.open('{{url('/report/print/billing_company')}}' + _val);
  });

</script>
@endsection
