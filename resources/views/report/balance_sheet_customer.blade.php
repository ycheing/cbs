@extends('layouts.app')
@section('title', '客户月份结单')
@section('content')
<div class="row page-title-header">
  <div class="col-12">
    <div class="page-header">
      <h4 class="page-title">客户月份结单</h4>
    </div>
  </div>
  <div class="col-md-12">
    <div class="page-header-toolbar">
      <form method="GET" action="{{url('/report/balance_sheet_customer')}}" style="width:100%">
        <div class="row">
          <div class="col-md-3">
            <div class="form-group row">
              <label class="col-4 col-form-label" for="customer_id" >客户</label>
              <div class="col-8">
                <select name="customer_id" id="customer_id" class="select2 form-control">
                  <option value=""></option>
                  @foreach($vm->GetCustomers() as $customer)
                    @if($customer->id == $vm->inputs['customer_id'])
                      <option value="{{$customer->id}}" selected>{{$customer->code}}</option>
                    @else
                      <option value="{{$customer->id}}">{{$customer->code}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-2 col-sm-12">
            <div class="form-group row">
              <label class="col-4 col-form-label " for="year" >年</label>
              <div class="col-8">
                <select class="form-control " name="year" id="year">
                  @foreach($vm->GetYears() as $k => $v)
                    @if($v == $vm->inputs['year'])
                    <option value="{{$v}}" selected>{{$v}}</option>
                    @else
                    <option value="{{$v}}" >{{$v}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-1">
            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm ">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center">
            <h2 class="card-title" style="text-transform:none">{!! $vm->title !!}</h2>
          </div>
          <div class="wrapper ml-auto action-bar">
            <button type="button" id="btnExport" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o mr-2"></i>输出 Excel</button>
            <button type="button" id="btnPrint" class="btn btn-info btn-sm"><i class="fa fa-print mr-2"></i>打印</button>

          </div>
        </div>
      </div>


      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead class=" text-center">
              <tr>
                <!-- <th></th> -->
                <th class="text-center">日期</th>
                <th>说明</th>
                <th>进账</th>
                <th>出账</th>
                <th style="border-right:solid 1px #000">余额</th>
              </tr>
            </thead>
            <tbody>
              @if(!empty($vm->initial_balance))
              <tr>
                <!-- <td></td> -->
                <td class="text-center"></td>
                <td>Balance b/f</td>
                <td></td>
                <td></td>
                <td class="text-right">{{$vm->initial_balance}}</td>
              </tr>
              @endif
              @foreach($vm->dto as $d)
              <tr>
                <!-- <td>{{$d->type}}</td> -->
                <td class="text-center">{{$d->date}}</td>
                <td>{{$d->description}}</td>
                <td class="text-right">{{$d->in}}</td>
                <td class="text-right">{{$d->out}}</td>
                <td class="text-right">{{$d->balance}}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>

            </tfoot>
          </table>
        </div>
      </div>
    </div>
  <div>
</div>
@endsection
@section('footer')
<script type="text/javascript">

  $('#btnExport').on('click', function() {
    var _val = '?customer_id=' +  $('#customer_id').val();
    _val += '&year=' +  $('#year').val();
    // _val += '&billing_company_id=' +  $('#billing_company_id').val();
    window.location.href = '{{url('/report/export/balance_sheet_customer')}}' + _val;
  });

  $('#btnPrint').on('click', function() {
    var _val = '?customer_id=' +  $('#customer_id').val();
    _val += '&year=' +  $('#year').val();
    // _val += '&billing_company_id=' +  $('#billing_company_id').val();
    window.open('{{url('/report/print/balance_sheet_customer')}}' + _val);
  });

</script>
@endsection
