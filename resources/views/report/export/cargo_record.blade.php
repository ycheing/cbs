<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>散货记录表</title>
<style>
  body{
    font-family: sans-serif;
    font-size: 12px;
  }
  table{
    font-size: 12px;
    width: 100%;
    border-collapse: collapse;
    padding: 0;
    border: solid 1px #000;
  }
  table th{
    border: solid 1px #000;
    padding: 4px;
  }
  table td{
    border: solid 1px #000;
    padding: 4px;
  }
</style>
</head>
<body>
<table>
  <thead>
    <tr>
      <td style="text-align: center" colspan="17" ><b>{{$data['title']}}</b></td>
    </tr>
    <tr>
      <th align="center">No.</th>
      <th align="center">日期</th>
      <th align="center">装柜日期</th>
      <th align="center">到港日期</th>
      <th align="center">客户代码</th>
      <th align="center">清关行</th>
      <th align="center">收货仓库</th>
      <th align="center">负责人</th>
      <th align="center">追踪编号</th>
      <th align="center">立方数</th>
      <th align="center">箱数</th>
      <th align="center">清关行运费单</th>
      <th align="center">金额 (RM)</th>
      <th align="center">运费单</th>
      <th align="center">金额 (RM)</th>
      <th align="center">清关行货单</th>
      <th align="center">金额 (RM)</th>
      <th align="center">客户货单</th>
      <th align="center">金额 (RM)</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data['results'] as $v)
    <tr>
      <td align="center">{{$v['no']}}</td>
      <td>{{$v['date']}}</td>
      <td>{{$v['loading_date']}}</td>
      <td>{{$v['arrival_date']}}</td>
      <td>{{$v['customer']}}</td>
      <td>{{$v['customs_broker']}}</td>
      <td>{{$v['warehouse']}}</td>
      <td>{{$v['owner']}}</td>
      <td>{{$v['tracking_no']}}</td>
      <td align="center">{{$v['cbm']}}</td>
      <td align="center">{{$v['pack']}}</td>
      <td>{{$v['supplier_inv_no']}}</td>
      <td align="right">{{$v['supplier_inv_amount']}}</td>
      <td>{{$v['transport_inv']}}</td>
      <td align="right">{{$v['transport_inv_amount']}}</td>
      <td>{{$v['supplier_goods_inv']}}</td>
      <td align="right">{{$v['supplier_goods_amount']}}</td>
      <td>{{$v['customer_goods_inv']}}</td>
      <td align="right">{{$v['customer_goods_amount']}}</td>
    </tr>
    @endforeach
  </tbody>

  <tfoot>
    <tr class="border-double-bottom">
      <td colspan="9" align="right"><strong style="text-right">总额</strong></td>
      <td  align="center">{{$total['cbm']}}</td>
      <td colspan="2"></td>
      <td align="right">{{$total['supplier_inv_amount']}}</td>
      <td></td>
      <td align="right">{{$total['transport_inv_amount']}}</td>
      <td></td>
      <td align="right">{{$total['supplier_goods_amount']}}</td>
      <td></td>
      <td align="right">{{$total['customer_goods_amount']}}</td>
    </tr>
    <tr>

      <td colspan="9" align="right"><strong  style="text-right">净额</strong></td>
      <td colspan="5"></td>
      <td align="right">{{$total['net_total_transport_inv_amount']}}</td>
      <td colspan="3"></td>
      <td align="right">{{$total['net_total_supplier_goods_amount']}}</td>
    </tr>
  </tfoot>
</table>
