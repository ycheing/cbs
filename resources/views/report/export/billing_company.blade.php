<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
  <table>
    <thead>
      <tr>
        <th align="center" colspan="15">{{$data['title']}}</th>
      </tr>
      <tr>
        <th rowspan="2" align="center">No.</th>
        <th rowspan="2" align="center">装柜日期</th>
        <th rowspan="2" align="center">到港日期</th>
        <th rowspan="2" align="center">客户</th>
        <th rowspan="2" align="center">预订号</th>
        <th rowspan="2" align="center">货柜号码</th>
        <th colspan="3" align="center">公司货单</th>
        <th colspan="4" align="center">供应商货单</th>
        <th align="center">SST</th>
        <th rowspan="2" align="center">清关行</th>
      </tr>
     <tr>
        <th align="center">货单日期</th>
        <th align="center">货单号</th>
        <th align="center">货单金额</th>
        <th align="center">货单日期</th>
        <th align="center">货单金额</th>
        <th align="center">货单号 </th>
        <th align="center">货单金额</th>
        <th align="center">付款</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data['results'] as $v)
      <tr>
        <td align="center">{{$v['no']}}</td>
        <td align="center">{{$v['loading_date']}}</td>
        <td align="center">{{$v['eta_date']}}</td>
        <td>{{$v['customer_name']}}</td>
        <td align="center">{{$v['booking_no']}}</td>
        <td align="center">{{$v['container_no']}}</td>
        <td align="center">{{$v['billing_goods_inv_date']}}</td>
        <td>{{$v['billing_goods_inv_no']}}</td>
        <td align="right">{{$v['billing_goods_amount']}}</td>
        <td align="center">{{$v['custom_goods_inv_date']}}</td>
        <td>{{$v['custom_goods_inv_no']}}</td>
        <td align="right">{{$v['custom_goods_amount']}}</td>
        <td>{{$v['custom_handling_inv_no']}}</td>
        <td align="center">{{$v['sst']}}</td>
        <td>{{$v['customs_broker']}}</td>
      </tr>
      @endforeach
    </tbody>
    <tfoot>
      <tr>
        <td colspan="8" align="right">Total</td>
        <td align="right"><strong>{{$data['company_inv_amount']}}</strong></td>
        <td colspan="2" align="right">Total</td>
        <td align="right"><strong>{{$data['supplier_inv_amount']}}</strong></td>
        <td colspan="3"></td>
      </tr>
      <tr>
        <td colspan="11" align="right">净额</td>
        <td align="right"><strong>{{$data['net_profit']}}</strong></td>
        <td colspan="3"></td>
      </tr>
    </tfoot>
  </table>
</body>
</html>
