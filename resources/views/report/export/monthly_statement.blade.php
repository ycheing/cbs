<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
 table, td {border:1pt solid black}
  table {border-collapse:collapse}</style>
</head>
<body>
<table>
  <thead>
    <tr>
      <td style="text-align: center" colspan="13" ><b>{{$data['title']}}</b></td>
      <td align="right"><b>{{$data['rate']}}</b></td>
    </tr>
    <tr>
      <th align="center">No.</th>
      <th align="center">装柜日期</th>
      <th align="center">客户</th>
      <th align="center">货柜号码</th>
      <th align="center">预订号</th>
      <th align="center">客户清关价格 (MYR)</th>
      <th align="center">其他费用 (MYR)</th>
      <th align="center">货柜清关费用 (MYR)</th>
      <th align="center">其他费用 (MYR)</th>
      <th align="center">总额 (MYR)</th>
      <th align="center">货柜费用 (RMB)</th>
      <th align="center">佣金 (RMB)</th>
      <th align="center">全包价 (RMB)</th>
      <th align="center">装柜费 (RMB)</th>
      <th align="center">总额 (RMB)</th>
      <th align="center">净额 (MYR)</th>
    </tr>
  </thead>
  <tbody>
    @if(isset($data['result']))
  @foreach($data['result'] as $result)
    <tr>
      <td align="center">{{$result['no']}}</td>
      <td align="center">{{$result['loading_date']}}</td>
      <td>{{$result['customer_name']}}</td>
      <td align="center">{{$result['container_no']}}</td>
      <td align="center">{{$result['booking_no']}}</td>
      <td align="right">{{$result['billing_handling_prices']}}</td>
      <td align="right">{{$result['billing_other_prices']}}</td>
      <td align="right">{{$result['custom_handling_prices']}}</td>
      <td align="right">{{$result['custom_other_prices']}}</td>
      <td align="right">{{$result['total']}}</td>
      <td align="right">{{$result['handling_charges']}}</td>
      <td align="right">{{$result['commission']}}</td>
      <td align="right">{{$result['net_freight_charges']}}</td>
      <td align="right">{{$result['container_loading_charges']}}</td>
      <td align="right">{{$result['total_charges']}}</td>
      <td align="right">{{$result['net_total']}}</td>
    </tr>
  @endforeach
  @endif
  </tbody>
  <tfoot>
    <tr>
      <td colspan="4"></td>
      <td align="right"><b>总额</b></td>
      <td align="right">{{$total['billing_handling']}}</td>
      <td align="right">{{$total['billing_other']}}</td>
      <td align="right">{{$total['customs_handling']}}</td>
      <td align="right">{{$total['customs_other']}}</td>
      <td align="right">{{$total['total']}}</td>
      <td align="right">{{$total['handling_charges']}}</td>
      <td align="right">{{$total['commission']}}</td>
      <td align="right">{{$total['net_freight_charges']}}</td>
      <td align="right">{{$total['loadingfee']}}</td>
      <td align="right">{{$total['total_charges']}}</td>
      <td align="right">{{$total['net_total']}}</td>
    </tr>
  </tfoot>
</table>
</body>
</html>
