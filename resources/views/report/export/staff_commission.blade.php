<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
  <table>
    <thead>
      <tr>
        <td colspan="{{ count($data['header']) }}" align="center">{{ $data['date']}} 员工提成报表</td>
      </tr>
      <tr>
          @foreach($data['header']  as $k => $v)
            <th align="center">{{$v}}</th>
          @endforeach
        </tr>
      </thead>
      <tbody>
        @foreach($data['results'] as $v)
        <tr>
          <td align="center">{{$v['no']}}</td>
          <td align="center">{{$v['loading_date']}}</td>
          <td>{{$v['customer_name']}}</td>
          <td align="center">{{$v['container_no']}}</td>
          <td align="center">{{$v['booking_no']}}</td>
          @foreach($v['employees'] as $e => $ev)
            @if(array_key_exists($e, $data['footer']))
              <td> {!! $ev !!}</td>
            @endif
          @endforeach
        </tr>
        @endforeach
      </tbody>
      <tfoot>
        <tr>
        <td colspan="5"></td>
          @foreach($data['footer'] as $ft)
          <td style="vertical-align:top">
            {{$ft['name']}}
            @foreach($ft['job'] as $jk => $jv)
             {{$jk}} : {{$jv}} {!! "<br/>" !!}
            @endforeach
          </td>
          @endforeach
        </tr>
      </tfoot>
    </table>
