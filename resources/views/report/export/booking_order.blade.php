<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
  body{
    font-family: sans-serif;
    font-size: 12px;
  }
  table{
    font-size: 12px;
    width: 100%;
    border-collapse: collapse;
    padding: 0;
    border: solid 1px #000;
  }
  table th{
    border: solid 1px #000;
    padding: 4px;
  }
  table td{
    border: solid 1px #000;
    padding: 4px;
  }
</style>
</head>
<body>
<table>
  <thead>
    <tr>
      <th align="center">No.</th>
      <th align="center">预订装柜日期</th>
      <th align="center">到港日期</th>
      <th align="center">客户</th>
      <th align="right">货柜费用（RMB）</th>
      <th align="center">预订号</th>
      <th align="center">货值</th>
      <th align="center">代理</th>
      <th align="right">全包价（RMB）</th>
      <th align="center">船公司</th>
      <th align="center">起运港</th>
      <th align="center">目的港</th>
    </tr>
  </thead>
  <tbody>
      @foreach($data['results'] as $v)
      <tr>
        <td align="center">{{$v['no']}}</td>
        <td align="center">{{$v['booking_date']}}</td>
        <td align="center">{{$v['eta_date']}}</td>
        <td>{{$v['customer_name']}}</td>
        <td align="right">{{$v['handling_charges']}}</td>
        <td align="center">{{$v['booking_no']}}</td>
        <td align="right">{{$v['goods_value']}}</td>
        <td align="center">{{$v['agent_name']}}</td>
        <td align="right">{{$v['freight_charges']}}</td>
        <td align="center">{{$v['shipping_company']}}</td>
        <td>{{$v['loading_port']}}</td>
        <td>{{$v['destination_port']}}</td>
      </tr>
      @endforeach
  </tbody>
</table>
</body>
</html>
