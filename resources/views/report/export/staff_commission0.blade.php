<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
  <table>
    <thead>
      <tr>
          <th align="center">No.</th>
          <th align="center">装柜日期</th>
          <th align="center">客户名称</th>
          <th align="center">货柜号码</th>
          <th align="center">预订号</th>
          @foreach($data['employees'] as $e)
          <th align="center">{{$e->name}}</th>
          @endforeach
        </tr>
      </thead>
      <tbody>
        @foreach($data['results'] as $v)
        <tr>
          <td align="center">{{$v['no']}}</td>
          <td align="center">{{$v['loading_date']}}</td>
          <td>{{$v['customer_name']}}</td>
          <td align="center">{{$v['container_no']}}</td>
          <td align="center">{{$v['booking_no']}}</td>
          @foreach($v['employees'] as $e)
          <td>{{$e['job']}}</td>
          @endforeach
        </tr>
        @endforeach
      </tbody>
    </table>
