<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
body{
  font-family: arial; font-size: 12px;
}
table{border:solid 1px #000; border-collapse: collapse;}
table th, table td{border:solid 1px #000}
</style>
</head>
<body >
  <table>
    <thead >
        <tr>
          <th align="center">收款日期</th>
          <th align="center">汇款日期</th>
          <th align="center">客户编号</th>
          <th align="center">金额(RM)</th>
          <th align="center">汇率</th>
          <th align="center">金额(RMB)</th>
          <th align="center">供应商</th>
          <th align="center">金额(RM)</th>
          <th align="center">汇率</th>
          <th align="center">金额(RMB)</th>
          <th align="center">银行收费(RMB)</th>
          <th align="center">净额(RMB)</th>
          <th align="center">接收日期</th>
          <th align="center">收款人</th>
          <th align="center">金额</th>
          <th align="center">付款证明</th>
        </tr>
      </thead>
      <tbody>
      @foreach($data['results'] as $v)
      <tr>
        <td rowspan="{{$v['rowspan']}}">{{$v['payment_date']}}</td>
        <td rowspan="{{$v['rowspan']}}">{{$v['remittance_date']}}</td>
        <td rowspan="{{$v['rowspan']}}">{{$v['customer_code']}}</td>
        <td rowspan="{{$v['rowspan']}}">{{$v['customer_amount']}}</td>
        <td rowspan="{{$v['rowspan']}}">{{$v['customer_exchange_rate']}}</td>
        <td rowspan="{{$v['rowspan']}}">{{$v['customer_total']}}</td>
        @if(count($v['suppliers']) > 0)
          @foreach($v['suppliers'] as $k => $s)
          @if($k == 0)
            <td rowspan="{{$s['rowspan']}}">{{$s['supplier_code']}}</td>
            <td rowspan="{{$s['rowspan']}}">{{$s['supplier_amount']}}</td>
            <td rowspan="{{$s['rowspan']}}">{{$s['supplier_exchange_rate']}}</td>
            <td rowspan="{{$s['rowspan']}}">{{$s['supplier_total']}}</td>
            <td rowspan="{{$s['rowspan']}}">{{$s['supplier_bank_charges']}}</td>
            <td rowspan="{{$s['rowspan']}}">{{$s['supplier_net_amount']}}</td>
            <td rowspan="{{$s['rowspan']}}">{{$s['received_date']}}</td>
            @if( count($s['beneficiaries']) > 0 )
              @foreach($s['beneficiaries'] as $k => $b)
                @if($k == 0)
                  <td>{{$b['beneficiary']}}</td>
                  <td>{{$b['amount']}}</td>
                  <td rowspan="{{$s['rowspan']}}">{{$s['slip']}}</td>
                  </tr>
                @else
                <tr>
                  <td>{{$b['beneficiary']}}</td>
                  <td>{{$b['amount']}}</td>
                </tr>
                @endif
              @endforeach
            @else
              <td></td>
              <td></td>
              <td rowspan="{{$s['rowspan']}}">{{$s['slip']}}</td>
             </tr>
           @endif

        @else
        <tr>
          <td rowspan="{{$s['rowspan']}}">{{$s['supplier_code']}}</td>
          <td rowspan="{{$s['rowspan']}}">{{$s['supplier_amount']}}</td>
          <td rowspan="{{$s['rowspan']}}">{{$s['supplier_exchange_rate']}}</td>
          <td rowspan="{{$s['rowspan']}}">{{$s['supplier_total']}}</td>
          <td rowspan="{{$s['rowspan']}}">{{$s['supplier_bank_charges']}}</td>
          <td rowspan="{{$s['rowspan']}}">{{$s['supplier_net_amount']}}</td>
          <td rowspan="{{$s['rowspan']}}">{{$s['received_date']}}</td>

          @if(count($s['beneficiaries']) > 0 )
            @foreach($s['beneficiaries'] as $k => $b)
             @if($k != 0)
             <tr>
               <td>{{$b['beneficiary']}}</td>
               <td>{{$b['amount']}}</td>
             </tr>
             @else
               <td>{{$b['beneficiary']}}</td>
               <td>{{$b['amount']}}</td>
               <td rowspan="{{$s['rowspan']}}">{{$s['slip']}}</td>
             </tr>
             @endif
            @endforeach
          @else
            <td></td>
            <td></td>
            <td rowspan="{{$s['rowspan']}}">{{$s['slip']}}</td>
           </tr>
          @endif

        @endif

        @endforeach

        @else
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        @endif
      @endforeach
    </tbody>
    <tfoot>
      <tr>
        <td align="right" colspan="3">总额</td>
        <td>{{$total['total_customer_amount']}}</td>
        <td></td>
        <td>{{$total['total_customer_total']}}</td>
        <td></td>
        <td>{{$total['total_supplier_amount']}}</td>
        <td></td>
        <td>{{$total['total_supplier_total']}}</td>
        <td>{{$total['total_supplier_bank_charges']}}</td>
        <td>{{$total['total_supplier_net_amount']}}</td>
        <td colspan="4"></td>
      </tr>
    </tfoot>
  </table>
</body>
</html>
