<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>
<body >
  <table>
    <thead >
      <tr>
        <th align="center">汇款日期</th>
        <th align="center">客户编号</th>
        <th align="center">金额(RM)</th>
        <th align="center">汇率</th>
        <th align="center">金额(RMB)</th>
        <th align="center">供应商</th>
        <th align="center">金额(RM)</th>
        <th align="center">汇率</th>
        <th align="center">金额(RMB)</th>
        <th align="center">银行收费(RMB)</th>
        <th align="center">净额(RMB)</th>
        <th align="center">接收日期</th>
        <th align="center">收款人</th>
        <th align="center">付款证明</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data['results'] as $v)
      <tr>
        <td align="center">{{$v['payment_date']}}</td>
        <td align="center">{{$v['customer_code']}}</td>
        <td align="right">{{$v['customer_amount']}}</td>
        <td align="right">{{$v['customer_exchange_rate']}}</td>
        <td align="right">{{$v['customer_total']}}</td>
        <td align="center">{{$v['supplier_code']}}</td>
        <td align="right">{{$v['supplier_amount']}}</td>
        <td align="right">{{$v['supplier_exchange_rate']}}</td>
        <td align="right">{{$v['supplier_total']}}</td>
        <td align="right">{{$v['supplier_bank_charges']}}</td>
        <td align="right">{{$v['supplier_net_amount']}}</td>
        <td align="center">{{$v['date_received']}}</td>
        <td align="center">{{$v['beneficiary']}}</td>
        <td align="center">{{$v['slip']}}</td>
      </tr>
      @endforeach
    </tbody>
    <tfoot>
      <tr>
        <td colspan="2" align="right">总额	</td>
        <td align="right">{{$total['total_customer_amount']}}</td>
        <td></td>
        <td align="right">{{$total['total_customer_total']}}</td>
        <td></td>
        <td align="right">{{$total['total_supplier_amount']}}</td>
        <td></td>
        <td align="right">{{$total['total_supplier_total']}}</td>
        <td align="right">{{$total['total_supplier_bank_charges']}}</td>
        <td align="right">{{$total['total_supplier_net_amount']}}</td>
        <td></td>
      </tr>
    </tfoot>
  </table>
</body>
</html>
