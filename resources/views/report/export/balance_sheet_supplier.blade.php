<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
  <table>
    <thead>
      <tr>
        <th align="center" colspan="5">{{$data['title']}}</th>
      </tr>
      <tr>
        <th align="center">日期</th>
        <th>说明</th>
        <th align="right">进账</th>
        <th align="right">出账</th>
        <th align="right">余额</th>
      </tr>
    </thead>
    <tbody>
      @if(!empty($data['initial_balance']))
      <tr>
        <td align="center"></td>
        <td>Balance b/f</td>
        <td></td>
        <td></td>
        <td align="right">{{$data['initial_balance']}}</td>
      </tr>
      @endif
      @if(isset($data['results']))
      @foreach($data['results'] as $d)
      <tr>
        <td align="center">{{$d['date']}}</td>
        <td>{{$d['description']}}</td>
        <td align="right">{{$d['in']}}</td>
        <td align="right">{{$d['out']}}</td>
        <td align="right">{{$d['balance']}}</td>
      </tr>
      @endforeach
      @endif
    </tbody>
  </table>
</body>
</html>
