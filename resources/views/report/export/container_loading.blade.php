<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
  <table>
    <thead>
      <tr><th colspan="13" align="center">{{$data['title']}} 货柜监装报表 {{$data['agent']}}</th></tr>
      <tr >
        <th align="center">No.</th>
        <th align="center">装柜日期</th>
        <th align="center">到港日期</th>
        <th align="center">货柜费用 (RMB)</th>
        <th align="center">其他费用</th>
        <th align="center">客户</th>
        <th align="center">货柜号码</th>
        <th align="center">预订号</th>
        <th align="center">货值</th>
        <th align="center">Form E</th>
        <th align="center">代理</th>
        <th align="center">全包价 (RMB)</th>
        <th align="center">异地费</th>
        <th align="center">船公司</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data['results'] as $v)
      <tr>
        <td align="center">{{$v['no']}}</td>
        <td align="center">{{$v['loading_date']}}</td>
        <td align="center">{{$v['eta_date']}}</td>
        <td align="right">{{$v['handling_charges']}}</td>
        <td  align="right">{{$v['total_other_fee']}}</td> 
        <td>{{$v['customer_name']}}</td>
        <td align="center">{{$v['container_no']}}</td>
        <td align="center">{{$v['booking_no']}}</td>
        <td align="right">{{$v['goods_value']}}</td>
        <td align="center">{{$v['forme_apply']}}</td>
        <td align="center">{{$v['agent_name']}}</td>
        <td align="right">{{$v['net_freight_charges']}}</td>
        <td align="right">{{$v['remote_fee']}}</td>
        <td align="center">{{$v['shipping_company']}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</body>
</html>
