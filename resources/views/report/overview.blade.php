@extends('layouts.app')
@section('title', '报表')
@section('content')
<h2 class="card-title">报表</h2>
<div class="row reports">

  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">

    <div class="card">
      <a href="{{url('report/staff_commission')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center">
            <i class="fa fa-money icon-lg text-primary d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4  col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">员工提成</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!--//card-->

  </div>

  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
    <div class="card">
      <a href="{{url('report/booking_order')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center ">
            <!-- <div class="col-md-6 col-sm-6 col-xs-4  d-flex text-right "> -->
            <i class="fa fa-bar-chart-o icon-lg text-primary  d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4  col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">货柜预订</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!--//card-->
  </div>

  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
    <div class="card">
      <a href="{{url('report/container_loading')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center">
            <i class="fa fa-cube  icon-lg text-primary d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4 col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">货柜监装</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!--//card-->
  </div>

  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
    <div class="card">
      <a href="{{url('report/billing_company')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center">
            <i class="fa fa-building  icon-lg text-primary d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4  col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">公司总报表</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!--//card-->
  </div>

  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
    <div class="card">
      <a href="{{url('report/monthly_statement')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center">
            <i class="fa fa-calendar  icon-lg text-primary d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4  col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">月份货柜总报表</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!--//card-->
  </div>

  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
    <div class="card">
      <a href="{{url('report/yearly_statement')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center">
            <i class="fa fa-calendar  icon-lg text-primary d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4  col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">年份货柜总报表</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!--//card-->
  </div>

  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
    <div class="card">
      <a href="{{url('report/cargo_record')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center">
            <i class="fa fa-cubes  icon-lg text-primary d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4  col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">散货记录表</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>

  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
    <div class="card">
      <a href="{{url('report/daily_payment_transfer')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center">
            <i class="mdi mdi-bank-transfer  icon-lg text-primary d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4  col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">货款支付</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>

  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
    <div class="card">
      <a href="{{url('report/daily_payment')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center">
            <i class="mdi mdi-bank-transfer  icon-lg text-primary d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4  col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">货款支付 2.0</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>

  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
    <div class="card">
      <a href="{{url('report/balance_sheet_customer')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center">
            <i class="mdi mdi-bank-transfer  icon-lg text-primary d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4  col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">客户月份结单</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>

  <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
    <div class="card">
      <a href="{{url('report/balance_sheet_supplier')}}">
        <div class="card-body">
          <div class="d-flex justify-content-center">
            <i class="mdi mdi-bank-transfer  icon-lg text-primary d-flex align-items-center"></i>
            <div class="d-flex flex-column ml-4  col-right">
              <span class="d-flex flex-column">
                <p class="mb-0">供应商月份结单</p>
                <h4 class="font-weight-bold">报表</h4>
              </span>
              <small class="text-muted">查看</small>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>
</div>
@endsection
@section('footer')
@endsection