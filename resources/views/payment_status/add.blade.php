@extends('layouts.app')
@section('title', '添加货款状态')
@section('content')

<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <!-- form -->
      <form method="post" id="form-payment_status" action="{{url('payment_status/add')}}" >
        @csrf
        <div class="card-header header-sm ">
          <div class="d-flex ">
              <div class="wrapper d-flex align-items-center">
                <h2 class="card-title mb4">添加货款状态</h2>
              </div>
              <div class="wrapper ml-auto action-bar">
                <button type="submit" data-toggle="tooltip" data-placement="top" data-original-title="储存" class="btn btn-icons btn-success btn-sm"><i class="fa fa-save"></i></button>
                <a class="btn btn-icons btn-outline-primary btn-sm"  data-toggle="tooltip" data-placement="top" data-original-title="返回"  href="{{url('/payment_statuss')}}"><i class="fa fa-close"></i></a>
              </div>
          </div>
        </div><!--//card-header-->

        <div class="card-body">
          <!-- form -->
            <div class="row">
              <div class="col-md-6">
                 <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="name">货款状态</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" name="name" required/>
                  </div><!--//col-md-9-->
                </div><!--//form-group-->
              </div><!--//col-->

              <div class="col-md-3">
               <div class="form-group row">
                 <label class="col-md-3 col-form-label" for="status">状态</label>
                 <div class="col-md-9">
                   <select name="status" class="form-control ">
                     @foreach($vm->GetStatuses() as $k=>$v)
                      <option value="{{$k}}">{{$v}}</option>
                     @endforeach
                   </select>
                 </div><!--//col-md-9-->
               </div><!--//form-group-->
             </div><!--//col-->

             <div class="col-md-3">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="sort_order">排列</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="sort_order"  value="0" required/>
                </div><!--//col-md-9-->
              </div><!--//form-group-->
            </div><!--//col-->

           </div><!--row-->

        </div><!--//card-body-->
      </form><!-- // form-->
    </div><!--card -->
  </div>
</div>
@endsection

@section('footer')

{!! JsValidator::formRequest('App\Http\Requests\Admin\PortRequest', '#form-payment_status'); !!}
<script>
    $('#form-payment_status').submit(function (e) {
        e.preventDefault();
        if (!$(this).valid()) return false;
        var _btn = $('button[type=submit]', this);
        startSpin(_btn);
        $.ajax({
            url: this.action,
            type: this.method,
            data: $(this).serialize(),
        }).fail(function(xhr, text, err) {
           notifySystemError(err);
        }).done(function(data) {
          notifySuccess('货款状态已添加。');
            setTimeout(function(){ location.href='{{url('/payment_statuses')}}'; }, 2000);
        }).always(function() {
            stopSpin(_btn);
        });
    });
</script>
@endsection
