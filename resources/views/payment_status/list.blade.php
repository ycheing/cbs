@extends('layouts.app')
@section('title', '货款状态清单')
@section('content')
<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center media-info">
            <h2 class="card-title">货款状态清单</h2>
          </div>
          <div class="wrapper ml-auto action-bar">
            <a href="{{url('/payment_status/add')}}" class="btn btn-primary btn-block"><i class="fa fa-plus "></i> 货款状态</a>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th style="width:10%"></th>
                <th style="width:10%" class="text-center">@sortablelink('status', ' 状态')</th>
                <th>@sortablelink('name', ' 货款状态')</th>
                <th style="width:20%" class="text-center">@sortablelink('sort_order', ' 排列')</th>
              </tr>
            </thead>
            <tbody>
              @foreach($vm->dto as $payment_status)
              <tr>
                <td>
                  <a href="{{url('/payment_status/edit/'.$payment_status->id.'')}}" class="btn btn-icons btn-success btn-action "><i class="fa fa-pencil"></i></a>
                  <button type="button" class="btn btn-icons btn-danger  btn-delete" data-id="{{$payment_status->id}}"><i class="fa fa-trash"></i></button>
                </td>
                <td class="text-center">{!! $payment_status->status !!}</td>
                <td>{{$payment_status->name}} </td>
                <td class="text-center">{{ $payment_status->sort_order }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div><!--table-responsive-->
        <div class="mt-5 ">
          {{$vm->paging->links()}}
        </div>
      </div><!--card-body-->
    </div>
  </div>
</div>

<form method="POST" id="form-delete" action="{{url('/payment_status/delete')}}">
  @csrf
  <input type="hidden" id="payment_status_id" name="id" value="" />
</form>
<!-- // form-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除货款状态</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>请确认是否要删除此货款状态?</p>
            </div>
            <div class="modal-footer">
                <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
                <button data-dismiss="modal"  class="btn btn-secondary">取消</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('footer')
<script>
    $('.btn-delete').on('click', function() {
       $('#modalConfirmDelete').modal();
       $('#payment_status_id').val($(this).attr('data-id'));
   });

   $('#btnModalConfirmDeleteOK').on('click', function() {
       if (!$('#form-delete').valid()) return false;

       $.ajax({
           url: '{{url('/payment_status/delete')}}',
           type: 'POST',
           data: $('#form-delete').serialize(),
       }).fail(function(xhr, text, err) {
          notifySystemError(err);
       }).done(function(data) {
         $('#modalConfirmDelete').modal('hide');
         if(data['error']){
           notifySystemError(data['error']);
         }else{
           notifySuccess('货款状态已删除。');
           setTimeout(function(){ location.href='{{url('/payment_statuses')}}'; }, 2000);
         }
       }).always(function() {
       });
   });
</script>

@endsection
