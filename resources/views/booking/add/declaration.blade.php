@extends('layouts.app')
@section('title', '添加清关资料')
@section('header')
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <!-- form -->
      <form method="POST" id="form-declaration" action="{{url('booking/add/declaration')}}">
        @csrf
        <div class="card-header header-sm ">
          <div class="d-flex ">
            <div class="wrapper d-flex align-items-center">
              <h2 class="card-title mb4"><i class="fa fa-plus text-primary mr-2"></i>添加清关资料</h2>
            </div>
            <div class="wrapper ml-auto action-bar">
              <button type="submit" data-toggle="tooltip" data-placement="top" data-original-title="储存"
                class="btn btn-icons btn-success btn-sm"><i class="fa fa-save"></i></button>

              <button type="button" id="btnBack" class="btn btn-icons btn-outline-primary btn-sm" title="返回"
                data-toggle="modal" data-target="#modalConfirmBack"><i class="fa fa-close"></i></button>
              <!-- <a class="btn btn-icons btn-outline-primary btn-sm"  data-toggle="tooltip" data-placement="top" data-original-title="返回"   href="{{url('/booking/add')}}"><i class="fa fa-close"></i></a> -->
            </div>
          </div>
        </div>
        <!--//card-header-->

        <div class="card-body">
          <div class="d-flex pb-2 mb-4 border-bottom  align-items-center">
            <i class="mdi mdi-ferry mr-3 icon-md text-primary"></i>信息
          </div>

          <div class="row">
            <div class="col-md-6 col-sm-12">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="booking_no">预定号码</label>
                <div class="col-md-4">
                  <input type="text" name="booking_no_1" class="form-control" placeholder="123" maxlength="3" />
                </div>
                <div class="col-md-5">
                  <input type="text" name="booking_no_2" class="form-control" placeholder="AB12" maxlength="5" />
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="loading_date">装柜日期</label>
                <div class="col-md-9">
                  <div class="input-group ">
                    <input id="loading_date" type="text" class="form-control date" name="loading_date"
                      data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                    <div class="input-group-append">
                      <span> <i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_id">客户编号</label>
                <div class="col-md-9">
                  <select name="customer_id" class="select2 form-control">
                    <option value=""></option>
                    @foreach($vm->GetCustomers() as $customer)
                    <option value="{{$customer->id}}">{{$customer->code}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_name">客户名字</label>
                <div class="col-md-9">
                  <input name="customer_name" id="input-customer-name" class="form-control" />
                  <input type="hidden" name="customer_code" />
                </div>
              </div>
              <!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="container_no">货柜号码</label>
                <div class="col-md-9">
                  <input name="container_no" type="text" class="form-control" placeholder="" />
                </div>
              </div>
              <!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="seal_no">封条号</label>
                <div class="col-md-9">
                  <input name="seal_no" type="text" class="form-control" placeholder="" />
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="shipping_company_id">船公司</label>
                <div class="col-md-9">
                  <select name="shipping_company_id" class="form-control select2">
                    <option value=""></option>
                    @foreach($vm->GetShippingCompanies() as $ShippingCompany)
                    <option value="{{$ShippingCompany->id}}">{{$ShippingCompany->code}}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="eta_date">到港日期</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="eta_date" type="text" class="date form-control " name="eta_date"
                      data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                    <div class="input-group-append ">
                      <span><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="loading_port_id">起运港</label>
                <div class="col-md-9">
                  <select name="loading_port_id" class="form-control select2">
                    <option value=""></option>
                    @foreach($vm->GetLoadingPorts() as $loading)
                    <option value="{{$loading->id}}">{{$loading->name}} ({{$loading->country}})</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="destination_port_id">目的港</label>
                <div class="col-md-9">
                  <select name="destination_port_id" class="form-control select2">
                    <option value=""></option>
                    @foreach($vm->GetDestinationPorts() as $destination)
                    <option value="{{$destination->id}}">{{$destination->name}} ({{$destination->country}})</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="inv_plist">清单 & 装箱单</label>
                <div class="col-md-9">
                  <input name="inv_plist" type="text" class="form-control" />
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label " for="forme_apply">Form E 申请</label>
                <div class="col-md-9">
                  <div class="form-group">
                    <div class="form-check form-check-flat mt-1 ">
                      <label class="form-check-label">
                        <input type="checkbox" name="forme_apply" value="1" class="form-check-input" />
                        Yes </label>
                    </div>
                  </div>
                </div>
              </div>
              <!--//form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="sst">SST</label>
                <div class="col-md-9">
                  <div class="form-group">
                    <div class="form-check form-check-flat  mt-1">
                      <label class="form-check-label">
                        <input type="checkbox" name="sst" value="1" class="form-check-input" />
                        Yes </label>
                    </div>
                  </div>
                </div>
              </div>
              <!--//form-group-->

            </div>
            <!--//col-->

            <div class="col-md-6 col-xs-12">

            </div>
            <!--//col-->
          </div>
          <!--row-->


          <div class="row mt-5 ">
            <div class="col-md-6 col-xs-12">

              <div class="d-flex pb-2 mb-4 border-bottom  align-items-center">
                <i class="mdi mdi-docker mr-3 icon-md text-primary"></i> 清关行
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="custom_borker_id">清关行</label>
                <div class="col-md-9">
                  <select name="custom_borker_id" class="form-control select2">
                    <option value=""></option>
                    @foreach($vm->GetCustomsBrokers() as $cb)
                    <option value="{{$cb->id}}">{{$cb->code}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="custom_goods_inv_date">货单日期</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="custom_goods_inv_date" type="text" class="date form-control "
                      name="custom_goods_inv_date" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                    <div class="input-group-append ">
                      <span><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>
              <!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="custom_goods_inv_no">货单号</label>
                <div class="col-md-9">
                  <input name="custom_goods_inv_no" type="text" class="form-control" />
                </div>
              </div>
              <!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="custom_goods_amount">货单金额</label>
                <div class="col-md-9">
                  <input name="custom_goods_amount" type="text" class="form-control" />
                </div>
              </div>
              <!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="custom_other_prices">其他收费</label>
                <div class="col-md-9">
                  <input name="custom_other_prices" type="text" class="form-control" />
                </div>
              </div>
              <!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="custom_handling_inv_date">费用单日期</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="custom_handling_inv_date" type="text" class="date form-control "
                      name="custom_handling_inv_date" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                    <div class="input-group-append ">
                      <span><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="custom_handling_inv_no">费用单号</label>
                <div class="col-md-9">
                  <input name="custom_handling_inv_no" type="text" class="form-control" />
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="custom_handling_prices">清关价格</label>
                <div class="col-md-9">
                  <input name="custom_handling_prices" type="text" class="form-control" />
                </div>
              </div>

            </div>
            <!--//col-->

            <div class="col-md-6 col-xs-12">
              <div class="d-flex pb-2 mb-4 border-bottom  align-items-center">
                <i class="mdi mdi-cash-register mr-3 icon-md text-primary"></i>开票公司
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_company_id">开票公司</label>
                <div class="col-md-9">
                  <select name="billing_company_id" class="form-control select2">
                    <option value=""></option>
                    @foreach($vm->GetBillingCompanies() as $company)
                    <option value="{{$company->id}}">{{$company->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_goods_inv_date">货单日期</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="billing_goods_inv_date" type="text" class="date form-control "
                      name="billing_goods_inv_date" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                    <div class="input-group-append ">
                      <span><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>
              <!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_goods_inv_no">货单号</label>
                <div class="col-md-9">
                  <input name="billing_goods_inv_no" type="text" class="form-control" />
                </div>
              </div>
              <!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_goods_amount">货单金额</label>
                <div class="col-md-9">
                  <input name="billing_goods_amount" type="text" class="form-control" />
                </div>
              </div>
              <!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_other_prices">其他收费</label>
                <div class="col-md-9">
                  <input name="billing_other_prices" type="text" class="form-control" />
                </div>
              </div>
              <!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_handling_inv_date">费用单日期</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="billing_handling_inv_date" type="text" class="date form-control "
                      name="billing_handling_inv_date" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                    <div class="input-group-append ">
                      <span><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>
              <!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_handling_inv_no">费用单号</label>
                <div class="col-md-9">
                  <input name="billing_handling_inv_no" type="text" class="form-control" />
                </div>
              </div>
              <!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_handling_prices">清关价格</label>
                <div class="col-md-9">
                  <input name="billing_handling_prices" type="text" class="form-control" />
                </div>
              </div>
              <!--form-group-->

            </div>
            <!--//col-->
          </div>
          <!--//row-->



        </div>
        <!--card body-->

      </form>
      <!-- // form-->
    </div>
    <!--card -->
  </div>
</div>


<div class="modal fade" id="modalConfirmBack" tabindex="-1" role="dialog" aria-labelledby="modalConfirmBackLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalConfirmBackLabel">是否储存资料</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>是否储存资料,并返回清单列表？</p>
      </div>
      <div class="modal-footer">
        <button id="btnModalConfirmSubmitBack" class="btn btn-primary">确认</button>
        <a href="{{url('/bookings')}}" class="btn btn-secondary">取消</a>
      </div>
    </div>
  </div>
</div>
@endsection

@section('footer')

{!! JsValidator::formRequest('App\Http\Requests\Admin\BookingDeclarationRequest', '#form-declaration'); !!}
<script>
  $('#btnModalConfirmSubmitBack').on('click', function() {
    $('#form-declaration').submit();
});

$('#form-declaration').submit(function (e) {
    $('#modalConfirmBack').modal('hide');
    e.preventDefault();
    if (!$(this).valid()) return false;
    var _btn = $('button[type=submit]', this);
    startSpin(_btn);
    $.ajax({
        url: this.action,
        type: this.method,
        data: $(this).serialize(),
    }).fail(function(xhr, text, err) {
       notifySystemError(err);
    }).done(function(data) {
        notifySuccess('清关资料已添加。');
        setTimeout(function(){ location.href='{{url('/bookings')}}'; }, 2000);
    }).always(function() {
        stopSpin(_btn);
    });
});

//customer change
$('select[name=\'customer_id\']').on('change', function() {

	$.ajax({
		type:"GET",
		url: '{{url('getcustomer?customer_id=')}}' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'customer_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
      if(json.hasOwnProperty('code')){
        if (json['code'] && json['code'] != '') {
          $('input[name=\'customer_code\']').val(json['code']);
        }
      }
      if(json.hasOwnProperty('name')){
        if (json['name'] && json['name'] != '') {
          $('input[name=\'customer_name\']').val(json['name']);
        }
      }
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'customer_id\']').trigger('change');
</script>
@endsection