@extends('layouts.app')
@section('title', '修改清关资料')
@section('header')
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <!-- form -->
      <form method="POST" id="form-declaration" action="{{url('booking/edit/declaration')}}" >
        @csrf
        <input type="hidden" name="id" value="{{$vm->dto->id}}" />
        <div class="card-header header-sm ">
          <div class="d-flex ">
              <div class="wrapper d-flex align-items-center">
                <h2 class="card-title mb4"><i class="fa fa-plus text-primary mr-2"></i>修改清关资料</h2>
              </div>
              <div class="wrapper ml-auto action-bar">
                <div class="d-none d-sm-block">
                @if($vm->dto->loading == 1)
                <a class="btn btn-icons btn-primary btn-sm" data-toggle="tooltip" data-original-title="装柜记录"  href="{{url('/booking/edit/loading/' . $vm->dto->id . '')}}">装柜记录</a>
                @endif
                @if($vm->dto->status_id == 4)
                <button type="button" id="btnCompleted" class="btn btn-icons btn-success btn-sm" data-toggle="modal" data-target="#modalConfirmCompleted"  ><i class="fa fa-check mr-1"></i>完成订单</button>
                @endif
                <button type="submit" class="btn btn-icons btn-success btn-sm" data-toggle="tooltip" data-original-title="提交" ><i class="fa fa-save"></i></button>
                <button  type="button" class="btn btn-icons btn-danger btn-sm" id="btnDelete"  data-toggle="modal" data-target="#modalConfirmDelete" data-original-title="删除">
                  <i class="fa fa-trash"></i>
                </button>
                <button type="button" id="btnBack" class="btn btn-icons btn-outline-primary btn-sm" title="返回"  data-toggle="modal" data-target="#modalConfirmBack"><i class="fa fa-close"></i></button>
                <!-- <a class="btn btn-icons btn-outline-primary btn-sm"  href="{{url('/bookings')}}" data-toggle="tooltip" data-original-title="返回" ><i class="fa fa-close"></i></a> -->
              </div>

              <div class="dropdown d-block d-sm-none float-right">
                <button class="btn btn-secondary  btn-sm dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 管理订单 </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton2">
                  @if($vm->dto->booking == 1)
                    <a href="{{url('/booking/edit/booking/' . $vm->dto->id)}}" class="dropdown-item">预定记录</a>
                  @endif
                  @if($vm->dto->loading == 1)
                  <a class="dropdown-item" data-toggle="tooltip" data-original-title="装柜记录"  href="{{url('/booking/edit/loading/' . $vm->dto->id . '')}}">装柜记录</a>
                  @endif
                  @if($vm->dto->status_id == 4)
                  <button type="button" id="btnCompleted"  class="dropdown-item" data-toggle="modal" data-target="#modalConfirmCompleted">完成订单</button>
                  @endif

                  <div class="dropdown-divider"></div>
                  <button type="submit" class="dropdown-item"><i class="fa fa-save"></i> 提交</button>
                  <button  type="button" class="dropdown-item" id="btnDelete"  data-toggle="modal" data-target="#modalConfirmDelete">
                    <i class="fa fa-trash"></i> 删除
                  </button>
                  <button type="button" id="btnBack" class="dropdown-item" data-toggle="modal" data-target="#modalConfirmBack"><i class="fa fa-close"></i> 返回</button>

                </div>
              </div>

              </div>
          </div>
        </div><!--//card-header-->

        <div class="card-body">
          <div class="d-flex pb-2 mb-4 border-bottom  align-items-center">
            <i class="mdi mdi-ferry mr-3 icon-md text-primary"></i>信息
          </div>
          <div class="row">
            <div class="col-md-6 col-xs-12">


              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="booking_no">预定号 </label>
                <div class="col-md-9">
                  <input type="text" name="booking_no" class="form-control" value="{{$vm->dto->booking_no}}" />
                  <!-- <span class="badge badge-primary">{{$vm->dto->booking_no}} </span> -->
                </div>
              </div><!--//form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label"  for="order_no" >订单号</label>
                <div class="col-md-9">
                  <span class="badge badge-primary">{{$vm->dto->order_no}}</span>
                </div>
              </div><!--//form-group-->



              <!-- <div class="form-group row">
                <label class="col-md-3 col-form-label"  for="declaration_date" >清关日期</label>
                <div class="col-md-9">
                   <div class="input-group ">
                     <input id="declaration_date" type="text" class="form-control date" name="declaration_date"  data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->declaration_date}}"/>
                     <div class="input-group-append">
                          <span> <i class="fa fa-calendar"></i></span>
                     </div>
                   </div>
                 </div>
              </div> -->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="status" >状态</label>
                <div class="col-md-9">
                {!! $vm->dto->status !!}
                </div>
              </div><!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label"  for="loading_date" >装柜日期</label>
                <div class="col-md-9">
                   <div class="input-group ">
                     <input id="loading_date" type="text" class="form-control date" name="loading_date"  data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->loading_date}}"/>
                     <div class="input-group-append">
                          <span> <i class="fa fa-calendar"></i></span>
                     </div>
                   </div>
                 </div>
              </div>

              <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="customer_id">客户编号</label>
                  <div class="col-md-9">
                    <select name="customer_id" class="select2 form-control" >
                      <option value=""></option>
                      @foreach($vm->GetCustomers() as $customer)
                        @if($vm->dto->customer_id == $customer->id)
                          <option value="{{$customer->id}}" selected>{{$customer->code}}</option>
                        @else
                          <option value="{{$customer->id}}">{{$customer->code}}</option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                </div><!--form-group-->

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="customer_name" >客户名字</label>
                    <div class="col-md-9">
                      <input name="customer_name" id="input-customer-name" class="form-control" value="{{$vm->dto->customer_name}}"  />
                      <input type="hidden" name="customer_code" value="{{$vm->dto->customer_code}}"/>
                    </div>
                </div><!--form-group-->

                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="container_no">货柜号码</label>
                  <div class="col-md-9">
                    <input name="container_no" type="text"  class="form-control" placeholder=""  value="{{$vm->dto->container_no}}"/>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="seal_no">封条号</label>
                  <div class="col-md-9">
                    <input name="seal_no" type="text"  class="form-control" placeholder="" value="{{$vm->dto->seal_no}}"/>
                  </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="shipping_company_id">船公司</label>
                    <div class="col-md-9">
                      <select name="shipping_company_id" class="select2 form-control" >
                        <option value=""></option>
                        @foreach($vm->GetShippingCompanies() as $ShippingCompany)
                          @if($vm->dto->shipping_company_id == $ShippingCompany->id)
                            <option value="{{$ShippingCompany->id}}" selected>{{$ShippingCompany->code}}</option>
                          @else
                            <option value="{{$ShippingCompany->id}}">{{$ShippingCompany->code}}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="eta_date">到港日期</label>
                  <div class="col-md-9">
                    <div class="input-group">
                      <input id="eta_date" type="text" class="date form-control " name="eta_date" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY"  value="{{$vm->dto->eta_date}}"/>
                      <div class="input-group-append ">
                          <span><i class="fa fa-calendar"></i></span>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="loading_port_id">起运港</label>
                  <div class="col-md-9">
                    <select name="loading_port_id" class="select2 form-control" >
                      <option value=""></option>
                      @foreach($vm->GetLoadingPorts() as $loading)
                        @if($vm->dto->loading_port_id == $loading->id)
                          <option value="{{$loading->id}}" selected>{{$loading->name}} ({{$loading->country}})</option>
                        @else
                          <option value="{{$loading->id}}">{{$loading->name}} ({{$loading->country}})</option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-3 col-form-label"  for="destination_port_id">目的港</label>
                  <div class="col-md-9">
                    <select name="destination_port_id" class="select2 form-control" >
                      <option value=""></option>
                      @foreach($vm->GetDestinationPorts() as $destination)
                        @if($vm->dto->destination_port_id == $destination->id)
                          <option value="{{$destination->id}}" selected>{{$destination->name}} ({{$destination->country}})</option>
                        @else
                          <option value="{{$destination->id}}">{{$destination->name}} ({{$destination->country}})</option>
                        @endif

                      @endforeach
                    </select>
                  </div>
                </div>



            </div>


          </div><!--//row-->

          <div class="row">
            <div class="col-md-6 col-xs-12">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="inv_plist">清单 & 装箱单</label>
                <div class="col-md-9">
                  <input name="inv_plist" type="text"  class="form-control" value="{{$vm->dto->inv_plist}}" />
                </div>
              </div>

              <div class="form-group row">
                  <label class="col-md-3 col-form-label " for="forme_apply">Form E 申请</label>
                  <div class="col-md-9">
                    <div class="form-group">
                       <div class="form-check form-check-flat mt-1 ">
                         <label class="form-check-label">
                           @if($vm->dto->forme_apply == 1)
                           <input type="checkbox" name="forme_apply" value="1" class="form-check-input" checked />
                           @else
                            <input type="checkbox" name="forme_apply" value="1" class="form-check-input"  />
                           @endif Yes </label>
                       </div>
                    </div>
                  </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="sst">SST</label>
                <div class="col-md-9">
                  <div class="form-group">
                     <div class="form-check form-check-flat  mt-1">
                         <label class="form-check-label">
                         @if($vm->dto->sst == 1)
                         <input type="checkbox" name="sst" value="1" class="form-check-input" checked />
                         @else
                          <input type="checkbox" name="sst" value="1" class="form-check-input"  />
                         @endif Yes </label>
                     </div>
                  </div>
                </div>
              </div><!--//row-->
            </div>

            <div class="col-md-6 col-xs-12">
              <!-- <div class="d-flex pb-2 mb-4 border-bottom  align-items-center">
                <i class="fa fa-files-o mr-3 icon-md text-primary"></i>附件
              </div> -->

              <table class="table table-border">
                <thead>
                  <tr class="bg-dark text-white">
                    <th></th>
                    <th>附件</th>
                  </tr>
                </thead>
                <tbody id="table-attachments">
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="2" class="text-right">
                        <button class="btn btn-success" type="button" data-toggle="modal" data-target="#ModalFiles" id="btnAttachment"><i class="fa fa-plus"></i></button>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div><!--//row-->


          <div class="row  mt-5 ">
            <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="d-flex pb-2 mb-4 border-bottom  align-items-center">
                <i class="mdi mdi-docker mr-3 icon-md text-primary"></i> 清关行
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="custom_borker_id">清关行</label>
                <div class="col-md-9">
                  <select name="custom_borker_id" class="form-control select2">
                    <option value=""></option>
                    @foreach($vm->GetCustomsBrokers() as $cb)
                      @if($vm->dto->custom_borker_id ==  $cb->id)
                        <option value="{{$cb->id}}" selected>{{$cb->code}}</option>
                      @else
                        <option value="{{$cb->id}}">{{$cb->code}}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
              </div><!--row-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="custom_goods_inv_date">货单日期</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="custom_goods_inv_date" type="text" class="date form-control " name="custom_goods_inv_date"  data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->custom_goods_inv_date}}"/>
                    <div class="input-group-append ">
                        <span><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="custom_goods_inv_no">货单号</label>
                <div class="col-md-9">
                  <input name="custom_goods_inv_no" type="text"  class="form-control"  value="{{$vm->dto->custom_goods_inv_no}}"/>
                </div>
              </div>

              <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="custom_goods_amount">货单金额</label>
                  <div class="col-md-9">
                    <input name="custom_goods_amount" type="text"  class="form-control"  value="{{$vm->dto->custom_goods_amount}}"/>
                  </div>
              </div><!--row-->

              <div class="form-group row">
                  <label class="col-md-3 col-form-label"for="custom_other_prices">其他收费</label>
                  <div class="col-md-9">
                    <input name="custom_other_prices" type="text"  class="form-control" value="{{$vm->dto->custom_other_prices}}" />
                  </div>
              </div><!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="custom_handling_inv_date">费用单日期</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="custom_handling_inv_date" type="text" class="date form-control " name="custom_handling_inv_date" value="{{$vm->dto->custom_handling_inv_date}}"data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                    <div class="input-group-append ">
                        <span><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="custom_handling_inv_no">费用单号</label>
                <div class="col-md-9">
                  <input name="custom_handling_inv_no" type="text"  class="form-control"  value="{{$vm->dto->custom_handling_inv_no}}"/>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="custom_handling_prices">清关价格</label>
                <div class="col-md-9">
                  <input name="custom_handling_prices" type="text"  class="form-control" value="{{$vm->dto->custom_handling_prices}}" />
                </div>
              </div>



            </div><!--//col-->


            <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="d-flex pb-2 mb-4 border-bottom  align-items-center">
                <i class="mdi mdi-cash-register mr-3 icon-md text-primary"></i>开票公司
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_company_id">开票公司</label>
                <div class="col-md-9">
                  <select name="billing_company_id" class="form-control select2">
                    <option value=""></option>
                    @foreach($vm->GetBillingCompanies() as $company)
                      @if($vm->dto->billing_company_id == $company->id)
                        <option value="{{$company->id}}" selected>{{$company->name}}</option>
                      @else
                        <option value="{{$company->id}}">{{$company->name}}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
              </div><!--row-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_goods_inv_date">货单日期</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="billing_goods_inv_date" type="text" class="date form-control " name="billing_goods_inv_date"  data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->billing_goods_inv_date}}"/>
                    <div class="input-group-append ">
                        <span><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_goods_inv_no">货单号</label>
                <div class="col-md-9">
                  <input name="billing_goods_inv_no" type="text"  class="form-control"  value="{{$vm->dto->billing_goods_inv_no}}"/>
                </div>
              </div>
              <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="billing_goods_amount">货单金额</label>
                  <div class="col-md-9">
                    <input name="billing_goods_amount" type="text"  class="form-control"  value="{{$vm->dto->billing_goods_amount}}"/>
                  </div>
              </div><!--row-->

              <div class="form-group row">
                  <label class="col-md-3 col-form-label"for="billing_other_prices">其他收费</label>
                  <div class="col-md-9">
                    <input name="billing_other_prices" type="text"  class="form-control" value="{{$vm->dto->billing_other_prices}}" />
                  </div>
              </div><!--form-group-->

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_handling_inv_date">费用单日期</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="billing_handling_inv_date" type="text" class="date form-control " name="billing_handling_inv_date" value="{{$vm->dto->billing_handling_inv_date}}"data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" />
                    <div class="input-group-append ">
                        <span><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_handling_inv_no">费用单号</label>
                <div class="col-md-9">
                  <input name="billing_handling_inv_no" type="text"  class="form-control"  value="{{$vm->dto->billing_handling_inv_no}}"/>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="billing_handling_prices">清关价格</label>
                <div class="col-md-9">
                  <input name="billing_handling_prices" type="text"  class="form-control" value="{{$vm->dto->billing_handling_prices}}" />
                </div>
              </div>

            </div><!--//col-->
          </div><!--//row-->

        </div> <!--card body-->

      </form>
      <!-- // form-->
   </div><!--card -->
  </div>
</div>

<!--form delete-->
<form method="POST" id="form-delete" action="{{url('/booking/delete')}}">
 @csrf
 <input type="hidden" name="id" value="{{$vm->dto->id}}" />
</form>
<!-- modal delete-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除订单</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>请确认您否要删除此订单？</p>
            </div>
            <div class="modal-footer">
                <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
                <button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">取消</button>
            </div>
        </div>
    </div>
</div>


<!-- upload files -->
<div class="modal fade" id="ModalFiles" tabindex="-1" role="dialog" aria-labelledby="modalImageLabel" aria-hidden="true">
  <form method="POST" id="form-files" action="{{url('booking/upload')}}">
      <div class="modal-dialog modal-dialog-centered " role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="modalImageLabel">上载附件</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                <input name="booking_id" type="hidden" value="{{$vm->dto->id}}"/>
                <div class="form-group row">
                  <label class="col-md-3 col-xs-12 col-form-label" for="type_id">说明</label>
                  <div class="col-md-9 col-xs-12">
                    <select name="type_id" class="form-control" >
                      <option value=""></option>
                      <option value="4">清关和装箱单</option>
                      <option value="5">Form E 申请单</option>
                      <option value="6">SST (K1 Form)</option>
                    </select>
                  </div>
                </div>
                <div id="dropFileState" ondragover="return false">
                    <div id="dragUploadFile">
                      <p>档案类型：Excel, PDF, JPG, PNG, GIF, TIFF<br/>
                       <!-- 档案大小上限：2MB -->
                     </p>
                        <input type="file" id="file_attachment" name="file_file" class="dropify"  data-allowed-file-extensions="csv xls xlsx pdf jpg png gif tiff txt"  />
                    </div>
                </div>
                <div class="text-center mt-2">
                  <button type="button" class="btn btn-success mb-3" id="btnUploadFile"><i class="fa fa-excel"></i> 上载</button>
                </div>
              </div>
          </div>
      </div>
  </form>
</div>

<div class="modal fade" id="modalConfirmBack" tabindex="-1" role="dialog" aria-labelledby="modalConfirmBackLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalConfirmBackLabel">是否储存资料</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>是否储存资料,并返回清单列表？</p>
            </div>
            <div class="modal-footer">
              <button id="btnModalConfirmSubmitBack" class="btn btn-primary">确认</button>
              <a href="{{url('/bookings')}}" class="btn btn-secondary">取消</a>
            </div>
        </div>
    </div>
</div>

<!-- // confirm completed-->
<div class="modal fade" id="modalConfirmCompleted" tabindex="-1" role="dialog" aria-labelledby="modalConfirmCompletedLabel" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
       <div class="modal-content">
           <div class="modal-header">
               <h5 class="modal-title" id="modalConfirmDeleteLabel">确认完成订单</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
           </div>
           <div class="modal-body">
               <p>是否确认完成此订单?</p>
           </div>
           <div class="modal-footer">
               <button id="btnModalConfirmCompletedOK" class="btn btn-primary">确认</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">取消</button>
           </div>
       </div>
   </div>
</div>
@endsection

@section('footer')

{!! JsValidator::formRequest('App\Http\Requests\Admin\BookingDeclarationEditRequest', '#form-declaration'); !!}
{!! JsValidator::formRequest('App\Http\Requests\Admin\BookingAttachmentRequest', '#form-file'); !!}
<script>

$('#table-attachments').load('{{url('/booking/getdeclarationattachments/' . $vm->dto->id . '')}}');
$('#btnModalConfirmSubmitBack').on('click', function() {
    $('#form-declaration').submit();
});
$('#form-declaration').submit(function (e) {
  $('#modalConfirmBack').modal('hide');
  e.preventDefault();
  if (!$(this).valid()) return false;
  var _btn = $('button[type=submit]', this);
  startSpin(_btn);
  $.ajax({
      url: this.action,
      type: this.method,
      data: $(this).serialize(),
  }).fail(function(xhr, text, err) {
     notifySystemError(err);
  }).done(function(data) {
      notifySuccess('清关资料已更改。');
      setTimeout(function(){ location.href='{{url('/bookings')}}'; }, 2000);
  }).always(function() {
      stopSpin(_btn);
  });
});

//Delete
$('#btnModalConfirmDeleteOK').on('click', function() {
  if (!$('#form-delete').valid()) return false;
  startSpin($('#btnDelete'));
  $.ajax({
    url: '{{url('/booking/delete')}}',
    type: 'POST',
    data: $('#form-delete').serialize(),
  }).fail(function(xhr, text, err) {
    notifySystemError(err);
  }).done(function(data) {
    $('#modalConfirmDelete').modal('hide');
    notifySuccess('订单已删除。');
    setTimeout(function(){ location.href='{{url('/bookings')}}'; }, 2000);
  }).always(function() {
    stopSpin($('#btnDelete'));
  });
});

$('#btnUploadFile').on('click', function() {
  if (!$('#form-files').valid()) return false;
  var formData = new FormData($('#form-files')[0]);
  var _btn = $('#btnUploadFile', this);
  startSpin(_btn);
  $.ajax({
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '{{url('booking/upload')}}',
      type: 'POST',
      data:formData,
      enctype: 'multipart/form-data',
      contentType: false,
      processData: false,
  }).fail(function(xhr, text, err) {
     notifySystemError(err);
  }).done(function(data) {
    if(data['result']){
      $('#table-attachments').load('{{url('/booking/getdeclarationattachments/' . $vm->dto->id . '')}}');
      notifySuccess('附件已上载。');
      $('#ModalFiles').modal('hide');
     var drEvent = $('#file_file').dropify();
     drEvent = drEvent.data('dropify');
     drEvent.resetPreview();
     drEvent.clearElement();
    }else{
      notifySystemError(data['error']);
    }
  }).always(function() {
      stopSpin(_btn);
  });
});

$(document).on('click', '.btn-delete-attachment', function(e) {
   e.preventDefault();
   e.stopImmediatePropagation();
   if (!confirm('是否删除此附件?')) return;
   var _id = $(this).attr('data-id');
   var _formData = new FormData();
   _formData.append('booking_attachment_id', _id);
   $.ajax({
       headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       },
       url: '{{url('/booking/attachment/delete')}}',
       type: 'POST',
       contentType: false,
       processData: false,
       data: _formData,
   }).fail(function(xhr, text, err) {
      notifySystemError(err);
   }).done(function(data){
     if(data['result']){
       notifySuccess('附件已删除。');
     }else{
       notifySystemError(data['error']);
     }
     $('#table-attachments').load('{{url('/booking/getdeclarationattachments/' . $vm->dto->id . '')}}');
   }).always(function(){
   });
});

//customer change
$('select[name=\'customer_id\']').on('change', function() {

	$.ajax({
		type:"GET",
		url: '{{url('getcustomer?customer_id=')}}' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'customer_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
      if(json.hasOwnProperty('code')){
        if (json['code'] && json['code'] != '') {
          $('input[name=\'customer_code\']').val(json['code']);
        }
      }
      if(json.hasOwnProperty('name')){
        if (json['name'] && json['name'] != '') {
          $('input[name=\'customer_name\']').val(json['name']);
        }
      }
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

//Booking Completed
$('#btnModalConfirmCompletedOK').on('click', function() {
  startSpin($('#btnCompleted'));
  var _formData = new FormData();
  _formData.append('id', '{{$vm->dto->id}}');
  _formData.append('status_id', '5');
  $.ajax({
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '{{url('/booking/update/status')}}',
      type: 'POST',
      contentType: false,
      processData: false,
      data: _formData,
  }).fail(function(xhr, text, err) {
     notifySystemError(err);
  }).done(function(data){
    $('#modalConfirmCompleted').modal('hide');
    notifySuccess('订单状态已修改。');
    location.href= '{{url('booking/edit/declaration/' . $vm->dto->id . '')}}';
  }).always(function() {
      stopSpin($('#btnCompleted'));
  });
});
</script>
@endsection
