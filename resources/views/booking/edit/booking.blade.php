@extends('layouts.app')
@section('title', '修改预定')
@section('header')
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <!-- form -->
      <form method="post" id="form-booking" action="{{url('booking/edit/booking')}}" >
          @csrf
          <input type="hidden" name="id" value="{{$vm->dto->id}}" />
      <div class="card-header header-sm ">
        <div class="d-flex ">
            <div class="wrapper d-flex align-items-center">
              <h2 class="card-title mb4"><i class="fa fa-plus text-primary mr-2"></i>修改预定</h2>
            </div>
            <div class="wrapper ml-auto action-bar">
              <div class="d-none d-sm-block">
              @if($vm->dto->loading == 0 && $vm->checkAddLoadingButton())
              <button type="button" class="btn btn-icons btn-info btn-sm"  id="btnLoading"  data-toggle="modal" data-target="#modalConfirmLoading">添加货柜记录</button>
              @elseif($vm->dto->loading == 1)
              <a href="{{url('/booking/edit/loading/' . $vm->dto->id)}}" class="btn btn-icons btn-info btn-sm"  data-toggle="tooltip" data-placement="top" data-original-title="货柜记录">货柜记录</a>
              @endif
              <button type="submit" class="btn btn-icons btn-success btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="储存"><i class="fa fa-save"></i></button>
              <button  type="button" class="btn btn-icons btn-danger btn-sm" id="btnDelete"  data-toggle="modal" data-target="#modalConfirmDelete">
                <i class="fa fa-trash"></i>
              </button>
              <button type="button" id="btnBack" class="btn btn-icons btn-outline-primary btn-sm" title="返回"  data-toggle="modal" data-target="#modalConfirmBack"><i class="fa fa-close"></i></button>
              <!-- <a class="btn btn-icons btn-outline-primary btn-sm"  href="{{url('/bookings')}}"><i class="fa fa-close"></i></a> -->
              </div>

              <div class="dropdown d-block d-sm-none float-right">
                <button class="btn btn-secondary  btn-sm dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 管理订单 </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton2">
                  @if($vm->dto->loading == 0)
                  <button type="button"  class="dropdown-item" id="btnLoading"  data-toggle="modal" data-target="#modalConfirmLoading">添加货柜记录</button>
                  @elseif($vm->dto->loading == 1)
                  <a href="{{url('/booking/edit/loading/' . $vm->dto->id)}}" class="dropdown-item" data-toggle="tooltip" data-placement="top" data-original-title="货柜记录">货柜记录</a>
                  @endif
                  <div class="dropdown-divider"></div>
                  <button type="submit" class="dropdown-item"><i class="fa fa-save"></i> 提交</button>
                  <button  type="button" class="dropdown-item" id="btnDelete"  data-toggle="modal" data-target="#modalConfirmDelete">
                    <i class="fa fa-trash"></i> 删除
                  </button>
                  <button type="button" id="btnBack" class="dropdown-item" data-toggle="modal" data-target="#modalConfirmBack"><i class="fa fa-close"></i> 返回</button>

                </div>
              </div>

            </div>
        </div>
      </div><!--//card-header-->

      <div class="card-body">
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="booking_no">预定号 : </label>
              <div class="col-md-9">
                <input type="text" name="booking_no" class="form-control" value="{{$vm->dto->booking_no}}" />
                <!-- <span class="badge badge-primary">{{$vm->dto->booking_no}} </span> -->
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group row">
              <label class="col-md-3 col-form-label"  for="order_no" >订单号</label>
              <div class="col-md-9">
                <span class="badge badge-primary">{{$vm->dto->order_no}}</span>
              </div>
            </div>
          </div>
        </div><!--//row-->

        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="form-group row">
                <label class="col-md-3 col-form-label"  for="booking_date" >预定装柜日期</label>
                <div class="col-md-9">
                   <div class="input-group ">
                     <input id="booking_date" type="text" class="form-control date" name="booking_date"  data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->booking_date}}"/>
                     <div class="input-group-append">
                          <span> <i class="fa fa-calendar"></i></span>
                     </div>
                   </div>
                 </div>
              </div>
          </div>
          <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="status" >状态</label>
                <div class="col-md-9">
                {!! $vm->dto->status !!}
                </div>
              </div>
          </div>
        </div>


        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">

            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="customer_id">客户编号</label>
                <div class="col-md-9">
                  <select name="customer_id" class="select2 form-control">
                    <option value=""></option>
                    @foreach($vm->GetCustomers() as $customer)
                      @if($vm->dto->customer_id == $customer->id)
                        <option value="{{$customer->id}}" selected>{{$customer->code}}</option>
                      @else
                        <option value="{{$customer->id}}">{{$customer->code}}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
              </div><!--form-group-->
          </div><!--//col-->

          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group row">
                <label for="customer_name" class="col-md-3 col-form-label" >客户名字</label>
                <div class="col-md-9">
                  <input name="customer_name" id="input-customer-name" class="form-control" value="{{$vm->dto->customer_name}}"  />
                  <input type="hidden" name="customer_code" value="{{$vm->dto->customer_code}}"/>
                </div>
            </div><!--form-group-->
          </div><!--//col-->
        </div><!--//row-->

        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="delivery_address_1">配送地址</label>
              <div class="col-md-9">
                <input type="text" name="delivery_address_1" class="form-control" placeholder="地址第一行" value="{{$vm->dto->delivery_address_1}}"/>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="delivery_address_2"></label>
              <div class="col-md-9">
                <input type="text" name="delivery_address_2" class="form-control" placeholder="地址第二行" value="{{$vm->dto->delivery_address_2}}"/>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="delivery_address_1">城市</label>
              <div class="col-md-9">
                <input type="text" name="delivery_city" class="form-control mb-1" placeholder="城市" value="{{$vm->dto->delivery_city}}"/>
              </div>
            </div>
          </div><!--//col-->
          <div class="col-md-6 col-sm-12 col-xs-12">

            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="delivery_country_id">国家</label>
              <div class="col-md-9">
                <select name="delivery_country_id" class="form-control select2">
                  <option value=""></option>
                  @foreach($vm->GetCountries() as $country)
                    @if($vm->dto->delivery_country_id == $country->id)
                      <option value="{{$country->id}}" selected>{{$country->name}}</option>
                    @else
                    <option value="{{$country->id}}">{{$country->name}}</option>
                    @endif
                  @endforeach
                </select>
                <input type="hidden" name="delivery_country" value="{{$vm->dto->delivery_country}}"/>
              </div>
            </div><!--//form-group-->

            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="delivery_state_id">州属</label>
              <div class="col-md-9">
                <select name="delivery_state_id" class="form-control">

                </select>
                  <input type="hidden" name="delivery_state" value="{{$vm->dto->delivery_state}}"/>
              </div>
            </div><!--//form-group-->

            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="delivery_postcode">邮编</label>
              <div class="col-md-9">
                <input type="text" name="delivery_postcode" class="form-control" placeholder="邮编" value="{{$vm->dto->delivery_postcode}}"/>
              </div>
            </div><!--//form-group-->

          </div><!--//col-->
        </div><!--//row-->

        <div class="row">
          <div class="col-md-6">

            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="handling_charges">货柜费用</label>
              <div class="col-md-9">
                <input name="handling_charges" type="text" class="form-control"  value="{{$vm->dto->handling_charges}}"/>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="goods_value">货值</label>
              <div class="col-md-9">
                <input type="text" name="goods_value" class="form-control" placeholder=""  value="{{$vm->dto->goods_value}}"/>
              </div>
            </div><!--//form-group-->
          </div><!--//col-->
        </div><!--//row-->

        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="agent_id">代理</label>
              <div class="col-md-9">
                <select name="agent_id" class="form-control select2">
                  <option value=""></option>
                  @foreach($vm->GetAgents() as $agent)
                    @if($vm->dto->agent_id == $agent->id)
                      <option value="{{$agent->id}}" selected>{{$agent->name}}</option>
                    @else
                      <option value="{{$agent->id}}">{{$agent->name}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div><!--//col-->

          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group row">
            <label class="col-md-3 col-form-label" for="shipping_company_id">船公司</label>
            <div class="col-md-9">
              <select name="shipping_company_id" class="form-control select2">
                <option value=""></option>
                @foreach($vm->GetShippingCompanies() as $ShippingCompany)
                  @if($vm->dto->shipping_company_id == $ShippingCompany->id)
                    <option value="{{$ShippingCompany->id}}" selected>{{$ShippingCompany->code}}</option>
                  @else
                    <option value="{{$ShippingCompany->id}}">{{$ShippingCompany->code}}</option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>
          </div><!--//col-->
        </div><!--//row-->

        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group row ">
              <label class="col-md-3 col-form-label" for="freight_charges">全包价</label>
              <div class="col-md-9">
                <input name="freight_charges" id="freight_charges" class="form-control"  value="{{$vm->dto->freight_charges}}"/>
              </div>
            </div><!--row-->
          </div><!--//col-->
        </div><!--//row-->

        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="loading_port_id">起运港</label>
              <div class="col-md-9">
                <select name="loading_port_id" class="form-control select2">
                  <option value=""></option>
                  @foreach($vm->GetLoadingPorts() as $loading)
                    @if($vm->dto->loading_port_id == $loading->id)
                      <option value="{{$loading->id}}" selected>{{$loading->name}} ({{$loading->country}})</option>
                    @else
                      <option value="{{$loading->id}}">{{$loading->name}} ({{$loading->country}})</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div><!--//col-->

          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group row">
              <label class="col-md-3 col-form-label"  for="destination_port_id">目的港</label>
              <div class="col-md-9">
              <select name="destination_port_id" class="form-control select2">
                <option value=""></option>
                @foreach($vm->GetDestinationPorts() as $destination)
                  @if($vm->dto->destination_port_id == $destination->id)
                    <option value="{{$destination->id}}" selected>{{$destination->name}} ({{$destination->country}})</option>
                  @else
                    <option value="{{$destination->id}}">{{$destination->name}} ({{$destination->country}})</option>
                  @endif

                @endforeach
              </select>
              </div>
            </div>
          </div><!--//col-->

        </div><!--//row-->

        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">

            <div class="form-group row">
              <label class="col-md-3 col-form-label"  for="closing_date">结关日期</label>
                <div class="col-md-9">
               <div class="input-group ">
                 <input id="closing_date" type="text" class="form-control date" name="closing_date"  data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->closing_date}}"/>
                 <div class="input-group-append">
                      <span> <i class="fa fa-calendar"></i></span>
                 </div>
               </div>
               </div>
             </div><!--//form-group-->
           </div>

          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group row">
            <label class="col-md-3 col-form-label"  for="sailing_date">开船日期</label>
              <div class="col-md-9">
            <div class="input-group">
              <input id="sailing_date" type="text" class="date form-control " name="sailing_date" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="{{$vm->dto->sailing_date}}"/>
              <div class="input-group-append ">
                  <span><i class="fa fa-calendar"></i></span>
              </div>
            </div>
            </div>
             </div><!--//form-group-->
          </div>

          </div><!--row-->

          <div class="row">

          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group row">
              <label class="col-md-3 col-form-label"  for="sailing_day">航行时间</label>
                <div class="col-md-9">
              <div class="input-group">
                <input id="sailing_day" type="text" class="form-control" name="sailing_day" value="{{$vm->dto->sailing_day}}" />
                 <div class="input-group-append ">
                     <span>天</span>
                 </div>
              </div>
              </div>
             </div><!--//form-group-->

             <div class="form-group row">
               <label class="col-md-3 col-form-label" for="rental_period">免柜租期</label>
               <div class="col-md-9">
                 <input name="rental_period" type="text" class="form-control"  value="{{$vm->dto->rental_period}}"/>
               </div>
              </div><!--//form-group-->
          </div>
        </div><!--row-->

      </div> <!--card body-->
    </div><!--card -->
  </form>
  <!-- // form-->
  </div>
</div>


  <form method="POST" id="form-delete" action="{{url('/booking/delete')}}">
    @csrf
    <input type="hidden" name="id" value="{{$vm->dto->id}}" />
  </form>
   <!-- // form-->
   <div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除订单</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <p>请确认您否要删除此订单？</p>
             </div>
             <div class="modal-footer">
               <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
               <button data-dismiss="modal"  class="btn btn-secondary">取消</button>
             </div>
         </div>
     </div>
   </div>

  <form method="POST" id="form-loading" action="{{url('/booking/add/loading')}}">
    @csrf
    <input type="hidden" name="id" value="{{$vm->dto->id}}" />
  </form>

  <!-- // form-->
 <div class="modal fade" id="modalConfirmLoading" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="modalConfirmDeleteLabel">添加装柜资料</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <p>请确认是否添加装柜资料？</p>
             </div>
             <div class="modal-footer">
               <button id="btnModalConfirmLoadingOK" class="btn btn-primary">确认</button>
               <button data-dismiss="modal"  class="btn btn-secondary">取消</button>
             </div>
         </div>
     </div>
 </div>



 <div class="modal fade" id="modalConfirmBack" tabindex="-1" role="dialog" aria-labelledby="modalConfirmBackLabel" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="modalConfirmBackLabel">是否储存资料</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <p>是否储存资料,并返回清单列表？</p>
             </div>
             <div class="modal-footer">
               <button id="btnModalConfirmSubmitBack" class="btn btn-primary">确认</button>
               <a href="{{url('/bookings')}}" class="btn btn-secondary">取消</a>
             </div>
         </div>
     </div>
 </div>
@endsection

@section('footer')
{!! JsValidator::formRequest('App\Http\Requests\Admin\BookingEditRequest', '#form-booking'); !!}
<script>

$('#btnModalConfirmSubmitBack').on('click', function() {
    $('#form-booking').submit();
});
  $('#form-booking').submit(function (e) {
    $('#modalConfirmBack').modal('hide');
      e.preventDefault();
      if (!$(this).valid()) return false;
      var _btn = $('button[type=submit]', this);
      startSpin(_btn);
      $.ajax({
          url: this.action,
          type: this.method,
          data: $(this).serialize(),
      }).fail(function(xhr, text, err) {
         notifySystemError(err);
      }).done(function(data) {
          notifySuccess('预定已修改。');
          setTimeout(function(){ location.href='{{url('/bookings')}}'; }, 2000);
      }).always(function() {
          stopSpin(_btn);
      });
  });

  $('#btnModalConfirmDeleteOK').on('click', function() {
     if (!$('#form-delete').valid()) return false;
     startSpin($('#btnDelete'));
     $.ajax({
         url: '{{url('/booking/delete')}}',
         type: 'POST',
         data: $('#form-delete').serialize(),
     }).fail(function(xhr, text, err) {
        notifySystemError(err);
     }).done(function(data) {
       $('#modalConfirmDelete').modal('hide');
       notifySuccess('订单已删除。');
       setTimeout(function(){ location.href='{{url('/bookings')}}'; }, 2000);
     }).always(function() {
         stopSpin($('#btnDelete'));
     });
  });

  $('#btnModalConfirmLoadingOK').on('click', function() {
    if (!$('#form-loading').valid()) return false;
    startSpin($('#btnLoading'));
    $.ajax({
        url: '{{url('/booking/add/loading')}}',
        type: 'POST',
        data: $('#form-loading').serialize(),
    }).fail(function(xhr, text, err) {
       notifySystemError(err);
    }).done(function(data) {
      $('#modalConfirmLoading').modal('hide');
      notifySuccess('前往货柜记录。');
      location.href= '{{url('booking/edit/loading/' . $vm->dto->id . '')}}';
    }).always(function() {
        stopSpin($('#btnLoading'));
    });
  });

  //customer change
  $('select[name=\'customer_id\']').on('change', function() {

  	$.ajax({
  		type:"GET",
  		url: '{{url('getcustomer?customer_id=')}}' + this.value,
  		dataType: 'json',
  		beforeSend: function() {
  			$('select[name=\'customer_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
  		},
  		complete: function() {
  			$('.fa-spin').remove();
  		},
  		success: function(json) {
        if(json.hasOwnProperty('code')){
          if (json['code'] && json['code'] != '') {
            $('input[name=\'customer_code\']').val(json['code']);
          }
        }
        if(json.hasOwnProperty('name')){
          if (json['name'] && json['name'] != '') {
            $('input[name=\'customer_name\']').val(json['name']);
          }
        }
        if(json.hasOwnProperty('shipping_address_1')){
          if (json['shipping_address_1'] && json['shipping_address_1'] != '') {
            $('input[name=\'delivery_address_1\']').val(json['shipping_address_1']);
          }
        }
        if(json.hasOwnProperty('shipping_address_2')){
          if (json['shipping_address_2'] && json['shipping_address_2'] != '') {
            $('input[name=\'delivery_address_2\']').val(json['shipping_address_2']);
          }
        }
        if(json.hasOwnProperty('shipping_city')){
          if (json['shipping_city'] && json['shipping_city'] != '') {
            $('input[name=\'delivery_city\']').val(json['shipping_city']);
          }
        }
        if(json.hasOwnProperty('shipping_postcode')){
          if (json['shipping_postcode'] && json['shipping_postcode'] != '') {
            $('input[name=\'delivery_postcode\']').val(json['shipping_postcode']);
          }
        }
        if(json.hasOwnProperty('shipping_country_id')){
          if (json['shipping_country_id'] && json['shipping_country_id'] != '') {
            $('select[name=\'delivery_country_id\']').val(json['shipping_country_id']);
            $('select[name=\'delivery_country_id\']').trigger('change', 'select2');
          }
        }
        if(json.hasOwnProperty('shipping_country')){
          if (json['shipping_country'] && json['shipping_country'] != '') {
            $('input[name=\'delivery_country\']').val(json['shipping_country']);
          }
        }

        if(json.hasOwnProperty('shipping_state_id')){
          if (json['shipping_state_id'] && json['shipping_state_id'] != '') {
            $('select[name=\'delivery_state_id\']').val(json['shipping_state_id']);
          }
        }
        if(json.hasOwnProperty('shipping_state')){
          if (json['shipping_state'] && json['shipping_state'] != '') {
            $('input[name=\'delivery_state\']').val(json['shipping_state']);
          }
        }
  		},
  		error: function(xhr, ajaxOptions, thrownError) {
  			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
  		}
  	});
  });

//  $('select[name=\'customer_id\']').trigger('change');

  $('select[name=\'delivery_country_id\']').on('change', function() {
  	$.ajax({
  		type:"GET",
  		url: '{{url('getcountry?country_id=')}}' + this.value,
  		dataType: 'json',
  		beforeSend: function() {
  			$('select[name=\'delivery_country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
  		},
  		complete: function() {
  			$('.fa-spin').remove();
  		},
  		success: function(json) {
  			html = '<option value="">{{__('请选择')}}</option>';

  			if (json['json']['states'] && json['json']['states'] != '') {
  				for (i = 0; i < json['json']['states'].length; i++) {
  					html += '<option value="' + json['json']['states'][i]['id'] + '"';
            if( json['json']['states'][i]['id'] == '{{$vm->dto->delivery_state_id}}' ){
              html += ' selected>' + json['json']['states'][i]['name'] + '</option>';
            }else{
              html += '>' + json['json']['states'][i]['name'] + '</option>';
            }
  				}
  			} else {
  				html += '<option value="0" selected="selected">{{__('无')}}</option>';
  			}

  			$('select[name=\'delivery_state_id\']').html(html);
  		},
  		error: function(xhr, ajaxOptions, thrownError) {
  			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
  		}
  	});
  });

  $('select[name=\'delivery_country_id\']').trigger('change');

</script>
@endsection
