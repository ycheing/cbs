@foreach($data as $d)
<tr>
  <td><button type="button" class="btn btn-icons btn-danger btn-delete-fee" data-id="{{$d->booking_fee_id}}"><i class="fa fa-trash"></i></button></td>
  <td>{{$d->description}}</td>
  <td>{{$d->amount}}
    <input type="hidden" value="{{$d->amount}}" name="fee_amount[]"/>
  </td>
</tr>
@endforeach
