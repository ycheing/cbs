@extends('layouts.app')
@section('title', '添加订单')
@section('header')
<style>

.record_type div {
    cursor:pointer;
}
.record_type input[type="radio"]{
  display: none;
}
.record_type div.selected .card{
  background: linear-gradient(to right, #24e8a6, #09cdd1);
  color: #fff;
}
</style>
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <!-- form -->
      <form method="post" id="form-booking" action="{{url('booking/add')}}" >
          @csrf
      <div class="card-header header-sm ">
        <div class="d-flex ">
            <div class="wrapper d-flex align-items-center">
              <h2 class="card-title mb4"><i class="fa fa-plus text-primary mr-2"></i>添加订单</h2>
            </div>
            <div class="wrapper ml-auto action-bar">
              <a class="btn btn-icons btn-outline-primary btn-sm"  href="{{url('/bookings')}}"><i class="fa fa-close"></i></a>
            </div>
        </div>
      </div><!--//card-header-->

      <div class="card-body">
        <!-- <div class="form-group row">
          <div class="col">
              <label for="billing_company_id" class="font-weight-bold">Select Billing Company 选择开票公司</label>
              <select class="form-control" name="billing_company_id">
                @foreach($vm->GetBillingCompanies() as $company)
                <option value="{{$company->id}}">{{$company->name}}</option>
                @endforeach
              </select>
           </div>
        </div> -->

        <div class="form-group row ">
          <div class="col">
              <label for="booking_type" class="mb-4 font-weight-bold">请选择服务类型</label>

              <div class="row record_type">
                  <div class="col-12 col-sm-4 col-md-4 grid-margin stretch-card justify-content-center align-items-center">
                    <input type="radio" name="booking_type" value="booking" />
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-center align-items-center">
                        <h4 class="font-weight-bold text-center"> 货柜预定记录 </h4>
                        </div>
                      </div>
                    </div>
                  </div><!--// stretch-card-->


                  <div class="col-12 col-sm-4 col-md-4 grid-margin stretch-card justify-content-center align-items-center">
                    <input type="radio" name="booking_type" value="declaration" />
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-center align-items-center">
                            <h4 class="font-weight-bold text-center">货柜清关记录</h4>
                        </div>
                      </div>
                    </div>
                  </div><!--// stretch-card-->

                  <div class="col-12 col-sm-4 col-md-4 grid-margin stretch-card justify-content-center align-items-center">
                    <input type="radio" name="booking_type" value="cargo" />
                    <div class="card">
                      <div class="card-body">
                        <div class="d-flex justify-content-center align-items-center">
                            <h4 class="font-weight-bold text-center">散货预定记录</h4>
                        </div>
                      </div>
                    </div>
                  </div><!--// stretch-card-->

              </div>
           </div><!--//col-->
        </div><!--//row-->

        <div class="form-group row mt-5">
          <div class="col text-right">
            <button class="btn btn-success" type="submit">下一步</button>
          </div><!--//col-->
       </div><!--//row-->

      </div> <!--card body-->
    </div><!--card -->
  </form>
  <!-- // form-->
  </div>
</div>
@endsection

@section('footer')
{!! JsValidator::formRequest('App\Http\Requests\Admin\BookingRequest', '#form-booking'); !!}
<script>
  $(".record_type :radio").hide().click(function(e){
      e.stopPropagation();
  });
  $(".record_type div").click(function(e){
      $(this).closest(".record_type").find("div").removeClass("selected");
      // $(this).addClass("selected").find("input:radio").attr('checked', true);
      $(this).addClass("selected").find("input:radio").click();
  });

  $('#form-booking').submit(function (e) {
      e.preventDefault();
      if (!$(this).valid()) return false;
      var _btn = $('button[type=submit]', this);
      startSpin(_btn);
      $.ajax({
          url: this.action,
          type: this.method,
          data: $(this).serialize(),
      }).fail(function(xhr, text, err) {
         notifySystemError(err);
      }).done(function(data) {
        if(data['location'] != ''){
          location.href= data['location'];
        }
          // setTimeout(function(){ location.href='{{url('/companies')}}'; }, 2000);
      }).always(function() {
          stopSpin(_btn);
      });
  });
</script>
@endsection
