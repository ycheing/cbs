@extends('layouts.app')
@section('title', '订单清单')

@section('content')
<div class="row">
  <div class="col-lg-12 col-sm-12 grid-margin">
    <div class="card">
      <div class="card-header header-sm">
        <div class="d-flex align-items-center">
          <div class="wrapper d-flex align-items-center media-info">
            <h2 class="card-title">订单清单</h2>
          </div>
          <div class="wrapper ml-auto action-bar">
            <a href="{{url('/booking/add')}}" class="btn btn-primary btn-block"><i class="fa fa-plus "></i> 添加订单</a>
          </div>
        </div>
      </div>
      <div class="card-body">

        <div class="page-header-toolbar">
          <form method="GET" action="{{url('/bookings')}}" style="width:100%">
            <div class="form-group row">
              <div class="col-md-6 col-xs-12">
                <input name="keyword" id="keyword" placeholder="关键词" class="form-control"
                  value="{{$vm->inputs['keyword']}}" />
              </div>
              <div class="col-md-2 col-xs-12">
                <select name="status_id" class="form-control">
                  @foreach($vm->GetBookingStatuses() as $k => $v)
                  @if($vm->inputs['status_id'] == $k)
                  <option value="{{$k}}" selected>{{$v}}</option>
                  @else
                  <option value="{{$k}}">{{$v}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
              <div class="col-md-2 col-xs-12">
                <select name="user_id" class="form-control">
                  <option value="">全部</option>
                  @foreach($vm->GetUsers() as $user)
                  @if($vm->inputs['user_id'] == $user->id)
                  <option value="{{$user->id}}" selected>{{$user->name}}</option>
                  @else
                  <option value="{{$user->id}}">{{$user->name}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
              <div class="col-md-1 col-xs-12">
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </form>
        </div>

        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th style="width:10%"></th>
                <th>@sortablelink('created_at', ' 订单日期 ')</th>
                <th style="width:10%" class="text-center">@sortablelink('status', ' 状态 ')</th>
                <th>@sortablelink('booking_date', ' 预定装柜日期')</th>
                <th>@sortablelink('loading_date', ' 装柜日期')</th>
                <th>@sortablelink('eta_date', ' 到港日期')</th>
                <!-- <th>@sortablelink('order_no', '订单号')</th> -->
                <th>@sortablelink('booking_no', ' 预定号')</th>
                <th>@sortablelink('customer_name', ' 客户名字')</th>
                <th>@sortablelink('container_no', ' 货柜号码')</th>
                <!-- <th>@sortablelink('agent_name', '代理')</th> -->
                <th>@sortablelink('owner_id', ' 创建者')</th>
              </tr>
            </thead>
            <tbody>
              @foreach($vm->dto as $booking)
              <tr>
                <td>
                  <a href="{{url('/booking/edit/'.$booking->id.'')}}" class="btn btn-icons btn-success btn-action "><i
                      class="fa fa-pencil"></i></a>
                  <button type="button" class="btn btn-icons btn-danger btn-delete" data-id="{{$booking->id}}"><i
                      class="fa fa-trash"></i></button>
                </td>
                <td>{{$booking->created_at}}</td>
                <td class="text-center">{!! $booking->status !!}</td>
                <td>{{$booking->booking_date}}</td>
                <td>{{$booking->loading_date}}</td>
                <td>{{$booking->eta_date}}</td>
                <!-- <td>{{$booking->order_no}}</td> -->
                <td>{{$booking->booking_no}}</td>
                <td>{{$booking->customer_name}} </td>
                <td>{{$booking->container_no}}</td>
                <!-- <td>{{$booking->agent_name}} </td> -->
                <td>{{$booking->created_by}} </td>
              </tr>
              @endforeach
            </tbody>

          </table>
        </div>
        <!--table-responsive-->
        <div class="mt-5 ">
          {{$vm->paging->links()}}
        </div>

      </div>
    </div>
  </div>
</div>

<form method="POST" id="form-delete" action="{{url('/booking/delete')}}">
  @csrf
  <input type="hidden" id="booking_id" name="id" value="" />
</form>
<!-- // form-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalConfirmDeleteLabel">确认删除订单</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>请确认您否要删除此订单？</p>
      </div>
      <div class="modal-footer">
        <button id="btnModalConfirmDeleteOK" class="btn btn-primary">确认</button>
        <button data-dismiss="modal" class="btn btn-secondary">取消</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('footer')

<script>
  $('.btn-delete').on('click', function() {
   $('#modalConfirmDelete').modal();
   $('#booking_id').val($(this).attr('data-id'));
});

$('#btnModalConfirmDeleteOK').on('click', function() {
   if (!$('#form-delete').valid()) return false;
   $.ajax({
       url: '{{url('/booking/delete')}}',
       type: 'POST',
       data: $('#form-delete').serialize(),
   }).fail(function(xhr, text, err) {
      notifySystemError(err);
   }).done(function(data) {
     $('#modalConfirmDelete').modal('hide');
    notifySuccess('订单已删除。');
     setTimeout(function(){ location.href='{{url('/bookings')}}'; }, 2000);
   }).always(function() {
   });
});
</script>
@endsection