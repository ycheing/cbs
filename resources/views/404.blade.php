<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>404 | {{ config('app.name') }}</title>

    <!-- Scripts -->
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('vendor/mdi/css/materialdesignicons.min.css')}}" />
    <link rel="stylesheet" href="{{ asset('vendor/simple-line-icon/css/simple-line-icons.css')}}" />
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css')}}" />
    <link rel="stylesheet" href="{{ asset('css/vendor.bundle.base.css')}}" />
    <link rel="stylesheet" href="{{ asset('css/vendor.bundle.addons.css')}}" />
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('css/shared/style.css')}}" />
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}" />
    <link rel="stylesheet" href="{{asset('css/custom.css')}}" />
    <!-- End Layout styles -->
    <link rel="shortcut icon" href="{{ asset('/images/favicon.png') }}" />

</head>
<body >

  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center text-center error-page bg-primary">
        <div class="row flex-grow">
          <div class="col-lg-7 mx-auto text-white">
            <div class="row align-items-center d-flex flex-row">
              <div class="col-lg-6 text-lg-right pr-lg-4">
                <h1 class="display-1 mb-0">404</h1>
              </div>
              <div class="col-lg-6 error-page-divider text-lg-left pl-lg-4">
                <h2>SORRY!</h2>
                <h3 class="font-weight-light">The page you’re looking for was not found.</h3>
              </div>
            </div>
            <div class="row mt-5">
              <div class="col-12 text-center mt-xl-2">
                <a class="text-white font-weight-medium" onclick="history.back()">Back to home</a>
              </div>
            </div>
            <div class="row mt-5">
              <div class="col-12 mt-xl-2">
                <p class="text-white font-weight-medium text-center">Copyright &copy; 2020 - {{date('Y')}} by Rakuntech Solution.</strong> All rights reserved.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
 <script src="{{asset('js/vendor.bundle.base.js')}}"></script>
 <script src="{{asset('js/vendor.bundle.addons.js')}}"></script>
 <script src="{{asset('js/off-canvas.js')}}"></script>
 <script src="{{asset('js/hoverable-collapse.js')}}"></script>
 <script src="{{asset('js/misc.js')}}"></script>
</body>
</html>
