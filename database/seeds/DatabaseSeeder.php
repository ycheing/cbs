<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // 创建角色
        $adminRole = Role::create(['name' => 'admin']);
        $agentRole = Role::create(['name' => 'staff']);

        // 创建权限
        Permission::create(['name' => 'manage users']);
        Permission::create(['name' => 'view reports']);

        // 为角色分配权限
        $adminRole->givePermissionTo('manage users');
        $agentRole->givePermissionTo('view reports');

        // 创建用户并分配角色
        $admin = User::create([
            'name' => 'Admin User',
            'email' => 'admin@example.com',
            'password' => bcrypt('password'),
            'status' => 1,
        ]);
        $admin->assignRole('admin');
    }
}