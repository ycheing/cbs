<?php
return [
	'roles' =>[
		'0' => '超级管理员',
		'1' => '管理员',
		// '2' => 'Agent'
	],
	'cargo_statuses' => [
		'0' => '新建',
		'1' => '处理中',
		'2' => '完成',
	],
	'statuses' => [
		'1' => '启用',
		'0' => '冻结'
	],
	'statuses_zh' => [
		'1' => '启用',
		'0' => '冻结'
	],
	'booking_statuses' => [
		'' => '全部',
    '1' => '预订',
		'2' => '装柜',
		'3' => '装柜完成',
		'4' => '清关',
		'5' => '完成',
	],
	'agent_booking_statuses_zh' => [
		'' => '全部',
		'1' => '预订',
		'2' => '装柜',
		'3' => '装柜完成',
		'4' => '清关',
	],
	'files_type' => [
    '1' => '封柜图',
	  '2' => '装车单',
		'3' => '装柜付款单',
		'4' => '清关和装箱单',
		'5' => 'Form E 申请单',
		'6' => 'SST (K1 Form)',
	],
	'months' => [
		'1' => 'January 一月',
		'2' => 'February 二月',
		'3' => 'March 三月',
		'4' => 'April 四月',
		'5' => 'May 五月',
		'6' => 'June 六月',
		'7' => 'July 七月',
		'8' => 'August 八月',
		'9' => 'September 九月',
		'10' => 'October 十月',
		'11' => 'November 十一月',
		'12' => 'December 十二月',
	],
	'days' => [
		'Sunday' => '周日 Sunday',
		'Monday' => '周一 Monday',
		'Tuesday' => '周二 Tuesday',
		'Wednesday' => '周三 Wednesday',
		'Thurday' => '周四 Thurday',
		'Friday' => '周五 Friday',
		'Saturday' => '周六 Saturday',
	],
	'currency_code' => [
		'AUD' => 'AUD',
		'CAD' => 'CAD',
		'CNY' => 'CNY',
		'EUR' => 'EUR',
		'GBP' => 'GBP',
		'INR' => 'INR',
		'JPY' => 'JPY',
		'KRW' => 'KRW',
		'MYR' => 'MYR',
		'NZD' => 'NZD',
		'SGD' => 'SGD',
		'THB' => 'THB',
		'TWD' => 'TWD',
		'USD' => 'USD',
	],
];
