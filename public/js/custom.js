/*-----------------------------------------------------------------------------------*/
/*  NOTIFY
/*-----------------------------------------------------------------------------------*/
function notifyAppError(error) {
    notifyAppError(error, "body");
}

function notifyAppError(error, el) {
    $.alert({
        title: "<i class='fa fa-exclamation-circle'></i> Error ",
        content: error,
        type: "red",
        typeAnimated: true,
    });
}

function notifySystemError(error) {
    notifySystemError(error, "body");
}

function notifySystemError(error, el) {
    $.alert({
        title: "<i class='fa fa-exclamation-circle'></i> Error ",
        content: error,
        type: "red",
        typeAnimated: true,
    });
}

function notifyError(error) {
    notifyError(error, "body");
}

function notifyError(error, el) {
    $.alert({
        title: "<i class='fa fa-exclamation-circle'></i> 错误 ",
        content: error,
        type: "red",
        typeAnimated: true,
    });
}

function notifySuccess(msg) {
    notifySuccess(msg, "body");
}

function notifySuccess(msg, el) {
    $.alert({
        title: "<i class='fa fa-exclamation-circle'></i> 成功 ",
        content: msg,
        type: "green",
        typeAnimated: true,
    });
}

/*-----------------------------------------------------------------------------------*/
/*  BUTTON SPINNER
/*-----------------------------------------------------------------------------------*/
function startSpin(el) {
    el.data("ori", el.html());
    el.html('<i class="fa fa-spinner"></i>');
    el.addClass("disabled");
}

function stopSpin(el) {
    el.html(el.data("ori"));
    el.removeClass("disabled");
}

(function ($) {
    "use strict";
    $(".dropify").dropify();
    $(".datetimepicker").datetimepicker({});
    $(".datepicker").datetimepicker({
        format: "L",
    });
    $(".date").datepicker({
        format: "dd/mm/yyyy",
        todayHighlight: true,
    });
    $(".select2").select2();
    $(".select22").select2({
        matcher: function (params, data) {
            // If there are no search terms, return all data
            if ($.trim(params.term) === "") {
                return data;
            }

            // `params.term` should be the search term, `data.text` is the option text
            if (
                data.text.toUpperCase().indexOf(params.term.toUpperCase()) === 0
            ) {
                return data;
            }

            // Return `null` if the term doesn't match from the start
            return null;
        },
    });
})(jQuery);
